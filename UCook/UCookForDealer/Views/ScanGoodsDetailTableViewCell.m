//
//  ScanGoodsDetailTableViewCell.m
//  UCook
//
//  Created by scihi on 14/11/4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ScanGoodsDetailTableViewCell.h"
#import<CoreText/CoreText.h>

@interface ScanGoodsDetailTableViewCell()<UITextFieldDelegate>
{
    //接收当前cell所在的行数
    int line;
    //标题数组
    NSArray *detailArr;
    
    //数据的key值
    NSArray *keyDetailArr;
    
    //右侧网络数据展示文本框
    UILabel *dataLabel;
    
    AddGoodsModel *model;
    
    // 推荐 不推荐
    UIButton *tmpBtn[2];
    
    //所属店铺 价格
    UIButton *stAndPcBtn;;
    UITextField *stAndPcText;
    
    //商店和价格的文字显示数组
    NSMutableArray *prArr;
    NSMutableArray *stArr;
    
}

@end
@implementation ScanGoodsDetailTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index
                     getData:(AddGoodsModel *)addModel
               getStoreArray:(NSMutableArray *)stoArr
               getPriceArray:(NSMutableArray *)priArr
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        _isOnce = YES;
        line = index;
        model = addModel;
        prArr = priArr;
        stArr = stoArr;

        [self initData];
        [self initContentView];
        
    }
    return self;
}

-(void)initData
{
   
    detailArr = @[@"产品信息",@"名称",@"品牌",@"分类",@"规格",@"参考范围",@"单位",
                  @"整/单位",@"是否推荐商品",@"定价信息",@"图片"];
    

}

-(void)initContentView
{
    self.backgroundColor = [UIColor whiteColor];
    
  
    //设置标题
    if(line<11)
    {
        self.textLabel.text = detailArr[line];
        self.textLabel.font = [UIFont boldSystemFontOfSize:14];
        self.textLabel.textColor = COLOR(86, 86, 86, 1);
    }
    if(line<=11)
    {
        
        //添加分割线
        UIView *lineView = [[UIView alloc]initWithFrame:AUTOLayer(0, 0, 320, 1, 1)];
        lineView.backgroundColor = COLOR(234, 234, 234, 1);
        [self addSubview:lineView];
    }
    
    //根据行数目判断 并初始化
    if(line == 0 || line == 9)
    {
        self.backgroundColor = COLOR(228, 228, 228, 1);
        self.textLabel.textColor = COLOR(50, 50, 50, 1);
    }
    else if(line>0 && line<9)
    {
        if(line<8)
        {
            CGRect rect = AUTOLayer(180, 0, 120, 30,1);
            dataLabel = [[UILabel alloc]initWithFrame: rect];
            dataLabel.textAlignment = NSTextAlignmentRight;
            dataLabel.font = [UIFont boldSystemFontOfSize:12];
            dataLabel.textColor = COLOR(131, 131, 131, 1);
            [self addSubview:dataLabel];
            
            line==1?dataLabel.text=[model name]:NULL;
            line==2?dataLabel.text=[model brand]:NULL;
            line==3?dataLabel.text=[model category]:NULL;
            line==4?dataLabel.text=[model spec]:NULL;
            line==5?dataLabel.text=[model rangeprice]:NULL;
            line==6?dataLabel.text=[model unit]:NULL;
            line==7?dataLabel.text=[model split]:NULL;
            
        }
        if(line == 8)
        {
            //推荐  不推荐
            NSArray *recommend = @[@"推荐",@"不推荐"];
            for (int i=0; i<2; ++i)
            {
                tmpBtn[i] = [UIButton buttonWithType:UIButtonTypeCustom];
                tmpBtn[i].frame = AUTOLayer(140+i*60, 7, 15, 15, 1);
                
                [tmpBtn[i] setImage:[UIImage imageNamed: @"checkbox@2x.png"] forState:UIControlStateNormal];
                [tmpBtn[i] setImage:[UIImage imageNamed: @"checkbox_on@2x.png"] forState:UIControlStateSelected];
                if(i==0)
                {
                    tmpBtn[i].selected = YES;
                }
                else
                {
                    tmpBtn[i].selected = NO;
                }
                tmpBtn[i].tag = i+200;
                [tmpBtn[i] addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:tmpBtn[i]];
                
                
                UILabel *recommendLabel = [[UILabel alloc]init];
                recommendLabel.frame = AUTOLayer(160+i*60, 0, 50, 30, 1);
                recommendLabel.text = recommend[i];
                recommendLabel.textColor = COLOR(50, 50, 50, 1);
                recommendLabel.font = [UIFont boldSystemFontOfSize:13];
                [self addSubview:recommendLabel];
            }
        }
    }
    else if(line ==10)
    {
    }
    else
    {
        
        //该产品所属的店铺  产品价格
        NSArray *stAndPrice = @[stArr[line-11],prArr[line-11]];
        for(int i=0;i<2;++i)
        {
            
            stAndPcText = [[UITextField alloc]init];
            stAndPcText.frame = AUTOLayer(20, 10+i*70, 280, 60, 1);
            stAndPcText.layer.borderColor = COLOR(234, 234, 234, 1).CGColor;
            stAndPcText.layer.borderWidth = 1;
            stAndPcText.text = stAndPrice[i];
            stAndPcText.tag = 600+line;
            stAndPcText.textColor = COLOR(86, 86, 86, 1);
            stAndPcText.font = [UIFont systemFontOfSize:15];
        
            stAndPcText.delegate = self;
            stAndPcText.textAlignment = NSTextAlignmentLeft;
            [self addSubview:stAndPcText];
            
            if(i==0)
            {
                stAndPcBtn = [UIButton buttonWithType:UIButtonTypeSystem];
                stAndPcBtn.frame = AUTOLayer(20, 10+i*70, 280, 60, 1);
                stAndPcBtn.tag = 300+line;
                [stAndPcBtn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:stAndPcBtn];
            }
        }
        
    }
}



//推荐 不推荐 商品所属店铺的按钮代理
-(void)buttonPressed:(UIButton *)sender
{
    if(sender.tag <210)
    {
        if(sender.selected == NO)
        {
            sender.selected = YES;
        }
        for(int i=0;i<2;++i)
        {
            if(tmpBtn[i]!=sender)
            {
                tmpBtn[i].selected = NO;
                break;
            }
        }
    }
    //设置委托代理
    if(_delegate && [_delegate respondsToSelector:@selector(cellBtnClick:)])
    {
        [_delegate cellBtnClick:(int)sender.tag];
    }
    
}

//输入框的判断 价格输入框的判断
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField.text isEqualToString:@"请填写产品价格"])
    {
        textField.text = @"";
    }
    
    //设置委托代理
    if(_delegate && [_delegate respondsToSelector:@selector(textFieldBegin:)])
    {
        [_delegate textFieldBegin:textField];
    }
}

//输入框结束的时候降输入框的内容读取到控制器
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //设置委托代理
    if(_delegate && [_delegate respondsToSelector:@selector(textFieldStop:)])
    {
        [_delegate textFieldStop:textField];
    }
}




- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
