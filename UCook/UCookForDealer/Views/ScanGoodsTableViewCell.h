//
//  ScanGoodsTableViewCell.h
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGoodsModel.h"

@protocol ScanGoodsDelegate <NSObject>

@optional
-(void)choiceGoods:(int)index;

@end



@interface ScanGoodsTableViewCell : UITableViewCell

@property(nonatomic,strong) AddGoodsModel *goodsModeldataSource;


-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index;


//委托代理
@property(nonatomic,assign)id<ScanGoodsDelegate>delegate;

@end
