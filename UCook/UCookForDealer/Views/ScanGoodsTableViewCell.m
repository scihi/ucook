//
//  ScanGoodsTableViewCell.m
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ScanGoodsTableViewCell.h"
#import "UIImageView+WebCache.h"

@interface ScanGoodsTableViewCell()
{
    int Tag;
}
@property(nonatomic,strong) UIImageView *youchuImageView;
@property(nonatomic,strong) UILabel *goodsNameLabel;
@property(nonatomic,strong) UILabel *goodsSizeLabel;
@property(nonatomic,strong) UILabel *unitLabel;

@end

@implementation ScanGoodsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        Tag = index;
        [self initContentView];
    }
    return self;
}

-(void)setGoodsModeldataSource:(AddGoodsModel *)goodsModeldataSource
{
    _goodsModeldataSource = goodsModeldataSource;

    //产品图片刷新
    NSString *imageStr = [NSString stringWithFormat:@"%@",
                          [_goodsModeldataSource images]];
    [_youchuImageView setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"default_pic.png"]];
    //产品名称
    _goodsNameLabel.text = [_goodsModeldataSource name];
    //产品规格
    _goodsSizeLabel.text = [NSString stringWithFormat:@"%@%@",
                           @"规格:",[_goodsModeldataSource spec]];
    //产品单位
    _unitLabel.text = [NSString stringWithFormat:@"%@%@",
                       @"单位:",[_goodsModeldataSource unit]];
}

-(void)initContentView
{
    self.backgroundColor = [UIColor whiteColor];
    
    //左侧选中按钮
    UIButton *choice = [UIButton buttonWithType:UIButtonTypeCustom];
    choice.frame = AUTOLayer(10, 45, 20, 20,1);
    
    [choice setImage:[UIImage imageNamed: @"checkbox@2x.png"] forState:UIControlStateNormal];
    [choice setImage:[UIImage imageNamed: @"checkbox_on@2x.png"] forState:UIControlStateSelected];
    choice.selected = NO;
    choice.tag = Tag-50;
    [choice addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:choice];
    
    //产品图片
    _youchuImageView = [[UIImageView alloc]init] ;
    _youchuImageView.frame = AUTOLayer
    (35, 15, 70, 70,1);
    _youchuImageView.backgroundColor = [UIColor grayColor];
    [self addSubview:_youchuImageView];
    
    //产品名称
    _goodsNameLabel = [[UILabel alloc]init];
    _goodsNameLabel.frame = AUTOLayer(110, 15, 100, 20,1);
    _goodsNameLabel.backgroundColor = [UIColor whiteColor];
    _goodsNameLabel.textAlignment = NSTextAlignmentLeft;
    _goodsNameLabel.font = [UIFont systemFontOfSize:_goodsNameLabel.frame.size.height*0.75];
    [self addSubview:_goodsNameLabel];
    
    //产品规格
    _goodsSizeLabel = [[UILabel alloc]init];
    _goodsSizeLabel.frame = AUTOLayer(110, 40, 120, 15,1);
    _goodsSizeLabel.backgroundColor = [UIColor whiteColor];
    _goodsSizeLabel.textAlignment = NSTextAlignmentLeft;
    _goodsSizeLabel.font = [UIFont systemFontOfSize:_goodsSizeLabel.frame.size.height*0.75];
    [self addSubview:_goodsSizeLabel];
    
    //单位
    _unitLabel = [[UILabel alloc]init];
    _unitLabel.frame = AUTOLayer(240, 40, 70, 15,1);
    _unitLabel.backgroundColor = [UIColor whiteColor];
    _unitLabel.textAlignment = NSTextAlignmentLeft;
    _unitLabel.font = [UIFont systemFontOfSize:_unitLabel.frame.size.height*0.75];
    [self addSubview:_unitLabel];
    
}


-(void)buttonPressed:(UIButton *)sender
{

    if(_delegate && [_delegate respondsToSelector:@selector(choiceGoods:)])
    {
        [_delegate choiceGoods:sender.tag];
    }
}






- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}









@end
