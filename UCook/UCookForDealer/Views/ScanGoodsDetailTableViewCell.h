//
//  ScanGoodsDetailTableViewCell.h
//  UCook
//
//  Created by scihi on 14/11/4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGoodsModel.h"

@protocol ScanGoodsDetailDelegate <NSObject>

@optional
-(void)cellBtnClick:(int)index;
-(void)textFieldBegin:(UITextField *)Text;
-(void)textFieldStop:(UITextField *)Text;


@end


@interface ScanGoodsDetailTableViewCell : UITableViewCell

@property(nonatomic,assign) BOOL isOnce;

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index
                     getData:(AddGoodsModel *)addModel
               getStoreArray:(NSMutableArray *)stoArr
               getPriceArray:(NSMutableArray *)priArr;

//设置委托代理
@property (nonatomic,assign) id<ScanGoodsDetailDelegate>delegate;

@end
