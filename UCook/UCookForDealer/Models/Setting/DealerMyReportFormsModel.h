//
//  DealerMyReportFormsModel.h
//  UCook
//
//  Created by huangrun on 14-10-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerMyReportFormsModel : NSObject

@property (copy , nonatomic) NSString *name;
@property (copy , nonatomic) NSString *url;

@end
