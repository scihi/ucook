//
//  StoreManageListModel.h
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoodsImageModel.h"

@interface GoodsInfoModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *category;
@property (copy, nonatomic) NSString *brand;    //  产品品牌
@property (copy, nonatomic) NSString *spec;     //产品规格
@property (copy, nonatomic) NSString *split;    //是否拆零（整、单）
@property (copy, nonatomic) NSString *unit;     //产品单位
@property (copy, nonatomic) NSString *desc;     //产品介绍
@property (copy, nonatomic) NSString *rangePrice;   //价格范围
@property (copy, nonatomic) GoodsImageModel *images;   //产品图片

@end
