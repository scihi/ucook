//
//  GoodsImageModel.h
//  UCook
//
//  Created by Apple on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsImageModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *path; //图片路径
@property (copy, nonatomic) NSString *url;  //图片访问地址

@end
