//
//  DealerAuctionModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerAuctionModel : NSObject

@property (assign, nonatomic) NSInteger id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *restaurantId;
@property (copy, nonatomic) NSString *restaurantName;
@property (assign, nonatomic) NSInteger leftTime;
@property (copy, nonatomic) NSString *timeStatus;
@property (copy, nonatomic) NSString *times;
@property (copy, nonatomic) NSString *currentPrice;
@property (copy, nonatomic) NSString *products;

@end
