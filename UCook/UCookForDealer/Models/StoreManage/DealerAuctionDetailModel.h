//
//  DealerAuctionDetailModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerAuctionDetailModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *restaurantId;
@property (copy, nonatomic) NSString *restaurantName;
@property (copy, nonatomic) NSString *rating;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *cycleTime;
@property (copy, nonatomic) NSString *leftTime;
@property (copy, nonatomic) NSString *timeStatus;
@property (copy, nonatomic) NSString *times;
@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *remark;
@property (copy, nonatomic) NSString *currentPrice;

@property (strong, nonatomic) NSMutableArray *stores;
@property (strong, nonatomic) NSMutableArray *myPrice;
@property (strong, nonatomic) NSMutableArray *record;
@property (strong, nonatomic) NSMutableArray *productList;

@end
