//
//  DealerAuctionDetailStoreModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerAuctionDetailStoreModel : NSObject

@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;

@end
