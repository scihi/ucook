//
//  StoreGoodsImageModel.h
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreGoodsImageModel : NSObject

@property (nonatomic, copy) NSString *id;//图片id
@property (nonatomic, copy) NSString *url;//图片url
@property (nonatomic, copy) NSString *order;//图片是否排序

@end
