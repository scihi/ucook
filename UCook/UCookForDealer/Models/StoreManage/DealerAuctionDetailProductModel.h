//
//  DealerAuctionDetailProductModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerAuctionDetailProductModel : NSObject

@property (copy, nonatomic) NSString *itemId;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *spec;
@property (copy, nonatomic) NSString *unit;
@property (copy, nonatomic) NSString *split;
@property (copy, nonatomic) NSString *num;

@end
