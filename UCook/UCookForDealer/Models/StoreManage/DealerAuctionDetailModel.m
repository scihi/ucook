//
//  DealerAuctionDetailModel.m
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerAuctionDetailModel.h"
#import "MJExtension.h"
#import "DealerAuctionDetailStoreModel.h"

@implementation DealerAuctionDetailModel

- (NSDictionary *)objectClassInArray
{
    return @{@"stores" : [DealerAuctionDetailStoreModel class]};
}

@end
