//
//  StoreManageListModel.h
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreGoodsImageModel.h"

@interface StoreGoodsListModel : NSObject

@property (copy, nonatomic) NSString *productId;    //产品ID
@property (copy, nonatomic) NSString *productName;  //产品名称
@property (copy, nonatomic) NSString *category; //产品分类ID
@property (copy, nonatomic) NSString *categoryName; //产品分类名称
@property (copy, nonatomic) NSString *brand;    //产品品牌id
@property (copy, nonatomic) NSString *brandName;    //产品品牌名称
@property (copy, nonatomic) NSString *producer; //生产厂家Id
@property (copy, nonatomic) NSString *producerName; //生产厂家名称
@property (copy, nonatomic) NSString *spec; //产品规格
@property (copy, nonatomic) NSString *split;    //是否拆零(整、零)
@property (copy, nonatomic) NSString *unit; //产品单位
@property (copy, nonatomic) NSString *store;    //产品所在店铺ID
@property (copy, nonatomic) NSString *storeName;    //所在店铺名称
@property (copy, nonatomic) NSString *price;    //产品价格
@property (copy, nonatomic) NSString *stock;    //产品库存
@property (copy, nonatomic) NSString *sales;    //产品销量
@property (copy, nonatomic) NSString *status;   //产品状态
@property (copy, nonatomic) NSString *statusName;   //产品状态名称
@property (copy, nonatomic) NSString *desc;   //产品简介（去html标签）
@property (strong, nonatomic) NSArray *images;   //产品的图片。第一张图片是封面图片(id, url)

@end
