//
//  StoreManageListModel.h
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#define ATsting @property (copy, nonatomic) NSString

@interface StoreManageListModel : NSObject


@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *logo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *area;
@property (copy, nonatomic) NSString *areaId;
@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *zipCode;
@property (copy, nonatomic) NSString *theme;
@property (copy, nonatomic) NSString *themeName;
@property (copy, nonatomic) NSString *notice;
@property (copy, nonatomic) NSString *intro;
@property (copy, nonatomic) NSString *introduction;
@property (copy, nonatomic) NSString *isDelivery;
@property (copy, nonatomic) NSString *qq;
@property (copy, nonatomic) NSString *tel;
@property (copy, nonatomic) NSString *miniAmount;
@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *realStatus;
@property (copy, nonatomic) NSString *realStatusName;

@end
