//
//  SelectGoodsCategoryModel.h
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectGoodsCategoryModel : NSObject

@property (nonatomic, copy) NSString *id;//商品类别id
@property (nonatomic, copy) NSString *name;//商品类别名称
@property (nonatomic, copy) NSString *level;//商品类别层次

@end
