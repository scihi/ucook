//
//  DepositModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepositModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *ucookb;
@property (copy, nonatomic) NSString *biddingMoney;
@property (assign, nonatomic) NSInteger isAllow;

@end
