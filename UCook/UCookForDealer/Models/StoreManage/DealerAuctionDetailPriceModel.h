//
//  DealerAuctionDetailPriceModel.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerAuctionDetailPriceModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *accept;

@end
