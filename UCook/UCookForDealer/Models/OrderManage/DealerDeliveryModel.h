//
//  DealerDeliveryModel.h
//  UCook
//
//  Created by huangrun on 14/10/24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealerDeliveryModel : NSObject

@property (assign, nonatomic) NSInteger deliveryFee;
@property (assign, nonatomic) NSInteger deliveryStatus;
@property (copy, nonatomic) NSString *deliveryFeedback;

@end
