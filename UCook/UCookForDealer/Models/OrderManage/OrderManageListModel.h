//
//  OrderManageListModel.h
//  UCook
//
//  Created by huangrun on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderManageListModel : NSObject

@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *name;
@property (nonatomic) NSInteger amount;


@end
