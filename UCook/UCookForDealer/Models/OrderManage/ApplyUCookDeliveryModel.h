//
//  ApplyUCookDeliveryModel.h
//  UCook
//
//  Created by huangrun on 14/10/24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DealerDeliveryModel;

@interface ApplyUCookDeliveryModel : NSObject

@property (copy, nonatomic) NSString *shipfee;
@property (copy, nonatomic) NSString *kilometer;
@property (copy, nonatomic) NSString *weight;
@property (copy, nonatomic) NSString *floor;
@property (copy, nonatomic) NSString *receiver_name;
@property (copy, nonatomic) NSString *receiver_mobile;
@property (copy, nonatomic) NSString *receiver_phone;
@property (copy, nonatomic) NSString *receiver_address;
@property (copy, nonatomic) NSString *receiver_district;
@property (copy, nonatomic) NSString *receiver_zip;
@property (copy, nonatomic) NSString *area_id;
@property (copy, nonatomic) NSString *address;
@property (strong, nonatomic) DealerDeliveryModel *delivery;

@end
