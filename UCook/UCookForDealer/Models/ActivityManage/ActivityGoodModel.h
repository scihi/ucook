//
//  ActivityGoodModel.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityGoodModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *productId;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *spec;
@property (copy, nonatomic) NSString *desc;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *discountPrice;
@property (copy, nonatomic) NSString *surplus;
@property (copy, nonatomic) NSString *unit;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *discount;
@property (copy, nonatomic) NSString *nop;
@property (copy, nonatomic) NSString *restriction;
@property (copy, nonatomic) NSString *sales;
@property (copy, nonatomic) NSString *haveSales;

@end
