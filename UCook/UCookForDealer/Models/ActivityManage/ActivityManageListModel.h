//
//  ActivityManageListModel.h
//  UCook
//
//  Created by huangrun on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityManageListModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *typeName;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *position;
@property (copy, nonatomic) NSString *positionName;
@property (copy, nonatomic) NSString *intro;
@property (copy, nonatomic) NSString *image;

@end
