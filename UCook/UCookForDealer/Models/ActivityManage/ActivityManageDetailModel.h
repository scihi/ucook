//
//  ActivityManageDetailModel.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityManageDetailModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *store;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *typeName;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *nop;
@property (copy, nonatomic) NSString *restriction;
@property (copy, nonatomic) NSString *createTime;
@property (copy, nonatomic) NSString *intro;
@property (copy, nonatomic) NSString *position;
@property (copy, nonatomic) NSString *positionName;

@end
