//
//  CRequestManager.h
//  UCook
//
//  Created by scihi on 14/11/1.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface CRequestManager : NSObject

+(void)requestWith:(NSDictionary *)paramars
            method:(NSString *)method
     requestSucess:(void (^)(id object))sucess
    requestFailure:(void (^)(void))failer;

+(void)requestWith:(NSDictionary *)dic
            andURL:(NSString *)string
            reques:(void (^) (id result))success
             faile:(void (^) (void))file
           setFile:(UIImage *)imageFile;



@end
