//
//  StoreDataUser.m
//  UCook
//
//  Created by scihi on 14/11/7.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreDataUser.h"


static StoreDataUser *storeData = nil;
@implementation StoreDataUser

+(id)ShareStoreData
{
    if(!storeData)
    {
        storeData = [[StoreDataUser alloc]init];
        storeData.storeIdArray = [NSMutableArray new];
        storeData.nameArray = [NSMutableArray new];
        storeData.numOfGoodsArray = [NSMutableArray new];
    }
    return storeData;
}

@end
