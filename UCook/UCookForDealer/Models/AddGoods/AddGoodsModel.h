//
//  AddGoodsModel.h
//  UCook
//
//  Created by scihi on 14/11/2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

//对应添加商品中的二维码扫描模型
//ScanGoodsViewController

#import <Foundation/Foundation.h>

@interface AddGoodsModel : NSObject

@property(nonatomic,strong) NSString *barcode;
@property(nonatomic,strong) NSString *brand;
@property(nonatomic,strong) NSString *category;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSArray *images;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *rangeprice;
@property(nonatomic,strong) NSString *spec;
@property(nonatomic,strong) NSString *split;
@property(nonatomic,strong) NSString *unit;

@end
