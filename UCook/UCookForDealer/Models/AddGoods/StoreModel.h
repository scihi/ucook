//
//  StoreModel.h
//  UCook
//
//  Created by scihi on 14/11/5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreModel : NSObject


@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *area;
@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSString *intro;
@property(nonatomic,strong) NSString *introduction;

@property(nonatomic,strong) NSArray *isDelivery;
@property(nonatomic,strong) NSString *logo;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *notice;
@property(nonatomic,strong) NSString *qq;

@property(nonatomic,strong) NSString *realStatus;
@property(nonatomic,strong) NSString *realStatusName;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *statusName;
@property(nonatomic,strong) NSString *theme;

@property(nonatomic,strong) NSString *themeName;
@property(nonatomic,strong) NSString *zipCode;


@end
