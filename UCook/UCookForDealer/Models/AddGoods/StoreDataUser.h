//
//  StoreDataUser.h
//  UCook
//
//  Created by scihi on 14/11/7.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreDataUser : NSObject

@property(nonatomic,strong) NSMutableArray *storeIdArray;
@property(nonatomic,strong) NSMutableArray *nameArray;

//选中的商品 和保存选中的号码
@property(nonatomic,strong) NSArray *goodsArray;
@property(nonatomic,strong) NSMutableArray *numOfGoodsArray;
+(id)ShareStoreData;

@end
