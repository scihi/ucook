

//==================================================


//此头文件定义公共宏


//==================================================

#ifndef _____Define_h
#define _____Define_h



//随机色 自定义颜色
#define COLOR_a [UIColor colorWithRed:arc4random() % 250 / 255.0f green:arc4random() % 250 / 255.0f blue:arc4random() % 250 / 255.0f alpha:1]
#define COLOR(_R,_G,_B,_A) [UIColor colorWithRed:_R / 255.0f green: _G / 255.0f blue:_B / 255.0f alpha:_A]
//设置四维
#define CGRM(_X,_Y,_W,_H) CGRectMake(_X, _Y, _W, _H)
//设置大图片
#define IMAGE(_NAME) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForAuxiliaryExecutable:_NAME]]

#define NAVIGATION (UINavigationController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController]


#define SCREENBounds [UIScreen mainScreen ].bounds;

//自动布局

typedef enum
{
    iPhone4S = 0,   //320*480
    iPhone5S,       //320*568
    iPhone6,        //1334*750
    iPhone6plus     //1920*1080
}theiPhone;

//一下为比例计算的自动布局 略微麻烦 但是实用

//相对于窗口的自动比例
#define AUTOLayer(_x,_y,_width,_height,iPhone)\
[UIView getOrgX:_x getOrgY:_y getSizeWidth:_width getSizeHeight:_height whatiPhone:iPhone]
//相对于父视图的比例
#define AUTOSuperView(_x,_y,_width,_height,_suWidth,_suHeight)\
[UIView getX:_x getY:_y getWidth:_width getHeight:_height getSuperWidth:_suWidth getSuperHeight:_suHeight];

//相对于父视图的比例宽度
#define AUTOSuperWidth(_x,_suWidth)\
[UIView getWidth:_x getSuperWidth:_suWidth];

//相对于父视图的比例高度
#define AUTOSuperHeight(_y,_suHeight)\
[UIView getHeight:_y getSuperHeight:_suHeight];

#endif












