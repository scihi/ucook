//
//  UIView+AutoLayer.m
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "UIView+AutoLayer.h"

@implementation UIView (AutoLayer)

//利用控件的位置和大小得出所在屏幕的比例
//iPhone为当前所使用的模拟器
+(CGRect)getOrgX:(double)x
         getOrgY:(double)y
    getSizeWidth:(double)width
   getSizeHeight:(double)height
      whatiPhone:(int)iPhone
{
    //获取当前模拟器的尺寸
    CGRect phone;
    switch (iPhone)
    {
        case iPhone4S:phone.size.width = 320;phone.size.height = 480; break;
        case iPhone5S:phone.size.width = 320;phone.size.height = 568; break;
        case iPhone6:phone.size.width = 750;phone.size.height = 1334; break;
        case iPhone6plus:phone.size.width = 1080;phone.size.height = 1020; break;
            //默认使用4寸模拟器
        default:phone.size.width = 320;phone.size.height = 568;break;
    }
    //得出视图在该模拟器的比例
    double _x = x/phone.size.width;
    double _y = y/phone.size.height;
    double _w = width/phone.size.width;
    double _h = height/phone.size.height;
    //得出视图在当前模拟器或真机上的尺寸和位置
    CGRect screenBouds = SCREENBounds;
    double a = screenBouds.size.width;
    double b = screenBouds.size.height;
    return CGRectMake( _x*a,_y*b,_w*a,_h*b);

}


//相对于父视图的布局
+(CGRect)getX:(double)x
         getY:(double)y
     getWidth:(double)width
    getHeight:(double)height
getSuperWidth:(double)suWidth
getSuperHeight:(double)suHeight
{
    return CGRectMake(x*suWidth, y*suHeight, width*suWidth, height*suHeight);
}

//返回等比例的宽度
+(double)getWidth:(double)width
    getSuperWidth:(double)suWidth
{
    return width*suWidth;
    
}

//返回等比例的高度
+(double)getHeight:(double)height
    getSuperHeight:(double)suHeight
{
    return height *suHeight;
}













@end
