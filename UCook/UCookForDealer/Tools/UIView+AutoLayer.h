//
//  UIView+AutoLayer.h
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AutoLayer)


//自动布局 相对于窗口布局
+(CGRect)getOrgX:(double)x
         getOrgY:(double)y
    getSizeWidth:(double)width
   getSizeHeight:(double)height
      whatiPhone:(int)iPhone;

//相对于父视图的布局
+(CGRect)getX:(double)x
         getY:(double)y
     getWidth:(double)width
    getHeight:(double)height
getSuperWidth:(double)suWidth
getSuperHeight:(double)suHeight;

//返回等比例的宽度
+(double)getWidth:(double)width
    getSuperWidth:(double)suWidth;

//返回等比例的高度
+(double)getHeight:(double)height
    getSuperHeight:(double)suHeight;

@end
