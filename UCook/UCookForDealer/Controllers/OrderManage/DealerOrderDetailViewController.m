//
//  DealerOrderDetailViewController.m
//  UCook
//
//  Created by huangrun on 14/10/27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerOrderDetailViewController.h"

#import "UIImageView+AFNetworking.h"
#import "AllOrdersDetailModel.h"
#import "EvaluateViewController.h"
#import "SalesReturnViewController.h"
#import "ApplyUCookDeliveryViewController.h"
#import "PayShipFeeViewController.h"
#import "DealerEvaluateViewController.h"
#import "CustomIOS7AlertView.h"

@interface DealerOrderDetailViewController ()
{
    AllOrdersDetailModel *_allOrdersDetailModel;
    UIAlertView *_alertView;
    UIActionSheet *_actionSheet;
    UITextField *_tf1;
    UITextField *_tf2;
}

@end

@implementation DealerOrderDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单详情";
    
    self.leftButton.layer.cornerRadius = 18.0;
    self.leftButton.layer.borderColor = kThemeCGColorDealer;
    self.leftButton.layer.borderWidth = 1.0;
    [self.leftButton setTitleColor:kThemeColorDealer forState:UIControlStateNormal];
    self.rightButton.layer.cornerRadius = 18.0;
    self.rightButton.layer.borderColor = kThemeCGColorDealer;
    self.rightButton.layer.borderWidth = 1.0;
    [self.rightButton setTitleColor:kThemeColorDealer forState:UIControlStateNormal];
    
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickPhoneBtn:(id)sender
{
    UIWebView *phoneWV = [[UIWebView alloc]init];
    NSString *phoneNumStr = [NSString stringWithFormat:@"tel://%@",_allOrdersDetailModel.storeTel];
    [phoneWV loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:phoneNumStr]]];
    [self.view addSubview:phoneWV];
}
- (IBAction)clickLeftBtn:(id)sender {
    
    
}
- (IBAction)clickRightBtn:(id)sender {
    
    if (_allOrdersDetailModel.status == 21) {
        
        if (_allOrdersDetailModel.delivery.deliveryStatus == 0 || _allOrdersDetailModel.delivery.deliveryStatus == 5) {
            
            //            _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请选择发货方式" delegate:self cancelButtonTitle:@"申请优厨发货" otherButtonTitles:@"直接发货", nil];
            //            _alertView.tag = 101;
            //            [_alertView show];
            
            _actionSheet  = [[UIActionSheet alloc]initWithTitle:@"请选择发货方式" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"申请优厨发货",@"直接发货", nil];
            _actionSheet.tag = 101;
            [_actionSheet showInView:self.view];
            
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 1 || _allOrdersDetailModel.delivery.deliveryStatus == 3 || _allOrdersDetailModel.delivery.deliveryStatus == 4) {
            ApplyUCookDeliveryViewController *audVC = [[ApplyUCookDeliveryViewController alloc]initWithNibName:@"ApplyUCookDeliveryViewController" bundle:nil];
            audVC.orderIdStr = _allOrdersDetailModel.orderId;
            audVC.storeIdStr = _allOrdersDetailModel.storeId;
            [self.navigationController pushViewController:audVC animated:YES];
            
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 2) {
            PayShipFeeViewController *psfVC = [[PayShipFeeViewController alloc]initWithNibName:@"PayShipFeeViewController" bundle:nil];
            psfVC.orderIdStr = _allOrdersDetailModel.orderId;
            psfVC.storeIdStr = _allOrdersDetailModel.storeId;
            [self.navigationController pushViewController:psfVC animated:YES];
            
        }
    } else if (_allOrdersDetailModel.status == 22 || _allOrdersDetailModel.status == 11) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请您在收到货后再确认收货，否则有可能钱货两空。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 105;
        [_alertView show];
    } else if (_allOrdersDetailModel.status == 20 || _allOrdersDetailModel.status == 32 || _allOrdersDetailModel.status == 10 || _allOrdersDetailModel.status == 33)
    {
        //        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要放弃此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        //        _alertView.tag = 106;
        //        [_alertView show];
        
        if (_allOrdersDetailModel.status == 32 || _allOrdersDetailModel.status == 33) {
            [self launchDialog:nil];
        }
        
    } else if (_allOrdersDetailModel.status == 30) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要删除此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 107;
        [_alertView show];
        
    } else if (_allOrdersDetailModel.status == 24) {
        [self acceptAndNotModify];
    } else if (_allOrdersDetailModel.status == 23) {
        DealerEvaluateViewController *deVC = [[DealerEvaluateViewController alloc]initWithNibName:@"DealerEvaluateViewController" bundle:nil];
        deVC.orderIdStr = _allOrdersDetailModel.orderId;
        deVC.goodsPicUrl = self.imgUrlStr;
        deVC.goodsNameStr = self.titleStr;
        deVC.goodsNumInteger = self.amountStr;
        deVC.goodsPriceFloat = self.payFeeFloat;
        [self.navigationController pushViewController:deVC animated:YES];
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 80.f;
    }else if (indexPath.section == 1) {
        return 90.f;
    } else if (indexPath.section == 2) {
        return 50.f;
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            return 230.f;
        } else {
            return 30.f;
        }
        
    } else {
        return 90.f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _allOrdersDetailModel.products.count;
    } else if (section == 3) {
        return 1 + (_allOrdersDetailModel.refund.count != 0 ? _allOrdersDetailModel.refund.count + 1:0);
    } else if (section == 4) {
        return _allOrdersDetailModel.refundProduct.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section, indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        if (indexPath.section == 0) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"OrdersStatusCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 1) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AddressCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 2) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ShopInfoCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 3) {
            if (indexPath.row == 0) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"OrderInfoCell" owner:self options:nil]firstObject];
            } else {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
        } else {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DealerRefundProductsCell" owner:self options:nil]firstObject];
        }
    }
    if (indexPath.section == 0) {
        //订单状态
        NSDictionary *goodsInfoDic = [_allOrdersDetailModel.products objectAtIndex:indexPath.row];
        
        UIImageView *goodsIV = (UIImageView *)[cell viewWithTag:101];
        NSString *imgUrlStr = [goodsInfoDic objectForKey:@"pic"];
        [goodsIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        
        UILabel *goodsNameLabel = (UILabel *)[cell viewWithTag:102];
        goodsNameLabel.text = [goodsInfoDic objectForKey:@"title"];
        
        NSString *amountString = [goodsInfoDic objectForKey:@"quantity"];
        UILabel *goodsAmountLabel = (UILabel *)[cell viewWithTag:103];
        goodsAmountLabel.text = [NSString stringWithFormat:@"共%@件商品",amountString];
        
        NSString *goodsPriceStr = [goodsInfoDic objectForKey:@"total_price"];
        UILabel *goodsPriceLabel = (UILabel *)[cell viewWithTag:104];
        goodsPriceLabel.textColor = kThemeColorDealer;
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",goodsPriceStr];
    } else if (indexPath.section == 1) {
        //收货地址
        UILabel *addressLabel = (UILabel *)[cell viewWithTag:101];
        addressLabel.text = _allOrdersDetailModel.receiverAddress;
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:102];
        nameLabel.text = _allOrdersDetailModel.receiverName;
        
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:103];
        phoneLabel.text = _allOrdersDetailModel.receiverMobile;
    } else if (indexPath.section == 2) {
        //商铺信息
        UILabel *shopNameLabel = (UILabel *)[cell viewWithTag:101];
        shopNameLabel.text = _allOrdersDetailModel.storeName;
        
        UILabel *shopPhoneLabel = (UILabel *)[cell viewWithTag:102];
        shopPhoneLabel.text = _allOrdersDetailModel.storeTel;
    } else if (indexPath.section == 3) {
        //订单信息
        if (indexPath.row == 0) {
            UILabel *orderIdLabel = (UILabel *)[cell viewWithTag:101];
            orderIdLabel.text = _allOrdersDetailModel.orderId;
            
            UILabel *orderDateLabel = (UILabel *)[cell viewWithTag:102];
            orderDateLabel.text = _allOrdersDetailModel.createTime;
            
            UILabel *payTimeLabel = (UILabel *)[cell viewWithTag:103];
            payTimeLabel.text = _allOrdersDetailModel.payTime;
            
            UILabel *payMethodLabel = (UILabel *)[cell viewWithTag:104];
            payMethodLabel.text = _allOrdersDetailModel.payMethodName;
            
            UILabel *deliveryTimeLabel = (UILabel *)[cell viewWithTag:105];
            deliveryTimeLabel.text = _allOrdersDetailModel.shipTime;
            
            UILabel *evaluateStatusLabel = (UILabel *)[cell viewWithTag:106];
            evaluateStatusLabel.text = _allOrdersDetailModel.commentStatusName;
            
            UILabel *salesReturnAmountLabel = (UILabel *)[cell viewWithTag:107];
            salesReturnAmountLabel.text = _allOrdersDetailModel.refundTimes;
            
            UILabel *salesReturnStatusLabel = (UILabel *)[cell viewWithTag:108];
            NSString *salesReturnStatusStr;
            if (_allOrdersDetailModel.refundStatus == 0) {
                salesReturnStatusStr = @"不用退货";
            } else if (_allOrdersDetailModel.refundStatus == 1) {
                salesReturnStatusStr = @"申请退货中";
            }
            salesReturnStatusLabel.text = salesReturnStatusStr;
            
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"退款记录";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.font = [UIFont systemFontOfSize:13.0];
        } else {
            NSDictionary *currentDic = [_allOrdersDetailModel.refund objectAtIndex:indexPath.row - 2];
            NSString *refundStatusStr;
            if ([[currentDic objectForKey:@"status"] isEqualToString:@"1"]) {
                refundStatusStr = @"申请退货中";
            } else if ([[currentDic objectForKey:@"status"] isEqualToString:@"0"]) {
                refundStatusStr = @"不用退货";
            } else if ([[currentDic objectForKey:@"status"] isEqualToString:@"2"]) {
                refundStatusStr = @"同意退货";
            }
            
            cell.textLabel.text = [NSString stringWithFormat:@"退款金额：%@元       退款状态：%@",[currentDic objectForKey:@"totalFee"],refundStatusStr];
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.font = [UIFont systemFontOfSize:12.0];
        }
    } else {
        //退货产品
        NSDictionary *dictionary = [_allOrdersDetailModel.refundProduct objectAtIndex:indexPath.row];
        UIImageView *titleIV = (UIImageView *)[cell viewWithTag:101];
        NSString *imgUrlStr = [dictionary objectForKey:@"pic"];
        [titleIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
        titleLabel.text = [dictionary objectForKey:@"title"];
        
        //规格
        UILabel *specLabel = (UILabel *)[cell viewWithTag:103];
        NSString *specStr = [dictionary objectForKey:@"spec"];
        specLabel.text = [NSString stringWithFormat:@"规格:%@",specStr];
        
        //单位
        UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];
        NSString *unitStr = [dictionary objectForKey:@"unit"];
        unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitStr];
        
        // 整/单
        UILabel *fullOrSingleLabel = (UILabel *)[cell viewWithTag:105];
        NSString *fullOrSingleStr = [dictionary objectForKey:@"split"];
        fullOrSingleLabel.text = [NSString stringWithFormat:@"整/单:%@",fullOrSingleStr];
        
        //数量
        UILabel *numberLabel = (UILabel *)[cell viewWithTag:106];
        NSString *numberStr = [dictionary objectForKey:@"quantity"];
        numberLabel.text = [NSString stringWithFormat:@"数量:%@",numberStr];
        
        //总分
        UILabel *totalPriceLabel = (UILabel *)[cell viewWithTag:107];
        NSString *totalPriceStr = [dictionary objectForKey:@"total_price"];
        totalPriceLabel.text = [NSString stringWithFormat:@"总价:%@",totalPriceStr];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;
    
    //为section底部添加分割线
//    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width - 10, 1);
//    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
//    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
//    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
//    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width - 10, 5)];
//    lineView.backgroundColor = kThemeColor;
//    [headerView addSubview:lineView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 9.5, 80, 25)];
    if (section == 0) {
        titleLabel.text = @"订单状态";
        UILabel *goodsStatusLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, BOUNDS.size.width - 100 - 20, 44)];
        goodsStatusLabel.font = [UIFont systemFontOfSize:17];
        goodsStatusLabel.textColor = kThemeColorDealer;
        goodsStatusLabel.textAlignment = NSTextAlignmentRight;
        goodsStatusLabel.text = _allOrdersDetailModel.statusName;
        [headerView addSubview:goodsStatusLabel];
    } else if (section == 1) {
        titleLabel.text = @"收货地址";
    } else if (section == 2) {
        titleLabel.text = @"商铺信息";
    } else if (section == 3) {
        titleLabel.text = @"订单信息";
    } else {
        titleLabel.text = @"退货产品";
    }
    
    [headerView addSubview:titleLabel];
    
    
    return headerView;
}

- (void)configTheView {
    if (_allOrdersDetailModel.status == 32 || _allOrdersDetailModel.status == 33) {
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        [self.leftButton setTitle:@"拒绝" forState:UIControlStateNormal];
        [self.rightButton setTitle:@"受理" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status == 20) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
    } else if (_allOrdersDetailModel.status == 23) {
        //2表示餐厅已评
        if ([_allOrdersDetailModel.commentStatus isEqualToString:@"0"] || [_allOrdersDetailModel.commentStatus isEqualToString:@"2"]) {
            self.leftButton.hidden = YES;
            self.rightButton.hidden = NO;
            [self.rightButton setTitle:@"评价" forState:UIControlStateNormal];
        } else if ([_allOrdersDetailModel.commentStatus isEqualToString:@"1"]) {
            self.leftButton.hidden = YES;
            [self.rightButton setTitle:@"评价" forState:UIControlStateNormal];
        }
        
        if (_allOrdersDetailModel.refundStatus == 2) {
            self.leftButton.hidden = NO;
            self.leftButton.userInteractionEnabled = NO;
            self.leftButton.backgroundColor = [UIColor lightGrayColor];
            [self.leftButton setTitle:@"已退货" forState:UIControlStateNormal];
            
        } else if (_allOrdersDetailModel.refundStatus == 3) {
            
            self.leftButton.hidden = NO;
            self.leftButton.userInteractionEnabled = NO;
            self.leftButton.backgroundColor = [UIColor lightGrayColor];
            [self.leftButton setTitle:@"拒绝退货" forState:UIControlStateNormal];
            
        } else if (_allOrdersDetailModel.refundStatus == 1) {
            self.leftButton.hidden = NO;
            [self.leftButton setTitle:@"处理退货" forState:UIControlStateNormal];
            self.leftButton.userInteractionEnabled = NO;
        }
    } else if (_allOrdersDetailModel.status == 21 ) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        
        if (_allOrdersDetailModel.delivery.deliveryStatus == 0 || _allOrdersDetailModel.delivery.deliveryStatus == 5) {
            [self.rightButton setTitle:@"发货" forState:UIControlStateNormal];
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 1 || _allOrdersDetailModel.delivery.deliveryStatus == 3 || _allOrdersDetailModel.delivery.deliveryStatus == 4) {
            [self.rightButton setTitle:@"配送详情" forState:UIControlStateNormal];
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 2) {
            [self.rightButton setTitle:@"支付运费" forState:UIControlStateNormal];
        }
        
    }  else if (_allOrdersDetailModel.status == 22 || _allOrdersDetailModel.status == 11) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
    } else if (_allOrdersDetailModel.status == 30) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
    } else if (_allOrdersDetailModel.status == 31) {
        ////        leftButton.hidden = NO;
        ////        rightButton.hidden = NO;
        ////        [leftButton setTitle:@"交易成功" forState:UIControlStateNormal];
        //        if (_allOrderModel.refundStatus == 1) {
        ////            [rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];                       rightButton.userInteractionEnabled = NO;
        //        } else if (_allOrderModel.refundStatus == 2) {
        //            leftButton.hidden = YES;
        //            [rightButton setTitle:@"已经退货" forState:UIControlStateNormal];
        //            rightButton.backgroundColor = [UIColor lightGrayColor];
        //            rightButton.userInteractionEnabled = NO;
        //        }
        
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        
        if (!_allOrdersDetailModel.delivery) {
            
            if (_allOrdersDetailModel.refundStatus == 2)
            {
                
                [self.rightButton setTitle:@"已经退货" forState:UIControlStateNormal];
                self.rightButton.backgroundColor = [UIColor lightGrayColor];
                self.rightButton.userInteractionEnabled = NO;
            } else if (_allOrdersDetailModel.refundStatus == 3) {
                [self.rightButton setTitle:@"拒绝退货" forState:UIControlStateNormal];
                self.rightButton.backgroundColor = [UIColor lightGrayColor];
                self.rightButton.userInteractionEnabled = NO;
            } else if (_allOrdersDetailModel.refundStatus == 0)
            {
                self.rightButton.hidden = YES;
                self.rightButton.hidden = YES;
            }
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 0 || _allOrdersDetailModel.delivery.deliveryStatus == 5) {
            [self.rightButton setTitle:@"发货" forState:UIControlStateNormal];
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 1 || _allOrdersDetailModel.delivery.deliveryStatus == 3 || _allOrdersDetailModel.delivery.deliveryStatus == 4) {
            [self.rightButton setTitle:@"配送详情" forState:UIControlStateNormal];
        } else if (_allOrdersDetailModel.delivery.deliveryStatus == 2) {
            [self.rightButton setTitle:@"支付运费" forState:UIControlStateNormal];
        }
        
        
        
    }  else if (_allOrdersDetailModel.status == 24)
    {
        //        leftButton.hidden = YES;
        //        rightButton.hidden = NO;
        //        [rightButton setTitle:@"申请退款中" forState:UIControlStateNormal];
        //        rightButton.userInteractionEnabled = NO;
        
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        [self.leftButton setTitle:@"拒绝" forState:UIControlStateNormal];
        [self.rightButton setTitle:@"同意" forState:UIControlStateNormal];
        
    } else if (_allOrdersDetailModel.status == 25) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
    } else if (_allOrdersDetailModel.status == 10) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        [self.rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status == 13) {
        self.leftButton.hidden = YES;
        self.rightButton.hidden = NO;
        [self.rightButton setTitle:@"餐厅评价" forState:UIControlStateNormal];
        self.rightButton.backgroundColor = [UIColor lightGrayColor];
        self.rightButton.userInteractionEnabled = NO;
    } else if (_allOrdersDetailModel.status == 27) {
        self.leftButton.hidden = YES;
        [self.rightButton setTitle:@"收货" forState:UIControlStateNormal];
    }

    
}

- (void)getTheInitData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"orderAgency",
                                 @"action": @"detail",
                                 @"orderId":_orderIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        [self stopRequest];
        if (RIGHT)
        {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _allOrdersDetailModel = [AllOrdersDetailModel objectWithKeyValues:dictionary];
            
            NSLog(@"%@",_allOrdersDetailModel.storeName);
            
            [self configTheView];
            [_theTableView reloadData];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


#pragma mark - 优厨币付款
- (void)payByUcookb {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"ucookbPay",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId,@"total_fee": @([_allOrdersDetailModel.payFee integerValue])};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


- (void)deleteTheOrder {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"delete",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)launchDialog:(id)sender
{
    // Here we need to pass a full frame
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"不修改", @"修改", @"取消", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (void)acceptAndNotModify {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId, @"type":@"confirm"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (UIView *)createDemoView
{
    //    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    //
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    //
    //    return demoView;
    
    UIView *showView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width - 20, 150)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 5, 290, 20)];
    titleLabel.text = @"是否修改订单运费?";
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 100, 20)];
    label1.text = @"运费金额:";
    _tf1 = [[UITextField alloc]initWithFrame:CGRectMake(5, 55, 290, 35)];
    _tf1.borderStyle = UITextBorderStyleRoundedRect;
    _tf1.text = @"0.00";
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(5, 100, 100, 20)];
    label2.text = @"订单金额:";
    _tf2 = [[UITextField alloc]initWithFrame:CGRectMake(5, 125, 290, 34)];
    _tf2.borderStyle = UITextBorderStyleRoundedRect;
    _tf2.text = [NSString stringWithFormat:@"%.2f",_allOrdersDetailModel.payFee];
    [showView addSubview:titleLabel];
    [showView addSubview:label1];
    [showView addSubview:label2];
    [showView addSubview:_tf1];
    [showView addSubview:_tf2];
    
    
    return showView;
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    
    if (buttonIndex == 0) {
        [self acceptAndNotModify];
    } else if (buttonIndex == 1)
    {
        [self acceptAndModify];
    }
    
    [alertView close];
}


- (void)acceptAndModify {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId, @"type":@"confirm",@"shipFee":_tf1.text,@"payFee":_tf2.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)refuse {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId, @"type":@"deny"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


@end
