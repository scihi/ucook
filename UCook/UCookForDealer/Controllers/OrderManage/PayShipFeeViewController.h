//
//  PayShipFeeViewController.h
//  UCook
//
//  Created by huangrun on 14/10/27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AlixLibService.h"

@interface PayShipFeeViewController : GeneralWithBackBtnViewController

@property (copy, nonatomic) NSString *orderIdStr;
@property (copy, nonatomic) NSString *storeIdStr;
@property (weak, nonatomic) IBOutlet UILabel *lowestMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *distenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellphoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *postcodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;
@property (weak, nonatomic) IBOutlet UIButton *balancePayBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayPayBtn;
- (IBAction)clickDoneBtn:(id)sender;
- (IBAction)clickBalancePayBtn:(id)sender;
- (IBAction)clickAlipayPayBtn:(id)sender;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调

@end
