//
//  DealerOrderViewController.m
//  UCook
//
//  Created by huangrun on 14-10-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerOrderViewController.h"
#import "AllOrdersModel.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"
#import "CGRectAdditionals.h"
//#import "AllOrdersDetailViewController.h"
#import "AllOrdersDetailModel.h"
#import "AlipayModel.h"

#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"

#import "RechargeViewController.h"

#import "SalesReturnViewController.h"

#import "DealerEvaluateViewController.h"
#import "ApplyUCookDeliveryViewController.h"
#import "PayShipFeeViewController.h"
#import "DealerOrderDetailViewController.h"

@interface DealerOrderViewController ()  {
    UITableView *_theTableView;
    AllOrdersModel *_allOrderModel;
    NSMutableArray *_allOrderModelMutArr;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    UIView *_noDataView;//无数据时的展示背景
    
    NSIndexPath *_indexPath;//点击付款按钮时需要的indexPath
    
    AllOrdersDetailModel *_allOrderDetailModel;//为了取出里面的product的id和number，本是详情页的数据
    
    UIAlertView *_alertView;
    UIActionSheet *_actionSheet;
    
    AlipayModel *_alipayModel;
    
    UITextField *_tf1;
    UITextField *_tf2;
    
    NSMutableArray *cellArray;
    
}

@end

@implementation DealerOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        [self initData];
        self.title = @"订单详情";
    }
    return self;
}

-(void)initData
{
    self.storeId = @"";
    cellArray = [NSMutableArray new];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getTheInitData];
}

#pragma mark - 为了解决支付宝WAP支付取消返回回来界面偏移问题
- (void)viewDidLayoutSubviews {
    
    if (self.view.frame.size.height != (BOUNDS.size.height - 64)) {
        
//        self.tabBarController.tabBar.hidden = YES;
        _theTableView.frame =CGRectMake(_theTableView.frame.origin.x, _theTableView.frame.origin.y,_theTableView.frame.size.width, BOUNDS.size.height - 64);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.titleString;
    
    _result = @selector(paymentResult:);//支付宝的应用内(wap方式)回调方法
    
//    //设置背景图片
//    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64:568 - 64 - 88)];
//    bgIV.image = [UIImage imageNamed:@"public_bg"];
//    [self.view addSubview:bgIV];
//    [self.view sendSubviewToBack:bgIV];
    
    
    _theTableView  = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(cellArray.count!=0)
    {
        return cellArray.count;
    }
    else
    {
        return _allOrderModelMutArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section, indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DealerOrderCell" owner:self options:nil]firstObject];
    }
    
    
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:indexPath.row];

    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = _allOrderModel.image;
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = _allOrderModel.title;
    
    UILabel *amountLabel = (UILabel *)[cell viewWithTag:103];
    NSString *amountString = [NSString stringWithFormat:@"共%d件商品",_allOrderModel.amout];
    amountLabel.text = amountString;
    
    UILabel *priceLabel = (UILabel *)[cell viewWithTag:104];
    NSString *priceString = [NSString stringWithFormat:@"￥%.2f",_allOrderModel.payFee];
    priceLabel.text = priceString;
    
    UIButton *leftButton = (UIButton *)[cell viewWithTag:105];
    leftButton.layer.masksToBounds = YES;
    leftButton.layer.borderWidth = 1.f;
    leftButton.layer.borderColor = kThemeCGColorDealer;
    leftButton.layer.cornerRadius = 12.f;
    [leftButton addTarget:self action:@selector(clickLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rightButton = (UIButton *)[cell viewWithTag:106];
    rightButton.layer.masksToBounds = YES;
    rightButton.layer.borderWidth = 1.f;
    rightButton.layer.borderColor = kThemeCGColorDealer;
    rightButton.layer.cornerRadius = 12.f;
    [rightButton addTarget:self action:@selector(clickRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_allOrderModel.status == 32 || _allOrderModel.status == 33) {
        leftButton.hidden = NO;
        rightButton.hidden = NO;
        [leftButton setTitle:@"拒绝" forState:UIControlStateNormal];
        [rightButton setTitle:@"受理" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 20) {
        leftButton.hidden = YES;
        rightButton.hidden = YES;
    } else if (_allOrderModel.status == 23) {
        //2表示餐厅已评
        if ([_allOrderModel.commentStatus isEqualToString:@"0"] || [_allOrderModel.commentStatus isEqualToString:@"2"]) {
            leftButton.hidden = YES;
            rightButton.hidden = NO;
            [rightButton setTitle:@"评价" forState:UIControlStateNormal];
        } else if ([_allOrderModel.commentStatus isEqualToString:@"1"]) {
            leftButton.hidden = YES;
            [rightButton setTitle:@"评价" forState:UIControlStateNormal];
        }
        
        if (_allOrderModel.refundStatus == 2) {
            leftButton.hidden = NO;
            leftButton.userInteractionEnabled = NO;
            leftButton.backgroundColor = [UIColor lightGrayColor];
            [leftButton setTitle:@"已退货" forState:UIControlStateNormal];
            
        } else if (_allOrderModel.refundStatus == 3) {
            
            leftButton.hidden = NO;
            leftButton.userInteractionEnabled = NO;
            leftButton.backgroundColor = [UIColor lightGrayColor];
            [leftButton setTitle:@"拒绝退货" forState:UIControlStateNormal];
            
        } else if (_allOrderModel.refundStatus == 1) {
            leftButton.hidden = NO;
            [leftButton setTitle:@"处理退货" forState:UIControlStateNormal];
            leftButton.userInteractionEnabled = NO;
        }
    } else if (_allOrderModel.status == 21 ) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        
        if (_allOrderModel.delivery.deliveryStatus == 0 || _allOrderModel.delivery.deliveryStatus == 5) {
            [rightButton setTitle:@"发货" forState:UIControlStateNormal];
        } else if (_allOrderModel.delivery.deliveryStatus == 1 || _allOrderModel.delivery.deliveryStatus == 3 || _allOrderModel.delivery.deliveryStatus == 4) {
            [rightButton setTitle:@"配送详情" forState:UIControlStateNormal];
        } else if (_allOrderModel.delivery.deliveryStatus == 2) {
            [rightButton setTitle:@"支付运费" forState:UIControlStateNormal];
        }
        
    }  else if (_allOrderModel.status == 22 || _allOrderModel.status == 11) {
        leftButton.hidden = YES;
        rightButton.hidden = YES;
    } else if (_allOrderModel.status == 30) {
        leftButton.hidden = YES;
        rightButton.hidden = YES;
    } else if (_allOrderModel.status == 31) {
////        leftButton.hidden = NO;
////        rightButton.hidden = NO;
////        [leftButton setTitle:@"交易成功" forState:UIControlStateNormal];
//        if (_allOrderModel.refundStatus == 1) {
////            [rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];                       rightButton.userInteractionEnabled = NO;
//        } else if (_allOrderModel.refundStatus == 2) {
//            leftButton.hidden = YES;
//            [rightButton setTitle:@"已经退货" forState:UIControlStateNormal];
//            rightButton.backgroundColor = [UIColor lightGrayColor];
//            rightButton.userInteractionEnabled = NO;
//        }
        
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        
        if (!_allOrderModel.delivery) {
            
             if (_allOrderModel.refundStatus == 2) {
            
            [rightButton setTitle:@"已经退货" forState:UIControlStateNormal];
            rightButton.backgroundColor = [UIColor lightGrayColor];
            rightButton.userInteractionEnabled = NO;
            } else if (_allOrderModel.refundStatus == 3) {
                [rightButton setTitle:@"拒绝退货" forState:UIControlStateNormal];
                rightButton.backgroundColor = [UIColor lightGrayColor];
                rightButton.userInteractionEnabled = NO;
            } else if (_allOrderModel.refundStatus == 0) {
                rightButton.hidden = YES;
                rightButton.hidden = YES;
            }
        } else if (_allOrderModel.delivery.deliveryStatus == 0 || _allOrderModel.delivery.deliveryStatus == 5) {
            [rightButton setTitle:@"发货" forState:UIControlStateNormal];
        } else if (_allOrderModel.delivery.deliveryStatus == 1 || _allOrderModel.delivery.deliveryStatus == 3 || _allOrderModel.delivery.deliveryStatus == 4) {
            [rightButton setTitle:@"配送详情" forState:UIControlStateNormal];
        } else if (_allOrderModel.delivery.deliveryStatus == 2) {
            [rightButton setTitle:@"支付运费" forState:UIControlStateNormal];
        }
    

        
    }  else if (_allOrderModel.status == 24) {
//        leftButton.hidden = YES;
//        rightButton.hidden = NO;
//        [rightButton setTitle:@"申请退款中" forState:UIControlStateNormal];
//        rightButton.userInteractionEnabled = NO;
        
        leftButton.hidden = NO;
        rightButton.hidden = NO;
        [leftButton setTitle:@"拒绝" forState:UIControlStateNormal];
        [rightButton setTitle:@"同意" forState:UIControlStateNormal];
        
    } else if (_allOrderModel.status == 25) {
        leftButton.hidden = YES;
        rightButton.hidden = YES;
    } else if (_allOrderModel.status == 10) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 13) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"餐厅评价" forState:UIControlStateNormal];
        rightButton.backgroundColor = [UIColor lightGrayColor];
        rightButton.userInteractionEnabled = NO;
    } else if (_allOrderModel.status == 27) {
        leftButton.hidden = YES;
        [rightButton setTitle:@"收货" forState:UIControlStateNormal];
    }
    
    
    UILabel *statusNameLabel = (UILabel *)[cell viewWithTag:107];
    statusNameLabel.text = _allOrderModel.statusName;
    
    return cell;
}


- (void)getTheInitData
{
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"list",@"uid":[AccountManager getUid],@"page": @(_pageNumber),@"status":_statusString};
    [self startRequestWithString:@"请稍候..."];
    
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        [self stopRequest];
        if (RIGHT)
        {
            NSArray *array = [responseObject objectForKey:@"data"];
            _allOrderModelMutArr = [NSMutableArray arrayWithArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
            
            //如果是确定店铺的未处理订单
            if(![self.storeId isEqualToString:@""])
            {
                //排除不是STOREID店铺的商品
                for(int i=0;i<_allOrderModelMutArr.count;++i)
                {
                    _allOrderModel = _allOrderModelMutArr[i];
                    if(![_allOrderModel.storeId isEqualToString:self.storeId])
                    {
                       NSLog(@"%@  %@",_allOrderModel.storeId,self.storeId);
                        
                       [_allOrderModelMutArr removeObject:_allOrderModel];
                    }
                }
            }
        
            
            [_theTableView reloadData];
        }
        else if ([[responseObject objectForKey:@"code"]integerValue] == -2)
        {
            [_allOrderModelMutArr removeAllObjects];
            [_theTableView reloadData];
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor clearColor];
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];
        } else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //已废弃 huangrun
    //    [self getOrderDetailData:indexPath];
    
    DealerOrderDetailViewController *dodVC = [[DealerOrderDetailViewController alloc]initWithNibName:@"DealerOrderDetailViewController" bundle:nil];
        dodVC.orderIdStr = [[_allOrderModelMutArr objectAtIndex:indexPath.row]orderId];
    dodVC.imgUrlStr = _allOrderModel.image;
    dodVC.titleStr = _allOrderModel.title;
    dodVC.amountStr = _allOrderModel.amout;
    dodVC.payFeeFloat = _allOrderModel.payFee;
    [self.navigationController pushViewController:dodVC animated:YES];
}


- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"list",@"uid":[AccountManager getUid],@"page": @(_pageNumber),@"status":_statusString};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            //            [self stopRequest];
            if (RIGHT)
            {
                //            [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_allOrderModelMutArr addObjectsFromArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
                
                //如果是确定店铺的未处理订单
                if(![self.storeId isEqualToString:@""])
                {
                    //排除不是STOREID店铺的商品
                    for(int i=0;i<_allOrderModelMutArr.count;++i)
                    {
                        _allOrderModel = _allOrderModelMutArr[i];
                        if(![_allOrderModel.storeId isEqualToString:self.storeId])
                        {
                            NSLog(@"%@  %@",_allOrderModel.storeId,self.storeId);
                            
                            [_allOrderModelMutArr removeObject:_allOrderModel];
                        }
                    }
                }
                
                [self doneWithView:refreshView];
            }
            else if ([[responseObject objectForKey:@"code"]integerValue] == -2)
            {
                
                //                    [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_allOrderModelMutArr addObjectsFromArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
                [self doneWithView:refreshView];
                
            }
            else
            {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (void)configTheNoDataViewForTheTableView
{
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}

- (void)clickLeftBtn:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
            cell = (UITableViewCell *)[[sender superview]superview];
    } else {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    _indexPath = [_theTableView indexPathForCell:cell];
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:_indexPath.row];
    
    //付款
    if (_allOrderModel.status == 20) {
        //优厨币支付
        if ([_allOrderModel.payMethod isEqualToString:@"3"]) {
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",_allOrderModel.orderId, _allOrderModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"优厨币支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 101;
            [_alertView show];
            
        } else if ([_allOrderModel.payMethod isEqualToString:@"1"]) {
            //支付宝支付
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",_allOrderModel.orderId, _allOrderModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"支付宝支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 103;
            [_alertView show];
            
        } else if ([_allOrderModel.payMethod isEqualToString:@"0"])
        {
            [GlobalSharedClass showAlertView:@"该商品为货到付款,请收到货再支付"];
            return;
        }
    
    } else if (_allOrderModel.status == 32 || _allOrderModel.status == 33) {
        [self refuse];
    } else if (_allOrderModel.status == 24) {
        [self refuse];
    }
}

- (void)clickRightBtn:(id)sender
{
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[sender superview]superview];
    } else {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    _indexPath = [_theTableView indexPathForCell:cell];
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:_indexPath.row];
    
    if (_allOrderModel.status == 21) {
        
        if (_allOrderModel.delivery.deliveryStatus == 0 || _allOrderModel.delivery.deliveryStatus == 5) {
            
//            _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请选择发货方式" delegate:self cancelButtonTitle:@"申请优厨发货" otherButtonTitles:@"直接发货", nil];
//            _alertView.tag = 101;
//            [_alertView show];
        
            _actionSheet  = [[UIActionSheet alloc]initWithTitle:@"请选择发货方式" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"申请优厨发货",@"直接发货", nil];
            _actionSheet.tag = 101;
            [_actionSheet showInView:self.view];

        } else if (_allOrderModel.delivery.deliveryStatus == 1 || _allOrderModel.delivery.deliveryStatus == 3 || _allOrderModel.delivery.deliveryStatus == 4) {
            ApplyUCookDeliveryViewController *audVC = [[ApplyUCookDeliveryViewController alloc]initWithNibName:@"ApplyUCookDeliveryViewController" bundle:nil];
            audVC.orderIdStr = _allOrderModel.orderId;
            audVC.storeIdStr = _allOrderModel.storeId;
            [self.navigationController pushViewController:audVC animated:YES];

        } else if (_allOrderModel.delivery.deliveryStatus == 2) {
            PayShipFeeViewController *psfVC = [[PayShipFeeViewController alloc]initWithNibName:@"PayShipFeeViewController" bundle:nil];
            psfVC.orderIdStr = _allOrderModel.orderId;
            psfVC.storeIdStr = _allOrderModel.storeId;
            [self.navigationController pushViewController:psfVC animated:YES];

        }
    } else if (_allOrderModel.status == 22 || _allOrderModel.status == 11) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请您在收到货后再确认收货，否则有可能钱货两空。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 105;
        [_alertView show];
    } else if (_allOrderModel.status == 20 || _allOrderModel.status == 32 || _allOrderModel.status == 10 || _allOrderModel.status == 33) {
//        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要放弃此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//        _alertView.tag = 106;
//        [_alertView show];
        
        if (_allOrderModel.status == 32 || _allOrderModel.status == 33) {
            [self launchDialog:nil];
        }
        
    } else if (_allOrderModel.status == 30) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要删除此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 107;
        [_alertView show];
        
    } else if (_allOrderModel.status == 24) {
        [self acceptAndNotModify];
    } else if (_allOrderModel.status == 23) {
        DealerEvaluateViewController *deVC = [[DealerEvaluateViewController alloc]initWithNibName:@"DealerEvaluateViewController" bundle:nil];
        deVC.orderIdStr = _allOrderModel.orderId;
        deVC.goodsPicUrl = _allOrderModel.image;
        deVC.goodsNameStr = _allOrderModel.title;
        deVC.goodsNumInteger = _allOrderModel.amout;
        deVC.goodsPriceFloat = _allOrderModel.payFee;
        [self.navigationController pushViewController:deVC animated:YES];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case 101:
            if (buttonIndex == 0) {
                ApplyUCookDeliveryViewController *audVC = [[ApplyUCookDeliveryViewController alloc]initWithNibName:@"ApplyUCookDeliveryViewController" bundle:nil];
                audVC.orderIdStr = _allOrderModel.orderId;
                audVC.storeIdStr = _allOrderModel.storeId;
                [self.navigationController pushViewController:audVC animated:YES];
            } else if (buttonIndex == 1) {
                [self acceptAndNotModify];
            }
            break;
            
        default:
            break;
    }

}

#pragma mark - 优厨币付款
- (void)payByUcookb
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"ucookbPay",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId,@"total_fee": @(_allOrderModel.payFee)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        [self stopRequest];
        if (RIGHT)
        {
            [GlobalSharedClass showAlertView:MESSAGE];
            [_allOrderModelMutArr removeObjectAtIndex:_indexPath.section];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:_indexPath.section];
            [_theTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

#pragma mark - 请求支付宝参数
- (void)getAlipayParameters {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSArray *orderIdArr = @[@{@"orderId": _allOrderModel.orderId}];
    NSString *orderIdArrJsonStr = [self arrayToJson:orderIdArr];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"payString",@"uid":[AccountManager getUid], @"orderIds": orderIdArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _alipayModel = [AlipayModel objectWithKeyValues:dictionary];
            
            [self pay];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

#pragma mark - 支付宝付款
- (void)pay {
    /*
	 *生成订单信息及签名
	 *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
	 */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
	
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];
    
}

- (NSString *)getOrderInfo {
    /*
	 *点击获取prodcut实例并初始化订单信息
	 */
	Product *product = [[Product alloc]init];
    product.subject = [_alipayModel.info objectForKey:@"subject"];
    product.body = [_alipayModel.info objectForKey:@"body"];
    product.price = [[_alipayModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
    //    order.partner = PartnerID;
    //    order.seller = SellerID;
    order.partner = [_alipayModel.info objectForKey:@"partner"];
    order.seller = [_alipayModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_alipayModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
	order.productName = product.subject; //商品标题
	order.productDescription = product.body; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
	order.notifyURL = [_alipayModel.info objectForKey:@"notify_url"]; //回调URL
	
	return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
			id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
			if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
			}
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}

#pragma mark - 退款更新状态为退款申请中(21->24)、退货更新为申请退货中(23->25)、退款更新为申请退款中(22->23)、放弃交易更新为结束交易 (20->30、32->30、10->30)
- (void)orderStatusUpdate
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSInteger wantStatus;//需要更新到的状态值
    if (_allOrderModel.status == 21)
    {
        wantStatus = 24;
    } else if (_allOrderModel.status == 23) {
        wantStatus = 25;
    } else if (_allOrderModel.status == 22) {
        wantStatus = 23;
    } else if (_allOrderModel.status == 20 || _allOrderModel.status == 32 || _allOrderModel.status == 10) {
        wantStatus = 30;
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"statusUpdate",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId,@"type": @"status",@"status": @(wantStatus)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)deleteTheOrder
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"delete",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self stopRequest];
        if (RIGHT)
        {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)launchDialog:(id)sender
{
    // Here we need to pass a full frame
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"不修改", @"修改", @"取消", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    
    if (buttonIndex == 0) {
        [self acceptAndNotModify];
    } else if (buttonIndex == 1) {
        [self acceptAndModify];
    }
    
    [alertView close];
}

- (UIView *)createDemoView
{
//    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
//    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
//    [imageView setImage:[UIImage imageNamed:@"demo"]];
//    [demoView addSubview:imageView];
//    
//    return demoView;
    
        UIView *showView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width - 20, 150)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 5, 290, 20)];
    titleLabel.text = @"是否修改订单运费?";
        UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 100, 20)];
        label1.text = @"运费金额:";
    _tf1 = [[UITextField alloc]initWithFrame:CGRectMake(5, 55, 290, 35)];
    _tf1.borderStyle = UITextBorderStyleRoundedRect;
    _tf1.text = @"0.00";
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(5, 100, 100, 20)];
    label2.text = @"订单金额:";
    _tf2 = [[UITextField alloc]initWithFrame:CGRectMake(5, 125, 290, 34)];
    _tf2.borderStyle = UITextBorderStyleRoundedRect;
    _tf2.text = [NSString stringWithFormat:@"%.2f",_allOrderModel.payFee];
    [showView addSubview:titleLabel];
    [showView addSubview:label1];
    [showView addSubview:label2];
    [showView addSubview:_tf1];
    [showView addSubview:_tf2];

    
    return showView;
}

- (void)acceptAndNotModify {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId, @"type":@"confirm"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)acceptAndModify {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId, @"type":@"confirm",@"shipFee":_tf1.text,@"payFee":_tf2.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)refuse
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"status",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId, @"type":@"deny"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

@end