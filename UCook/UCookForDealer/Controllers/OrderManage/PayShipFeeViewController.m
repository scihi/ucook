//
//  PayShipFeeViewController.m
//  UCook
//
//  Created by huangrun on 14/10/27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "PayShipFeeViewController.h"
#import "ApplyUCookDeliveryModel.h"
#import "NSStringAdditions.h"
#import "DealerDeliveryModel.h"

#import "AlipayModel.h"

#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"
#import "RechargeViewController.h"

@interface PayShipFeeViewController () {
    ApplyUCookDeliveryModel *_audModel;
    
        AlipayModel *_alipayModel;
}

@end

@implementation PayShipFeeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"支付运费";
    
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"deliveryDetail",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"storeId": self.storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _audModel = [ApplyUCookDeliveryModel objectWithKeyValues:dictionary];
          
            self.lowestMoneyLabel.text = [NSString stringWithFormat:@"%@",_audModel.shipfee];
            self.orderIdLabel.text = self.orderIdStr;
            self.weightLabel.text = [NSString stringWithFormat:@"%@",_audModel.weight];
            self.floorLabel.text = _audModel.floor;
            self.distenceLabel.text = [NSString stringWithFormat:@"%@",_audModel.kilometer];
            self.nameLabel.text = _audModel.receiver_name;
            self.cellphoneLabel.text = _audModel.receiver_mobile;
            self.telLabel.text = _audModel.receiver_phone;
            self.addressLabel.text = _audModel.receiver_address;
            self.postcodeLabel.text = _audModel.receiver_zip;
            self.deliveryAddressLabel.text = _audModel.address;
            self.feedbackLabel.text = _audModel.delivery.deliveryFeedback;
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


- (IBAction)clickDoneBtn:(id)sender {

    if (self.balancePayBtn.selected == YES) {
        [self balancePay];
    } else {
        [self alipayPay];
    }
    
}

- (IBAction)clickBalancePayBtn:(id)sender {
    self.balancePayBtn.selected = YES;
    self.alipayPayBtn.selected = NO;
}

- (IBAction)clickAlipayPayBtn:(id)sender {
    self.alipayPayBtn.selected = YES;
    self.balancePayBtn.selected = NO;
}

- (void)balancePay {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"deliveryPay",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"payWay": @"3"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)alipayPay {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"deliveryPay",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"payWay": @"1"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _alipayModel = [AlipayModel objectWithKeyValues:dictionary];
            
            [self pay];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
   
}

#pragma mark - 支付宝付款
- (void)pay {
    /*
     *生成订单信息及签名
     *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
     */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
    
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];
    
}

- (NSString *)getOrderInfo {
    /*
     *点击获取prodcut实例并初始化订单信息
     */
    Product *product = [[Product alloc]init];
    product.subject = [_alipayModel.info objectForKey:@"subject"];
    product.body = [_alipayModel.info objectForKey:@"body"];
    product.price = [[_alipayModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
    //    order.partner = PartnerID;
    //    order.seller = SellerID;
    order.partner = [_alipayModel.info objectForKey:@"partner"];
    order.seller = [_alipayModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_alipayModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
    order.productName = product.subject; //商品标题
    order.productDescription = product.body; //商品描述
    order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
    order.notifyURL = [_alipayModel.info objectForKey:@"notify_url"]; //回调URL
    
    return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
    if (result)
    {
        
        if (result.statusCode == 9000)
        {
            /*
             *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
             */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
            id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
            if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
            }
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}


@end
