//
//  ApplyUCookDeliveryViewController.h
//  UCook
//
//  Created by huangrun on 14/10/23.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ApplyUCookDeliveryViewController : GeneralWithBackBtnViewController

@property (copy, nonatomic) NSString *orderIdStr;
@property (copy, nonatomic) NSString *storeIdStr;
@property (weak, nonatomic) IBOutlet UILabel *lowestMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *distenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellphoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *postcodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *shipFeeTF;
- (IBAction)clickSubmitBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *successShipFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *successStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *shipFeeLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
