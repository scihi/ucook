//
//  DealerEvaluateViewController.m
//  UCook
//
//  Created by huangrun on 14/10/24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerEvaluateViewController.h"
#import "NSStringAdditions.h"
#import "UIImageView+AFNetworking.h"

@interface DealerEvaluateViewController () {
    float _starNum;
}

@end

@implementation DealerEvaluateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"评价";
    
    UIBarButtonItem *rigthItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(clickSubmitBtn:)];
    self.navigationItem.rightBarButtonItem = rigthItem;
    
    [_starView setImagesDeselected:@"ico_star_big_dark" partlySelected:@"1.png" fullSelected:@"ico_star_big_light" andDelegate:self];
    [_starView displayRating:0];
    
    [self.goodsPicIV setImageWithURL:[NSURL URLWithString:self.goodsPicUrl] placeholderImage:kDefaultImage];
    self.goodsNameLabel.text = self.goodsNameStr;
    self.goodsNumLabel.text = [NSString stringWithFormat:@"共有%i件商品",self.goodsNumInteger];
    self.goodsPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",self.goodsPriceFloat];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ratingChanged:(float)newRating {
    _starNum = newRating;
}

- (void)clickSubmitBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_evaluateTV.text]) {
        [GlobalSharedClass showAlertView:@"请输入评价内容"];
        return;
    }
    if (_starNum == 0) {
        [GlobalSharedClass showAlertView:@"评分不能为零"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"commentAgency",@"uid":[AccountManager getUid],@"content":self.evaluateTV.text,@"photosExit": @1,@"rating":@(_starNum), @"orderId": self.orderIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self.navigationController popViewControllerAnimated:YES];
            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)clickGoodCheckBtn:(id)sender {
    UIButton *button = sender;
    button.selected = YES;
    self.badCheckBtn.selected = NO;
    
}

- (IBAction)clickBadCheckBtn:(id)sender {
    UIButton *button = sender;
    button.selected = YES;
    self.goodCheckBtn.selected = NO;
}
@end
