//
//  OrderManageViewController.h
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"

@interface OrderManageViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate>

@end
