//
//  DealerOrderDetailViewController.h
//  UCook
//
//  Created by huangrun on 14/10/27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface DealerOrderDetailViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UILabel *goodsTotalPriceLabel;

- (IBAction)clickPhoneBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
- (IBAction)clickLeftBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)clickRightBtn:(id)sender;

@property (copy, nonatomic) NSString *orderIdStr;

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (copy, nonatomic) NSString *imgUrlStr;
@property (copy, nonatomic) NSString *titleStr;
@property (assign, nonatomic) NSInteger amountStr;
@property (assign, nonatomic) CGFloat payFeeFloat;

@end
