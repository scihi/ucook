//
//  OrderManageViewController.m
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "OrderManageViewController.h"
#import "OrderManageListModel.h"
#import "DealerOrderViewController.h"

@interface OrderManageViewController ()
{
    UITableView *_theTableView;
    NSArray *_omlModelArr;
}

@end

@implementation OrderManageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.title = @"订单管理";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestTheListData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView  = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
//    [self requestTheListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _omlModelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"OrderManageCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    OrderManageListModel *omlModel = [_omlModelArr objectAtIndex:indexPath.row];
    cell.textLabel.text = omlModel.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d",omlModel.amount];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
        DealerOrderViewController *doVC = [[DealerOrderViewController alloc]init];
        OrderManageListModel *omlModel = [_omlModelArr objectAtIndex:indexPath.row];
        doVC.statusString = omlModel.status;
        doVC.hidesBottomBarWhenPushed = YES;
    if (indexPath.row == 0) {
        doVC.titleString = @"全部订单";
    } else if (indexPath.row == 1) {
        doVC.titleString = @"待处理";
    } else if (indexPath.row == 2) {
        doVC.titleString = @"已处理";
    }
    
        [self.navigationController pushViewController:doVC animated:YES];
}

- (void)requestTheListData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"orderAgency",
                                 @"action": @"index",
                                 @"uid":[AccountManager getUid]};
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
//        [self stopRequest];
        if (RIGHT)
        {
            NSArray *array = [responseObject objectForKey:@"data"];
            _omlModelArr = [OrderManageListModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

@end
