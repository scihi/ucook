//
//  DealerOrderViewController.h
//  UCook
//
//  Created by huangrun on 14-10-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AlixLibService.h"

#import "CustomIOS7AlertView.h"

@interface DealerOrderViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate,UIActionSheetDelegate, CustomIOS7AlertViewDelegate>

@property (copy, nonatomic) NSString *titleString;
@property (copy, nonatomic) NSString *statusString;

//确认店铺 店铺名称
@property(nonatomic,strong) NSString *storeId;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调

@end
