//
//  DealerEvaluateViewController.h
//  UCook
//
//  Created by huangrun on 14/10/24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "RatingView.h"

@interface DealerEvaluateViewController : GeneralWithBackBtnViewController <RatingViewDelegate>

@property (strong, nonatomic) IBOutlet RatingView *starView;
@property (strong, nonatomic) IBOutlet UITextView *evaluateTV;

@property (copy, nonatomic) NSString *orderIdStr;
@property (weak, nonatomic) IBOutlet UIImageView *goodsPicIV;
@property (weak, nonatomic) IBOutlet UILabel *goodsNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *goodCheckBtn;
@property (weak, nonatomic) IBOutlet UIButton *badCheckBtn;

@property (copy, nonatomic) NSString *goodsPicUrl;
@property (copy, nonatomic) NSString *goodsNameStr;
@property (assign, nonatomic) NSInteger goodsNumInteger;
@property (assign, nonatomic) CGFloat goodsPriceFloat;

- (IBAction)clickGoodCheckBtn:(id)sender;
- (IBAction)clickBadCheckBtn:(id)sender;

@end
