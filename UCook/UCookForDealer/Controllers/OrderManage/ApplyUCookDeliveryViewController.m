//
//  ApplyUCookDeliveryViewController.m
//  UCook
//
//  Created by huangrun on 14/10/23.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ApplyUCookDeliveryViewController.h"
#import "ApplyUCookDeliveryModel.h"
#import "NSStringAdditions.h"
#import "DealerDeliveryModel.h"

@interface ApplyUCookDeliveryViewController () {
    ApplyUCookDeliveryModel *_audModel;
}

@end

@implementation ApplyUCookDeliveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"申请优厨发货";
    
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"deliveryDetail",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"storeId": self.storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _audModel = [ApplyUCookDeliveryModel objectWithKeyValues:dictionary];
            self.shipFeeLabel.hidden = NO;
            self.shipFeeTF.hidden = NO;
            self.successShipFeeLabel.hidden = YES;
            self.successStatusLabel.hidden = YES;
            
            self.lowestMoneyLabel.text = [NSString stringWithFormat:@"%@",_audModel.shipfee];
            self.orderIdLabel.text = self.orderIdStr;
            self.weightLabel.text = [NSString stringWithFormat:@"%@",_audModel.weight];
            self.floorLabel.text = _audModel.floor;
            self.distenceLabel.text = [NSString stringWithFormat:@"%@",_audModel.kilometer];
            self.nameLabel.text = _audModel.receiver_name;
            self.cellphoneLabel.text = _audModel.receiver_mobile;
            self.telLabel.text = _audModel.receiver_phone;
            self.addressLabel.text = _audModel.receiver_address;
            self.postcodeLabel.text = _audModel.receiver_zip;
            self.deliveryAddressLabel.text = [NSString stringWithFormat:@"%@%@",_audModel.area_id,_audModel.address];
            
            if (_audModel.delivery.deliveryStatus == 1 ) {
                self.shipFeeLabel.hidden = YES;
                self.shipFeeTF.hidden = YES;
                self.successShipFeeLabel.hidden = NO;
                self.successStatusLabel.hidden = NO;
                self.successShipFeeLabel.text = [NSString stringWithFormat:@"实付运费：￥%@",self.shipFeeTF.text];
                [self.submitBtn setTitle:@"撤销申请" forState:UIControlStateNormal];
            } else if (_audModel.delivery.deliveryStatus == 0) {
                self.shipFeeLabel.hidden = NO;
                self.shipFeeTF.hidden = NO;
                self.successShipFeeLabel.hidden = YES;
                self.successStatusLabel.hidden = YES;
                [self.submitBtn setTitle:@"提交申请" forState:UIControlStateNormal];
            } else if (_audModel.delivery.deliveryStatus == 3) {
                self.shipFeeLabel.hidden = YES;
                self.shipFeeTF.hidden = YES;
                self.successShipFeeLabel.hidden = NO;
                self.successStatusLabel.hidden = NO;
                self.successStatusLabel.text = @"状态：等待优厨发货";
                self.successShipFeeLabel.text = [NSString stringWithFormat:@"实付运费：￥%d",_audModel.delivery.deliveryFee];
                [self.submitBtn setTitle:@"等待优厨发货" forState:UIControlStateNormal];
                self.submitBtn.backgroundColor = [UIColor lightGrayColor];
            }
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)clickSubmitBtn:(id)sender {
    if (_audModel.delivery.deliveryStatus == 0 ) {
        [self delivery];
    } else if (_audModel.delivery.deliveryStatus == 1 ) {
        [self cancelDelivery];
    }
   
}

- (void)delivery {
    if ([NSString isEmptyOrWhitespace:self.shipFeeTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入实付运费"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"delivery",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"storeId": self.storeIdStr,@"shipFee": self.shipFeeTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            self.shipFeeLabel.hidden = YES;
            self.shipFeeTF.hidden = YES;
            self.successShipFeeLabel.hidden = NO;
            self.successStatusLabel.hidden = NO;
            self.successShipFeeLabel.text = [NSString stringWithFormat:@"实付运费：%@",self.shipFeeTF.text];
            [self.submitBtn setTitle:@"撤销申请" forState:UIControlStateNormal];
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _audModel = [ApplyUCookDeliveryModel objectWithKeyValues:dictionary];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

#pragma mark - 撤销申请优厨发货

- (void)cancelDelivery {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderAgency",@"action": @"deliveryCancel",@"uid":[AccountManager getUid],@"orderId":self.orderIdStr,@"storeId": self.storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            self.shipFeeLabel.hidden = NO;
            self.shipFeeTF.hidden = NO;
            self.successShipFeeLabel.hidden = YES;
            self.successStatusLabel.hidden = YES;
            [self.submitBtn setTitle:@"提交申请" forState:UIControlStateNormal];
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _audModel = [ApplyUCookDeliveryModel objectWithKeyValues:dictionary];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}


@end
