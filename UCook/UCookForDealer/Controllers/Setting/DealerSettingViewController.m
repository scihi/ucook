//
//  DealerSettingViewController.m
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerSettingViewController.h"
#import "DealerModifyPsdViewController.h"
#import "MyMessageViewController.h"
#import "RechargeViewController.h"
#import "RechargeModel.h"
#import "WithdrawViewController.h"
#import "WithdrawWithAccountViewController.h"
#import "MyReportFormsViewController.h"

@interface DealerSettingViewController ()
{
    UITableView *_theTableView;
    NSArray *_theSectionArr;
    NSArray *_theRowArr;
    UISwitch *_theSwitch;
    
    RechargeModel *_rechargeModel;
}
　　　　
@end

@implementation DealerSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.title = @"设置";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _theSectionArr = @[@"切换模式",@"基本设置",@"账户设置",@"经营数据"];
    _theRowArr = @[@[@"立即采购商品"],@[@"图片模式"],@[@"修改密码",@"注销账户",@"我的消息",@"账户充值",@"账户提现"],@[@"经营报表"]];
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME_WITHOUT_TOOLBAR style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    _theSwitch = [[UISwitch alloc]init];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
        _theSwitch.on = YES;
    } else
    {
        _theSwitch.on = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _theSectionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_theRowArr objectAtIndex:section]count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [_theSectionArr objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"DealerSettingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }


    cell.textLabel.text = [[_theRowArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];

    
    
    if (indexPath.section == 1) {
        [_theSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = _theSwitch;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DealerModifyPsdViewController *dmpvc;
    MyMessageViewController *mmvc;
    RechargeViewController *rvc;
    MyReportFormsViewController *mrfvc;
    
    switch (indexPath.section)
    {
        case 0:
            [[NSUserDefaults standardUserDefaults]setObject:@"rest" forKey:NS_DEALER_LAST_PLACE];//餐厅模块
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSNotificationCenter defaultCenter]postNotificationName:NS_GOTO_REST_MODULE object:nil];
            
            break;
            
        case 2:
            switch (indexPath.row)
        {
                case 0:
                    dmpvc = [[DealerModifyPsdViewController alloc]initWithNibName:@"DealerModifyPsdViewController" bundle:nil];
                    dmpvc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:dmpvc animated:YES];
                    
                    break;
                    
                case 1:
                    [GlobalSharedClass showAlertView:@"确定注销登录?" delegate:self];
                    break;
                    
                case 2:
                    mmvc = [[MyMessageViewController alloc]init];
                    mmvc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:mmvc animated:YES];
                    
                    break;
                    
                case 3:
                    rvc = [[RechargeViewController alloc]initWithNibName:@"DealerRechargeViewController" bundle:nil];
                    rvc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:rvc animated:YES];
                    break;
                    
                case 4:
                    [self getTheInitData];
                    
                    break;
                    
                    
                default:
                    break;
            
            }
            
            break;
            
        case 3:
            mrfvc = [[MyReportFormsViewController alloc]init];
            mrfvc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mrfvc animated:YES];
            
            break;
            
        default:
            break;
    }
    
    
}

- (void)switchAction:(UISwitch *)theSwith
{
    if (theSwith.on)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:GLOBLE_IMAGE_TOGGLE];
        [[NSUserDefaults standardUserDefaults]synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:GLOBLE_IMAGE_TOGGLE];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:GLOBLE_IMAGE_TOGGLE object:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self logout];
    } else {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

- (void)logout {
    [AccountManager logout];
    TheTabBarViewController *ttbvc = [[TheTabBarViewController alloc]init];
    self.view.window.rootViewController = ttbvc;
}

#pragma mark - 提现
- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"ucookb",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self stopRequest];
        if (RIGHT)
        {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _rechargeModel = [RechargeModel objectWithKeyValues:dictionary];
            
            if (!_rechargeModel.account)
            {
                WithdrawViewController *withdrawVC = [[WithdrawViewController alloc]initWithNibName:@"WithdrawViewController" bundle:nil];
                withdrawVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:withdrawVC animated:YES];
            }
            else
            {
                WithdrawWithAccountViewController *withdrawWithAccountVC = [[WithdrawWithAccountViewController alloc]initWithNibName:@"WithdrawWithAccountViewController" bundle:nil];
                withdrawWithAccountVC.theDataDic = _rechargeModel.account;
                withdrawWithAccountVC.isCashInteger = [_rechargeModel.isCash integerValue];
                withdrawWithAccountVC.ucookbAccountStr = [NSString stringWithFormat:@"%.2f元",[_rechargeModel.ucookbAccount floatValue]];
                withdrawWithAccountVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:withdrawWithAccountVC animated:YES];
            }
            
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


@end
