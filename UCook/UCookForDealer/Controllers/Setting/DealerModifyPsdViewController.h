//
//  DealerModifyPsdViewController.h
//  UCook
//
//  Created by huangrun on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealerModifyPsdViewController : GeneralWithBackBtnViewController

@property (strong, nonatomic) IBOutlet UITextField *cellPhoneNumTF;
- (IBAction)clickVeriCodeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *veriCodeTF;
- (IBAction)clickNextStepBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *veriCodeBtn;

@end
