//
//  DealerSettingViewController.h
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"

@interface DealerSettingViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@end
