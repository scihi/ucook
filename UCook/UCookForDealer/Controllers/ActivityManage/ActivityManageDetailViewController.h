//
//  ActivityManageDetailViewController.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "ModifyActivityRecomViewController.h"

@interface ActivityManageDetailViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, ModifyActivityRecomDelegate>

@property (copy, nonatomic) NSString *iDStr;

@end
