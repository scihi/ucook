//
//  ActivityManageDetailViewController.m
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ActivityManageDetailViewController.h"
#import "ActivityManageDetailModel.h"
#import "UIImageView+AFNetworking.h"
#import "ActivityGoodViewController.h"

@interface ActivityManageDetailViewController () {
    UITableView *_theTableView;
    ActivityManageDetailModel *_amdModel;
    NSArray *_rowTitleArr;
}

@end

@implementation ActivityManageDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"活动管理详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //观察图片模式
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configThePicType:) name:GLOBLE_IMAGE_TOGGLE object:nil];
    
    _rowTitleArr = @[@"名称",@"活动期限",@"参与人数",@"限购数量",@"归属店铺",@"活动简介"];
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 60)];
    UIButton *footerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    footerButton.frame = CGRectMake(0, 8, BOUNDS.size.width, 44);
    [footerButton setTitle:@"查看活动商品" forState:UIControlStateNormal];
    [footerButton setBackgroundImage:[UIImage imageNamed:@"title_bg_44"] forState:UIControlStateNormal];
    [footerButton addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerButton];
    _theTableView.tableFooterView = footerView;

    
    [self requestTheListData];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 7;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
             if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
            NSString *imgUrlStr = _amdModel.image;
            UIImageView *cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 90)];
            [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            [cell.contentView addSubview:cellIV];
             }
        } else {
        cell.textLabel.text = [_rowTitleArr objectAtIndex:indexPath.row - 1];
            
            switch (indexPath.row) {
                case 1:
                    cell.detailTextLabel.text = _amdModel.name;
                    break;
                case 2:
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@至%@",_amdModel.startTime,_amdModel.endTime];
                    break;
                case 3:
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@人",_amdModel.nop];
                    break;
                case 4:
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@件/人",_amdModel.restriction];
                    break;
                case 5:
                    cell.detailTextLabel.text = _amdModel.store;
                    break;
                case 6:
                    cell.detailTextLabel.text = _amdModel.intro;
                    break;

                default:
                    break;
            }
        }
        
    } else if (indexPath.section == 1) {
        cell.textLabel.text = @"店铺推荐位";
        cell.detailTextLabel.text = _amdModel.positionName;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
             if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
            return 90;
             } else {
                 return 0;
             }
        } else if (indexPath.row == 6) {
            return 80;
        }
    }
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"活动详情";
    } else {
        return @"推荐位信息";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        ModifyActivityRecomViewController *marVC = [[ModifyActivityRecomViewController alloc]init];
        marVC.activityIDStr = _amdModel.id;
        marVC.delegate = self;
        [self.navigationController pushViewController:marVC animated:YES];
    }
}

- (void)requestTheListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"promotionAgency",@"action": @"detail",@"id":self.iDStr};
       [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
               [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _amdModel = [ActivityManageDetailModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)clickFooterBtn:(id)sender {
    ActivityGoodViewController *agVC = [[ActivityGoodViewController alloc]init];
    agVC.activityIDStr = _amdModel.id;
    [self.navigationController pushViewController:agVC animated:YES];
}

- (void)activityRecomDidModify:(ModifyActivityRecomViewController *)modifyActivityRecomVC {
    [self requestTheListData];
}

- (void)configThePicType:(NSNotification *)notification {
    [_theTableView reloadData];
}

@end
