//
//  ActivityGoodViewController.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ActivityGoodViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (copy, nonatomic) NSString *activityIDStr;

@end
