//
//  ActivityGoodViewController.m
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ActivityGoodViewController.h"
#import "ActivityGoodModel.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"

@interface ActivityGoodViewController () {
    UITableView *_theTableView;
    NSArray *_agModelArr;
}

@end

@implementation ActivityGoodViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"活动商品";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self requestTheListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _agModelArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ModifyActivityRecomCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ActivityGoodCell" owner:self options:nil]firstObject];
        } else {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ActivityGoodWithoutPicCell" owner:self options:nil]firstObject];
        }
    }
    
    ActivityGoodModel *agModel = [_agModelArr objectAtIndex:indexPath.row];
    
    UIImageView *headerIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = agModel.image;
    [headerIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = agModel.name;
    
    UILabel *leftLabel = (UILabel *)[cell viewWithTag:103];
    leftLabel.text = [NSString stringWithFormat:@"剩余:%@",agModel.sales];
    
    //规格
    UILabel *specLabel = (UILabel *)[cell viewWithTag:104];
    specLabel.text = [NSString stringWithFormat:@"规格:%@",agModel.spec];
    
    UILabel *unitLabel = (UILabel *)[cell viewWithTag:105];
    unitLabel.text = [NSString stringWithFormat:@"单位:%@",agModel.unit];
    
    UILabel *introLabel = (UILabel *)[cell viewWithTag:106];
    introLabel.text = [agModel.desc isEqualToString:@""]?@"暂无介绍":agModel.desc;
    
    UILabel *discountPriceLabel = (UILabel *)[cell viewWithTag:107];
    discountPriceLabel.text = [NSString stringWithFormat:@"￥%@",agModel.discountPrice];
    
    StrikeThroughLabel *priceLabel = (StrikeThroughLabel *)[cell viewWithTag:108];
    priceLabel.text = [NSString stringWithFormat:@"￥%@",agModel.price];
    priceLabel.strikeThroughEnabled = YES;
    
    UILabel *maxSaleLabel = (UILabel *)[cell viewWithTag:109];
    maxSaleLabel.text = [NSString stringWithFormat:@"最大销售量:%@",agModel.haveSales];
    
    return cell;
}


- (void)requestTheListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"promotionAgency",@"action": @"products",@"id":self.activityIDStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _agModelArr = [ActivityGoodModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


@end
