//
//  ModifyActivityRecomViewController.m
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ModifyActivityRecomViewController.h"
#import "ModifyActivityRecomModel.h"

@interface ModifyActivityRecomViewController () {
    UITableView *_theTableView;
    NSArray *_marArr;
}

@end

@implementation ModifyActivityRecomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"修改活动推荐位";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self requestTheListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _marArr.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ModifyActivityRecomCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    ModifyActivityRecomModel *marModel = [_marArr objectAtIndex:indexPath.row];
    
    cell.textLabel.text = marModel.name;
       
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *positionIDStr = [[_marArr objectAtIndex:indexPath.row]id];
    [self modifyActivityRecom:positionIDStr];
}

- (void)requestTheListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"promotionAgency",@"action": @"position"};
        [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _marArr = [ModifyActivityRecomModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)modifyActivityRecom:(NSString *)positionIDStr {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"promotionAgency",@"action": @"update",@"position_id":positionIDStr,@"id":self.activityIDStr,@"uid":[AccountManager getUid]};
        [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [self stopRequest];
        if (RIGHT) {
            if (self.delegate) {
                [self.delegate activityRecomDidModify:self];
            }
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

@end
