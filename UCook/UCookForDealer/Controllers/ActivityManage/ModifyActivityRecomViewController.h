//
//  ModifyActivityRecomViewController.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class ModifyActivityRecomViewController;

@protocol ModifyActivityRecomDelegate <NSObject>

- (void)activityRecomDidModify:(ModifyActivityRecomViewController *)modifyActivityRecomVC;

@end

@interface ModifyActivityRecomViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (copy, nonatomic) NSString *activityIDStr;
@property (assign, nonatomic) id <ModifyActivityRecomDelegate> delegate;

@end
