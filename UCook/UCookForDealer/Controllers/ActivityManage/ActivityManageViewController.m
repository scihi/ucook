//
//  ActivityManageViewController.m
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ActivityManageViewController.h"
#import "activitymanageListModel.h"
#import "UIImageView+AFNetworking.h"
#import "ActivityManageDetailViewController.h"

@interface ActivityManageViewController () {
    UITableView *_theTableView;
    NSArray *_amlModelArr;
    
}

@end

@implementation ActivityManageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (self.storeIdStr == nil) {
            self.title = @"活动管理";
        }else{
            self.title = self.nameStr;
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //观察图片模式
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configThePicType:) name:GLOBLE_IMAGE_TOGGLE object:nil];
    
    if (self.storeIdStr == nil) {
        _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME_WITHOUT_TOOLBAR style:UITableViewStylePlain];
    }else{
        _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    }
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self requestTheListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _amlModelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ActivityManageCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
        cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"ActivityManageCell" owner:self options:nil]firstObject];
        } else {
            cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"ActivityManageWithoutPicCell" owner:self options:nil]firstObject];
        }
    }
    
    ActivityManageListModel *amlModel = [_amlModelArr objectAtIndex:indexPath.row];
    
    UIImageView *headIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = amlModel.image;
    [headIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = amlModel.name;
    
    UILabel *adPositionLabel = (UILabel *)[cell viewWithTag:104];
    adPositionLabel.text = [NSString stringWithFormat:@"广告位置:%@",amlModel.positionName];
    
    UILabel *activityIntroLabel = (UILabel *)[cell viewWithTag:105];
    
    NSString *activityIntroStr = [amlModel.intro isEqualToString:@""]?@"暂无活动简介":[NSString stringWithFormat:@"活动简介:%@",amlModel.intro];
    activityIntroLabel.text = activityIntroStr;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ActivityManageListModel *amlModel = [_amlModelArr objectAtIndex:indexPath.row];
    ActivityManageDetailViewController *amdVC = [[ActivityManageDetailViewController alloc]init];
    amdVC.iDStr = amlModel.id;
    amdVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:amdVC animated:YES];
}

- (void)requestTheListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameter = @{@"key": APP_KEY,@"controller": @"promotionAgency",@"action": @"index",@"uid":[AccountManager getUid]};
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:parameter];
    if (self.storeIdStr != nil) {
        [parameters setObject:self.storeIdStr forKey:@"storeId"];
    }
    //    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _amlModelArr = [ActivityManageListModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configThePicType:(NSNotification *)notification {
    [_theTableView reloadData];
}


@end
