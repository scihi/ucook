//
//  RootViewController.h
//  NewProject
//
//  Created by 学鸿 张 on 13-11-29.
//  Copyright (c) 2013年 Steven. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GeneralViewController.h"
//#import "ZBarSDK.h"

#import "VPImageCropperViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>


#define ORIGINAL_MAX_WIDTH 640.0f


@protocol ResultDelegate <NSObject>

@optional
- (void)resultDidOutput:(NSString *)result;

- (void)getResult: (NSString *)text;

- (void)getResult: (NSString *)text andTag:(NSInteger)tag;

@end//huangrun添加

@interface AddGoodsViewController : GeneralViewController<UIImagePickerControllerDelegate>
{
    int num;
    BOOL upOrdown;
    NSTimer * timer;
}
@property (nonatomic, retain) UIImageView * line;


@end
