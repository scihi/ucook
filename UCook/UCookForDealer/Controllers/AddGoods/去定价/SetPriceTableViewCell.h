//
//  SetPriceTableViewCell.h
//  UCook
//
//  Created by scihi on 14/11/7.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGoodsModel.h"

@protocol SetPriceDelegate <NSObject>

@optional

-(void)setPriceGoods:(int)index;

@end

@interface SetPriceTableViewCell : UITableViewCell

@property(nonatomic,strong) AddGoodsModel *goodsModeldataSource;


-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index;


//委托代理
@property(nonatomic,assign)id<SetPriceDelegate>delegate;

@end
