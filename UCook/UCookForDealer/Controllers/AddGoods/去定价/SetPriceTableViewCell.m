//
//  SetPriceTableViewCell.m
//  UCook
//
//  Created by scihi on 14/11/7.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SetPriceTableViewCell.h"
#import "UIImageView+WebCache.h"

@interface SetPriceTableViewCell ()
{
    int Tag;
}

@property(nonatomic,strong) UIImageView *youchuImageView;
@property(nonatomic,strong) UILabel *goodsNameLabel;
@property(nonatomic,strong) UILabel *goodsSizeLabel;
@property(nonatomic,strong) UILabel *unitLabel;

@end
@implementation SetPriceTableViewCell

//初始化的时候返回cell的tag值
-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier
                  getCellTag:(int)index
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        Tag = index;
        [self initContentView];
    }
    return self;
    
}
-(void)setGoodsModeldataSource:(AddGoodsModel *)goodsModeldataSource
{
    _goodsModeldataSource = goodsModeldataSource;
    
    //产品图片刷新
    NSString *imageStr = [NSString stringWithFormat:@"%@",
                          [_goodsModeldataSource images]];
    [_youchuImageView setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"default_pic.png"]];
    //产品名称
    _goodsNameLabel.text =[NSString stringWithFormat:@"(%@)%@",
                           [_goodsModeldataSource split],[_goodsModeldataSource name]];
    
    //产品规格
    _goodsSizeLabel.text = [NSString stringWithFormat:@"%@%@",
                            @"规格:",[_goodsModeldataSource spec]];
    //产品单位
    _unitLabel.text = [NSString stringWithFormat:@"%@%@",
                       @"单位:",[_goodsModeldataSource unit]];
}

-(void)initContentView
{
    self.backgroundColor = [UIColor whiteColor];
    
    //产品图片
    _youchuImageView = [[UIImageView alloc]init] ;
    _youchuImageView.frame = AUTOLayer
    (20, 15, 70, 70,1);
    _youchuImageView.backgroundColor = [UIColor grayColor];
    [self addSubview:_youchuImageView];
    
    //产品名称
    _goodsNameLabel = [[UILabel alloc]init];
    _goodsNameLabel.frame = AUTOLayer(110, 25, 140, 20,1);
    _goodsNameLabel.backgroundColor = [UIColor whiteColor];
    _goodsNameLabel.textAlignment = NSTextAlignmentLeft;
    _goodsNameLabel.font = [UIFont systemFontOfSize:_goodsNameLabel.frame.size.height*0.75];
    [self addSubview:_goodsNameLabel];
    
    //产品规格
    _goodsSizeLabel = [[UILabel alloc]init];
    _goodsSizeLabel.frame = AUTOLayer(110, 50, 120, 15,1);
    _goodsSizeLabel.backgroundColor = [UIColor whiteColor];
    _goodsSizeLabel.textAlignment = NSTextAlignmentLeft;
    _goodsSizeLabel.font = [UIFont systemFontOfSize:_goodsSizeLabel.frame.size.height*0.75];
    [self addSubview:_goodsSizeLabel];
    
    //单位
    _unitLabel = [[UILabel alloc]init];
    _unitLabel.frame = AUTOLayer(240, 50, 70, 15,1);
    _unitLabel.backgroundColor = [UIColor whiteColor];
    _unitLabel.textAlignment = NSTextAlignmentLeft;
    _unitLabel.font = [UIFont systemFontOfSize:_unitLabel.frame.size.height*0.75];
    [self addSubview:_unitLabel];
    
}


-(void)buttonPressed:(UIButton *)sender
{
    
    if(_delegate && [_delegate respondsToSelector:@selector(setPriceGoods:)])
    {
        [_delegate setPriceGoods:sender.tag];
    }
}













- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
