//
//  SetPriceViewController.m
//  UCook
//
//  Created by scihi on 14/11/7.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SetPriceViewController.h"
#import "SetPriceTableViewCell.h"
#import "AddGoodsModel.h"
#import "ScanGoodsDetailViewController.h"

@interface SetPriceViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    //读取单例中保存的商品数据信息
    NSMutableArray *dataArray;
     StoreDataUser *storeData;
}

@property(nonatomic,strong) UITableView *setPriceTableView;

@end

@implementation SetPriceViewController

-(instancetype)init
{
    if(self == [super init])
    {
        self.title = @"待定价产品列表";
    }
    return self;
}

-(void)initData
{
    //先将所有数据读取出来
    dataArray = [NSMutableArray new];
    storeData = [StoreDataUser ShareStoreData];
    for(int i=0;i<storeData.numOfGoodsArray.count;++i)
    {
        AddGoodsModel *model = storeData.goodsArray[ [storeData.numOfGoodsArray[i] intValue]];
        [dataArray addObject:model];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initializeUserInterFace];
}

-(void)initializeUserInterFace
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    //布局表单
    _setPriceTableView = [[UITableView alloc]init];
    _setPriceTableView.frame = AUTOLayer(0, 0, 320, 524, 1);
    _setPriceTableView.dataSource = self;
    _setPriceTableView.delegate = self;
    [self.view addSubview:_setPriceTableView];
    
    [_setPriceTableView reloadData];
}

#pragma UITableView 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *cellStr = @"priceCell";
    SetPriceTableViewCell *priceCell = [tableView
                        dequeueReusableHeaderFooterViewWithIdentifier:cellStr];
    
    if(!priceCell)
    {
        priceCell = [[SetPriceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr getCellTag:indexPath.row];
    }
    
    [priceCell setGoodsModeldataSource:dataArray[indexPath.row]];
    //cell点击不变色
    priceCell.selectionStyle = UITableViewCellEditingStyleNone;
    return  priceCell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    
    ScanGoodsDetailViewController *sgoodsDeVc = [[ScanGoodsDetailViewController alloc]initWidthModel:dataArray[indexPath.row]];
    sgoodsDeVc.ISONCE = NO;
    [self.navigationController pushViewController:sgoodsDeVc animated:YES];
}


//控制器不存在的时候  对数据不做保存  情况数组
-(void)dealloc
{
    [storeData.numOfGoodsArray removeAllObjects];
}



















@end
