//
//  ScanGoodsViewController.m
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ScanGoodsViewController.h"
#import "ScanGoodsTableViewCell.h"
#import "ScanGoodsDetailViewController.h"

@interface ScanGoodsViewController ()<UITableViewDataSource,
UITableViewDelegate,ScanGoodsDelegate>
{
    //是否有按钮被选中
    BOOL isChoice;
    
    //是否为单个添加
    BOOL _isOnce;
}

//接受网络请求回来的数据
@property(nonatomic,strong) NSArray *addGoodsArray;
//当前表单
@property(nonatomic,strong) UITableView *scanGoodsTableView;
//扫描返回的二维码 作为网络请求的参数之一
@property(nonatomic,strong) NSString *barcodeString;
//添加商品中临时按钮
@property(nonatomic,strong) UIButton *choiceButton;
//添加商品临时按钮的所有选中按钮编号
@property(nonatomic,strong) NSMutableArray *choiceBtnArr;


@end

@implementation ScanGoodsViewController

-(id)initWithString:(NSString *)string
            getBool:(BOOL)isOnce
{
    if(self == [super init])
    {
        self.title = @"选择需要添加的商品";
        _barcodeString = string;
        _isOnce = isOnce;
    }
    return self;
}

-(void)initData
{
    isChoice = NO;
    _choiceBtnArr = [NSMutableArray new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initializeUserInterFace];
}

-(void)initializeUserInterFace
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    //导航栏右侧按钮
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(barButtonPressed:)];
    self.navigationItem.rightBarButtonItem = barItem;
    
    //添加表单
    _scanGoodsTableView = [[UITableView alloc]initWithFrame:
                           AUTOLayer(0, 0, 320, self.view.bounds.size.height,1)];
    
    _scanGoodsTableView.delegate = self;
    _scanGoodsTableView.dataSource = self;
    [self.view addSubview:_scanGoodsTableView];
    
    [self netWorking];
}

-(void)netWorking
{
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"productAgency",
                                 @"action": @"scan",
                                 @"barcode":_barcodeString,
                                 @"code":@11,
                                 @"total":@2};
    
    [CRequestManager requestWith:parameters method:BASIC_URL
        requestSucess:^(id object)
     {
         if([object[@"msg"] isEqualToString:@"没有记录"])
         {
             [GlobalSharedClass showAlertView:@"没有记录"];
             _addGoodsArray = nil;
            [_scanGoodsTableView reloadData];
         }
         
         else
         {
             _addGoodsArray = [NSArray setMyValue2:object[@"data"]
                                     className:@"AddGoodsModel"];
         [_scanGoodsTableView reloadData];
         }
     }
        requestFailure:^
     {
         NSLog(@"访问失败");
     }];
    
}




#pragma UITableView


//返回分组
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


//返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_addGoodsArray count];
}

//返回行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect rect = SCREENBounds;
    double h = rect.size.height;
    return (CGFloat)AUTOSuperHeight(100.0/568.0, h);
}

//返回cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = @"scanGoodsCell";
    ScanGoodsTableViewCell *scanGoodsCell = [tableView dequeueReusableCellWithIdentifier:string];
    if(!scanGoodsCell)
    {
        scanGoodsCell = [[ScanGoodsTableViewCell alloc]initWithStyle:
                         UITableViewCellStyleDefault reuseIdentifier:string getCellTag:indexPath.row+100];
    }
    //cell点击不会响应
    scanGoodsCell.delegate = self;
    scanGoodsCell.selectionStyle = UITableViewCellEditingStyleNone;
    [scanGoodsCell setGoodsModeldataSource:_addGoodsArray[indexPath.row]];
    return scanGoodsCell;
}

//击中测试
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//选中商品后完成按钮 根据是否为单个添加 或者为批量添加
-(void)barButtonPressed:(UIBarButtonItem *)sender
{
    //如果是单个添加
    if(_isOnce)
    {
        //如果没有选中商品
        if(isChoice == NO)
        {
            [GlobalSharedClass showAlertView:@"您还没有选择商品"];
            return;
        }
        
        //根据选中的按钮读取数组的数据
        ScanGoodsDetailViewController *sgoodsDeVc = [[ScanGoodsDetailViewController alloc]initWidthModel:_addGoodsArray[_choiceButton.tag-50]];
        sgoodsDeVc.ISONCE = YES;
        [self.navigationController pushViewController:sgoodsDeVc animated:YES];
    }
    //如果是批量添加
    else
    {
        NSLog(@"返回");
        
        //降选中的产品数据存入单例
        //直接返回主页面
        StoreDataUser *storeData = [StoreDataUser ShareStoreData];
        storeData.goodsArray = _addGoodsArray;
        for(int i=0;i<_choiceBtnArr.count;++i)
        {
            [storeData.numOfGoodsArray addObject:[NSNumber numberWithInt:[_choiceBtnArr[i] tag]-50]];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//响应cell 的代理方法
//按钮的tag值从100开始
-(void)choiceGoods:(int)index
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:index];
    //单个添加  只能选一个
    if(_isOnce)
    {
        //默认没有选中
        if(isChoice ==NO)
        {
            btn.selected = YES;
            _choiceButton = btn;
            isChoice = YES;
        }
        else
        {
            _choiceButton.selected = NO;
            btn.selected = YES;
            _choiceButton = btn;
        }
    }
    //批量添加  可以多选
    else
    {
        btn.selected =!btn.selected;
        if(btn.selected)
        {
            [_choiceBtnArr addObject:btn];
        }
        else
        {
            [_choiceBtnArr removeObject:btn];
        }
    }
}






//

@end
