//
//  ScanGoodsViewController.h
//  UCook
//
//  Created by scihi on 14/11/3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanGoodsViewController : UIViewController

-(id)initWithString:(NSString *)string
            getBool:(BOOL)isOnce;

@end
