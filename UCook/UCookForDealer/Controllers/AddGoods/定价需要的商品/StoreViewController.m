//
//  StoreViewController.m
//  UCook
//
//  Created by scihi on 14/11/5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreViewController.h"
#import "AccountManager.h"

@interface StoreViewController ()<UITableViewDataSource,UITableViewDelegate>
{
}
@property(nonatomic,strong) UITableView *storeTableView;
@property(nonatomic,strong) NSString *ID;
@property(nonatomic,strong) NSArray *addStoresArray;

@property(nonatomic,strong) NSMutableArray *storeArray;

@end

@implementation StoreViewController

-(instancetype)init
{
    if(self == [super init])
    {
        self.title = @"选择店铺";
    }
    return  self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initializeUserInterFace];
}

-(void)initData
{
    _storeArray = [NSMutableArray new];
}
-(void)initializeUserInterFace
{
    _storeTableView = [[UITableView alloc]init];
    _storeTableView.frame = AUTOLayer(0, 0, 320, 508, 1);
    _storeTableView.dataSource = self;
    _storeTableView.delegate = self;
    //隐藏分割线
    [_storeTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview: _storeTableView];
    
    [self netWorking];
}


-(void)netWorking
{
    _ID = [AccountManager getUid];
    NSDictionary *parameters = @{@"key":APP_KEY,
                                 @"controller":@"storeAgency",
                                 @"action":@"index",
                                 @"uid":_ID};
    
    [CRequestManager requestWith:parameters method:BASIC_URL
                   requestSucess:^(id object)
     {
         _addStoresArray = [NSArray setMyValue2:object[@"data"]
                                     className:@"StoreModel"];
         
        StoreDataUser *dataUser = [StoreDataUser ShareStoreData];

         
         for(int i=0;i<_addStoresArray.count;++i)
         {
             StoreModel *stMod  =  _addStoresArray[i];
             [_storeArray addObject:stMod.name];

      
             if(dataUser.storeIdArray.count<_addStoresArray.count)
             {
               [dataUser.storeIdArray addObject:stMod.id];
               [dataUser.nameArray addObject:stMod.name];
             }
             
         }
         
         [_storeTableView reloadData];
         
     }
                  requestFailure:^
     {
         NSLog(@"访问失败");
     }];
    
}

#pragma UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _storeArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellStr = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellStr];
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr];
    }
    cell.selected = NO;
    cell.textLabel.text = _storeArray[indexPath.row];
    UIButton *back = [UIButton buttonWithType:UIButtonTypeSystem];
    back.frame = AUTOLayer(-1, 0, 322, cell.frame.size.height, 1);
    back.layer.borderColor = COLOR(150, 150, 150, 1).CGColor ;
    back.layer.borderWidth = indexPath.row%2==0?1:0;
    back.tag = indexPath.row +20;
    [back addTarget:self action:@selector(popToPreviousVc:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:back];
    return  cell;
}

//商品的按钮tag值从 20 到 24
-(void)popToPreviousVc:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    //讲店铺信息用通知传递给上层
    NSString *storeText = _storeArray[sender.tag-20];
    
    //发送通知
    // 2.通知
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetSoreStr" object:nil userInfo:@{@"Store":storeText}];
    
}











@end
