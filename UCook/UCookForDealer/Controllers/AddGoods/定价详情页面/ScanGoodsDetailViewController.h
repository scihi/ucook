//
//  ScanGoodsDetailViewController.h
//  UCook
//
//  Created by scihi on 14/11/4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGoodsModel.h"
@interface ScanGoodsDetailViewController : UIViewController

@property(nonatomic,assign) BOOL ISONCE;
-(instancetype)initWidthModel:(AddGoodsModel *)addGoodsModel;


@end
