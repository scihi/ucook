//
//  ScanGoodsDetailViewController.m
//  UCook
//
//  Created by scihi on 14/11/4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ScanGoodsDetailViewController.h"
#import "ScanGoodsDetailTableViewCell.h"
#import "AddGoodsViewController.h"
#import "StoreViewController.h"
#import "AccountManager.h"
#import "GeneralViewController.h"

#import "StoreModel.h"
@interface ScanGoodsDetailViewController ()
<UITableViewDataSource,UITableViewDelegate,ScanGoodsDetailDelegate>
{
    //新建一个按钮覆盖tabbar
    UIButton *tabBarButton ;
    //商店数量
    int numOfStore;
    
    //商品所属店铺的编号
    int numOfButton;
    
    //存储cell的输入框
    UITextField *getStr;
    
    //存储店铺 和 价格 的数组
    NSMutableArray *storeArray;
    NSMutableArray *priceArray;
    //获取cell中的输入框 并判断
    UITextField *getCellTextField;
    //判断所存在的商品有没有信息
    BOOL haveMsg;
    
}
@property(nonatomic,strong) UITableView *goodsDetailTableView;
@property(nonatomic,strong) AddGoodsModel *model;
//经销商ID
@property(nonatomic,strong) NSString *ID;

@end

@implementation ScanGoodsDetailViewController

-(instancetype)initWidthModel:(AddGoodsModel *)addGoodsModel;
{
    if(self == [super init])
    {

        _model = addGoodsModel;
        self.title = @"待定产品详情";
    }
    return  self;
}

-(void)initData
{
    numOfStore = 1;
    numOfButton = 0;
    
    storeArray = [NSMutableArray new];
    [storeArray addObject:@"请选择产品所属店铺"];
    
    priceArray = [NSMutableArray new];
    [priceArray addObject:@"请填写产品价格"];
    haveMsg = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self initData];
    [self initUserInterFace];
    

}

-(void)initUserInterFace
{
    
    //重写导航栏左侧按钮
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(barButtonPressed:)];
    leftBtn.tag = 100;
    self.navigationItem.leftBarButtonItem = leftBtn;
    
    //导航栏右侧按钮
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(barButtonPressed:)];
    rightBtn.tag = 101;
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    //添加表单
    _goodsDetailTableView = [[UITableView alloc]init];
    _goodsDetailTableView.frame = AUTOLayer(0, 0, 320, 504, 1);
    _goodsDetailTableView.delegate  = self;
    _goodsDetailTableView.dataSource = self;
    
    //隐藏分割线
    [_goodsDetailTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:_goodsDetailTableView];

    [_goodsDetailTableView reloadData];
}



#pragma UITableView

//返回分组
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//返回行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 11+numOfStore;
}

//返回行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect rect = SCREENBounds;
    int index  = indexPath.row;
    if(index == 0 || index == 9)
    {
        return  AUTOSuperHeight(25.0/568, rect.size.height);
    }
    else if(index>0 && index<9)
    {
        return   AUTOSuperHeight(30.0/568, rect.size.height);
    }
    else if(index ==10)
    {
        return   AUTOSuperHeight(80.0/568, rect.size.height);
    }
    else
    {
        return   AUTOSuperHeight(150.0/568, rect.size.height);
    }
}

//返回cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = @"scanGoodsDetailCell";
    ScanGoodsDetailTableViewCell *scanGoodsDetailCell = [tableView dequeueReusableCellWithIdentifier:string];
    
    if(numOfStore>1)
    {
        scanGoodsDetailCell = [[ScanGoodsDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string getCellTag:indexPath.row getData:_model getStoreArray:storeArray getPriceArray:priceArray];
        scanGoodsDetailCell.isOnce = _ISONCE;
        scanGoodsDetailCell.delegate = self;
        scanGoodsDetailCell.selectionStyle = UITableViewCellEditingStyleNone;
        return scanGoodsDetailCell;
    }
    if(!scanGoodsDetailCell)
    {
        scanGoodsDetailCell = [[ScanGoodsDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string getCellTag:indexPath.row getData:_model getStoreArray:storeArray getPriceArray:priceArray];
        
        scanGoodsDetailCell.isOnce = _ISONCE;
        scanGoodsDetailCell.delegate = self;
    }
    //cell点击不会变色
    //添加箭头提示
    if(indexPath.row == 10)
    {
        scanGoodsDetailCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    scanGoodsDetailCell.selectionStyle = UITableViewCellEditingStyleNone;

    return scanGoodsDetailCell;
}

//击中测试
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}

#pragma  UIBarButton

//返回按钮 和 完成按钮
-(void)barButtonPressed:(UIBarButtonItem *)sender
{
    if(sender.tag == 100)
    {
        //如果是批量添加 返回上一层
        if(!_ISONCE)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
             //遍历导航控制器  直接返回到需要返回的视图控制器
            for (UIViewController *controller in self.navigationController.viewControllers)
            {
                
                if ([controller isKindOfClass:[AddGoodsViewController class]])
                {
                    [self.navigationController popToViewController:controller animated:YES];
                }
            }
        }
    }
    
    //导航栏右侧完成按钮  将商品添加到各个店铺中去
    //点击完成按钮的时候对商品的信息填写要进行检查
    if(sender.tag == 101)
    {
        //判断用户填写的心是否完整
        for(int i=0;i<priceArray.count;++i)
        {
            if([priceArray[i] isEqualToString:@"请填写产品价格"]||
               [storeArray[i] isEqualToString:@"请选择产品所属店铺"])
            {
                [GlobalSharedClass showAlertView:@"请讲信息填写完整"];
                haveMsg = NO;
                break;
            }
        }
        if(haveMsg)
        {
            priceArray[getCellTextField.tag-611] = getCellTextField.text;
            [self netWorking];
        }
        haveMsg = YES;
    }
}

//完成对商品的添加 添加到各个商铺中去
-(void)netWorking
{

    //经销商ID
    _ID = [AccountManager getUid];
    //产品ID
    NSString *goodsId = _model.id;
    
    //商店ID 跟商店数组的名称对比  存在的话 就读取id
    //整个商店的信息包括 商店id  产品定价
  
    NSMutableArray *Array = [NSMutableArray new];
    StoreDataUser *storeData = [StoreDataUser ShareStoreData];
    
    for (int i=0; i<storeArray.count; ++i)
    {
        for(int j=0;j<storeData.storeIdArray.count;++j)
        {
            if([storeArray[i] isEqualToString:storeData.nameArray[j]])
            {
                NSMutableDictionary *storeDic = [NSMutableDictionary new];
                //该店铺id
                int sid = [storeData.storeIdArray[j] intValue];
                [storeDic setObject:[NSNumber numberWithFloat:sid] forKey:@"storeId"];
                
                //该店铺中此商品的价格
                float pri = [priceArray[i] floatValue];
                [storeDic setObject:[NSNumber numberWithFloat:pri] forKey:@"price"];
                [Array addObject:storeDic];
                break;
            }
        }
    }
    
    //将店铺的数组转换成json格式 做参数传递
    GeneralViewController *general = [[GeneralViewController alloc]init];
    NSString *parametStr = [general arrayToJson:Array];

    NSDictionary *parameters = @{@"key":APP_KEY,
                                 @"controller":@"productAgency",
                                 @"action":@"addnew",
                                 @"uid":_ID,
                                 @"itemId":goodsId,
                                 @"stores":parametStr,
                                 @"recommend":@1};
    
    
    
     [CRequestManager requestWith:parameters method:BASIC_URL
                    requestSucess:^(id object)
    {
                        NSLog(@"%@",object);
                    }
                   requestFailure:^
    {
                        NSLog(@"网络请求失败！");
                    }];
}


#pragma UITableViewCell  Delegate cell的代理方法实现

//推荐  不推荐  商品所属店铺按钮的代理
//200   201   311....
-(void)cellBtnClick:(int)index
{
    if(index == 200 || index == 201)
    {
        
    }
    else
    {
      
        numOfButton = index+300;
        self.navigationController.tabBarController.tabBar.alpha = 0.001;
        self.title = nil;
        StoreViewController *storeVc = [[StoreViewController alloc]init];
        [self.navigationController pushViewController:storeVc animated:YES];
    }
}

//输入框开始输入的代理响应
-(void)textFieldBegin:(UITextField *)Text
{
    getCellTextField = Text;
    priceArray[Text.tag-611] = Text.text;
}

//输入框停止输入(光标落在输入框以外）
-(void)textFieldStop:(UITextField *)Text
{
    int index = Text.tag;
    priceArray[index-611] = Text.text;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"待定产品详情";
    
    //重写tabbar
    tabBarButton = [UIButton buttonWithType:UIButtonTypeSystem];
    tabBarButton.backgroundColor =  COLOR(120, 190, 240, 1);
    tabBarButton.frame = AUTOLayer(0,0, 320, 60, 1);
    [tabBarButton setTitle:@"为其它店铺添加该产品" forState:UIControlStateNormal];
    [tabBarButton setTintColor:[UIColor whiteColor]];
    tabBarButton.tag = 50;
    [tabBarButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.tabBarController.tabBar addSubview:tabBarButton];

    self.navigationController.tabBarController.tabBar.alpha = 1;
    
    // 注册监听通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(respondsToNotification:) name:@"GetSoreStr" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    tabBarButton.hidden = YES;

}


//底部添加方框的按钮
-(void)buttonPressed:(UIButton *)sender
{
    if(sender.tag == 50)
    {
        
        //要添加的店铺的数目
        numOfStore++;
        
        [storeArray addObject:@"请选择产品所属店铺"];
        [priceArray addObject:@"请填写产品价格"];
        //添加一行
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:numOfStore+10 inSection:0];
        [_goodsDetailTableView insertRowsAtIndexPaths:@[indexPath]
                                     withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}


//通知接受的事件响应方法
-(void)respondsToNotification:(NSNotification *)notification
{
    getStr = (UITextField *)[self.view viewWithTag:numOfButton];
    getStr.text = [notification.userInfo objectForKey:@"Store"];
    
   storeArray[numOfButton-611] = [notification.userInfo objectForKey:@"Store"];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"GetSoreStr" object:nil];
}




@end
