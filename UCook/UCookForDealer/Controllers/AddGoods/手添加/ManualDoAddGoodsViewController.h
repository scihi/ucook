//
//  ManualDoAddGoodsViewController.h
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AddGoodsViewController.H"


@interface ManualDoAddGoodsViewController : GeneralWithBackBtnViewController<UITextFieldDelegate>

@property (assign, nonatomic) NSString *_subString;
@property (assign ,nonatomic) NSInteger _labelTag;
@property (assign, nonatomic) id<ResultDelegate> resultDelegate;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UITextView *textView;

-(id)initWithSubstring: (NSString *)subString andLabelTag: (NSInteger)tag;

@end
