//
//  ManualAddGoodsViewController.m
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ManualAddGoodsViewController.h"
#import "ManualDoAddGoodsViewController.h"
#import "HRNavViewController.h"

#import "VPImageCropperViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>


#define ORIGINAL_MAX_WIDTH 640.0f

@interface ManualAddGoodsViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, VPImageCropperDelegate>
{
    UITableView *theTableView;
    NSArray *theRowArr;
    NSInteger nowAddImageIndex;
    
    NSString *barcode;
    NSString *name;
    NSString *price;
}

@property (nonatomic, strong) UIImageView *introImageView1;
@property (nonatomic, strong) UIImageView *introImageView2;
@property (nonatomic, strong) UIImageView *introImageView3;

@end

@implementation ManualAddGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"增加产品图片";
        [self initData];
    }
    return self;
}

-(void)initData
{
    _introImageView1 = nil;
    _introImageView2 = nil;
    _introImageView3 = nil;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    theRowArr = @[@"条形码",@"名字",@"价格"];
    theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 90, BOUNDS.size.width, TABLE_VIEW_FRAME.size.height - 90) style:UITableViewStylePlain];
    theTableView.dataSource = self;
    theTableView.delegate = self;
    [self.view addSubview:theTableView];
    
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 70)];
    photoView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:photoView];
    UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 60, 60)];
    [photoButton setBackgroundImage:[UIImage imageNamed:@"add_pic.png"] forState:UIControlStateNormal];
    
    [photoButton addTarget:self action:@selector(editPortrait) forControlEvents:UIControlEventTouchUpInside];
    [photoView addSubview:photoButton];
    
    _introImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 5, 60, 60)];
    [photoView addSubview:_introImageView1];
    _introImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(135, 5, 60, 60)];
    [photoView addSubview:_introImageView2];
    _introImageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(200, 5, 60, 60)];
    [photoView addSubview:_introImageView3];
}

#pragma mark - ResultDelegate delegate
- (void)getResult:(NSString *)text andTag:(NSInteger)tag
{
    UILabel *label = (UILabel *)[self.view viewWithTag:tag];
    label.text = text;
    if (tag == 100) {
        barcode = text;
    }else if (tag == 101){
        name = text;
    }else if (tag == 102){
        price = text;
    }
    
}


-(BOOL)isEmpty:(NSString *)aliase andName:(NSString *)andName
{
    if (aliase == nil || [[aliase stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ) {
        [GlobalSharedClass showAlertView:[NSString stringWithFormat:@"%@不能为空",andName]];
        return YES;
    }
    return NO;
}

// 商品条码标准位13位
- (BOOL)checkBarcode:(NSString *)_text
{
    NSString *regex = @"^\\d{13}$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [test evaluateWithObject:_text];
}

-(BOOL)checkMoney:(NSString *)_text
{
    NSString *regex = @"\\d*\\.?\\d{0,2}";
    NSPredicate *Test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [Test evaluateWithObject:_text];
}

- (void)clickFinishBtn:(UIButton *)button
{
    if (_introImageView1.image == nil &&
        _introImageView2.image == nil &&
        _introImageView3.image == nil)
    {
        [GlobalSharedClass showAlertView:@"请添加商品图片"];
        return;
    }
    else if (barcode == nil || ![self checkBarcode:barcode])
    {
        [GlobalSharedClass showAlertView:@"请输入有效的条形码（提示标准商品条形码位13位）"];
        return;
    }
    else if ([self isEmpty:name andName:@"名字"])
    {
        return;
    }
    else if (price == nil || ![self checkMoney:price])
    {
        [GlobalSharedClass showAlertView:@"请输入有效的金额"];
        return;
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"manually",@"uid":[AccountManager getUid],@"barcode":barcode,@"name":name,@"price":price};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes =
    [NSSet setWithObject:@"text/html"];
    
    
    NSData *imageData1;
    NSData *imageData2;
    NSData *imageData3;
    if (_introImageView1.image != nil) {
        imageData1 = UIImageJPEGRepresentation(_introImageView1.image, 0.1);
    }else if (_introImageView2 != nil){
        imageData2 = UIImageJPEGRepresentation(_introImageView2.image, 0.1);
    }else if (_introImageView3 != nil){
        imageData2 = UIImageJPEGRepresentation(_introImageView3.image, 0.1);
    }
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (_introImageView1.image != nil) {
            [formData appendPartWithFileData :imageData1 name:@"ItemHandleadd[images]" fileName:@"productIntroImage1.jpg" mimeType:@"image/jpeg"];
        }else if (_introImageView2 != nil){
            [formData appendPartWithFileData :imageData2 name:@"ItemHandleadd[images]" fileName:@"productIntroImage2.jpg" mimeType:@"image/jpeg"];
        }else if (_introImageView3 != nil){
            [formData appendPartWithFileData :imageData3 name:@"ItemHandleadd[images]" fileName:@"productIntroImage3.jpg" mimeType:@"image/jpeg"];
        }
        
    }  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:MESSAGE];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return theRowArr.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ManualAddGoodsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [theRowArr objectAtIndex:indexPath.row];
    
    UILabel *goodsInfo = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, BOUNDS.size.width - 70 - 45, 50)];
    goodsInfo.textAlignment = NSTextAlignmentRight;
    goodsInfo.textColor = [UIColor grayColor];
    [goodsInfo setTag:100 + indexPath.row];
    [cell addSubview:goodsInfo];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ManualDoAddGoodsViewController *manualDoAddGoodsVC = [[ManualDoAddGoodsViewController alloc] initWithSubstring:[theRowArr objectAtIndex:indexPath.row] andLabelTag:100+indexPath.row ];
    manualDoAddGoodsVC.resultDelegate = self;
    [self.navigationController pushViewController:manualDoAddGoodsVC animated:YES];
}


- (void)editPortrait {
    
    if (_introImageView3.image != nil) {
        [GlobalSharedClass showAlertView:@"最多只能添加三张图片"];
        return;
    }
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照", @"从相册中选取", nil];
    [choiceSheet showInView:self.view];
}

#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    if (_introImageView1.image == nil) {
        _introImageView1.image = editedImage;
        [self saveImage:editedImage WithName:@"introImage1"];
    }else if (_introImageView2.image == nil) {
        _introImageView2.image = editedImage;
        [self saveImage:editedImage WithName:@"introImage2"];
    }else if (_introImageView3.image == nil){
        _introImageView3.image = editedImage;
        [self saveImage:editedImage WithName:@"introImage3"];
    }
    
    
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
     
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos])
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([self isFrontCameraAvailable]) {
                controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
        
    } else if (buttonIndex == 1)
    {
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
        
    }];
}

#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}



- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName

{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    [imageData writeToFile:fullPathToFile atomically:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
