//
//  ManualAddGoodsViewController.h
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AddGoodsViewController.h"

@interface ManualAddGoodsViewController : GeneralWithBackBtnViewController<UITableViewDelegate,UITableViewDataSource,ResultDelegate>

@end
