//
//  ManualDoAddGoodsViewController.m
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ManualDoAddGoodsViewController.h"

@interface ManualDoAddGoodsViewController ()
{
    
}
@end

@implementation ManualDoAddGoodsViewController

@synthesize _subString, _labelTag, textField, textView;

-(id)initWithSubstring: (NSString *)subString andLabelTag: (NSInteger)tag
{
    self = [super init];
    if (self) {
        self._subString = subString;
        self._labelTag = tag;
        self.title = [NSString stringWithFormat:@"填写%@",subString];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    if ([@"公告" isEqualToString: _subString]) {
        textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 240)];
        textView.backgroundColor = [UIColor whiteColor];
        textView.font = [UIFont fontWithName:textView.font.fontName size:18];
        [self.view addSubview:textView];
        
    }
    else
    {
        UIView *textFieldView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 50)];
        textFieldView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:textFieldView];
        
        textField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, BOUNDS.size.width - 5, 40)];
        textField.placeholder = [NSString stringWithFormat:@"请输入%@", _subString];
        if ([_subString isEqualToString:@"条形码"])
        {
            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if ([_subString isEqualToString:@"价格"])
        {
            textField.keyboardType = UIKeyboardTypeDecimalPad;
        }
        [textField becomeFirstResponder];
        [textFieldView addSubview:textField];

    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [textField resignFirstResponder];
}


- (void)clickFinishBtn: (UIButton *)button
{
    if([self.resultDelegate respondsToSelector:@selector(getResult: andTag:)]){
        if ([@"公告" isEqualToString: _subString]) {
            [self.resultDelegate getResult:textView.text andTag:_labelTag];
        }else{
            [self.resultDelegate getResult:textField.text andTag:_labelTag];
        }
    }
//    [self.navigationController dismissViewControllerAnimated:YES completion:^{
//        
//    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
