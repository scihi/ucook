
//
//  RootViewController.m
//  NewProject
//
//  Created by 学鸿 张 on 13-11-29.
//  Copyright (c) 2013年 Steven. All rights reserved.
//

#import "AddGoodsViewController.h"
#import "ManualAddGoodsViewController.h"
#import "GoodsInfoModel.h"
#import "ZBarSDK.h"

#import "SetPriceViewController.h"
#import "ScanGoodsViewController.h"

@interface AddGoodsViewController ()<ZBarReaderViewDelegate>
{
    NSInteger segSelectIndex;
    UISegmentedControl *segmentedController;
    GoodsInfoModel *_goodsInfo;
    //扫描二维码
    ZBarReaderView *_readerView;
    ZBarCameraSimulator *cameraSimulator;
    
    //是否是单个添加
    BOOL isOnce;
    //计算扫描区域的宽和高
    float a,b;
}

@end

@implementation AddGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
         self.title = @"添加产品";
    }
    return self;
}

-(void)initData
{
    //扫描线条的移动距离
    num = 0;
    //扫描线条上移还是下移
    upOrdown = NO;
    //扫描线条的定时器
    timer = nil;
    //二维码扫描类
    [ZBarReaderView class];
    //导航栏单选框的 确定选择第一个
    segmentedController.selectedSegmentIndex = 0;
    segSelectIndex = 0;
    //默认选择单个添加
    isOnce = YES;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initNavView];
   // [self initZBarReader];
     timer.fireDate = [NSDate date];
    [_readerView start];
}

//导航栏初始化
-(void)initNavView
{
    NSArray *btnArray  = @[@"去定价",@"手添加"];
    NSArray *imageArray  = @[@"ico_price@2x",@"ico_manual@2x"];
    for(int i=0;i<2;++i)
    {
        //按钮的图片
        UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:
                                      (CGRectMake(10, 10, 30, 25))];
        leftImageView.image = [UIImage imageNamed:imageArray[i]];
        
        //按钮
        UIButton *navBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
        [navBtn addTarget:self action:@selector(clickNavBtn:) forControlEvents:UIControlEventTouchUpInside];
        navBtn.tag = 10+i;
        [navBtn addSubview:leftImageView];
        
        //按钮添加到导航栏的左右按钮上面
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithCustomView:navBtn];
        if(i==0) self.navigationItem.leftBarButtonItem  = barItem;
        else     self.navigationItem.rightBarButtonItem = barItem;
        
        //文字信息
        UILabel *Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 40, 20)];
        Label.font = [UIFont fontWithName:Label.font.fontName size:12];
        Label.text= btnArray[i];
        Label.textColor = [UIColor whiteColor];
        Label.textAlignment = NSTextAlignmentCenter;
        [navBtn addSubview:Label];
    }
    
    
    //单选按钮   单个添加 批量添加
    NSArray *array = [NSArray arrayWithObjects:@"单个添加",@"批量添加", nil];
    segmentedController = [[UISegmentedControl alloc] initWithItems:array];
    [segmentedController addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = segmentedController;
    
    
    UIButton *hahaha = [UIButton buttonWithType:UIButtonTypeSystem];
    hahaha.frame = AUTOLayer(80, 150, 160, 50, 1);
    [hahaha setTitle:@"扫描成功" forState:UIControlStateNormal];
    hahaha.backgroundColor = [UIColor redColor];
    [hahaha addTarget:self action:@selector(saomiaochenggong:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:hahaha];
    
    
}

//扫描二维码初始化
-(void)initZBarReader
{
    _readerView = [[ZBarReaderView alloc]init];
    _readerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    _readerView.readerDelegate = self;
    //关闭闪光灯
    _readerView.torchMode = 0;
    //开启摄像头
    if (TARGET_IPHONE_SIMULATOR)
    {
        cameraSimulator = [[ZBarCameraSimulator alloc]
                           initWithViewController:self];
        cameraSimulator.readerView = _readerView;
    }
    [self.view addSubview:_readerView];
    //扫描区域计算
    _readerView.scanCrop = CGRectMake(0.1,0.2,0.8,0.4);
    [_readerView start];
    

    //遮罩层
    for(int i=0;i<2;++i)
    {
        UIView * viewLR = [[UIView alloc] initWithFrame:
                           CGRectMake(i*300, 0,20, self.view.frame.size.height)];
        viewLR.backgroundColor = [UIColor blackColor];
        viewLR.alpha = 0.6;
        [self.view addSubview:viewLR];
        
        UIView * viewTD = [[UIView alloc] initWithFrame:
                           CGRectMake(20,i*360,280, (self.view.frame.size.height-280)/2)];
        viewTD.backgroundColor = [UIColor blackColor];
        viewTD.alpha = 0.6;
        [self.view addSubview:viewTD];
    }
    
    
    //添加扫描边框
    UIImageView * image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pick_bg.png"]];
    a  = _readerView.frame.size.width;
    b  = _readerView.frame.size.height;
    image.frame = CGRectMake(0.1*a,0.2*b,0.8*a,0.4*b);
    [_readerView addSubview:image];
    
    //定时器 保证中间的扫描线条不间断地扫描
    _line = [[UIImageView alloc] initWithFrame:
             CGRM(0,0,0.8*a,3)];
    _line.image = [UIImage imageNamed:@"scanning_line"];
    [image addSubview:_line];
    [self startTimer];
}

-(void)startTimer
{
    if(!timer)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.007 target:self selector:@selector(lineAniMate)
                                               userInfo:nil repeats:YES];
    }
}

//计算扫描区域
-(CGRect)getScanCrop:(CGRect)rect andReaderViewBounds:(CGRect)readerViewBounds
{
    CGFloat x,y,width,height;
    
    x = rect.origin.x/readerViewBounds.size.width;
    y = rect.origin.y/readerViewBounds.size.height;
    width = rect.size.width/readerViewBounds.size.width;
    height = rect.size.height/readerViewBounds.size.height;
    
    return CGRectMake(x, y, width, height);
}

//扫描结束
- (void)readerView:(ZBarReaderView *)readerView didReadSymbols:(ZBarSymbolSet *)symbols fromImage:(UIImage *)image
{
    //定时器从头开始 线条再上面预备
     timer.fireDate = [NSDate distantFuture];
    _line.frame = CGRectMake(0,0, 0.8*a, 3);
    
    for (ZBarSymbol *symbol in symbols)
    {
        NSLog(@"%@", symbol.data);
        break;
    }
    [_readerView stop];
}


//实现条形码中间的滚动条的上下移动
-(void)lineAniMate
{
    if (upOrdown == NO)
    {
        num ++;
        _line.frame = CGRectMake(0,0+num, 0.8*a, 3);
        if (num >= 0.4*b)
        {
            upOrdown = YES;
        }
    }
    else
    {
        _line.frame = CGRectMake(0,0, 0.8*a, 3);
        upOrdown = NO;
        num = 0;
    }
}

//定价按钮和手添加按钮
-(void)clickNavBtn:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 10:[self navLeftPush];break;
        case 11:[self navRightPush];break;
            
        default:break;
    }
}
//扫描成功
-(void)saomiaochenggong:(UIButton *)sender
{
    ScanGoodsViewController *scanVC = [[ScanGoodsViewController alloc]
                                       initWithString:@"6901028046886" getBool:isOnce];
    [self.navigationController pushViewController:scanVC animated:NO];
}
//去定价
- (void)navLeftPush
{
    SetPriceViewController *setPrice = [[SetPriceViewController alloc]init];
    [self.navigationController pushViewController:setPrice animated:YES];
}

//手添加
- (void)navRightPush
{
    ManualAddGoodsViewController *manualAddGoodsVC = [[ManualAddGoodsViewController alloc] init];
    manualAddGoodsVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:manualAddGoodsVC animated:YES];
}

//单选框的按钮选择 确认是为单个添加还是批量添加
- (void)segmentAction: (UISegmentedControl *)segController
{
    segSelectIndex = segController.selectedSegmentIndex;
    if(segSelectIndex == 0)
    {
        isOnce = YES;
    }
    else
    {
        isOnce = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    segmentedController.selectedSegmentIndex = 0;
    segSelectIndex = 0;
    isOnce = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}




//视图加载时内存不足
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}














@end
