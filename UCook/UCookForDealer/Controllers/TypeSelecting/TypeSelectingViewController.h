//
//  TypeSelectingViewController.h
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeSelectingViewController : GeneralWithBackBtnViewController
- (IBAction)clickManageStoreBtn:(id)sender;
- (IBAction)clickBuyGoodsBtn:(id)sender;

@end
