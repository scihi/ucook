//
//  TypeSelectingViewController.m
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "TypeSelectingViewController.h"

@interface TypeSelectingViewController ()

@end

@implementation TypeSelectingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"类型选择";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"title_bg_64"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickManageStoreBtn:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"dealer" forKey:NS_DEALER_LAST_PLACE];//餐厅模块
    [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSNotificationCenter defaultCenter]postNotificationName:NS_GOTO_DEALER_MODULE object:nil];
}
- (IBAction)clickBuyGoodsBtn:(id)sender {
        [[NSUserDefaults standardUserDefaults]setObject:@"rest" forKey:NS_DEALER_LAST_PLACE];//经销商模块
        [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:NS_GOTO_REST_MODULE object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
