//
//  AuctionPriceViewController.h
//  UCook
//
//  Created by huangrun on 14/10/29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class AuctionPriceViewController;

@protocol AuctionPriceDelegate <NSObject>

- (void)priceDidInput:(AuctionPriceViewController *)auctionPriceVC price:(NSString *)price;

@end

@interface AuctionPriceViewController : GeneralWithBackBtnViewController
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (assign, nonatomic) id <AuctionPriceDelegate> delegate;

@end
