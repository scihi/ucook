//
//  DealerStoreListViewController.m
//  UCook
//
//  Created by huangrun on 14/10/29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerStoreListViewController.h"
#import "DealerAuctionDetailStoreModel.h"

@interface DealerStoreListViewController () {
    UITableView *_theTableView;
}

@end

@implementation DealerStoreListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"店铺列表";
    
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME_WITHOUT_TOOLBAR style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.storesArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"DealerStoreList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [(DealerAuctionDetailStoreModel *)[self.storesArr objectAtIndex:indexPath.row]storeName];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_delegate && [_delegate
                      respondsToSelector:@selector(storeDidSelect:store:storeId:)])
    {
        [self.delegate storeDidSelect:self store:[(DealerAuctionDetailStoreModel *)[self.storesArr objectAtIndex:indexPath.row]storeName] storeId:[(DealerAuctionDetailStoreModel *)[self.storesArr objectAtIndex:indexPath.row]storeId]];
        
        
        
    }
    [self.navigationController popViewControllerAnimated:YES];
}



@end
