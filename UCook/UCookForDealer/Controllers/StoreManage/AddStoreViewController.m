//
//  AddStoreViewController.m
//  UCook
//
//  Created by Apple on 14-10-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AddStoreViewController.h"
#import "HRNavViewController.h"
#import "ManualDoAddGoodsViewController.h"
#import "SelectAddressViewController.h"
#import "StoreManageListModel.h"

#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"

#import "VPImageCropperViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>


#define ORIGINAL_MAX_WIDTH 640.0f

@interface AddStoreViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, VPImageCropperDelegate>
{
    NSArray *cellArray;
    UITableView *theTableView;
    StoreManageListModel *_smlModel;
}
@property (nonatomic, strong) UIImageView *cellImageView;


@end


@implementation AddStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"创建店铺";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    theTableView.dataSource = self;
    theTableView.delegate = self;
    [self.view addSubview:theTableView];
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;

    _smlModel = [[StoreManageListModel alloc] init];
    cellArray = @[@"标志",@"店名",@"所在地区",@"具体地址",@"手机号码",@"邮编",@"QQ",@"起订金额",@"公告",@"货到付款"];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)clickFinishBtn:(UIButton *)button
{
    
    if (_cellImageView.image == nil){
        [GlobalSharedClass showAlertView:@"请添加店铺图片"];
        return;
    } else if ([NSString isEmptyOrWhitespace:_smlModel.name]) {
        [GlobalSharedClass showAlertView:@"名称不能为空"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.area]){
        [GlobalSharedClass showAlertView:@"所在地区不能为空"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.address]){
        [GlobalSharedClass showAlertView:@"具体地址不能为空"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.tel] ||
              ![NSString isValidatedMobilePhoneNumber:_smlModel.tel]){
        [GlobalSharedClass showAlertView:@"请输入有效的电话号码"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.zipCode]){
        [GlobalSharedClass showAlertView:@"邮编不能为空"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.miniAmount]||
              ![NSString isValidatedMoney:_smlModel.miniAmount]){
        [GlobalSharedClass showAlertView:@"请输入有效的金额"];
        return;
    }else if ([NSString isEmptyOrWhitespace:_smlModel.notice]){
        [GlobalSharedClass showAlertView:@"公告不能为空"];
        return;
    }
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"create",@"uid":[AccountManager getUid],@"name":_smlModel.name,@"area":_smlModel.area,@"address":_smlModel.address,@"tel":_smlModel.tel,@"zipCode":_smlModel.zipCode,@"miniAmount":_smlModel.miniAmount,@"isDelivery":_smlModel.isDelivery,@"qq":_smlModel.qq,@"notice":_smlModel.notice};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSData *imageData = UIImageJPEGRepresentation(_cellImageView.image, 0.1);
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData :imageData name:@"Store[logo]" fileName:@"1.jpg" mimeType:@"image/jpeg"];
    }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     
    
    {
        
        [self stopRequest];
        [GlobalSharedClass showAlertView:MESSAGE];
        [self.reloadDelegate reloadDataWhenSuccess];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
 
    
    
//    NSString *homePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/storeLogo"];
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:BASIC_URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileURL:[NSURL fileURLWithPath:homePath] name:@"Store[logo]" fileName:@"storeLogo.png" mimeType:@"image/jpeg" error:nil];
//    } error:nil];
//    
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    NSProgress *progress = nil;
//    
//    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        if (error) {
//            NSLog(@"Error: %@", error);
//        } else {
//            NSLog(@"%@ %@", response, responseObject);
//        }
//    }];
//    
//    [uploadTask resume];
}


#pragma mark - ResultDelegate delegate
- (void)getResult:(NSString *)text andTag:(NSInteger)tag
{
    UITableViewCell *cell = (UITableViewCell *)[self.view viewWithTag:tag];
    cell.detailTextLabel.text = text;
//    [cell.detailTextLabel sizeToFit];
//    [cell sizeToFit];
    if (tag == 101) {
        _smlModel.name = text;
    }else if (tag == 103){
        _smlModel.address = text;
    }else if (tag == 104){
        _smlModel.tel = text;
    }else if (tag == 105){
        _smlModel.zipCode = text;
    }else if (tag == 106){
        _smlModel.qq = text;
    }else if (tag == 107){
        _smlModel.miniAmount = text;
    }else if (tag == 108){
        _smlModel.notice = text;
    }    
}


- (void)addressDidSelect:(SelectAddressViewController *)selectAddressVC address:(NSString *)aAddress idsDictionary:(NSDictionary *)aIdsDictionary
{
    UITableViewCell *cell = (UITableViewCell *)[self.view viewWithTag:102];
    cell.detailTextLabel.text = aAddress;
    _smlModel.area = aAddress;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSLog(@"index: %@",indexPath);
    if (indexPath.row == 0) {
        return 60;
    }
    return 40;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//cell点击之后恢复颜色
    if (indexPath.row == 0) {
        [self editPortrait];
    }else if (indexPath.row == 2) {
        
        SelectAddressViewController *selectAddressVC = [[SelectAddressViewController alloc]init];
        selectAddressVC.delegate = self;
        [self.navigationController pushViewController:selectAddressVC animated:YES];
    }else{
        ManualDoAddGoodsViewController *manualDoAddStoreVC = [[ManualDoAddGoodsViewController alloc] initWithSubstring:[cellArray objectAtIndex:indexPath.row] andLabelTag:100+indexPath.row ];
//        HRNavViewController *navCityListVC = [[HRNavViewController alloc]initWithRootViewController:manualDoAddStoreVC];
//        [self presentViewController:navCityListVC animated:YES completion:^{
//            manualDoAddStoreVC.resultDelegate = self;
//        }];
         manualDoAddStoreVC.resultDelegate = self;
        [self.navigationController pushViewController:manualDoAddStoreVC animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return cellArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AddStoreCell";
    UITableViewCell *cell ;
    if (!cell) {
//        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }else{
        while ([cell.contentView.subviews lastObject] != nil) {
            [(UIView*)[cell.contentView.subviews lastObject] removeFromSuperview];  //删除并进行重新分配
        }
    }
    
    
    cell.textLabel.text = [cellArray objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        _cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(BOUNDS.size.width - 90, 5, 50, 50)];
        _cellImageView.image = [UIImage imageNamed:@""];
        [_cellImageView setTag:100];
        [cell addSubview:_cellImageView];
    }else if (indexPath.row == cellArray.count - 1){
        QRadioButton *oppositeRadio = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId"];
        oppositeRadio.frame = CGRectMake(110, 0, cell.bounds.size.width / 3, cell.bounds.size.height);
        [oppositeRadio setTitle:@"不支持" forState:UIControlStateNormal];
        [oppositeRadio setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        [oppositeRadio setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
        [oppositeRadio setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        [oppositeRadio.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [oppositeRadio setImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
        [oppositeRadio setImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
        [oppositeRadio setChecked:YES];
        [cell addSubview:oppositeRadio];
        
        QRadioButton *supportRadio = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId"];
        supportRadio.frame = CGRectMake(cell.bounds.size.width / 3 + 110, 0, cell.bounds.size.width / 3, cell.bounds.size.height);
        [supportRadio setTitle:@"支持" forState:UIControlStateNormal];
        [supportRadio setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [supportRadio setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        [supportRadio.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [supportRadio setImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
        [supportRadio setImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
        [supportRadio setChecked:NO];
        [cell addSubview:supportRadio];
        
    }else{
        [cell setTag:100 + indexPath.row];
    }
    
    if (indexPath.row != cellArray.count - 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    return cell;
}

#pragma mark - QRadioButtonDelegate

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId {
    //NSLog(@"did selected radio:%@ groupId:%@", radio.titleLabel.text, groupId);
    if ([@"不支持" isEqualToString:radio.titleLabel.text]) {
        _smlModel.isDelivery = @"0";
    }else{
        _smlModel.isDelivery = @"1";
    }
}


- (void)editPortrait {
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照", @"从相册中选取", nil];
    [choiceSheet showInView:self.view];
}

#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    _cellImageView.image = editedImage;
    [self saveImage:editedImage WithName:@"storeLogo"];
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([self isFrontCameraAvailable]) {
                controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
        
    } else if (buttonIndex == 1) {
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
       
    }];
}

#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}



- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName

{
    
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString* documentsDirectory = [paths objectAtIndex:0];
    
    // Now we get the full path to the file
    
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    // and then we write it out
    
    [imageData writeToFile:fullPathToFile atomically:NO];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
