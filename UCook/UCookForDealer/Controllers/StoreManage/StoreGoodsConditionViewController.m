//
//  StoreGoodsConditionViewController.m
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreGoodsConditionViewController.h"
#import "MLTableAlert.h"
#import "SelectGoodsCategoryViewController.h"


@interface StoreGoodsConditionViewController ()
{
    NSArray *cellTitileArray;
    UITableView *theTableView;
//    UITableViewCell *theSelectedCell;
    
//    NSInteger priceIndex;
//    NSString *goodsCategoryIdStr;
//    NSInteger statusIndex;
}

@end

@implementation StoreGoodsConditionViewController

@synthesize _searchConditionDelegate, _searchDic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"筛选条件";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    cellTitileArray = @[@"价格",@"类别"];
    theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
//    theTableView.accessibilityActivate = UITableViewCellStyleValue1;
    theTableView.dataSource = self;
    theTableView.delegate = self;
    [self.view addSubview:theTableView];
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
}

- (void)clickFinishBtn: (UIButton *)button
{
    if ([self._searchConditionDelegate respondsToSelector:@selector(returnSearchCondition:)]) {
        [self._searchConditionDelegate returnSearchCondition:_searchDic];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellTitileArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StoreGoodsSearchConditionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    cell.tag = 100 + indexPath.row;
    cell.textLabel.text = [cellTitileArray objectAtIndex:indexPath.row];
    
    if (cell.tag == 100 &&[self._searchDic objectForKey:@"priceIndex"] != nil) {
        cell.detailTextLabel.text = [self._searchDic objectForKey:@"priceIndex"];
    }else if (cell.tag == 101 && [self._searchDic objectForKey:@"goodsCategoryStr"] != nil) {
        cell.detailTextLabel.text = [self._searchDic objectForKey:@"goodsCategoryStr"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.accessoryType = UITableViewCellStyleValue1;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        [self showPriceAlertView];
    }else if (indexPath.row == 1){
        SelectGoodsCategoryViewController *selectGoodsGategoryVC = [[SelectGoodsCategoryViewController alloc] init];
        selectGoodsGategoryVC.hidesBottomBarWhenPushed = YES;
        selectGoodsGategoryVC.delegate = self;
        [self.navigationController pushViewController:selectGoodsGategoryVC animated:YES];
    }
//    else if (indexPath.row == 2){
//        [self showStatusAlertView];
//    }
    
}

- (void)showPriceAlertView
{
    NSArray *priceCellArray = @[@"0-100",@"100-200",@"200-300",@"300-400",@"400-500",@"500-600",@"600-700",@"700-800",@"800-900",@"900-1000"];
	MLTableAlert *priceAlert = [MLTableAlert tableAlertWithTitle:@"请选择价格范围" cancelButtonTitle:@"取消" numberOfRows:^NSInteger (NSInteger section)
                                {
                                    return priceCellArray.count;
                                }
                                          andCells:^UITableViewCell* (MLTableAlert *anAlert, NSIndexPath *indexPath)
                  {
                      static NSString *PriceCellIdentifier = @"PriceCellIdentifier";
                      UITableViewCell *cell = [priceAlert.table dequeueReusableCellWithIdentifier:PriceCellIdentifier];
                      if (!cell)
                          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:PriceCellIdentifier];
                      
                      cell.textLabel.text = [priceCellArray objectAtIndex:indexPath.row];
                      
                      return cell;
                  }];
	
    
	priceAlert.height = 350;
	UITableViewCell *selectedCell = (UITableViewCell *)[self.view viewWithTag:100];
    
	[priceAlert configureSelectionBlock:^(NSIndexPath *selectedIndex){
		selectedCell.detailTextLabel.text = [priceCellArray objectAtIndex:selectedIndex.row];
        [_searchDic setObject:[priceCellArray objectAtIndex:selectedIndex.row] forKey:@"priceIndex"];
        NSLog(@"priceIndex.row:%@",[priceCellArray objectAtIndex:selectedIndex.row]);
	
    
    
    
    
    
    
    } andCompletionBlock:^{
        selectedCell.detailTextLabel.text = nil;
        if ([_searchDic objectForKey:@"priceIndex"] != nil) {
            [_searchDic removeObjectForKey:@"priceIndex"];
        }
	}];
	
	[priceAlert show];
}


//- (void)showStatusAlertView
//{
//    NSArray *statusCellArray = @[@"已上架",@"已下架"];
//	MLTableAlert *statusAlert = [MLTableAlert tableAlertWithTitle:@"请选择商品状态" cancelButtonTitle:@"取消" numberOfRows:^NSInteger (NSInteger section)
//                                {
//                                    return statusCellArray.count;
//                                }
//                                                        andCells:^UITableViewCell* (MLTableAlert *anAlert, NSIndexPath *indexPath)
//                                {
//                                    static NSString *StatusCellIdentifier = @"StatusCellIdentifier";
//                                    UITableViewCell *cell = [statusAlert.table dequeueReusableCellWithIdentifier:StatusCellIdentifier];
//                                    if (!cell)
//                                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:StatusCellIdentifier];
//                                    
//                                    cell.textLabel.text = [statusCellArray objectAtIndex:indexPath.row];
//                                    
//                                    return cell;
//                                }];
//	
//	// Setting custom alert height
//	statusAlert.height = 80;
//	UITableViewCell *selectedCell = (UITableViewCell *)[self.view viewWithTag:102];
//	// configure actions to perform
//	[statusAlert configureSelectionBlock:^(NSIndexPath *selectedIndex){
//		selectedCell.detailTextLabel.text = [statusCellArray objectAtIndex:selectedIndex.row];
////        statusIndex =selectedIndex.row;
//        [_searchDic setObject:[NSNumber numberWithInteger:selectedIndex.row] forKey:@"statusIndex"];
//	} andCompletionBlock:^{
//        selectedCell.detailTextLabel.text = nil;
//        [_searchDic setObject:[NSNumber numberWithInteger:-1] forKey:@"statusIndex"];
//	}];
//	
//	// show the alert
//	[statusAlert show];
//}


#pragma mark SelectGoodsCategoryVCDelegate delegate
- (void)goodsCategoryDidSelect:(SelectGoodsCategoryViewController *)goodsCategoryVC category:(NSString *)aCategory idsDictionary:(NSDictionary *)aIdsDictionary
{
    UITableViewCell *cell = (UITableViewCell *)[self.view viewWithTag:101];
    cell.detailTextLabel.text = aCategory;
    if (aIdsDictionary != nil) {
        [_searchDic setObject:[aIdsDictionary objectForKey:@"lastId"] forKey:@"goodsCategoryIdStr"];
        [_searchDic setObject:aCategory forKey:@"goodsCategoryStr"];
    }else{
        [_searchDic removeObjectForKey:@"goodsCategoryIdStr"];
        [_searchDic removeObjectForKey:@"goodsCategoryStr"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
