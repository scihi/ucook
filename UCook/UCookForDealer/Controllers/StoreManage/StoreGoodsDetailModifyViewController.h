//
//  ManualDoAddGoodsViewController.h
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AddGoodsViewController.H"


@interface StoreGoodsDetailModifyViewController : GeneralWithBackBtnViewController<UITextFieldDelegate>

@property (nonatomic, strong) id<ResultDelegate> resultDelegate;
@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, copy) NSString *beModifiedPrice;

@end
