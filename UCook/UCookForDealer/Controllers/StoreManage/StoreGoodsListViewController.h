//
//  StoreGoodsListViewController.h
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "StoreGoodsConditionViewController.h"
#import "ZBarSDK.h"

@interface StoreGoodsListViewController : GeneralWithBackBtnViewController<UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate, ResultDictionarySearchConditionDelegate,ZBarReaderDelegate>
{
    //与二维码扫面定义有关的三个属性
    int num;
    BOOL upOrdown;
    NSTimer * timer;
    
}
//二维码定义中上下运动的扫描横向
@property (nonatomic, strong) UIImageView * line;

@property(nonatomic, copy)NSString *storeIdStr;
@property(nonatomic, copy)NSString *storeNameStr;

@property(nonatomic, copy)NSString *goodsStatus;
@property(nonatomic, copy)NSString *goodsPriceRangeIndex;
@property(nonatomic, copy)NSString *goodsCategoryIdStr;

@end
