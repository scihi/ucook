//
//  DealerSettingViewController.m
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreGoodsDetailViewController.h"
#import "StoreGoodsListModel.h"
#import "StoreGoodsImageModel.h"
#import "StoreGoodsDetailModifyViewController.h"

#import "UIImageView+AFNetworking.h"

@interface StoreGoodsDetailViewController () {
    UITableView *_theTableView;
    NSArray *_theSectionArr;
    NSArray *_theRowArr;
    StoreGoodsListModel *_storeGoodsDetailModel;
    
    UIView *modelView;
    UIButton *doGoodsButtom;
    
    NSString *modifyPrice;
    NSString *modifyStatus;
}

@end

@implementation StoreGoodsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithName:(NSString *)name
{
    self = [super init];
    if (self)
    {
        self.title = name;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _theSectionArr = @[@"",@"产品信息",@"定价信息"];
    _theRowArr = @[@[@"产品状态"],@[@"名称",@"品牌",@"分类",@"规格",@"单位",@"整/单",@"所属店铺"],@[@"图片",@"价格"]];
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 100) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    modelView = [[UIView alloc] init];
    modelView.frame = CGRectMake(0, BOUNDS.size.height - 100, BOUNDS.size.width, 49);
    modelView.backgroundColor = [UIColor whiteColor];
    
    doGoodsButtom = [[UIButton alloc] initWithFrame:CGRectMake(BOUNDS.size.width/2 -40, 5, 80, 26)];
    [doGoodsButtom.layer setMasksToBounds:YES];
    [doGoodsButtom.layer setCornerRadius:10.0];;
    doGoodsButtom.backgroundColor = [[UIColor alloc] initWithRed:17/255.0 green:129/255.0 blue:188/255.0 alpha:0.7];
    [doGoodsButtom addTarget:self action:@selector(changeStatusAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [modelView addSubview:doGoodsButtom];
    [self.view addSubview:modelView];
    
    [self requestStoreGoodsDetailData];
}


#pragma mark ResultDelegate delegate
- (void)getResult: (NSString *)text
{
    modifyPrice = text;
    
    [self requestStoreGoodsDetailModifyData:@"price" andValue:text];
}


- (void)changeStatusAction: (UIButton *)button
{
    [self requestStoreGoodsDetailModifyData:@"status" andValue:modifyStatus];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 0) {
        return 60;
    }
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _theSectionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_theRowArr objectAtIndex:section]count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [_theSectionArr objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row ];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }


    cell.textLabel.text = [[_theRowArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        cell.detailTextLabel.textColor = [UIColor greenColor];
        cell.detailTextLabel.text = _storeGoodsDetailModel.statusName;
    }else if (indexPath.section == 1 && indexPath.row == 0){
        cell.detailTextLabel.text = _storeGoodsDetailModel.productName;
    }else if (indexPath.section == 1 && indexPath.row == 1){
        cell.detailTextLabel.text = _storeGoodsDetailModel.brandName;
    }else if (indexPath.section == 1 && indexPath.row == 2){
        cell.detailTextLabel.text = _storeGoodsDetailModel.categoryName;
    }else if (indexPath.section == 1 && indexPath.row == 3){
        cell.detailTextLabel.text = _storeGoodsDetailModel.spec;
    }else if (indexPath.section == 1 && indexPath.row == 4){
        cell.detailTextLabel.text = _storeGoodsDetailModel.unit;
    }else if (indexPath.section == 1 && indexPath.row == 5){
        cell.detailTextLabel.text = _storeGoodsDetailModel.split;
    }else if (indexPath.section == 1 && indexPath.row == 6){
        cell.detailTextLabel.text = self.storeName;
    }else if (indexPath.section == 2 && indexPath.row == 0){
        
        if (_storeGoodsDetailModel.images != nil && _storeGoodsDetailModel.images.count > 0) {
            for (int i = 0; i < _storeGoodsDetailModel.images.count; i ++) {
                NSDictionary *image = [_storeGoodsDetailModel.images objectAtIndex:i];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(BOUNDS.size.width - 60 - i * 55, 5, 45, 45)];
             //   NSLog(@"%@",[image objectForKey:@"url"]);
                [imageView setImageWithURL:[NSURL URLWithString:[image objectForKey:@"url"]]];
                [cell.contentView addSubview:imageView];
            }
        }
    }else if (indexPath.section == 2 && indexPath.row == 1){
        cell.tag = 101;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = _storeGoodsDetailModel.price;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 2 && indexPath.row == 1) {
        StoreGoodsDetailModifyViewController *storeGoodsdetailModifyVC = [[StoreGoodsDetailModifyViewController alloc] init];
        storeGoodsdetailModifyVC.beModifiedPrice = _storeGoodsDetailModel.price;
        storeGoodsdetailModifyVC.hidesBottomBarWhenPushed = YES;
        storeGoodsdetailModifyVC.resultDelegate = self;
        [self.navigationController pushViewController:storeGoodsdetailModifyVC animated:YES];
    }

}

- (void)requestStoreGoodsDetailData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"detail",@"productId":_productId,@"uid":[AccountManager getUid]};
//    ,@"storeId":self.iDStr
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _storeGoodsDetailModel = [StoreGoodsListModel objectWithKeyValues:dictionary];
            if ([_storeGoodsDetailModel.status isEqualToString:@"1"]) {
                [doGoodsButtom setTitle:@"下架" forState:UIControlStateNormal
                 ];
                modifyStatus = @"0";
            }else if ([_storeGoodsDetailModel.status isEqualToString:@"0"]) {
                [doGoodsButtom setTitle:@"上架" forState:UIControlStateNormal
                 ];
                modifyStatus = @"1";
            }
            
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)requestStoreGoodsDetailModifyData:(NSString *)aAttributeName andValue:(NSString *)aValue {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"update",@"productId":_productId,@"uid":[AccountManager getUid],@"attributed":aAttributeName,@"value":aValue};
    //    ,@"storeId":self.iDStr
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:MESSAGE];
        if (RIGHT) {
            if ([aAttributeName isEqualToString:@"price"]) {
                [self requestStoreGoodsDetailData];
            }else if ([aAttributeName isEqualToString:@"status"]){
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
