//
//  ActivityManageDetailViewController.h
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "SelectAddressViewController.h"
#import "AddGoodsViewController.h"


@interface StoreManageDetailViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ResultDelegate,SelectAddressVCDelegate>

@property (copy, nonatomic) NSString *iDStr;

-(id)initWithTitle: (NSString *)title;
@end
