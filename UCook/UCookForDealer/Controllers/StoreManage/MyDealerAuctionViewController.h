//
//  MyDealerAuctionViewController.h
//  UCook
//
//  Created by huangrun on 14/10/29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MyDealerAuctionViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
