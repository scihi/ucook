//
//  StoreGoodsConditionViewController.h
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "SelectGoodsCategoryViewController.h"

@protocol ResultDictionarySearchConditionDelegate <NSObject>

- (void) returnSearchCondition: (NSMutableDictionary *)searchCondition;

@end

@interface StoreGoodsConditionViewController : GeneralWithBackBtnViewController<UITableViewDelegate, UITableViewDataSource, SelectGoodsCategoryVCDelegate>

@property(nonatomic, strong)id<ResultDictionarySearchConditionDelegate> _searchConditionDelegate;
@property(nonatomic, strong)NSMutableDictionary *_searchDic;

@end
