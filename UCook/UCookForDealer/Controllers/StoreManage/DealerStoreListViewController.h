//
//  DealerStoreListViewController.h
//  UCook
//
//  Created by huangrun on 14/10/29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class DealerStoreListViewController;

@protocol DealerStoreListVCDelegate <NSObject>

- (void)storeDidSelect:(DealerStoreListViewController *)dealerStoreListVC store:(NSString *)store storeId:(NSString *)storeId;

@end

@interface DealerStoreListViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *storesArr;

@property (assign, nonatomic) id <DealerStoreListVCDelegate> delegate;

@end
