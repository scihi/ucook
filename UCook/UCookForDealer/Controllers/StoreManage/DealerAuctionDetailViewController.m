//
//  DealerAuctionDetailViewController.m
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerAuctionDetailViewController.h"
//#import "MyAuctionDetailModel.h"
#import "UIImageView+AFNetworking.h"

#import "AllOrdersDetailViewController.h"
#import "DealerAuctionDetailModel.h"

#import "DealerAuctionDetailPriceModel.h"
#import "DealerAuctionDetailProductModel.h"
#import "DealerAuctionDetailRecordModel.h"
#import "DealerAuctionDetailStoreModel.h"
#import "DepositModel.h"
#import "NSStringAdditions.h"

@interface DealerAuctionDetailViewController ()
{
    UITableView *_theTableView;
    DealerAuctionDetailModel *_dadModel;
    NSString *_auctionRecordIdStr;
    DepositModel *_depositModel;
    
    NSString *_storeIdStr;//选择的店铺id 完成竞拍时用
}

@end

@implementation DealerAuctionDetailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"商品包竞拍详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarBtnItem:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheInitData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"biddingAgency",
                                 @"action": @"detail",
                                 @"uid":[AccountManager getUid],
                                 @"id": _auctionBagIdStr};
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _dadModel = [DealerAuctionDetailModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0)
    {
        return 200.f;
    }
    else if(indexPath.section == 1)
    {
        return 40.f;
    }
    else
    {
        return 80.f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 2;
    } else if (section == 2) {
        return _dadModel.record.count;
    } else if (section == 3) {
        return _dadModel.productList.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        
        if (indexPath.section == 0)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DealerAuctionDetailCell" owner:self options:nil]firstObject];
        }  else if (indexPath.section == 1) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        } else if (indexPath.section == 2) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AuctionRecordCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 3) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
    }
    
    if (indexPath.section == 0) {
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
        
        nameLabel.text = _dadModel.name;
        
        
        if (self.auctionVCType == auctionVCTypeMyAuction) {
            UILabel *statusLabel = (UILabel *)[cell viewWithTag:102];
            if ([_dadModel.status isEqualToString:@"1"]) {
                statusLabel.text = @"竞拍结束";
            } else if ([_dadModel.status isEqualToString:@"2"]) {
            statusLabel.text = @"竞拍成功";
            }
            
            UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:109];
            currentPriceLabel.text = [NSString stringWithFormat:@"当前价:￥%@",_dadModel.currentPrice];
            
            UILabel *cycleLabel = (UILabel *)[cell viewWithTag:111];
            cycleLabel.text = [NSString stringWithFormat:@"%@",_dadModel.cycleTime];
            
            
            UILabel *myPriceLabel = (UILabel *)[cell viewWithTag:112];
            myPriceLabel.text = [NSString stringWithFormat:@"我出价￥%@",_dadModel.currentPrice];
            
            UILabel *rangeLabel = (UILabel *)[cell viewWithTag:113];
            rangeLabel.text = @"幅度:￥1";
            
            UILabel *beginAuctionPriceLabel = (UILabel *)[cell viewWithTag:114];
            beginAuctionPriceLabel.text = [NSString stringWithFormat:@"起拍价:%@",_dadModel.price];
            
        } else {
        UILabel *numberLabel = (UILabel *)[cell viewWithTag:102];
        numberLabel.text = [NSString stringWithFormat:@"出价%@次",_dadModel.times];
            
        UILabel *beginPriceLabel = (UILabel *)[cell viewWithTag:111];
        NSString *beginPriceStr = _dadModel.price;
        beginPriceLabel.text = [NSString stringWithFormat:@"￥%@",beginPriceStr];

        }
        UILabel *addressLabel = (UILabel *)[cell viewWithTag:103];
        addressLabel.text = [NSString stringWithFormat:@"地址:%@",_dadModel.address];
        
        //星星
        CGFloat score1 = [[[_dadModel.record objectAtIndex:indexPath.row]objectForKey:@"rating"]integerValue];
        NSInteger score2;
        if (score1 == 0 ) {
            score2 = 0;
        } else if (score1 > 0 && score1 <= 1) {
            score2 = 1;
        } else if (score1 > 1 && score1 <= 2) {
            score2 = 2;
        } else if (score1 > 2 && score1 <= 3) {
            score2 = 3;
        } else if (score1 > 3 && score1 <= 4) {
            score2 = 4;
        } else {
            score2 = 5;
        }
        
        UIImageView *starIV1 = (UIImageView *)[cell viewWithTag:104];
        UIImageView *starIV2 = (UIImageView *)[cell viewWithTag:105];
        UIImageView *starIV3 = (UIImageView *)[cell viewWithTag:106];
        UIImageView *starIV4 = (UIImageView *)[cell viewWithTag:107];
        UIImageView *starIV5 = (UIImageView *)[cell viewWithTag:108];
        
        switch (score2) {
            case 0:
                break;
            case 1:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 2:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 3:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 4:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 5:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV5.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            default:
                break;
        }

        
        UILabel *priceLabel = (UILabel *)[cell viewWithTag:109];
        priceLabel.text = [NSString stringWithFormat:@"￥%@",_dadModel.price];
        
        
        if (_dadModel.leftTime == 0) {
            UILabel *leftTimeLabel = (UILabel *)[cell viewWithTag:110];
            leftTimeLabel.text = @"已结束";
        } else {
            
            __block int timeout=[_dadModel.leftTime intValue]; //倒计时时间
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout<=0){ //倒计时结束，关闭
                    dispatch_source_cancel(_timer);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        
                        
                    });
                }else{
                    int hours = timeout / 3600;
                    int minutes = timeout / 60 % 60;
                    int seconds = timeout % 60;
                    NSString *strTime = [NSString stringWithFormat:@"%.2d:%.2d:%.2d", hours, minutes, seconds];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        
                        UILabel *leftTimeLabel = (UILabel *)[cell viewWithTag:110];
                        leftTimeLabel.text = strTime;
                        
                    });
                    timeout--;
                    
                }
            });
            dispatch_resume(_timer);
            
        }
        
    }else if (indexPath.section == 1) {
        DealerAuctionDetailStoreModel *storeModel = [_dadModel.stores firstObject];
        if (indexPath.row == 0) {
            cell.textLabel.text = @"店铺";
            cell.detailTextLabel.text = storeModel.storeName;
        } else {
           cell.textLabel.text = @"价格";
           cell.detailTextLabel.text = @"输入竞价的价格";
        }
        if (self.auctionVCType != auctionVCTypeMyAuction) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    } else if (indexPath.section == 2) {
        NSDictionary *currentDic = [_dadModel.record objectAtIndex:indexPath.row];
        
        UILabel *lowestPriceLabel = (UILabel *)[cell viewWithTag:101];
        NSString *lowestPriceStr =[currentDic objectForKey:@"price"];
        lowestPriceLabel.text = [NSString stringWithFormat:@"￥%@",lowestPriceStr];
        
        UILabel *timeLabel = (UILabel *)[cell viewWithTag:102];
        NSString *timeString = [currentDic objectForKey:@"time"];
        timeLabel.text = timeString;
        
        UILabel *auctionShopLabel = (UILabel *)[cell viewWithTag:103];
        auctionShopLabel.text = [currentDic objectForKey:@"storeName"];
        
        
        //星星
        CGFloat score1 = [[[_dadModel.record objectAtIndex:indexPath.row]objectForKey:@"rating"]integerValue];
        NSInteger score2;
        if (score1 == 0 ) {
            score2 = 0;
        } else if (score1 > 0 && score1 <= 1) {
            score2 = 1;
        } else if (score1 > 1 && score1 <= 2) {
            score2 = 2;
        } else if (score1 > 2 && score1 <= 3) {
            score2 = 3;
        } else if (score1 > 3 && score1 <= 4) {
            score2 = 4;
        } else {
            score2 = 5;
        }
        
        UIImageView *starIV1 = (UIImageView *)[cell viewWithTag:105];
        UIImageView *starIV2 = (UIImageView *)[cell viewWithTag:106];
        UIImageView *starIV3 = (UIImageView *)[cell viewWithTag:107];
        UIImageView *starIV4 = (UIImageView *)[cell viewWithTag:108];
        UIImageView *starIV5 = (UIImageView *)[cell viewWithTag:109];
        
        switch (score2) {
            case 0:
                break;
            case 1:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 2:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 3:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 4:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 5:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV5.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            default:
                break;
        }
        
        UILabel *dealLabel = (UILabel *)[cell viewWithTag:104];
        dealLabel.hidden = YES;
        
    } else if (indexPath.section == 3) {
        NSDictionary *currentDic = [_dadModel.productList objectAtIndex:indexPath.row];
        
        NSString *imgUrlStr = [currentDic objectForKey:@"image"];
        [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        cell.textLabel.text = [currentDic objectForKey:@"name"];
        
        NSString *specificationString = [currentDic objectForKey:@"spec"];
        NSString *unitString = [currentDic objectForKey:@"unit"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"规格:%@ 单位:%@",specificationString, unitString];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return @"我要出价";
    } else if (section == 2) {
        return @"竞价记录";
    } else if (section == 3) {
        return @"商品包详情";
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.f;
    } else {
        return 44.f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.auctionVCType == auctionVCTypeMyAuction) {
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            DealerStoreListViewController *dealerStoreListVC = [[DealerStoreListViewController alloc]init];
            dealerStoreListVC.delegate = self;
            dealerStoreListVC.storesArr = _dadModel.stores;
            [self.navigationController pushViewController:dealerStoreListVC animated:YES];
        } else {
             [self requestDeposit];
        }
    }
}

//获取竞拍保证金
- (void)requestDeposit {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"biddingAgency",@"action": @"biddingMoney",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr,@"type": @1};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dict = [responseObject objectForKey:@"data"];
            _depositModel = [DepositModel objectWithKeyValues:dict];
            
            if (_depositModel.isAllow == 0) {
                NSString *messageStr = [NSString stringWithFormat:@"优厨币:%@\n需要缴纳的保证金:%@",_depositModel.ucookb,_depositModel.biddingMoney];
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:messageStr delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"缴纳", nil];
                [alertView show];
            } else {
                AuctionPriceViewController *auctionPriceVC = [[AuctionPriceViewController alloc]initWithNibName:@"AuctionPriceViewController" bundle:nil];
                auctionPriceVC.delegate = self;
                [self.navigationController pushViewController:auctionPriceVC animated:YES];
            }

            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

//缴纳保证金
- (void)payDeposit {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"biddingAgency",@"action": @"biddingMoney",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr,@"type": @2};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dict = [responseObject objectForKey:@"data"];
            _depositModel = [DepositModel objectWithKeyValues:dict];
            [_theTableView reloadData];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    } else {
        [self payDeposit];
    }
}

- (void)priceDidInput:(AuctionPriceViewController *)auctionPriceVC price:(NSString *)price {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = price;
}

- (void)storeDidSelect:(DealerStoreListViewController *)dealerStoreListVC storeName:(NSString *)storeName storeId:(NSString *)storeId {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = storeName;
    _storeIdStr = storeId;
}

- (void)clickRightBarBtnItem:(id)sender {
    DealerAuctionDetailStoreModel *storeModel = [_dadModel.stores firstObject];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
    if ([NSString isEmptyOrWhitespace:cell.detailTextLabel.text] || [cell.detailTextLabel.text isEqualToString:@"输入竞价的价格"]) {
        [GlobalSharedClass showAlertView:@"请输入竞价价格"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"biddingAgency",@"action": @"add",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr,@"storeId": _storeIdStr?_storeIdStr:storeModel.storeId,@"price": cell.detailTextLabel.text};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

@end
