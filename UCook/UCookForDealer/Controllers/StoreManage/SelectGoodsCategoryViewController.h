//
//  SelectAddressViewController.h
//  UCook
//
//  Created by scihi on 14-10-20.
//  Copyright (c) 2014年 chenbo. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class SelectGoodsCategoryViewController;

@protocol SelectGoodsCategoryVCDelegate <NSObject>

- (void)goodsCategoryDidSelect:(SelectGoodsCategoryViewController *)goodsCategoryVC category:(NSString *)aCategory idsDictionary:(NSDictionary *)aIdsDictionary;

@end

@interface SelectGoodsCategoryViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id <SelectGoodsCategoryVCDelegate> delegate;

@end
