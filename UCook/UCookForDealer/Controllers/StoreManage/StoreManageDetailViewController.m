//
//  ActivityManageDetailViewController.m
//  UCook
//
//  Created by huangrun on 14-10-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreManageDetailViewController.h"
#import "StoreManageListModel.h"
#import "UIImageView+AFNetworking.h"
#import "StoreInfoModifyViewController.h"
#import "StoreGoodsListViewController.h"

#import "ActivityManageViewController.h"
#import "VPImageCropperViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

//待处理订单
#import "DealerOrderViewController.h"
//商品ID单例
#import "AccountManager.h"
//网络请求
#import "CRequestManager.h"
#import "OrderManageListModel.h"

#define ORIGINAL_MAX_WIDTH 640.0f

#define kLogoffStore @"店铺注销"
#define kReleaseStore @"发布店铺"


@interface StoreManageDetailViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, VPImageCropperDelegate>
{
    UITableView *_theTableView;
    StoreManageListModel *_smlAddModel;
    NSArray *_rowTitleArr;
    UIImageView * _logoImageView;
    UIButton *_footerButton4;
    
    NSInteger _tempSelectedCellTag;
    NSString *_tempValue;
    NSString *_tempArea;
}

@property (nonatomic, strong) UIImageView *cellImageView;

@end

@implementation StoreManageDetailViewController

@synthesize iDStr;

-(id)initWithTitle:(NSString *)titleStr
{
    self = [super init];
    if (self)
    {
        self.title = titleStr;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //观察图片模式
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configThePicType:) name:GLOBLE_IMAGE_TOGGLE object:nil];

    _rowTitleArr = @[@"标志",@"店名",@"所在地区",@"具体地址",@"邮编",@"QQ",@"起订金额",@"公告",@"货到付款"];
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(BOUNDS.size.width - 75, 10, 40, 40)];
    

    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 250)];
    UIButton *footerButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    footerButton1.frame = CGRectMake(0, 20, BOUNDS.size.width, 44);
    [footerButton1 setTitle:@"查看本店商品" forState:UIControlStateNormal];
    [footerButton1 setBackgroundImage:[UIImage imageNamed:@"title_bg_44"] forState:UIControlStateNormal];
    [footerButton1 setTag:11];
    [footerButton1 addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerButton1];
    
    UIButton *footerButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    footerButton2.frame = CGRectMake(0, 74, BOUNDS.size.width, 44);
    [footerButton2 setTitle:@"查看本店待处理订单" forState:UIControlStateNormal];
    [footerButton2 setBackgroundImage:[UIImage imageNamed:@"title_bg_44"] forState:UIControlStateNormal];
    [footerButton2 setTag:12];
    [footerButton2 addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerButton2];
    
    UIButton *footerButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
    footerButton3.frame = CGRectMake(0, 128, BOUNDS.size.width, 44);
    [footerButton3 setTitle:@"查看本店活动" forState:UIControlStateNormal];
    [footerButton3 setBackgroundImage:[UIImage imageNamed:@"title_bg_44"] forState:UIControlStateNormal];
    [footerButton3 setTag:13];
    [footerButton3 addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerButton3];
    
    _footerButton4 = [UIButton buttonWithType:UIButtonTypeCustom];
    _footerButton4.frame = CGRectMake(0, 192, BOUNDS.size.width, 44);
    [_footerButton4 setTag:14];
    _footerButton4.backgroundColor = [UIColor redColor];
    [_footerButton4 addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_footerButton4];
    _theTableView.tableFooterView = footerView;
    
    
    [self requestTheData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _rowTitleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //定义动态的cellIdentifier是消除重影的一个方法
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == 0)
    {
        NSString *imgUrlStr = _smlAddModel.logo;
        [_logoImageView setImageWithURL:[NSURL URLWithString:imgUrlStr]];
//        [cell.detailTextLabel addSubview:_logoImageView];
//        cell.accessoryView = _logoImageView;
        [cell.contentView addSubview:_logoImageView];
    }else if (indexPath.row == 1){
        cell.detailTextLabel.text = _smlAddModel.name;
    }else if (indexPath.row == 2){
        cell.detailTextLabel.text = _smlAddModel.area;
    }else if (indexPath.row == 3){
        cell.detailTextLabel.text = _smlAddModel.address;
    }else if (indexPath.row == 4){
        cell.detailTextLabel.text = _smlAddModel.zipCode;
    }else if (indexPath.row == 5){
        cell.detailTextLabel.text = _smlAddModel.qq;
    }else if (indexPath.row == 6){
        cell.detailTextLabel.text = _smlAddModel.miniAmount;
    }else if (indexPath.row == 7){
        cell.detailTextLabel.text = _smlAddModel.notice;
         [cell.detailTextLabel setNumberOfLines:1];//可以显示1行
        cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
    }else if (indexPath.row == 8){
        if ([_smlAddModel.isDelivery isEqualToString:@"0"])
        {
            cell.detailTextLabel.text = @"不支持";
        }else{
            cell.detailTextLabel.text = @"支持";
        }
        if ([_smlAddModel.status isEqualToString:@"0"]) {
            [_footerButton4 setTitle:kReleaseStore forState:UIControlStateNormal];
        }else if([_smlAddModel.status isEqualToString:@"1"]){
            [_footerButton4 setTitle:kLogoffStore forState:UIControlStateNormal];
        }
    }
    cell.tag = 100 + indexPath.row;
    cell.textLabel.text = [_rowTitleArr objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 60;
    }
    else
    {
        return 40;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if (indexPath.section == 1) {
//        ModifyActivityRecomViewController *marVC = [[ModifyActivityRecomViewController alloc]init];
//        marVC.activityIDStr = _amdModel.id;
//        marVC.delegate = self;
//        [self.navigationController pushViewController:marVC animated:YES];
//    }
    
    StoreInfoModifyViewController *storeModiVC;
    if (indexPath.row == 0)
    {
        [self editPortrait];
        return;
    }else if (indexPath.row == 1)
    {
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.name];
    }else if (indexPath.row == 2){
        SelectAddressViewController *selectAddressVC = [[SelectAddressViewController alloc]init];
        selectAddressVC.delegate = self;
        [self.navigationController pushViewController:selectAddressVC animated:YES];
        return;
    }else if (indexPath.row == 3){
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.address];
    }else if (indexPath.row == 4){
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.zipCode];
    }else if (indexPath.row == 5){
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.qq];
    }else if (indexPath.row == 6){
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.miniAmount];
    }else if (indexPath.row == 7){
        storeModiVC = [[StoreInfoModifyViewController alloc] initWithAttribute:[_rowTitleArr objectAtIndex:indexPath.row] andValue:_smlAddModel.notice];
    }else if (indexPath.row == 8){
        UIAlertView *isDeliveryAlertView = [[UIAlertView alloc] initWithTitle:nil message:@"是否支持货到付款" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"支持",@"不支持", nil];
        [isDeliveryAlertView show];
        return;
    }
    
    storeModiVC.resultDelegate = self;
    storeModiVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeModiVC animated:YES];
}



- (void)clickFooterBtn:(UIButton *)button
{

    //NSLog(@"current: %@", button.currentTitle);
    
    if (button.tag == 11)
    {
        StoreGoodsListViewController *storeGoodsListVC = [[StoreGoodsListViewController alloc] init];
        storeGoodsListVC.storeIdStr = self.iDStr;
        storeGoodsListVC.storeNameStr = _smlAddModel.name;
        storeGoodsListVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:storeGoodsListVC animated:YES];
    }
    //查看本店待处理订单
    else if (button.tag == 12)
    {
        NSLog(@"%@",_smlAddModel.id);
        [self netWorking];
    }
    else if (button.tag == 13)
    {
        
        ActivityManageViewController *activityManageVC = [[ActivityManageViewController alloc] init];
        activityManageVC.storeIdStr = _smlAddModel.id;
        activityManageVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:activityManageVC animated:YES];
    }else if (button.tag == 14){
        if ([_smlAddModel.status isEqualToString:@"0"]) {
            _tempValue = @"1";
            _tempSelectedCellTag = -1;
            [self requestModifyAttri:@"status"];
        }else if ([_smlAddModel.status isEqualToString:@"1"])
        {
                [self requestTheCloseData];
        }
        
    }
}

//网络请求
-(void)netWorking
{
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"orderAgency",
                                 @"action": @"index",
                                 @"uid":[AccountManager getUid]};
    //网络请求
    [CRequestManager requestWith:parameters
                          method:BASIC_URL
                   requestSucess:^(id object)
     {
         NSArray *array = [object objectForKey:@"data"];
         NSArray *_omlModelArr = [OrderManageListModel objectArrayWithKeyValuesArray:array];
         
         DealerOrderViewController *doVC = [[DealerOrderViewController alloc]init];
         doVC.storeId = _smlAddModel.id;
         OrderManageListModel *omlModel = [_omlModelArr objectAtIndex:1];
         doVC.statusString = omlModel.status;
         doVC.hidesBottomBarWhenPushed = YES;
         doVC.titleString = @"本店未处理订单订单";
         [self.navigationController pushViewController:doVC animated:YES];
         
     }
                  requestFailure:^
     {
         NSLog(@"网络访问失败");
     }];
}

#pragma mark AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self requestModifyIsDelivery:@"1"];
    }else if (buttonIndex == 2){
        [self requestModifyIsDelivery:@"0"];
    }
}

#pragma mark - ResultDelegate delegate
- (void)getResult:(NSString *)text andTag:(NSInteger)tag
{
    //    [self req]
    //    UITableViewCell *cell = (UITableViewCell *)[self.view viewWithTag:tag];
    //    cell.detailTextLabel.text = text;
    //    [cell.detailTextLabel sizeToFit];
    //    [cell sizeToFit];
    _tempValue = text;
    if (tag == 101) {
        _tempSelectedCellTag = 101;
        [self requestModifyAttri:@"name"];
    }else if (tag == 103){
        _tempSelectedCellTag = 103;
        [self requestModifyAttri:@"address"];
    }else if (tag == 104){
        _tempSelectedCellTag = 104;
        [self requestModifyAttri:@"zip_code"];
    }else if (tag == 105){
        _tempSelectedCellTag = 105;
        [self requestModifyAttri:@"qq"];
    }else if (tag == 106){
        _tempSelectedCellTag = 106;
        [self requestModifyAttri:@"mini_amount"];
    }else if (tag == 107){
        _tempSelectedCellTag = 107;
        [self requestModifyAttri:@"notice"];
    }
}



#pragma mark SelectAddressVCDelegate delegate
- (void)addressDidSelect:(SelectAddressViewController *)selectAddressVC address:(NSString *)aAddress idsDictionary:(NSDictionary *)aIdsDictionary
{
    _tempValue = [aIdsDictionary objectForKey:@"districtId"];
    _tempArea = aAddress;
    _tempSelectedCellTag = 102;
    
    [self requestModifyAttri:@"area_id"];
}


- (void)editPortrait {
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照", @"从相册中选取", nil];
    [choiceSheet showInView:self.view];
}

#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    _cellImageView.image = editedImage;
    [self saveImage:editedImage WithName:@"storeLogo"];
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([self isFrontCameraAvailable]) {
                controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
        
    } else if (buttonIndex == 1)
    {
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
        
    }];
}

#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}



- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    
    [imageData writeToFile:fullPathToFile atomically:NO];
    
}

- (void)requestTheData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"detail",@"uid":[AccountManager getUid],@"storeId":self.iDStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _smlAddModel = [StoreManageListModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)requestModifyStoreLogo {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"uploadLogo",@"uid":[AccountManager getUid],@"storeId": self.iDStr};
    NSData *imageData = UIImageJPEGRepresentation(_cellImageView.image, 0.1);
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData :imageData name:@"Store[logo]" fileName:@"1.jpg" mimeType:@"image/jpeg"];
    }  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)requestModifyIsDelivery:(NSString *)isDeliveryContent {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"update",@"uid":[AccountManager getUid],@"storeId": self.iDStr, @"attributed":@"is_delivery", @"value":isDeliveryContent};
    [self startRequestWithString:@"请稍候..."];
    
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            UITableViewCell *cell109 = (UITableViewCell *)[self.view viewWithTag:109];
            if ([isDeliveryContent isEqualToString:@"0"]) {
                _smlAddModel.isDelivery = @"不支持";
                cell109.detailTextLabel.text = _smlAddModel.isDelivery;
            }else if ([isDeliveryContent isEqualToString:@"1"]){
                _smlAddModel.isDelivery = @"支持";
                cell109.detailTextLabel.text = _smlAddModel.isDelivery;

            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)requestModifyAttri:(NSString *)theAttributeName {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"update",@"uid":[AccountManager getUid],@"storeId": self.iDStr,@"attributed":theAttributeName,@"value":_tempValue};
    [self startRequestWithString:@"请稍候..."];

    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            UITableViewCell *tempCell = (UITableViewCell *)[self.view viewWithTag:_tempSelectedCellTag];
            if (_tempSelectedCellTag == 101) {
                _smlAddModel.name = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.name;
            }else if (_tempSelectedCellTag == 102) {
                _smlAddModel.area = _tempArea;
                _smlAddModel.areaId = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.area;
            }else if (_tempSelectedCellTag == 103){
                _smlAddModel.address = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.address;
            }else if (_tempSelectedCellTag == 104){
                _smlAddModel.zipCode = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.zipCode;
            }else if (_tempSelectedCellTag == 105){
                _smlAddModel.qq = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.qq;
            }else if (_tempSelectedCellTag == 106){
                _smlAddModel.miniAmount = _tempValue;
               tempCell.detailTextLabel.text = _smlAddModel.miniAmount;
            }else if (_tempSelectedCellTag == 107){
                _smlAddModel.notice = _tempValue;
                tempCell.detailTextLabel.text = _smlAddModel.notice;
            }else if (_tempSelectedCellTag == -1){
                _smlAddModel.status = _tempValue;
//                _footerButton4.titleLabel.text = _tempValue;
                [_footerButton4 setTitle:kLogoffStore forState:UIControlStateNormal];
                [GlobalSharedClass showAlertView:@"店铺发布成功"];
                return ;
            }
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


- (void)requestTheCloseData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"close",@"uid":[AccountManager getUid],@"storeId":self.iDStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:@"店铺注销成功"];            _smlAddModel.status = @"0";
//            _footerButton4.titleLabel.text = kReleaseStore;
            [_footerButton4 setTitle:kReleaseStore forState:UIControlStateNormal];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

@end
