//
//  DealerAuctionDetailViewController.h
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AuctionPriceViewController.h"
#import "DealerStoreListViewController.h"

//vc的类型
typedef NS_ENUM(NSInteger, AuctionVCType) {
    auctionVCTypeNormal,
    auctionVCTypeMyAuction
};

@interface DealerAuctionDetailViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate, AuctionPriceDelegate, DealerStoreListVCDelegate>

@property (copy, nonatomic) NSString *auctionBagIdStr;
@property (assign, nonatomic) AuctionVCType auctionVCType;

@end
