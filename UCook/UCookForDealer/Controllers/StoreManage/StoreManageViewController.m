//
//  StoreManageViewController.m
//  UCook
//
//  Created by huangrun on 14-10-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreManageViewController.h"
#import "StoreManageListModel.h"
#import "UIImageView+AFNetworking.h"
#import "AddStoreViewController.h"
#import "LoginViewController.h"
#import "StoreManageDetailViewController.h"
#import "DealerAuctionViewController.h"
#import "OrderManageListModel.h"

@interface StoreManageViewController () {
    UITableView *_theTableView;
    StoreManageListModel *_smlModel;
    NSArray *_smlModelArr;
}

@end

@implementation StoreManageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.title = @"店铺管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        UINavigationController *navLoginVC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
        return;
    }

    
    //观察图片模式
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configThePicType:) name:GLOBLE_IMAGE_TOGGLE object:nil];
    
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME_WITHOUT_TOOLBAR style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate  = self;
    [self.view addSubview:_theTableView];
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 20)];
    [navRightBtn setTitle:@"添加" forState:UIControlStateNormal];
    //    navRightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    //    navRightBtn.titleLabel.textColor = [UIColor blackColor];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 20)];
    [navLeftBtn setTitle:@"竞价" forState:UIControlStateNormal];
    
    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [self requestTheListData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _smlModelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"StoreManageCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {

        if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
            cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"StoreManageCell" owner:self options:nil]firstObject];
        } else {
            cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"StoreManageWithoutPicCell" owner:self options:nil]firstObject];
        }
    }
    
    _smlModel = [_smlModelArr objectAtIndex:indexPath.row];
    
    //图片
    
    UIImageView *headIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = _smlModel.logo;
    [headIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:[UIImage imageNamed:@"default_pic"]];
    //标题
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = _smlModel.name;
    
    //店铺入驻
    UILabel *enterLabel = (UILabel *)[cell viewWithTag:103];
    if ([_smlModel.realStatus isEqualToString:@"2"])
    {
        enterLabel.text = @"店铺已入驻";
    }
    
    //地址
    UILabel *addressLabel = (UILabel *)[cell viewWithTag:104];
    addressLabel.text = [NSString stringWithFormat:@"地址:%@",_smlModel.address];
    
    //公告
    UILabel *noticeLabel = (UILabel *)[cell viewWithTag:105];
    NSString *noticeStr = [_smlModel.notice isEqualToString:@""]?@"暂无店铺公告":_smlModel.notice;
    noticeLabel.text = [NSString stringWithFormat:@"公告:%@",noticeStr];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    StoreManageListModel *model = [_smlModelArr objectAtIndex:indexPath.row];
    StoreManageDetailViewController *detailVC = [[StoreManageDetailViewController alloc] initWithTitle:model.name];
    detailVC.iDStr = model.id;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)clickNavRightBtn: (UIButton *)button
{
    
    AddStoreViewController * addStoreVC = [[AddStoreViewController alloc] init];
    addStoreVC.hidesBottomBarWhenPushed = YES;
    addStoreVC.reloadDelegate = self;
    [self.navigationController pushViewController:addStoreVC animated:YES];
}

- (void)clickNavLeftBtn: (UIButton *)button
{
    DealerAuctionViewController *daVC = [[DealerAuctionViewController alloc]init];
    daVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:daVC animated:YES];
}

#pragma -mark ReloadStoreManageListDelegate delegate
-(void)reloadDataWhenSuccess
{
    [self requestTheListData];
}

- (void)requestTheListData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"storeAgency",@"action": @"index",@"uid":[AccountManager getUid]};
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
//        [self stopRequest];
        if (RIGHT)
        {
            NSArray *array = [responseObject objectForKey:@"data"];
            _smlModelArr = [StoreManageListModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configThePicType:(NSNotification *)notification {
    [_theTableView reloadData];
}


@end
