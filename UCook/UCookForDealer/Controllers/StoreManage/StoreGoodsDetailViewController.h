//
//  DealerSettingViewController.h
//  UCook
//
//  Created by huangrun on 14-10-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"
#import "AddGoodsViewController.h"

@interface StoreGoodsDetailViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ResultDelegate>

@property(nonatomic, copy)NSString *productId;
@property(nonatomic, copy)NSString *storeName;

- (id)initWithName:(NSString *)name;

@end
