//
//  ManualDoAddGoodsViewController.m
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreGoodsDetailModifyViewController.h"

#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"

@interface StoreGoodsDetailModifyViewController ()
{
    
}
@end

@implementation StoreGoodsDetailModifyViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIView *textFieldView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 50)];
    textFieldView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:textFieldView];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, BOUNDS.size.width - 10, 40)];
    _textField.text = _beModifiedPrice;
    _textField.placeholder = @"请输入修改后的价格";
    _textField.keyboardType = UIKeyboardTypeDecimalPad;
    [textFieldView addSubview:_textField];
    
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [textField resignFirstResponder];
//}


- (void)clickFinishBtn: (UIButton *)button
{
    if (([NSString isEmptyOrWhitespace:_textField.text] ||
                                                ![NSString isValidatedMoney:_textField.text])){
        [GlobalSharedClass showAlertView:@"请输入有效的金额"];
        return;
    }
    if([self.resultDelegate respondsToSelector:@selector(getResult:)]){
        
        [self.resultDelegate getResult:_textField.text];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
