//
//  AuctionPriceViewController.m
//  UCook
//
//  Created by huangrun on 14/10/29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AuctionPriceViewController.h"

@interface AuctionPriceViewController ()

@end

@implementation AuctionPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"输入价格";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarBtnItem:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickRightBarBtnItem:(id)sender {
    if (self.delegate) {
        [self.delegate priceDidInput:self price:self.priceTF.text];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
