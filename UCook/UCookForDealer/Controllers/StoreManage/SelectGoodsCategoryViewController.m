//
//  SelectAddressViewController.m
//  UCook
//
//  Created by scihi on 14-8-6.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SelectGoodsCategoryViewController.h"
#import "SelectGoodsCategoryModel.h"

#import "NSObject+MJKeyValue.h"
#import "AFHTTPRequestOperation.h"

static NSString *CellIdentifer = @"SelectGoodsCategoryCell";

@interface SelectGoodsCategoryViewController ()
{
    NSString *_goodsCategoryIdString;   //产品类别ID
    SelectGoodsCategoryModel *_selectgoodsCategoryModel;    //产品类别模型
    
    
    NSArray *_selectGoodsCategoryModelArr;  //产品类别数组
    
    UITableView *_theTableView;     //产品类别列表
    NSString *_headerTitleStr0;//一级类别名称
    NSString *_headerTitleStr1;//二级类别名称
    NSString *_headerTitleStr2;//三级类别名称
    NSString *_headerTitleStr3;//四级类别名称
    NSString *_firstLayerIdStr;//一级类别ID
    NSString *_secondLayerIdStr;//二级类别ID
    NSString *_thirdLayerIdStr;//三级类别ID
    NSString *_fourthLayerIdStr;//四级类别ID
    
    
    NSInteger _headerNumTag;//用来标记进入tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath的次数 防止第一次_headerNumber ++
    
    NSInteger _headerNumber;    //列表头行数
}

@end

@implementation SelectGoodsCategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"选择产品类别";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _headerNumTag = 0;
    _headerNumber = 1;
    
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 50)];
    
    [finishBtn setTitle:@"撤销" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickRemoveBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self initTheTableView];
    [self getSelectGoodsCategoryListData];
}

- (void)clickRemoveBtn:(UIButton *)button
{
    [_delegate goodsCategoryDidSelect:self category:nil idsDictionary:nil];
    [self.navigationController popViewControllerAnimated:YES];

}


- (void)getSelectGoodsCategoryListData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *dictionary = @{@"key": APP_KEY,@"controller": @"commonAgency",@"action": @"category",@"type":@"sub"};
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (_goodsCategoryIdString != nil) {
        [parameters setObject:_goodsCategoryIdString forKey:@"categoryId"];
    }
    [self startRequestWithString:@"请稍候..."];
    
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self stopRequest];
        if (RIGHT)
        {
            NSArray *array = [responseObject objectForKey:@"data"];
            _selectGoodsCategoryModelArr = [SelectGoodsCategoryModel
                                            objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
            if (_selectGoodsCategoryModelArr.count != 0)
            {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:_headerNumber - 1];
            [_theTableView scrollToRowAtIndexPath:indexPath atScrollPosition:
             UITableViewScrollPositionTop animated:NO];
            }
        }
        else
        {
            [self outAction];
//            [GlobalSharedClass showAlertView:MESSAGE];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)outAction
{
    if (_headerNumber == 4) {
        NSString *goodsCategoryString = [[[[[[_headerTitleStr0 stringByAppendingString:@"." ] stringByAppendingString:_headerTitleStr1] stringByAppendingString:@"." ]stringByAppendingString:_headerTitleStr2] stringByAppendingString:@"."] stringByAppendingString:_headerTitleStr3];
        NSDictionary *idsDictionary = @{@"firstId": _firstLayerIdStr,@"secondId": _secondLayerIdStr,@"thirdId": _thirdLayerIdStr,@"lastId":_fourthLayerIdStr};
        [_delegate goodsCategoryDidSelect:self category:goodsCategoryString idsDictionary:idsDictionary];
        [self.navigationController popViewControllerAnimated:YES];
    } else if (_headerNumber == 3) {
        NSString *goodsCategoryString = [[[[_headerTitleStr0 stringByAppendingString:@"."] stringByAppendingString:_headerTitleStr1] stringByAppendingString:@"." ] stringByAppendingString:_headerTitleStr2];
        NSDictionary *idsDictionary = @{@"firstId": _firstLayerIdStr,@"secondId": _secondLayerIdStr,@"lastId": _thirdLayerIdStr};
        [_delegate goodsCategoryDidSelect:self category:goodsCategoryString idsDictionary:idsDictionary];
        [self.navigationController popViewControllerAnimated:YES];
    } else if (_headerNumber == 2) {
        NSString *goodsCategoryString = [[_headerTitleStr0 stringByAppendingString:@"." ]stringByAppendingString:_headerTitleStr1];
        NSDictionary *idsDictionary = @{@"firstId": _firstLayerIdStr,@"lastId": _secondLayerIdStr};
        [_delegate goodsCategoryDidSelect:self category:goodsCategoryString idsDictionary:idsDictionary];
        [self.navigationController popViewControllerAnimated:YES];
    }else if (_headerNumber == 1) {
        NSString *goodsCategoryString = _headerTitleStr0;
        NSDictionary *idsDictionary = @{@"lastId": _firstLayerIdStr};
        [_delegate goodsCategoryDidSelect:self category:goodsCategoryString idsDictionary:idsDictionary];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_headerNumber == 1) {
            return _selectGoodsCategoryModelArr.count;
    } else if (_headerNumber == 2) {
        if (section == 0) {
            return 0;
        } else if (section == 1) {
            return _selectGoodsCategoryModelArr.count;
        }
    } else if (_headerNumber == 3) {
        if (section == 0) {
            return 0;
        } else if (section == 1) {
            return 0;
        } else {
            return _selectGoodsCategoryModelArr.count;
        }
    }else if (_headerNumber == 4) {
        if (section == 0) {
            return 0;
        } else if (section == 1) {
            return 0;
        }else if (section == 2) {
            return 0;
        } else {
            return _selectGoodsCategoryModelArr.count;
        }
    }
    
//    if (_selectGoodsCategoryModelArr != nil && _selectGoodsCategoryModelArr.count > 0) {
//        return _selectGoodsCategoryModelArr.count;
//    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"GoodsCategoryCell";
    UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    SelectGoodsCategoryModel *selectGoodsCategoryModel = [_selectGoodsCategoryModelArr objectAtIndex:indexPath.row];
    cell.textLabel.text = selectGoodsCategoryModel.name;
    
    return cell;
}

- (void)initTheTableView {
    _theTableView = [[UITableView alloc]initWithFrame:TABLE_VIEW_FRAME];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifer];
    [self.view addSubview:_theTableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _goodsCategoryIdString = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row] id];
    
    if (_headerNumTag != 0) {
        _headerNumber ++;
    }
    if (_headerNumber == 1) {
        _headerTitleStr0 = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]name];
        _firstLayerIdStr = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]id];
    } else if (_headerNumber == 2) {
        _headerTitleStr1 = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]name];
        _secondLayerIdStr = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]id];
    } else  if (_headerNumber == 3){
        _headerTitleStr2 = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]name];
        _thirdLayerIdStr = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]id];
    }else  if (_headerNumber == 4){
        _headerTitleStr3 = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]name];
        _fourthLayerIdStr = [[_selectGoodsCategoryModelArr objectAtIndex:indexPath.row]id];
    }

    _headerNumTag++;
    [self getSelectGoodsCategoryListData];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _headerTitleStr0;
    } else if (section == 1) {
        return _headerTitleStr1;
    } else if (section == 2) {
        return _headerTitleStr2;
    }else if (section == 3) {
        return _headerTitleStr3;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _headerNumber;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
