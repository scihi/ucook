//
//  StoreGoodsListViewController.m
//  UCook
//
//  Created by Apple on 14-10-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreGoodsListViewController.h"
#import "StoreGoodsConditionViewController.h"
#import "StoreGoodsDetailViewController.h"
#import "StoreGoodsListModel.h"

#import "UIImageView+AFNetworking.h"
#import "MJRefresh.h"

#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"

#define kPublic     @"publish"
#define kUnpublic   @"unpublish"

@interface StoreGoodsListViewController ()
{
    UITableView *_theTableView;
    NSMutableArray *_theDataMutArr;
//    StoreGoodsListModel *_sglModel;
//    NSArray *_sglModelArr;
    
    NSString *segSelectType;
    UISegmentedControl *segmentedController;
    
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    UISearchBar *theSearchBar;
    NSString *searchBarText;
    NSString *QRCodeStr;
    NSMutableDictionary *returnSearchDic;   //筛选页面返回的查询条件
    
    UIView *modelView;
    UIButton *doGoodsButtom;
    
    
}

@end

@implementation StoreGoodsListViewController

@synthesize storeIdStr, goodsStatus, goodsPriceRangeIndex, goodsCategoryIdStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [theSearchBar resignFirstResponder];
}
//[[self findFirstResponderBeneathView:self] resignFirstResponder];

- (void)viewDidLoad
{
    [super viewDidLoad];
    returnSearchDic = [[NSMutableDictionary alloc] init];
    
    NSArray *array = [NSArray arrayWithObjects:@"全部",@"上架",@"下架", nil];
    segmentedController = [[UISegmentedControl alloc] initWithItems:array];
    [segmentedController addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    segmentedController.selectedSegmentIndex = 0;
    self.navigationItem.titleView = segmentedController;
    
    
    
    //观察图片模式
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configThePicType:) name:GLOBLE_IMAGE_TOGGLE object:nil];
    
    _theTableView = [[UITableView alloc] initWithFrame:TABLE_VIEW_FRAME];
    _theTableView.delegate = self;
    _theTableView.dataSource = self;
    [self.view addSubview:_theTableView];
    _theDataMutArr = [NSMutableArray arrayWithCapacity:10];
    
    
    UIButton *conditionBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
    [conditionBtn setTitle:@"筛选" forState:UIControlStateNormal];
    
    [conditionBtn addTarget:self action:@selector(clickConditionBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:conditionBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 40)];
    theSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(5, 5, BOUNDS.size.width - 60, 30)];
    theSearchBar.delegate = self;
    theSearchBar.placeholder = @"请输入要搜索的商品";
    [headView addSubview:theSearchBar];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(BOUNDS.size.width - 50, 0, 50, 40)];
    [button setBackgroundImage:[UIImage imageNamed:@"ico_scanning.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(scanBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:button];
    headView.backgroundColor = [UIColor colorWithRed:187/255.0 green:186/255.0 blue:193/255.0 alpha:0.8];
    
    _theTableView.tableHeaderView = headView;
    
    [self initBottomView];
    
    
//    [self setUpForDismissKeyboard];
    [self requestTheListData];
    [self addFooter];
    
    
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
}




-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [theSearchBar resignFirstResponder];
}


- (void)setUpForDismissKeyboard {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    UITapGestureRecognizer *singleTapGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapAnywhereToDismissKeyboard:)];
    NSOperationQueue *mainQuene =[NSOperationQueue mainQueue];
    [nc addObserverForName:UIKeyboardWillShowNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view addGestureRecognizer:singleTapGR];
                }];
    [nc addObserverForName:UIKeyboardWillHideNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view removeGestureRecognizer:singleTapGR];
                }];
}

- (void)tapAnywhereToDismissKeyboard:(UIGestureRecognizer *)gestureRecognizer {
    //此method会将self.view里所有的subview的first responder都resign掉
    [self.view endEditing:YES];
}


- (void)segmentAction: (UISegmentedControl *)segController
{
    //操作输入框时，就将二维码扫描的条件移除
    QRCodeStr = nil;
    
    [theSearchBar resignFirstResponder];
    [modelView removeFromSuperview];
    if (segController.selectedSegmentIndex == 0)
    {
        segSelectType = nil;
        goodsStatus = nil;
    }else if (segController.selectedSegmentIndex == 1)
    {
        segSelectType = kUnpublic;
        goodsStatus = @"1";
        [doGoodsButtom setTitle:@"下架" forState:(UIControlStateNormal)];
    }else if (segController.selectedSegmentIndex == 2)
    {
        segSelectType = kPublic;
        goodsStatus = @"0";
        [doGoodsButtom setTitle:@"上架" forState:(UIControlStateNormal)];
    }
    
    [self requestTheListData];
}


#pragma mark UISearchBarDelegate delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    searchBarText = searchText;
    //操作输入框时，就将二维码扫描的条件移除
    QRCodeStr = nil;
}

#pragma mark UISearchBarDelegate delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //NSLog(@"searchText%@", searchBarText);
    [searchBar resignFirstResponder];
    [self requestTheListData];
}



#pragma mark ResultDictionarySearchConditionDelegate delegate
- (void)returnSearchCondition:(NSMutableDictionary *)searchCondition
{
    returnSearchDic = searchCondition;
    [self requestTheListData];
}


-(void)initBottomView
{
    modelView = [[UIView alloc] init];
    modelView.frame = CGRectMake(0, BOUNDS.size.height - 100, BOUNDS.size.width, 49);
    modelView.backgroundColor = [UIColor whiteColor];
    
    doGoodsButtom = [[UIButton alloc] initWithFrame:CGRectMake(BOUNDS.size.width/2 -40, 5, 80, 26)];
    [doGoodsButtom.layer setMasksToBounds:YES];
    [doGoodsButtom.layer setCornerRadius:10.0];;
    doGoodsButtom.backgroundColor = [[UIColor alloc] initWithRed:17/255.0 green:129/255.0 blue:188/255.0 alpha:0.7];
    [doGoodsButtom addTarget:self action:@selector(requestChangeGoodsType) forControlEvents:(UIControlEventTouchUpInside)];
    [modelView addSubview:doGoodsButtom];

}

- (void)clickConditionBtn: (UIButton *)button
{
    //操作输入框时，就将二维码扫描的条件移除
    QRCodeStr = nil;
    //移除查询框弹出的键盘
    [theSearchBar resignFirstResponder];
    StoreGoodsConditionViewController *storeGoodsConditionVC = [[StoreGoodsConditionViewController alloc] init];
    storeGoodsConditionVC.hidesBottomBarWhenPushed = YES;
    storeGoodsConditionVC._searchConditionDelegate = self;
    storeGoodsConditionVC._searchDic = returnSearchDic;
    [self.navigationController pushViewController:storeGoodsConditionVC animated:YES];
}


- (void)configThePicType:(NSNotification *)notification {
    [_theTableView reloadData];
}

-(void)scanBtnAction
{
    num = 0;
    upOrdown = NO;
    //初始话ZBar
    ZBarReaderViewController * reader = [ZBarReaderViewController new];
    //设置代理
    reader.readerDelegate = self;
    //支持界面旋转
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.showsHelpOnFail = NO;
    reader.readerView.torchMode = 0;
    reader.scanCrop = CGRectMake(0.1, 0.2, 0.8, 0.8);//扫描的感应框
    ZBarImageScanner * scanner = reader.scanner;
    [scanner setSymbology:ZBAR_I25
                   config:ZBAR_CFG_ENABLE
                       to:0];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
    view.backgroundColor = [UIColor clearColor];
    reader.cameraOverlayView = view;
    
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 40)];
    label.text = @"请将扫描的二维码至于下面的框内\n谢谢！";
    label.textColor = [UIColor whiteColor];
    label.textAlignment = 1;
    label.lineBreakMode = 0;
    label.numberOfLines = 2;
    label.backgroundColor = [UIColor clearColor];
    [view addSubview:label];
    
    UIImageView * image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pick_bg.png"]];
    image.frame = CGRectMake(20, 80, 280, 280);
    [view addSubview:image];
    
    
    _line = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, 220, 2)];
    _line.image = [UIImage imageNamed:@"line.png"];
    [image addSubview:_line];
    //定时器，设定时间过0.02秒，
    timer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(animation) userInfo:nil repeats:YES];
    
    [self presentViewController:reader animated:YES completion:^{
        
    }];
}
-(void)animation
{
    if (upOrdown == NO) {
        num ++;
        _line.frame = CGRectMake(30, 10+2*num, 220, 2);
        if (2*num == 260) {
            upOrdown = YES;
        }
    }else {
        num --;
        _line.frame = CGRectMake(30, 10+2*num, 220, 2);
        if (num == 0) {
            upOrdown = NO;
        }
    }
    
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [timer invalidate];
    _line.frame = CGRectMake(30, 10, 220, 2);
    num = 0;
    upOrdown = NO;
    [picker dismissViewControllerAnimated:YES completion:^{
        [picker removeFromParentViewController];
    }];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [timer invalidate];
    _line.frame = CGRectMake(30, 10, 220, 2);
    num = 0;
    upOrdown = NO;
    [picker dismissViewControllerAnimated:YES completion:^{
        [picker removeFromParentViewController];
        UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
        //初始化
        ZBarReaderController * read = [ZBarReaderController new];
        //设置代理
        read.readerDelegate = self;
        CGImageRef cgImageRef = image.CGImage;
        ZBarSymbol * symbol = nil;
        id <NSFastEnumeration> results = [read scanImage:cgImageRef];
        for (symbol in results)
        {
            break;
        }
        NSString * result;
        if ([symbol.data canBeConvertedToEncoding:NSShiftJISStringEncoding])
        {
            result = [NSString stringWithCString:[symbol.data cStringUsingEncoding: NSShiftJISStringEncoding] encoding:NSUTF8StringEncoding];
        }else{
            result = symbol.data;
        }
        
        
        //NSLog(@"%@",result);
        //当扫描成功后，初始化所有查询条件，以防止查询条件中只有二维码这个条件。这样处理的原因是，二维码所对应的物质只有少量的一两条，没有必要添加额外的查询条件进行查询
        [returnSearchDic removeAllObjects];
        searchBarText = nil;
        goodsStatus = nil;
        QRCodeStr = result;
        [self requestTheListData];
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _theDataMutArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StoreGoodsListCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:GLOBLE_IMAGE_TOGGLE]isEqualToString:@"on"]) {
            cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"StoreGoodsListWithPicCell" owner:self options:nil]firstObject];
        } else {
            cell = (UITableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"StoreGoodsListWithoutPicCell" owner:self options:nil]firstObject];
        }
    }
    
    if (_theDataMutArr.count != 0) {
        
        NSDictionary *currentRowDic = [_theDataMutArr objectAtIndex:indexPath.row];
        
        UIImageView *headIV = (UIImageView *)[cell viewWithTag:101];
        
        NSString *imgUrlStr;
        if ([currentRowDic objectForKey:@"images"] != nil) {
            NSArray *images = [StoreGoodsImageModel objectArrayWithKeyValuesArray:[currentRowDic objectForKey:@"images"]];
            StoreGoodsImageModel *image = [images objectAtIndex:0];
            imgUrlStr = image.url;
        }
        [headIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];

       
        //标题
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
        titleLabel.text = [NSString stringWithFormat:@"(%@)%@",[currentRowDic objectForKey:@"split"],[currentRowDic objectForKey:@"productName"]];

        //规格
        UILabel *specLabel = (UILabel *)[cell viewWithTag:103];
        specLabel.text = [NSString stringWithFormat:@"%@", [currentRowDic objectForKey:@"spec"]];
        
        //单位
        UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];
        unitLabel.text = [NSString stringWithFormat:@"单位:%@",[currentRowDic objectForKey:@"unit"]];
        
        //介绍
        UILabel *realStatusLabel = (UILabel *)[cell viewWithTag:105];
        realStatusLabel.text = (([currentRowDic objectForKey:@"realStatus"] == nil) ||[[currentRowDic objectForKey:@"realStatus"] isEqualToString:@""])?@"暂无介绍":[currentRowDic objectForKey:@"realStatus"];
        
        //价格
        UILabel *priceLabel = (UILabel *)[cell viewWithTag:106];
        priceLabel.text = [NSString stringWithFormat:@"￥ %@",[currentRowDic objectForKey:@"price"]];
        
        //已售
        UILabel *salesLabel = (UILabel *)[cell viewWithTag:107];
        salesLabel.text = [NSString stringWithFormat:@"已售%@",[currentRowDic objectForKey:@"sales"]];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = [_theDataMutArr objectAtIndex:indexPath.row];
    
    
    StoreGoodsDetailViewController *detailVC = [[StoreGoodsDetailViewController alloc] initWithName:[dic objectForKey:@"productName"]];
    detailVC.productId = [dic objectForKey:@"productId"];
    detailVC.storeName = self.storeNameStr;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)requestTheListData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    NSDictionary *dictionary = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"index",@"page": @(_pageNumber),@"storeId":storeIdStr};
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (goodsStatus != nil) {
        [parameters setObject:goodsStatus forKey:@"status"];
    }
    
    if (![NSString isEmptyOrWhitespace:searchBarText]) {
        [parameters setObject:searchBarText forKey:@"name"];
    }
    
    if ([returnSearchDic objectForKey:@"priceIndex"] != nil) {
        
        
        NSString *priceRange = (NSString *)[returnSearchDic objectForKey:@"priceIndex"];
        NSArray *priceArr = [priceRange componentsSeparatedByString:@"-"];
        [parameters setObject:[priceArr objectAtIndex:0] forKey:@"minPrice"];
        
        [parameters setObject:[priceArr objectAtIndex:1]  forKey:@"maxPrice"];
    }
    
    if ([returnSearchDic objectForKey:@"goodsCategoryIdStr"] != nil) {
        [parameters setObject:[returnSearchDic objectForKey:@"goodsCategoryIdStr"] forKey:@"categoryId"];
    }
    
    if (QRCodeStr != nil) {
        [parameters setObject:QRCodeStr forKey:@"barcode"];
    }
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        [_theDataMutArr removeAllObjects];
        if (RIGHT) {
//            NSArray *array = [responseObject objectForKey:@"data"];
//            _sglModelArr = [StoreGoodsListModel objectArrayWithKeyValuesArray:array];
            
            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
            [_theTableView reloadData];
            if ([segSelectType isEqualToString:kUnpublic]){
                [self.view addSubview:modelView];
                [doGoodsButtom setTitle:@"下架" forState:(UIControlStateNormal)];
            }else if ([segSelectType isEqualToString:kPublic]){
                [self.view addSubview:modelView];
                [doGoodsButtom setTitle:@"上架" forState:(UIControlStateNormal)];
            }
        } else {
            [modelView removeFromSuperview];
            [_theTableView reloadData];
//            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        if (_footer.refreshing) {
            return;
        }
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        
        NSDictionary *dictionary = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"index",@"page": @(_pageNumber),@"storeId":storeIdStr};
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
        if (goodsStatus != nil) {
            [parameters setObject:goodsStatus forKey:@"status"];
        }
        
        if (![NSString isEmptyOrWhitespace:searchBarText]) {
            [parameters setObject:searchBarText forKey:@"name"];
        }
        
        if ([returnSearchDic objectForKey:@"priceIndex"] != nil) {
            
            
            NSString *priceRange = (NSString *)[returnSearchDic objectForKey:@"priceIndex"];
            NSArray *priceArr = [priceRange componentsSeparatedByString:@"-"];
            [parameters setObject:[priceArr objectAtIndex:0] forKey:@"minPrice"];
            
            [parameters setObject:[priceArr objectAtIndex:1]  forKey:@"maxPrice"];
        }
        
        if ([returnSearchDic objectForKey:@"goodsCategoryIdStr"] != nil) {
            [parameters setObject:[returnSearchDic objectForKey:@"goodsCategoryIdStr"] forKey:@"categoryId"];
        }
        
        if (QRCodeStr != nil) {
            [parameters setObject:QRCodeStr forKey:@"barcode"];
        }
        
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                
                //            [_theDataArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
                [self doneWithView:refreshView];
                
            }else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                [GlobalSharedClass showAlertView:MESSAGE];
                //            [_theDataArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                [self doneWithView:refreshView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
   // NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}


- (void)requestChangeGoodsType {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSMutableArray *productIds = [[NSMutableArray alloc] init];
    if(_theDataMutArr != nil && _theDataMutArr.count > 0){
        for (int i = 0; i < _theDataMutArr.count; i ++) {
            NSDictionary *goods = [_theDataMutArr objectAtIndex:i];
            NSString *productId = [goods objectForKey:@"productId"];
//            [productIds addObject:productId];
            [productIds addObject:@{@"productId": productId}];
        }
    }
    
    NSString *goodsInfoMutArrJsonStr = [self arrayToJson:productIds];
    
    
    NSDictionary *dictionary = @{@"key": APP_KEY,@"controller": @"productAgency",@"action": @"batchPublish",@"uid":[AccountManager getUid]};
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (segSelectType != nil) {
        [parameters setObject:segSelectType forKey:@"type"];
    }
    if (productIds.count > 0) {
        [parameters setObject:goodsInfoMutArrJsonStr forKey:@"productIds"];
    }
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            //            NSArray *array = [responseObject objectForKey:@"data"];
            //            _sglModelArr = [StoreGoodsListModel objectArrayWithKeyValuesArray:array];
            
//            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
//            [_theTableView reloadData];
            [_theDataMutArr removeAllObjects];
        } else {
//            [_theTableView reloadData];
            //            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
