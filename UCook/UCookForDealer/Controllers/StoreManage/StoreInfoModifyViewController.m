//
//  ManualDoAddGoodsViewController.m
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "StoreInfoModifyViewController.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"

@interface StoreInfoModifyViewController ()
{
    
    NSString *_modifyAttributeName;
    NSString *_modifyAttributeValue;
}



@end

@implementation StoreInfoModifyViewController

@synthesize attribute, value, theTextField, theTextView;

-(id)initWithAttribute: (NSString *)theAttribute andValue: (NSString *)theValue
{
    self = [self init];
    if (self) {
        self.title = [NSString stringWithFormat:@"修改%@",theAttribute];
        self.attribute = theAttribute;
        self.value = theValue;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *finishBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 40, 60)];
    
    [finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    [finishBtn addTarget:self action:@selector(clickFinishBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    if ([@"公告" isEqualToString: attribute]) {
        theTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 240)];
        theTextView.backgroundColor = [UIColor whiteColor];
        theTextView.text = value;
        theTextView.font = [UIFont fontWithName:theTextView.font.fontName size:18];
        [self.view addSubview:theTextView];
        
    }else{
        UIView *textFieldView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, BOUNDS.size.width, 50)];
        textFieldView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:textFieldView];
        
        theTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, BOUNDS.size.width - 5, 40)];
        theTextField.text = value;
        theTextField.placeholder = [NSString stringWithFormat:@"请输入%@", attribute];
        if ([attribute isEqualToString:@"邮编"]) {
            theTextField.keyboardType = UIKeyboardTypeNumberPad;
        }else if ([attribute isEqualToString:@"起订金额"]){
            theTextField.keyboardType = UIKeyboardTypeDecimalPad;
        }
        [textFieldView addSubview:theTextField];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (theTextField != nil) {
        [theTextField resignFirstResponder];
    }else if (theTextView != nil){
        [theTextView resignFirstResponder];
    }
    
}


- (void)clickFinishBtn: (UIButton *)button
{
    if (theTextField != nil) {
        self.value = theTextField.text;
    }else if (theTextView != nil){
        self.value = theTextView.text;
    }
    if ([attribute isEqualToString:@"店名"] && [NSString isEmptyOrWhitespace:value]) {
        [GlobalSharedClass showAlertView:@"名称不能为空"];
        return;
    }else if ([attribute isEqualToString:@"具体地址"] && [NSString isEmptyOrWhitespace:value]){
        [GlobalSharedClass showAlertView:@"具体地址不能为空"];
        return;
    }else if ([attribute isEqualToString:@"邮编"] && [NSString isEmptyOrWhitespace:value]){
        [GlobalSharedClass showAlertView:@"邮编不能为空"];
        return;
    }else if ([attribute isEqualToString:@"起订金额"] && ([NSString isEmptyOrWhitespace:value] ||
              ![NSString isValidatedMoney:value])){
        [GlobalSharedClass showAlertView:@"请输入有效的金额"];
        return;
    }else if ([attribute isEqualToString:@"公告"] && [NSString isEmptyOrWhitespace:value]){
        [GlobalSharedClass showAlertView:@"公告不能为空"];
        return;
    }
    
    if([self.resultDelegate respondsToSelector:@selector(getResult:andTag:)]){
        if ([@"公告" isEqualToString: attribute]) {
            [self.resultDelegate getResult:value andTag:107];
        }else if([@"店名" isEqualToString: attribute]){
            [self.resultDelegate getResult:value andTag:101];
        }else if([@"具体地址" isEqualToString: attribute]){
            [self.resultDelegate getResult:value andTag:103];
        }else if([@"邮编" isEqualToString: attribute]){
            [self.resultDelegate getResult:value andTag:104];
        }else if([@"QQ" isEqualToString: attribute]){
            [self.resultDelegate getResult:value andTag:105];
        }else if([@"起订金额" isEqualToString: attribute]){
            [self.resultDelegate getResult:value andTag:106];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
