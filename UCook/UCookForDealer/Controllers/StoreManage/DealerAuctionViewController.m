//
//  DealerAuctionViewController.m
//  UCook
//
//  Created by huangrun on 14/10/28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealerAuctionViewController.h"
#import "MJRefresh.h"
//#import "MyAuctionModel.h"
#import "UIImageView+AFNetworking.h"
//#import "MyAuctionDetailViewController.h"
#import "DealerAuctionModel.h"
#import "DealerAuctionDetailViewController.h"
#import "MyDealerAuctionViewController.h"

@interface DealerAuctionViewController ()
{
    UITableView *_theTableView;
//    MyAuctionModel *_myAuctionModel;
//    NSMutableArray *_myAuctionModelMutArr;
    DealerAuctionModel *_daModel;
    NSMutableArray *_daModelMutArr;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
}

@end

@implementation DealerAuctionViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getTheInitData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"商品包竞拍";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"我的竞拍" style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarBtnItem:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _daModelMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"DealerAuctionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DealerAuctionCell" owner:self options:nil]firstObject];
    }
    _daModel = [_daModelMutArr objectAtIndex:indexPath.row];
    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = _daModel.image;
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = _daModel.name;
    
    UILabel *leftTimeLabel = (UILabel *)[cell viewWithTag:103];
    NSString *string;
    if (_daModel.leftTime == 0)
    {
        string = @"已结束";
    } else {
        string = [self timeFormatted:_daModel.leftTime];
    }
    leftTimeLabel.text = string;
    leftTimeLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *productsLabel = (UILabel *)[cell viewWithTag:104];
    productsLabel.text = _daModel.products;
    
    UILabel *currentLowestPriceLabel = (UILabel *)[cell viewWithTag:105];
    if (!_daModel.price)
    {
        currentLowestPriceLabel.text = @"暂无商家出价";
    }
    else
    {
        currentLowestPriceLabel.text = [NSString stringWithFormat:@"￥%@",_daModel.price];
    }
    
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:106];
    numberLabel.text = [NSString stringWithFormat:@"%@次出价",_daModel.times];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DealerAuctionModel *daModel = [_daModelMutArr objectAtIndex:indexPath.row];
    //    if (!myAuctionModel.storePrice) {
    //        [GlobalSharedClass showAlertView:@"暂无商家出价"];
    //    } else {
    DealerAuctionDetailViewController *dadVC = [[DealerAuctionDetailViewController alloc]init];
    NSInteger auctionBagId = daModel.id;
    dadVC.auctionBagIdStr = [NSString stringWithFormat:@"%ld",(long)auctionBagId];
    [self.navigationController pushViewController:dadVC animated:YES];
    //    }
}

- (void)getTheInitData
{
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"biddingAgency",
                                 @"action": @"list",
                                 @"uid":[AccountManager getUid],
                                 @"page": @(_pageNumber)};
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _daModelMutArr = [NSMutableArray arrayWithArray:[DealerAuctionModel objectArrayWithKeyValuesArray:array]];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
       
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView)
    {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSDictionary *parameters = @{@"key": APP_KEY,
                                     @"controller": @"biddingAgency",
                                     @"action": @"list",
                                     @"uid":[AccountManager getUid],
                                     @"page": @(_pageNumber)};
        
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            //            [self stopRequest];
            if (RIGHT)
            {
                //            [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_daModelMutArr addObjectsFromArray:[DealerAuctionModel objectArrayWithKeyValuesArray:array]];
                
                [self doneWithView:refreshView];
            }
            else if ([[responseObject objectForKey:@"code"]integerValue] == -2)
            {
                
                //                    [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_daModelMutArr addObjectsFromArray:[DealerAuctionModel objectArrayWithKeyValuesArray:array]];
                [self doneWithView:refreshView];
                
            }
            else
            {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

//我的竞价
- (void)clickRightBarBtnItem:(id)sender
{
    MyDealerAuctionViewController *myDealerAuctionVC = [[MyDealerAuctionViewController alloc]init];
    [self.navigationController pushViewController:myDealerAuctionVC animated:YES];
}

















@end
