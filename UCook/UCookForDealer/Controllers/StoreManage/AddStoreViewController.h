//
//  AddStoreViewController.h
//  UCook
//
//  Created by Apple on 14-10-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AddGoodsViewController.h"
#import "QRadioButton.h"
#import "SelectAddressViewController.h"

@protocol ReloadStoreManageListDelegate <NSObject>

-(void)reloadDataWhenSuccess;

@end

@interface AddStoreViewController : GeneralWithBackBtnViewController<UITableViewDataSource, UITableViewDelegate,ResultDelegate,QRadioButtonDelegate,SelectAddressVCDelegate>

@property(nonatomic, strong) id<ReloadStoreManageListDelegate> reloadDelegate;

@end
