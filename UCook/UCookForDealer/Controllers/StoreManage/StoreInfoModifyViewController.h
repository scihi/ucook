//
//  ManualDoAddGoodsViewController.h
//  UCook
//
//  Created by Apple on 14-10-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AddGoodsViewController.h"


@interface StoreInfoModifyViewController : GeneralWithBackBtnViewController

@property (copy, nonatomic) NSString *attribute;
@property (copy ,nonatomic) NSString *value;
@property (strong, nonatomic) id<ResultDelegate> resultDelegate;
@property (copy, nonatomic) NSString *idStr;
@property (nonatomic, strong) UITextField *theTextField;
@property (nonatomic, strong) UITextView *theTextView;


-(id)initWithAttribute: (NSString *)theAttribute andValue: (NSString *)theValue;

@end
