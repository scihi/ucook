
#import <Foundation/Foundation.h>

@interface ADModel : NSObject

@property (nonatomic, copy)     NSString *id;
@property (nonatomic, copy)     NSString *type;
@property (nonatomic, copy)     NSString *link;
@property (nonatomic, copy)     NSString *name;
@property (nonatomic, copy)     NSString *text;
@property (nonatomic, copy)     NSString *itemId;
@property (nonatomic, copy)     NSString *image;

@end
