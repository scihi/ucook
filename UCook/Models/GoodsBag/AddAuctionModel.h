//
//  AddAuctionModel.h
//  UCook
//
//  Created by scihi on 14-8-4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddAuctionModel : NSObject

@property (nonatomic, copy) NSString *biddingId;

@end
