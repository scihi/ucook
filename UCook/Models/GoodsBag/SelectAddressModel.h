//
//  SelectAddressModel.h
//  UCook
//
//  Created by scihi on 14-8-8.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectAddressModel : NSObject

@property (nonatomic, copy) NSString *id;//地区id
@property (nonatomic, copy) NSString *name;//地区名字
@end
