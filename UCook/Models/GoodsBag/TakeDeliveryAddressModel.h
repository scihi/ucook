//
//  TakeDeliveryAddressModel.h
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TakeDeliveryAddressModel : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *contactName;
@property (nonatomic, copy) NSString *address;

@end

