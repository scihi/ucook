//
//  AddAddressModel.h
//  UCook
//
//  Created by huangrun on 14-8-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddAddressModel : NSObject

@property (nonatomic, copy) NSString *id;//收货地址id
@property (nonatomic, copy) NSString *stateName;//省名称
@property (nonatomic, copy) NSString *cityName;//市名称
@property (nonatomic, copy) NSString *districtName;//区名称
@property (nonatomic, copy) NSString *zipcode;//邮编
@property (nonatomic, copy) NSString *address;//街道地址
@property (nonatomic, copy) NSString *contactName;//收货人姓名
@property (nonatomic, copy) NSString *mobilePhone;//手机号码
@property (nonatomic, copy) NSString *phone;//电话号码

@end
