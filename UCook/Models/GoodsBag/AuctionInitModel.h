//
//  AuctionInitModel.h
//  UCook
//
//  Created by scihi on 14-8-4.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuctionInitModel : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *contactName;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *ucookb;
@property (nonatomic, copy) NSString *biddingMoney;
@property (nonatomic, copy) NSString *isAllow;

@end
