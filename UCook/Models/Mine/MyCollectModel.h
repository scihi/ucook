//
//  MyCollectModel.h
//  UCook
//
//  Created by huangrun on 14-8-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCollectModel : NSObject


@property (nonatomic) NSInteger id;
@property (nonatomic) NSInteger type;
@property (nonatomic) NSInteger item;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *introduction;
@property (copy, nonatomic) NSString *hot;
@property (copy, nonatomic) NSString *spec;
@property (copy, nonatomic) NSString *split;
@property (copy, nonatomic) NSString *unit;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *discountPrice;
@property (copy, nonatomic) NSString *address;

@end
