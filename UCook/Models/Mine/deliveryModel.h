//
//  DeliveryModel.h
//  UCook
//
//  Created by huangrun on 14-10-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeliveryModel : NSObject

@property (copy, nonatomic) NSString *deliveryFee;
@property (assign, nonatomic) NSInteger deliveryStatus;
@property (copy, nonatomic) NSString *deliveryFeedback;

@end
