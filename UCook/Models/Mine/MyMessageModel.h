//
//  MyMessageModel.h
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyMessageModel : NSObject

@property (copy, nonatomic) NSString *last_time;
@property (strong, nonatomic) NSMutableArray *list;

@end
