//
//  MyAuctionDetailModel.h
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyAuctionDetailModel : NSObject

@property (nonatomic) NSInteger id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *cycleTime;
@property (copy, nonatomic) NSString *leftTime;
@property (copy, nonatomic) NSString *timeStatus;
@property (copy, nonatomic) NSString *times;
@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *remark;
@property (copy, nonatomic) NSString *partId;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *storePrice;

@property (strong, nonatomic) NSMutableArray *record;
@property (strong, nonatomic) NSMutableArray *products;

@end
