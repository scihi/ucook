//
//  MyMessageListModel.h
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//


//暂未使用此model 用的集合代替 huangrun 0813

#import <Foundation/Foundation.h>

@interface MyMessageListModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *typeName;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *time;
@property (copy, nonatomic) NSString *uri;

@end
