//
//  MyReportFormsModel.h
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyReportFormsModel : NSObject

@property (copy , nonatomic) NSString *statement;
@property (copy , nonatomic) NSString *statementURL;
@property (strong, nonatomic) NSMutableArray *products;

@end
