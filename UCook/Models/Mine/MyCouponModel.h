//
//  MyCouponModel.h
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCouponModel : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *couponId;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *condition;
@property (copy, nonatomic) NSString *money;
@property (copy, nonatomic) NSString *maxnum;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeLogo;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *number;

@end
