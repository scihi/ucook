//
//  MyAuctionModel.h
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyAuctionModel : NSObject

@property (nonatomic) NSInteger id;
@property (copy, nonatomic) NSString *name;
@property (nonatomic) NSInteger status;//竞拍状态，0未发布；1已发布；2已结束；-2已删除
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *price;
@property (nonatomic) NSInteger leftTime;
@property (copy, nonatomic) NSString *timeStatus;
@property (nonatomic) NSInteger times;
@property (copy, nonatomic) NSString *partId;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *storePrice;
@property (copy, nonatomic) NSString *accept;//是否采纳，0：未采纳；1：已采纳
@property (nonatomic) NSInteger updateEtime;


@end
