//
//  AllOrdersModel.h
//  UCook
//
//  Created by huangrun on 14-8-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeliveryModel.h"

@interface AllOrdersModel : NSObject

@property (copy, nonatomic) NSString *orderId;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *payMethod;
@property (copy, nonatomic) NSString *payMethodName;
@property (nonatomic) CGFloat payFee;
@property (nonatomic) NSInteger status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *commentStatus;
@property (copy, nonatomic) NSString *commentStatusName;
@property (nonatomic) NSInteger amount;

//文档中没有
@property (nonatomic) NSInteger refundProductNum;
@property (nonatomic) NSInteger refundStatus;
@property (nonatomic) NSInteger refundTimes;

//经销商增加
@property (copy, nonatomic) NSString *type;
@property (nonatomic) NSInteger amout;//写接口的命名能不能不要这么粗心 huangrun
@property (strong, nonatomic) DeliveryModel *delivery;


@end
