//
//  RechargeAccountModel.h
//  UCook
//
//  Created by scihi on 14-8-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeAccountModel : NSObject

@property (nonatomic) NSInteger accounts;
@property (copy, nonatomic) NSString *bankDistrict;
@property (copy, nonatomic) NSString *bankName;
@property (nonatomic) NSInteger id;
@property (copy, nonatomic) NSString *opiningName;
@property (copy, nonatomic) NSString *receiverName;

@end
