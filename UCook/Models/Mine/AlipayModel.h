//
//  AlipayModel.h
//  UCook
//
//  Created by scihi on 14-8-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlipayModel : NSObject

@property (copy, nonatomic) NSString *orderId;
@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *payMethodId;
@property (strong, nonatomic) NSDictionary *info;

@end
