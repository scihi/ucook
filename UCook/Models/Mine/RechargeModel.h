//
//  RechargeModel.h
//  UCook
//
//  Created by scihi on 14-8-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeModel : NSObject

@property (copy, nonatomic) NSString *ucookbAccount;
@property (copy, nonatomic) NSString *ucookb;
@property (copy, nonatomic) NSString *ucookbUnconver;
@property (copy, nonatomic) NSString *isCash;

@property (strong, nonatomic) NSMutableArray *payMethods;
@property (strong, nonatomic) NSDictionary *account;

@end
