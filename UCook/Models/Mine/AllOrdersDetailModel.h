//
//  AllOrdersDetailModel.h
//  UCook
//
//  Created by huangrun on 14-8-17.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "deliveryModel.h"

@interface AllOrdersDetailModel : NSObject

@property (copy, nonatomic) NSString *orderId;
@property (copy, nonatomic) NSString *type;
@property (assign, nonatomic) NSInteger status;
@property (copy, nonatomic) NSString *statusName;
@property (copy, nonatomic) NSString *commentStatus;
@property (copy, nonatomic) NSString *commentStatusName;
@property (copy, nonatomic) NSString *payMethod;
@property (copy, nonatomic) NSString *payMethodName;
@property (copy, nonatomic) NSString *totalFee;
@property (copy, nonatomic) NSString *shipFee;
@property (copy, nonatomic) NSString *payFee;
@property (copy, nonatomic) NSString *receiverAddress;
@property (copy, nonatomic) NSString *receiverName;
@property (copy, nonatomic) NSString *receiverZip;
@property (copy, nonatomic) NSString *receiverMobile;
@property (copy, nonatomic) NSString *receiverPhone;
@property (copy, nonatomic) NSString *memo;
@property (copy, nonatomic) NSString *payTime;
@property (copy, nonatomic) NSString *shipTime;
@property (copy, nonatomic) NSString *createTime;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *storeTel;
@property (strong, nonatomic) NSMutableArray *products;

//接口文档中没有的
@property (copy, nonatomic) NSString *refundTimes;
@property (copy, nonatomic) NSString *amount;
@property (copy, nonatomic) NSString *refundProductNum;
@property (nonatomic) NSInteger refundStatus;
@property (strong, nonatomic) NSArray *refund;
@property (strong, nonatomic) NSArray *refundProduct;
@property (strong, nonatomic) DeliveryModel *delivery;

@end
