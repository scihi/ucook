//
//  DealRecordModel.h
//  UCook
//
//  Created by huangrun on 14-9-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealRecordModel : NSObject

@property (copy, nonatomic) NSString *createTime;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *entity;
@property (copy, nonatomic) NSString *receiver;
@property (copy, nonatomic) NSString *pay;
@property (copy, nonatomic) NSString *ucookb;
@property (copy, nonatomic) NSString *status;

@end
