//
//  ShopModel.h
//  UCook
//
//  Created by huangrun on 14-8-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopModel : NSObject

@property (nonatomic) NSInteger id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *rating;
@property (copy, nonatomic) NSString *notice;
@property (nonatomic) NSInteger activityNum;
@property (copy, nonatomic) NSString *activity;
@property (nonatomic) NSInteger productNum;
@property (strong, nonatomic) NSArray *products;

@end
