//
//  MarketListOrder.h
//  UCook
//
//  Created by huangrun on 14-8-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketListOrder : NSObject

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *address;
@property (nonatomic) NSInteger storeNum;
@property (strong, nonatomic) NSMutableArray *stores;

@end
