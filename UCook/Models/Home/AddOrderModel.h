//
//  AddOrderModel.h
//  UCook
//
//  Created by huangrun on 14-8-23.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddOrderModel : NSObject

@property (strong, nonatomic) NSDictionary *info;
@property (nonatomic) NSInteger isPay;
@property (copy, nonatomic) NSString *orderPay;
@property (copy, nonatomic) NSString *paymentMethodId;
@property (strong, nonatomic) NSArray *orderIds;

@end
