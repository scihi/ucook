//
//  PaymethodModel.h
//  UCook
//
//  Created by scihi on 14-8-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayMethodModel : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *name;

@end
