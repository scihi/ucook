//
//  OrdersModel.h
//  UCook
//
//  Created by scihi on 14-8-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductsModel.h"

@interface OrdersModel : NSObject

@property (nonatomic, copy) NSString *storeTotal;
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, copy) NSString *storeId;
@property (nonatomic, copy) NSString *shipTotal;
@property (nonatomic, copy) NSString *miniAmount;
@property (nonatomic, copy) NSString *isAmount;

@property (nonatomic, copy) ProductsModel *productsModel;

@end
