//
//  ProductsModel.h
//  UCook
//
//  Created by scihi on 14-8-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductsModel : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *spec;
@property (nonatomic, copy) NSString *split;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *unit;

@end
