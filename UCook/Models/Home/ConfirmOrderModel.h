//
//  ConfirmOrderModel.h
//  UCook
//
//  Created by scihi on 14-8-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UcookbModel.h"
#import "RecAddressModel.h"

@interface ConfirmOrderModel : NSObject

@property (nonatomic, strong) UcookbModel *ucookb;
@property (nonatomic, strong) RecAddressModel *recAddress;

@property (nonatomic, strong) NSMutableArray *payMothed;
@property (nonatomic, strong) NSMutableArray *orders;

@end
