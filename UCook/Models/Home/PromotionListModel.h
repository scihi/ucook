//
//  PromotionListModel.h
//  UCook
//
//  Created by scihi on 14-8-22.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromotionListModel : NSObject

@property (nonatomic) NSInteger promotionId;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *startTime;
@property (copy, nonatomic) NSString *endTime;
@property (copy, nonatomic) NSString *discount;
@property (copy, nonatomic) NSString *nop;
@property (copy, nonatomic) NSString *restriction;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *typeName;
@property (copy, nonatomic) NSString *storeId;
@property (copy, nonatomic) NSString *storeName;
@property (copy, nonatomic) NSString *intro;
@property (strong, nonatomic) NSMutableArray *list;

@end
