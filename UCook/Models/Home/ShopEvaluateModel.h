//
//  ShopEvaluateModel.h
//  UCook
//
//  Created by scihi on 14-9-9.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopEvaluateModel : NSObject

@property (copy, nonatomic) NSString *reviewId;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSString *rating;
@property (copy, nonatomic) NSString *customerId;
@property (copy, nonatomic) NSString *customerName;
@property (copy, nonatomic) NSString *photos_exit;
@property (copy, nonatomic) NSString *images;

@end
