//
//  UcookbModel.h
//  UCook
//
//  Created by scihi on 14-8-11.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UcookbModel : NSObject

@property (nonatomic) CGFloat currency;
@property (nonatomic) CGFloat points;

@end
