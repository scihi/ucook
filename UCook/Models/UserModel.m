//
//  UserModel.m
//  tywj
//
//  Created by chenhai on 14-3-26.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "UserModel.h"
#import "NSDictionaryAdditions.h"

@implementation UserModel


- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        [self setTheAccount:[dict getStringValueForKey:@"username" defaultValue:@""]];
        [self setThePassword:[dict getStringValueForKey:@"password" defaultValue:@""]];
        [self setTheUid:[NSString stringWithFormat:@"%d",[dict getIntValueForKey:@"uid" defaultValue:NO]]];
        
        [self setRole:[dict getStringValueForKey:@"role" defaultValue:@""]];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    UserModel *newModel = [[UserModel allocWithZone:zone]init];
    [newModel setTheAccount:_theAccount];
    [newModel setThePassword:_thePassword];
    [newModel setTheUid:_theUid];
    
    [newModel setRole: _role];
    return newModel;
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    
    return [self copyWithZone:zone];
}

@end
