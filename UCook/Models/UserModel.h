//
//  UserModel.h
//  tywj
//
//  Created by chenhai on 14-3-26.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject <NSCopying, NSMutableCopying>

@property (nonatomic, copy) NSString *theAccount;
@property (nonatomic, copy) NSString *thePassword;
@property (nonatomic, copy) NSString *theUid;

@property (copy, nonatomic) NSString *role;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
