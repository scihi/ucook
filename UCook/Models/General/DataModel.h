//
//  DataModel.h
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TakeDeliveryAddressModel;

@interface DataModel : NSObject

@property (nonatomic, copy) NSString *data;

//商品包
@property (nonatomic, strong) TakeDeliveryAddressModel *takeDeliveryAddressModel;

@end
