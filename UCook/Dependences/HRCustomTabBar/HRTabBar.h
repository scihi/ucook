//
//  HRTabBar.h
//
//  Created by scihi on 14-7-02.
//  Copyright (c) 2014年 huangrun All rights reserved.
//

#import <UIKit/UIKit.h>
@class HRTabBar;

@protocol HRTabBarDelegate <NSObject>
/**
 *  工具栏按钮被选中, 记录从哪里跳转到哪里. (方便以后做相应特效)
 */
//- (void) tabBar:(HRTabBar *)tabBar selectedFrom:(NSInteger) from to:(NSInteger)to;
- (void) tabBar:(HRTabBar *)tabBar selectedFrom:(NSInteger) from to:(NSInteger)to button:(UIButton *)button;//增加button 作用是当tabBar跳转到指定页面如登录页时好判断逻辑并控制跳转 huangrun修改

@end

@interface HRTabBar : UIView
@property(nonatomic,weak) id<HRTabBarDelegate> delegate;
@property (nonatomic, weak) UIButton *selectedBtn;
/**
 *  使用特定图片来创建按钮, 这样做的好处就是可扩展性. 拿到别的项目里面去也能换图片直接用
 *
 *  @param image         普通状态下的图片
 *  @param selectedImage 选中状态下的图片
 */
-(void)addButtonWithImage:(UIImage *)image selectedImage:(UIImage *) selectedImage;
@end
