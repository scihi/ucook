
#import <Foundation/Foundation.h>

@interface NSString (RegexKitLiteAdditionsEx)

+ (NSString*)GetMobileNumber:(NSString*)str;
+ (NSString*)replaceReturnString:(NSString*)str;

+ (BOOL)isDigitalString:(NSString*)str;
+ (BOOL)isValidatedEmail:(NSString*)str;
+ (BOOL)isChineseWords:(NSString*)str;
+ (BOOL)isValidatedMoney:(NSString*)str;
+ (BOOL)isValidatedMobilePhoneNumber:(NSString *)str;

+ (NSString *)processNewsHtml:(NSString *)htmlContent;

+ (NSArray*)getSplitArray:(NSString*)str spit:(NSString*)splitString;

@end
