
#import "RegexKitLiteAdditions.h"
#import "RegexKitLite.h"

@implementation NSString (RegexKitLiteAdditionsEx)

+ (NSString*)GetMobileNumber:(NSString*)str
{
    //NSString* regEx = @"^((\\+86)|(86))?\\d{11}";
    //NSString* regEx = @"^((\\+86)|(86))?(13)\\d{9}$";
    //NSString* regEx = @"^((\\+86)|(86))?\\d{11}$";
    NSString* regEx = @"^(1[3|5|8])\\d{9}$";
    //NSString* regEx = @"^(130|131|132|133|134|135|136|137|138|139)\\d{8}$";
    //NSString* regEx = @"[0-9]{3}-[0-9]{3}-[0-9]{4}";
    //NSString* regEx = @"[0-9]{11}";
    int iCount = 0;
    NSString* strippedNumber = nil;
    NSString* newPhone = nil;
    NSRange range;
    
    range.location=0;
    range.length=2;
    
    if(!str|| [str isEqualToString:@""])
        return nil;
    
    //去除所有除[0-9]的字符 
    newPhone = [str stringByReplacingOccurrencesOfRegex:@"[^0-9]" withString:@""];
    if(!newPhone)
        return nil;
    
    //去除86
    if([[newPhone substringWithRange:range] isEqualToString:@"86"])
    {
        newPhone = [newPhone substringFromIndex:2];
    }
    
    //判断是否手机号
    for (NSString *match in [newPhone componentsMatchedByRegex:regEx]) 
    {
        NSLog(@ "Phone number is %@" , match); 
        iCount++;
        strippedNumber = match;
    }
    
    if(iCount<=0)
        return nil;
    
    return strippedNumber;
}

+ (NSString*)replaceReturnString:(NSString*)str
{
    return [str stringByReplacingOccurrencesOfRegex:@"\\n\\n" withString:@""];
}

+ (BOOL)isDigitalString:(NSString*)str
{
    NSString* regexString = @"^[0-9]*$";
    
    if(!str|| [str isEqualToString:@""])
        return NO;
    
    NSInteger portInteger = [[str stringByMatching:regexString capture:1L] integerValue];
    return (portInteger>0) ? YES : NO;
}
+ (BOOL)isValidatedEmail:(NSString*)str
{
	NSString *regEx = @"^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
	NSString *match = nil;
	
	match = [str stringByMatching:regEx];
    if(!match)
        return NO;
	if(match.length<=0)
		return NO;
    
    return YES;
}
+ (BOOL)isChineseWords:(NSString*)str
{
    return YES;
}

+(BOOL)isValidatedMoney:(NSString *)str
{
    NSString *regex = @"^\\d*\\.?\\d{0,2}$";
//    NSPredicate *Test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
//    return [Test evaluateWithObject:str];
    return [str isMatchedByRegex:regex];
}

+ (BOOL)isValidatedMobilePhoneNumber:(NSString *)str
{
//	return [str isMatchedByRegex:@"(^(130|131|132|133|155|156|185|186)\\d{8}$)|(^0\\d{10,11}$)"];
	return [str isMatchedByRegex:@"(^(13|14|15|18)\\d{9}$)|(^0\\d{10,11}$)"];
}
+ (NSString *)processNewsHtml:(NSString *)htmlContent
{
//	//去掉换行
//	NSString *res = [[NSString stringWithString:htmlContent] stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];

	//去掉换行
    NSString *res = [[NSString stringWithString:htmlContent] stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];

    //替换html换行为字符串换行
    res = [[NSString stringWithString:htmlContent] stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];

    //替换html
    res = [res stringByReplacingOccurrencesOfRegex:@"<[^>]+>" withString:@" "];
    
    //替换html特殊字符
	//res = [res stringByReplacingOccurrencesOfRegex:@"&nbsp;&nbsp;&nbsp;&nbsp;" withString:@" "];
	res = [res stringByReplacingOccurrencesOfRegex:@"&nbsp;" withString:@""];
	res = [res stringByReplacingOccurrencesOfRegex:@"&ldquo;" withString:@"\""];
	res = [res stringByReplacingOccurrencesOfRegex:@"&mdash;" withString:@"-"];
    res = [res stringByReplacingOccurrencesOfRegex:@"&rdquo;" withString:@""];
    
//	//掉去顶部,底部js
//	res = [res stringByReplacingOccurrencesOfRegex:@"<script(.(?!/script))*/js/(top|bottom).js(.(?!script))*</script>" withString:@""];
//    
//	//去掉顶部导航链接
//	res = [res stringByReplacingOccurrencesOfRegex:@"<table(.(?!/table))*cc201001051710287061\\.gif(.(?!table))*</table>" withString:@""];
//	
//	//去掉更多，上下翻页等
//	res = [res stringByReplacingOccurrencesOfRegex:@"<iframe (.(?!/iframe))*sendMail.asp(.(?!iframe))*</iframe>" withString:@""];
//	res = [res stringByReplacingOccurrencesOfRegex:@"<iframe (.(?!/iframe))*nexttitle.asp(.(?!iframe))*</iframe>" withString:@""];
//	
//	//去掉列表页，顶部tab导航
//	res = [res stringByReplacingOccurrencesOfRegex:@"<table(.(?!/table))*hot_.{1,10}asp(.(?!table))*</table>" withString:@""];
//	
//	//去掉底部其他话题 div
//	res = [res stringByReplacingOccurrencesOfRegex:@"<div(.(?!/div))*/iphone/aboutus/(hot_.{1,10}|list_topic\\.)asp(.(?!div))*</div>" withString:@""];
	
	return	res;
}

+ (NSArray*)getSplitArray:(NSString*)str spit:(NSString*)splitString
{
    return [str componentsSeparatedByString: splitString];
    //NSArray *chunks = [string componentsSeparatedByString: @":"];
}
@end
