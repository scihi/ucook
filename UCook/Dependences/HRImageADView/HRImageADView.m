
#import "HRImageADView.h"
#import "UIButton+WebCache.h"
#import "ADModel.h"
#import "UIButton+AFNetworking.h"

@interface HRImageADView ()
- (void)handleTimer:(NSTimer *)timer;
- (void)buttonPressed:(id)sender;
@end

@implementation HRImageADView

@synthesize theDataSourceArray;
//@synthesize theCurrentIndex;
@synthesize delegate;
@synthesize thePageControl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView* iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        iconImageView.image = [UIImage imageNamed:@"login_bg_top.png"];
        //iconImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:iconImageView];
        
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{    
    if(delegate)
        [delegate imageADButtonPressed:nil index:0];
}

- (void)changeDataSourceArray:(NSArray*)array
{
    if(theTimer)
    {
        [theTimer invalidate];
        theTimer=nil;
    }
    
    theDataSourceArray = [array mutableCopy];
    if(!theDataSourceArray)
        return;
        
    if(theScrollView)
    {
        [theScrollView removeFromSuperview];
        
        [thePageControl removeFromSuperview];

    }
    theScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    theScrollView.backgroundColor = [UIColor whiteColor];
    [theScrollView setCanCancelContentTouches:NO];
    theScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    theScrollView.showsHorizontalScrollIndicator = NO;
    theScrollView.showsVerticalScrollIndicator = NO;
    theScrollView.clipsToBounds = YES;
    theScrollView.scrollEnabled = YES;
    theScrollView.pagingEnabled = YES;
    theScrollView.bounces = NO;//防止首张或末张继续滑动露出下层View的背景 huangrun 20140517
    theScrollView.delegate = self;
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGRect fitRect = CGRectMake(0, 0, width, height);
    
    if (theDataSourceArray.count != 0) {
    UIButton *firstButton = [[UIButton alloc]initWithFrame:fitRect];
    ADModel* model = [theDataSourceArray lastObject];
    [firstButton setBackgroundImageWithURL:[NSURL URLWithString:model.image] forState:UIControlStateNormal placeholderImage:kDefaultImage];
    [theScrollView addSubview:firstButton];
    }
    
    for(int i=0;i<theDataSourceArray.count;i++)
    {
        ADModel* model = [theDataSourceArray objectAtIndex:i];
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectZero];
        button.tag = 100+i;
        button.frame = CGRectMake((i + 1)*theScrollView.frame.size.width, 0, theScrollView.frame.size.width, theScrollView.frame.size.height);
        //[button setBackgroundImageWithURL:[NSURL URLWithString:record.theImageUrlAFile] placeholderImage:[UIImage imageNamedWithScheme:@"imageAD"]];
        [button setBackgroundImageWithURL:[NSURL URLWithString:model.image] forState:UIControlStateNormal placeholderImage:kDefaultImage];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [theScrollView addSubview:button];
    }
    
    if (theDataSourceArray.count != 0) {
    UIButton *lastButton = [[UIButton alloc]initWithFrame:CGRectMake(width*(array.count+1), 0, width, height)];
    ADModel* model2 = [theDataSourceArray firstObject];
    [lastButton setBackgroundImageWithURL:[NSURL URLWithString:model2.image] forState:UIControlStateNormal placeholderImage:kDefaultImage];
    [theScrollView addSubview:lastButton];
    }
    
    theScrollView.contentSize = CGSizeMake(theScrollView.frame.size.width*(array.count + 2), theScrollView.frame.size.height);
    [self addSubview:theScrollView];
    
//    thePageControl = [[UIPageControl alloc]initWithFrame:CGRectMake((self.frame.size.width-120)/2, self.frame.size.height-28, 120, 28)];
    thePageControl.hidesForSinglePage = YES;
    thePageControl.userInteractionEnabled = NO;
    thePageControl.numberOfPages = theDataSourceArray.count;
    thePageControl.alpha = 0.8f;
    thePageControl.backgroundColor = [UIColor clearColor];
    [thePageControl setCurrentPage:0];
    thePageControl.pageIndicatorTintColor = [UIColor blackColor];
    thePageControl.currentPageIndicatorTintColor = kThemeColor;
    [self addSubview:thePageControl];
    
//    theTimer = [NSTimer scheduledTimerWithTimeInterval:2 //HR修改 默认为3 让首页图片广告滚动减慢一点
//                                                target:self
//                                              selector:@selector(nextPage:)
//                                              userInfo:nil
//                                               repeats:YES];
    
    [theScrollView scrollRectToVisible:CGRectMake(width, 0, width, height) animated:NO];
    if (theDataSourceArray.count != 1)
    theTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(nextPage:) userInfo:nil repeats:YES];
    else {
        theScrollView.scrollEnabled = NO;
    }//huangrun优化
    
//    theCurrentIndex = 0;
}

//#pragma mark -
//#pragma mark scroll text
//- (void)handleTimer:(id)sender
//{
//    if(!theDataSourceArray)
//        return;
//    if(theDataSourceArray.count<=1)
//        return;
//
//    theCurrentIndex++;
//    if(theCurrentIndex>=theDataSourceArray.count)
//        theCurrentIndex = 0;
//    
////    theScrollView.contentOffset = CGPointMake(theScrollView.frame.size.width * theCurrentIndex, theScrollView.contentOffset.y);
//    [theScrollView setContentOffset:CGPointMake(theScrollView.frame.size.width * theCurrentIndex, theScrollView.contentOffset.y) animated:YES];//此方法带动画 huangrun
//    thePageControl.currentPage = theCurrentIndex;
//
//}

#pragma mark -
#pragma mark button event
- (void)buttonPressed:(id)sender
{
    if(delegate)
        [delegate imageADButtonPressed:sender index:thePageControl.currentPage];
}

#pragma mark -
#pragma mark scrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    if(!theTimer)
//        
//        theTimer = [NSTimer scheduledTimerWithTimeInterval:4
//                                                     target:self
//                                                   selector:@selector(handleTimer:)
//                                                   userInfo:nil
//                                                    repeats:YES];//HR添加 手动时禁止自动
//    
//    int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
//    int iCount = theDataSourceArray ? theDataSourceArray.count : 0;
//    
////    NSLog(@"%i, %f, %f", index, scrollView.contentOffset.x, scrollView.contentSize.width);//0627最后版本优化注销注释
//    
//    if(index<iCount)
//    {
//        theCurrentIndex = index;
//        thePageControl.currentPage = theCurrentIndex;
//    }
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView //HR添加 手动时禁止自动
{
    
    [theTimer invalidate];
    theTimer = nil;
}

//自动滚动到下一页
-(IBAction)nextPage:(id)sender
{
    
    CGFloat pageWidth = theScrollView.frame.size.width;
    int currentPage = theScrollView.contentOffset.x/pageWidth;
    if (currentPage == 0) {
        thePageControl.currentPage = theDataSourceArray.count-1;
    }
    else if (currentPage == theDataSourceArray.count+1) {
        thePageControl.currentPage = 0;
    }
    else {
        thePageControl.currentPage = currentPage-1;
    }
    int currPageNumber = thePageControl.currentPage;
    CGSize viewSize = theScrollView.frame.size;
    CGRect rect = CGRectMake((currPageNumber+2)*pageWidth, 0, viewSize.width, viewSize.height);
    [theScrollView scrollRectToVisible:rect animated:YES];
    currPageNumber++;
    if (currPageNumber == theDataSourceArray.count) {
        CGRect newRect=CGRectMake(viewSize.width, 0, viewSize.width, viewSize.height);
        [theScrollView scrollRectToVisible:newRect animated:NO];
        currPageNumber = 0;
    }
    thePageControl.currentPage = currPageNumber;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    CGFloat width = theScrollView.frame.size.width;
    CGFloat heigth = theScrollView.frame.size.height;
    //当手指滑动scrollview，而scrollview减速停止的时候 开始计算当前的图片的位置
    int currentPage = theScrollView.contentOffset.x/width;
    if (currentPage == 0) {
        [theScrollView scrollRectToVisible:CGRectMake(width*theDataSourceArray.count, 0, width, heigth) animated:NO];
        thePageControl.currentPage = theDataSourceArray.count-1;
    }
    else if (currentPage == theDataSourceArray.count+1) {
        [theScrollView scrollRectToVisible:CGRectMake(width, 0, width, heigth) animated:NO];
        thePageControl.currentPage = 0;
    }
    else {
        thePageControl.currentPage = currentPage - 1;
    }
    //拖动完毕的时候 重新开始计时器控制跳转
    theTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(nextPage:) userInfo:nil repeats:YES];
}

@end
