
#import <UIKit/UIKit.h>

@protocol ImageADViewDelegate <NSObject>
- (void)imageADButtonPressed:(id)sender index:(int)index;
@end

@interface HRImageADView : UIView <UIScrollViewDelegate>
{
    NSTimer*                theTimer;
    UIScrollView*           theScrollView;
    UIPageControl*          thePageControl;
}

@property (nonatomic, assign) id<ImageADViewDelegate> delegate;
@property (nonatomic, retain) NSMutableArray*   theDataSourceArray;
//@property (nonatomic, assign) int theCurrentIndex;
@property (nonatomic, strong) UIPageControl*          thePageControl;

- (void)changeDataSourceArray:(NSArray*)array;

@end
