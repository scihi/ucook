
#import <UIKit/UIKit.h>
#import "CustomTableView.h"

@interface SharePopupTableView : CustomTableView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSMutableArray* theImagesArray;
@property (nonatomic, retain) NSMutableArray* theTextArray;

@property (nonatomic, strong) UIColor *textColor;


@end
