
#import "SharePopupTableView.h"
#import "UITableViewCellAdditionals.h"
#import "UIImageView+AFNetworking.h"

@implementation SharePopupTableView {
    NSString *_currentValue;
}

@synthesize theImagesArray;
@synthesize theTextArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = self;
        self.delegate = self;
        if(IS_IOS7) {
        self.separatorInset = UIEdgeInsetsZero;// 让iOS7的分割线左边不留空白 huangrun
        }
        self.separatorColor = [UIColor blackColor];
    }
    return self;
}

#pragma mark - table view delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger iCount = theTextArray ? theTextArray.count : 0;
    iCount = theImagesArray ? theImagesArray.count : iCount;
    return  iCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [UITableViewCell getCell:tableView identifier:@"SharePopupTableView"];
    cell.backgroundColor = [UIColor clearColor];
    if(theTextArray)
    {
        cell.textLabel.textColor = _textColor;
        cell.textLabel.font = [UIFont systemFontOfSize:17];
//        cell.textLabel.adjustsFontSizeToFitWidth = YES;//huangrun注释 避免输入很长字符过后出现字符太小看不清的问题
        cell.textLabel.textAlignment = NSTextAlignmentCenter;//huangrun添加
        cell.textLabel.text = [theTextArray objectAtIndex:indexPath.row];
    }
    if(![[theImagesArray objectAtIndex:indexPath.row]isEqual:@""] && theTextArray)
       [ cell.imageView setImageWithURL:[theImagesArray objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"public_img_error.png"]];
    float sw=40/cell.imageView.image.size.width;
    float sh=40/cell.imageView.image.size.height;
    cell.imageView.transform=CGAffineTransformMakeScale(sw,sh);
    
//    NSString *item = [theTextArray objectAtIndex:indexPath.row];
//    if ([item isEqualToString:_currentValue]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    } else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }//优厨项目不需要显示勾勾

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"selectItemsNotification" object:theTextArray userInfo:@{@"index": [NSNumber numberWithInt:indexPath.row]}];

    NSString *titleString = [theTextArray objectAtIndex:indexPath.row];
    _currentValue = titleString;
    
    if(self.theCustomDelegate)
        [self.theCustomDelegate didSelectRowAtIndexPath:tableView indexPath:indexPath];
}



@end
