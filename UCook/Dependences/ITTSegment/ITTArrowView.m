//
//  ITTArrowView.m
//  AiXinDemo
//
//  Created by shaofa on 14-2-17.
//  Copyright (c) 2014年 shaofa. All rights reserved.
//

#import "ITTArrowView.h"

@implementation ITTArrowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.states = UPStates;
        self.image = [UIImage imageNamed:@"ico_price"];
    }
    return self;
}

-(void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    if (isSelected) {
        ArrowStates states =_states ^ 1;
        self.states = states;
    }else {
//        [self setStates:_states];//记住状态 如果上次是下 那么点其他回来再次选中是上 huangrun
        [self setStates:UPStates];//不记住状态 如果上次是下 那么点其他回来再次选中还是下 每次都是下 huangrun
    }
    
}

-(void)setStates:(ArrowStates)states
{
    if (states != _states) {
        _states = states;
    }
    
    if (_isSelected) {
        if (_states == UPStates) {
            self.image = [UIImage imageNamed:@"ico_price_low"];
        } else if (_states == DOWNStates) {
            self.image = [UIImage imageNamed:@"ico_price_high"];
        }
        if (self.block != nil) {
            self.block(_states);
        }
    } else {
        if (_states == UPStates) {
            self.image = [UIImage imageNamed:@"ico_price"];
        } else if (_states == DOWNStates) {
            self.image = [UIImage imageNamed:@"ico_price"];
        }
    }
}

@end
