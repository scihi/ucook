//
//  UIView+OTStatusBarReference.h
//  OTNotificationViewDemo
//
//  Created by openthread on 8/9/13.
//
//

#import <UIKit/UIKit.h>

static UIView *statusBarInstance = nil;

@interface UIView (ComOpenThreadOTScreenshotHelperStatusBarReference)

+ (UIView *)statusBarInstance_ComOpenThreadOTScreenshotHelper;

@end
