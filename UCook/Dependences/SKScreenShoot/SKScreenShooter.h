//
//  SKScreenShooter.h
//  Sidekick
//
//  Created by Alexandra Vtyurina on 07/04/14.
//  Copyright (c) 2014 Applifto Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

#define RADIANS(deg) ((deg) * M_PI / 180.0f)

@interface SKScreenShooter : NSObject

//takes a screenshot of an entire screen
+ (UIImage *)takeScreenshot;

@end
