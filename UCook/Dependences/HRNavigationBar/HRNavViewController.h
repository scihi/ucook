//
//  HRNavViewController.h
//
//  Created by scihi on 14-7-02.
//  Copyright (c) 2014年 huangrun All rights reserved.
//设置导航栏在iOS7和iOS6的兼容，作为父类使用

#import <UIKit/UIKit.h>

@interface HRNavViewController : UINavigationController

@end
    