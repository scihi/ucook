//
//  PrettySearchBar.m
//  SearchDemo
//
//  Created by lijian qiu on 12-4-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PrettySearchBar.h"



@implementation PrettySearchBar

@synthesize searchBarTextField = _searchBarTextField;
@synthesize normalBackgroundImage,highlightBackgroundImage;

//-(void)dealloc
//{
//	[_searchBarTextField release],_searchBarTextField = nil;
//	[normalBackgroundImage release],normalBackgroundImage = nil;
//	[highlightBackgroundImage release],highlightBackgroundImage = nil;
//	[super dealloc];
//}

-(id)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame]) {
		self.normalBackgroundImage = [UIImage imageNamed:@"searchFrame.png"];
		self.highlightBackgroundImage = [UIImage imageNamed:@"searchFrameHighlight.png"];
        
//        self.placeholder = @"请输入关键字";
//        self.normalBackgroundImage = [UIImage imageNamed:@"searchFrame.png"];
//        self.highlightBackgroundImage = [UIImage imageNamed:@"searchFrameHighlight.png"];
//        
//        [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"input"] forState:UIControlStateNormal];
//        [[UISearchBar appearance] setSearchTextPositionAdjustment:UIOffsetMake(38, 0)];
//        //    [[UISearchBar appearance] setSearchFieldBackgroundPositionAdjustment:UIOffsetMake(30, 0)];
//        //    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"search_replace"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];//去掉searchBar前面的搜索图标 方法二 huangrun
//        [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];//去掉searchBar前面的搜索图标 方法一 huangrun
//        //    self.tintColor = [UIColor whiteColor];
//        
//        UIButton *testButton = [UIButton buttonWithType:UIButtonTypeSystem];
//        [testButton setTitle:@"test" forState:UIControlStateNormal];
//        //    testButton.backgroundColor = [UIColor whiteColor];
//        testButton.frame = CGRectMake(10, 6, 40, 30);
//        [[UISearchBar appearance]addSubview:testButton];

	}
	return self;
}

//-(void)awakeFromNib
//{
//	[super awakeFromNib];
//	self.normalBackgroundImage = [UIImage imageNamed:@"searchFrame.png"];
//	self.highlightBackgroundImage = [UIImage imageNamed:@"searchFrameHighlight.png"];
//    
//    [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"input"] forState:UIControlStateNormal];
//    [[UISearchBar appearance] setSearchTextPositionAdjustment:UIOffsetMake(35, 0)];
////    [[UISearchBar appearance] setSearchFieldBackgroundPositionAdjustment:UIOffsetMake(30, 0)];
////    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"search_replace"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];//去掉searchBar前面的搜索图标 方法二 huangrun
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];//去掉searchBar前面的搜索图标 方法一 huangrun
////    self.tintColor = [UIColor whiteColor];
//    
//    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeSystem];
//    [testButton setTitle:@"test" forState:UIControlStateNormal];
////    testButton.backgroundColor = [UIColor whiteColor];
//    testButton.frame = CGRectMake(10, 6, 40, 30);
//    [[UISearchBar appearance]addSubview:testButton];
//}

-(void)layoutSubviews
{
	[super layoutSubviews];
	//get searchBar textField
	if ([self isFirstResponder]) {
		[self.searchBarTextField setBackground:highlightBackgroundImage];
	}else {
		[self.searchBarTextField setBackground:normalBackgroundImage];
	}

}


-(UITextField *)searchBarTextField
{
	if (_searchBarTextField == nil) {
		for (UIView *v in [self subviews]) {
			if ([v isKindOfClass:[UITextField class]]) {
				UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchFrameBg.png"]];
				[self insertSubview:imageView belowSubview:_searchBarTextField];
				return (UITextField *)v;
			}
		}
	}
	return _searchBarTextField;
}


@end
