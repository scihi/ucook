
#import <UIKit/UIKit.h>

@class CustomTableView;

@protocol CustomTableViewDelegate <NSObject>
@optional

- (void)didSelectRowAtIndexPath:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;
- (void)loadingMoreData:(CustomTableView *)tableView;

@end

@interface CustomTableView : UITableView

@property (nonatomic, assign) id<CustomTableViewDelegate> theCustomDelegate;
@property BOOL  theLoading;
@property BOOL  theFinished;
@property int   thePage;
@property BOOL  theNeedLoadingMore;

- (int)processFinishedStatus:(int)iCount;

- (void)customScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)customScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)customScrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end
