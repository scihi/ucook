
#import <Foundation/Foundation.h>

@interface CGRectAdditionals : NSObject

+ (CGRect)rectWithNewWidth:(CGRect)rect width:(CGFloat)width;
+ (CGRect)rectWithNewHeight:(CGRect)rect height:(CGFloat)height;
+ (CGRect)rectWithNewWidthAndHeight:(CGRect)rect width:(CGFloat)width height:(CGFloat)height;

+ (CGRect)rectAddWidth:(CGRect)rect width:(CGFloat)width;
+ (CGRect)rectAddHeight:(CGRect)rect height:(CGFloat)height;
+ (CGRect)rectAddWidthAndHeight:(CGRect)rect width:(CGFloat)width height:(CGFloat)height;

@end
