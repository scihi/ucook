
#import "AccountManager.h"
#import "NSDictionaryAdditions.h"
#import "NSStringAdditions.h"

@implementation AccountManager

static AccountManager *_gAccountManager = nil;

@synthesize theAccount;
@synthesize thePassword;
@synthesize theIsWriteToFile;
@synthesize theIsAutoLogin;
@synthesize theIsEnablePush;
@synthesize theIsRememberLoginInfo;
@synthesize theRegisterVarificationCode;
@synthesize theForgotPwdVarificationCode;
@synthesize theUid;
@synthesize theIsLogined;

@synthesize role;

@synthesize theCreateDate;
@synthesize theModifyDate;


#pragma mark -
#pragma mark get user default methods
- (NSString*)getUserDefaultStringValue:(NSUserDefaults*)defaults key:(NSString*)key
{
    if(![defaults stringForKey:key])
        return @"";
    return [defaults stringForKey:key];
}
- (NSString*)getUserDefaultStringValueEx:(NSUserDefaults*)defaults key:(NSString*)key defaultValue:(NSString *)defaultValue
{
    if(![defaults stringForKey:key])
        return defaultValue;
    return [defaults stringForKey:key];
}
- (BOOL)getUserDefaultBOOLValue:(NSUserDefaults*)defaults key:(NSString*)key
{
    if(![defaults stringForKey:key])
        return NO;
    return [NSString isEmptyOrWhitespace:[self getUserDefaultStringValue:defaults key:key]] ? NO : YES;
}
- (BOOL)getUserDefaultBOOLValueEx:(NSUserDefaults*)defaults key:(NSString*)key defaultValue:(BOOL)defaultValue
{
    if(![defaults stringForKey:key])
        return defaultValue;
    return [NSString isEmptyOrWhitespace:[self getUserDefaultStringValue:defaults key:key]] ? defaultValue : !defaultValue;
}

#pragma mark -
#pragma mark load and write methods
- (BOOL)LoadDataFromFile
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self setThePassword:[self getUserDefaultStringValueEx:defaults key:kPassword defaultValue:@""]];
    [self setTheAccount:[self getUserDefaultStringValueEx:defaults key:kAccount defaultValue:@""]];
    [self setTheUid:[self getUserDefaultStringValueEx:defaults key:kUid defaultValue:@""]];
    theIsAutoLogin = [self getUserDefaultBOOLValue:defaults key:kIsAutoLogin];
    theIsRememberLoginInfo = [self getUserDefaultBOOLValue:defaults key:kRememberLoginInfo];
    [self setTheRegisterVarificationCode:@""];
    [self setTheForgotPwdVarificationCode:@""];
    theIsEnablePush = [self getUserDefaultBOOLValueEx:defaults key:kEnablePush defaultValue:YES];
    
    [self setRole:[self getUserDefaultStringValueEx:defaults key:kRole defaultValue:@""]];
    
    [self setTheCreateDate:[self getUserDefaultStringValueEx:defaults key:kCreateDate defaultValue:@""]];
    [self setTheModifyDate:[self getUserDefaultStringValueEx:defaults key:kModifyDate defaultValue:@""]];


    return YES;
}

- (BOOL)WriteDataToFile
{
    NSUserDefaults			*defaults = [NSUserDefaults standardUserDefaults];
        
    [defaults setObject: self.thePassword forKey:kPassword];
    [defaults setObject: self.theAccount forKey:kAccount];
    [defaults setObject: self.theUid forKey:kUid];
    
    [defaults setObject:self.role forKey:kRole];
    
    [defaults setObject:self.theCreateDate forKey:kCreateDate];
    [defaults setObject:self.theModifyDate forKey:kModifyDate];
    
    if (self.theIsAutoLogin)
        [defaults setObject:kIsAutoLogin forKey:kIsAutoLogin];
    else
        [defaults setObject:@"" forKey:kIsAutoLogin];
    
    if (self.theIsRememberLoginInfo) {
        [defaults setObject:kRememberLoginInfo forKey:kRememberLoginInfo];
    } else {
        [defaults setObject:@"" forKey:kRememberLoginInfo];
    }

    if (self.theIsEnablePush)
        [defaults setObject: @"" forKey:kEnablePush];
    else
        [defaults setObject: kEnablePush forKey:kEnablePush];
    
    
    [defaults synchronize];
        
    return YES;
}

#pragma mark -
#pragma mark properties methods
+ (AccountManager*)sharedInstance
{
	if (_gAccountManager == nil) {
		_gAccountManager = [[AccountManager alloc] init];
        [_gAccountManager LoadDataFromFile];
	}
	
	return _gAccountManager;
}
+ (void)logout
{
    AccountManager* manager = [AccountManager sharedInstance];
    
    [manager setTheUid:@""];
    [manager setThePassword:@""];
//    if(!manager.theIsRememberLoginInfo)
//    {
//        [manager setThePassword:@""];
//    }//是否自动登录在此处写事件 huangrun
//    [manager setTheAccount:@""];
    [manager setTheRegisterVarificationCode:@""];
    [manager setTheForgotPwdVarificationCode:@""];
    [manager WriteDataToFile];

}

+ (BOOL)isSessionExist
{
    AccountManager* manager = [AccountManager sharedInstance];
    return ![manager.theUid isEmptyOrWhitespace];
}

+ (BOOL)isLogged
{
    AccountManager* manager = [AccountManager sharedInstance];
    if ([manager.theUid isEqualToString:@"0"]) {
        return NO;
    }
    return ![manager.theUid isEmptyOrWhitespace];
}

+ (NSString*)getUid
{
    return [[AccountManager sharedInstance] theUid];
}

+ (NSString*)getAccount
{
    return [[AccountManager sharedInstance] theAccount];
}
+ (NSString*)getPassword
{
    return [[AccountManager sharedInstance] thePassword];
}
+ (BOOL)getIsAutoLogin
{
    return [[AccountManager sharedInstance] theIsAutoLogin];
}
+ (BOOL)getIsEnablePush
{
    return [[AccountManager sharedInstance] theIsEnablePush];
}
+ (NSString*)getDeviceToken
{
    return [[AccountManager sharedInstance] theDevideToken];
}
+ (BOOL)getIsRememberLoginInfo
{
    return [[AccountManager sharedInstance] theIsRememberLoginInfo];
}

+ (NSString *)getRole
{
    return [[AccountManager sharedInstance] role];
}

+ (NSString *)getCreateDate {
    return [[AccountManager sharedInstance] theCreateDate];
}
+ (NSString *)getModifyDate {
    return [[AccountManager sharedInstance] theModifyDate];
}
@end

