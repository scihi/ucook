
#import "CustomTableView.h"

@implementation CustomTableView

@synthesize theCustomDelegate;
@synthesize theFinished;
@synthesize theLoading;
@synthesize thePage;
@synthesize theNeedLoadingMore;

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) {
        theFinished = NO;
        thePage = 0;
        theLoading = NO;
        theNeedLoadingMore = NO;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame style:UITableViewStylePlain];
}

#pragma mark -
#pragma mark public methods
- (int)processFinishedStatus:(int)iCount
{
    theFinished = iCount<PAGE_SIZE_LIMITED ? YES : NO;
    theLoading = NO;
    
    return thePage;
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)customScrollViewDidScroll:(UIScrollView *)scrollView
{
    if(!theNeedLoadingMore)
        return;
    
    if(theFinished)
        return;
    
    if(theLoading)
        return;
    
    if((scrollView.contentOffset.y+scrollView.frame.size.height) > scrollView.contentSize.height)
    {
        theLoading = YES;
        thePage += 1;//csmao页面加1
        if(theCustomDelegate && [theCustomDelegate respondsToSelector:@selector(loadingMoreData:)])
            [theCustomDelegate loadingMoreData:self];
    }
}
- (void)customScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}
- (void)customScrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

@end
