
#import "CGRectAdditionals.h"

@implementation CGRectAdditionals

+ (CGRect)rectWithNewWidth:(CGRect)rect width:(CGFloat)width
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(width, rect.size.height);
    
    newRect.size = size;
    
    return newRect;
}
+ (CGRect)rectWithNewHeight:(CGRect)rect height:(CGFloat)height
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(rect.size.width, height);
    
    newRect.size = size;
    
    return newRect;
}
+ (CGRect)rectWithNewWidthAndHeight:(CGRect)rect width:(CGFloat)width height:(CGFloat)height
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(rect.size.width, rect.size.height);
    
    newRect.size = size;
    
    return newRect;
}

+ (CGRect)rectAddWidth:(CGRect)rect width:(CGFloat)width
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(rect.size.width + width, rect.size.height);
    
    newRect.size = size;
    
    return newRect;
}

+ (CGRect)rectAddHeight:(CGRect)rect height:(CGFloat)height
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(rect.size.width, rect.size.height + height);
    
    newRect.size = size;
    
    return newRect;
}

+ (CGRect)rectAddWidthAndHeight:(CGRect)rect width:(CGFloat)width height:(CGFloat)height
{
    CGRect newRect = rect;
    CGSize size = CGSizeMake(rect.size.width + width, rect.size.height + height);
    
    newRect.size = size;
    
    return newRect;
}

@end
