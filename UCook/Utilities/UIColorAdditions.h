
#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+ (UIColor*)colorWithRGBAHex:(int)r g:(int)g b:(int)b alphi:(float)alphi;

+ (UIColor*)titleTextColor:(CGFloat)alphi;
+ (UIColor*)calendarTextTextColor:(CGFloat)alphi;

@end
