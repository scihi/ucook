
#import "UITableViewCellAdditionals.h"
#import "UIColorAdditions.h"

@implementation UITableViewCell (Additionals)

+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier accessoryImage:(NSString*)accessoryImageFile target:(id)buttonTarget sel:(SEL)buttonSel row:(int)row;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)] ;
        [button setBackgroundImage:[UIImage imageNamed:accessoryImageFile] forState:UIControlStateNormal];
        [button addTarget:buttonTarget action:buttonSel forControlEvents:UIControlEventTouchUpInside];
        button.tag = row;
        cell.accessoryView = button;
    }
    
    return cell;
}

+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier
{
    //static NSString *CellIdentifier = identifier;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        //cell.textLabel.textColor = [UIColor titleTextColor:1.0f];
    }
    
    return cell;
}

+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier backgroundImage:(UIImage*)backgroundImage
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        if(backgroundImage)
            cell.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage ];
        else
            cell.backgroundView = nil;
        
//        cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cell_normal.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
//        cell.selectedBackgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cell_pressed.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    }
    
    return cell;
}

@end
