
#import "UIColorAdditions.h"

@implementation UIColor (Additions)

+ (UIColor*)colorWithRGBAHex:(int)r g:(int)g b:(int)b alphi:(float)alphi
{
    float fr = (float)r;
    float fg = (float)g;
    float fb = (float)b;
    
    return [UIColor colorWithRed:fr/255 green:fg/255 blue:fb/255 alpha:alphi];
}

+ (UIColor*)titleTextColor:(CGFloat)alphi
{
    return [self colorWithRGBAHex:0x55 g:0x2a b:0x08 alphi:1.0];
}

+ (UIColor*)calendarTextTextColor:(CGFloat)alphi
{
//    return [self colorWithRGBAHex:0x55 g:0x2a b:0x08 alphi:alphi];
    return [self colorWithRGBAHex:0x25 g:0xdb b:0xf9 alphi:alphi];
}

@end
