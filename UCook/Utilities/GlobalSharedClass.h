//
//  GlobalShareSource.h
//  shareApp
//
//  Created by liu sy on 12-7-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalSharedClass : NSObject{
    NSString *userID;
}
+(GlobalSharedClass*)sharedClass;



//检查文件是否存在
+(BOOL)checkFileExsit:(NSString *)fileName Dir:(NSString *)fileDir;
+ (UIAlertView *)showAlertView:(NSString *)message;
+ (UIAlertView *)showAlertView:(NSString *)message delegate:(id)delegate;
+ (UIAlertView *)showAlertViewWithOneBtn:(NSString *)message delegate:(id)delegate;
+ (UIActivityIndicatorView *)activityIndicatorView:(CGRect)frame;
@end
