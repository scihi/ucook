
#import <Foundation/Foundation.h>

@class VersionRecord;

#define kAccount				@"Account"
#define kPassword				@"Password"
#define kIsAutoLogin            @"AutoLogin"
#define kEnablePush             @"EnablePush"
#define kUid				    @"Uid"
#define kRememberLoginInfo      @"RememberLoginInfo"

#define kRole                   @"Role"

#define kCreateDate             @"CreateDate"
#define kModifyDate             @"ModifyDate"

@interface AccountManager : NSObject

@property (nonatomic, copy) NSString*       theAccount;
@property (nonatomic, copy) NSString*       thePassword;
@property (nonatomic, assign) BOOL          theIsWriteToFile;
@property (nonatomic, assign) BOOL          theIsAutoLogin;
@property (nonatomic, assign) BOOL          theIsLogined;
@property (nonatomic, assign) BOOL          theIsEnablePush;
@property (nonatomic, assign) BOOL          theIsRememberLoginInfo;
@property (nonatomic, copy) NSString*       theRegisterVarificationCode;
@property (nonatomic, copy) NSString*       theForgotPwdVarificationCode;
@property (nonatomic, copy) NSString*       theUid;
@property (nonatomic, copy) NSString*       theDevideToken;

@property (copy, nonatomic) NSString *role; //用于判断登录角色

@property (nonatomic, copy) NSString*       theCreateDate;
@property (nonatomic, copy) NSString*       theModifyDate;//此两项为车管家项目需要



+ (AccountManager*)sharedInstance;

- (BOOL)WriteDataToFile;

+ (void)logout;

+ (BOOL)isLogged;

+ (NSString*)getAccount;
+ (NSString*)getPassword;
+ (BOOL)getIsAutoLogin;
+ (BOOL)getIsRememberLoginInfo;
+ (BOOL)getIsEnablePush;
+ (BOOL)getIsLogined;
+ (NSString*)getUid;
+ (NSString*)getDeviceToken;

+(NSString *)getRole;

+ (NSString *)getCreateDate;
+ (NSString *)getModifyDate;

@end
