//
//  GlobalShareSource.m
//  shareApp
//
//  Created by liu sy on 12-7-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "GlobalSharedClass.h"

#define FileDir @"SHARE_HOUSE"


@implementation GlobalSharedClass
static GlobalSharedClass *sharedClass = nil;

-(id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

+(GlobalSharedClass*)sharedClass
{
    @synchronized(self) {
        if (sharedClass == nil) {
            sharedClass =[[self alloc] init]; 
        }
    }
    return sharedClass;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedClass == nil) {
            sharedClass = [super allocWithZone:zone];
            return sharedClass;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    
    return self;
}

//检查文件是否存在
+(BOOL)checkFileExsit:(NSString *)fileName Dir:(NSString *)fileDir{
    BOOL result = NO;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *testPath = [[documentDirectory stringByAppendingPathComponent:FileDir] stringByAppendingPathComponent:fileName];
    NSFileManager *fm = [NSFileManager defaultManager];
    if([fm fileExistsAtPath:testPath]){
        result = YES;
    }
    return result;
}

//以下为自创的方便方法 huangrun
+ (UIAlertView *)showAlertView:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    return alertView;
}

+ (UIAlertView *)showAlertView:(NSString *)message delegate:(id)delegate
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:delegate cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
    return alertView;
}

+ (UIAlertView *)showAlertViewWithOneBtn:(NSString *)message delegate:(id)delegate {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    return alertView;
}

+(UIActivityIndicatorView *)activityIndicatorView:(CGRect)frame
{
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView setCenter:CGPointMake(frame.size.height / 2, frame.size.width / 2)];
    return activityIndicatorView;
}

@end
