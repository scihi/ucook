
#import <UIKit/UIKit.h>

@interface UITableViewCell (Additionals)

+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier;
+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier accessoryImage:(NSString*)accessoryImageFile target:(id)buttonTarget sel:(SEL)buttonSel row:(int)row;

+ (UITableViewCell*)getCell:(UITableView*)tableView identifier:(NSString*)CellIdentifier backgroundImage:(UIImage*)backgroundImage;

@end
