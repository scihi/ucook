//
//  ClassifyViewController.h
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"

@interface ClassifyViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate>

@end
