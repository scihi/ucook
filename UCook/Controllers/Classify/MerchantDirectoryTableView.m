
#import "MerchantDirectoryTableView.h"
#import "UITableViewCellAdditionals.h"


@implementation MerchantDirectoryTableView

@synthesize theDataSourceArray;
@synthesize theCurrentRow;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        /*
        theDataSourceArray = [[NSMutableArray alloc] init];
        [theDataSourceArray addObject:@"成都"];
        [theDataSourceArray addObject:@"德阳"];
        [theDataSourceArray addObject:@"自贡"];
        [theDataSourceArray addObject:@"绵阳"];
        [theDataSourceArray addObject:@"德州"];*/
        theCurrentRow = 0;
    }
    return self;
}


- (void)deselectCurrentRow:(int)newRow
{
    NSIndexPath* currentIndexPath = [NSIndexPath indexPathForRow:theCurrentRow inSection:0];

    if(newRow!=theCurrentRow)
    {
        [self deselectRowAtIndexPath:currentIndexPath animated:YES];
    }
    
    theCurrentRow = newRow;
}

- (void)selectNewRow:(int)newRow
{
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:newRow inSection:0];
    [self selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];    
}

#pragma mark - table view delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  theDataSourceArray ?  [theDataSourceArray count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [UITableViewCell getCell:tableView identifier:@"MerchantDirectoryTableViewCell"];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = [theDataSourceArray objectAtIndex:indexPath.row];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = kTableViewBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
