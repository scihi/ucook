//
//  SubClassifyViewController.m
//  UCook
//
//  Created by huangrun on 14-7-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SubClassifyViewController.h"
#import "CGRectAdditionals.h"
#import "SearchViewController.h"
#import "MoreViewController.h"

@interface SubClassifyViewController () {
    NSMutableArray *_theFirstNameArr;//第一个列表的名字数组
    NSMutableArray *_theSecondNameArr;//第二个列表的名字数组
    NSMutableArray *_theSecondIdArr;//第二个列表的id数组
    NSMutableArray *_theSelectedSecondIdArr;//选中的第二个列表的id数组
}

@end

@implementation SubClassifyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64)];
    bgIV.image = [UIImage imageNamed:@"public_bg.png"];
    [self.view addSubview:bgIV];//尤其注意背景图片的坐标 如果设置不恰当 会使底部留白 会误认为tabBar没隐藏等 huangrun

    [self configTheNavBar];
    
    _theFirstNameArr = [NSMutableArray array];

    for (NSDictionary *dictionary in _theFirstDataArr) {
        [_theFirstNameArr addObject:[dictionary objectForKey:@"name"]];
    }
    
    _theSecondNameArr = [[NSMutableArray alloc]init];
    _theSecondIdArr = [NSMutableArray arrayWithCapacity:10];
    
    //遍历名称
    NSMutableArray *array;
    for (NSDictionary *dictionary in _theSecondDataArr) {
        array = [[NSMutableArray alloc]init];
        for (NSDictionary *dictionary2 in [dictionary objectForKey:@"sub"]) {
            
            [array addObject:[dictionary2 objectForKey:@"name"]];
        }
        
        [_theSecondNameArr addObject:array];
    }
    
    //遍历id
    NSMutableArray *array2;
    for (NSDictionary *dictionary in _theSecondDataArr) {
        array2 = [[NSMutableArray alloc]init];
        for (NSDictionary *dictionary2 in [dictionary objectForKey:@"sub"]) {
            
            [array2 addObject:[dictionary2 objectForKey:@"id"]];
        }
        
        [_theSecondIdArr addObject:array2];
    }
    
    const int leftWidth = 100;
    theTableView = [[MerchantDirectoryTableView alloc] initWithFrame:CGRectZero];
    theTableView.tag = 100;
    theTableView.frame = [CGRectAdditionals rectWithNewWidth:[self BottomViewRect] width:leftWidth];
    theTableView.dataSource = theTableView;
    theTableView.delegate = self;
    theTableView.theDataSourceArray = _theFirstNameArr;
    [self.view addSubview:theTableView];
    
    theSubTableView = [[MerchantSubDirectoryTableView alloc] initWithFrame:CGRectZero];
    theSubTableView.backgroundColor = kTableViewBackgroundColor;
    theSubTableView.tag = 101;
    theSubTableView.frame = CGRectOffset([self BottomViewRect], leftWidth, 0);
    theSubTableView.frame = [CGRectAdditionals rectWithNewWidth:theSubTableView.frame width:[self BottomViewRect].size.width - leftWidth];
    theSubTableView.dataSource = theSubTableView;
    theSubTableView.delegate = self;
    [self reloadSubTableViewData:0];
    [self.view addSubview:theSubTableView];
    
    [theTableView selectNewRow:0];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadSubTableViewData:(int)index
{
    if (_theSecondNameArr.count != 0)
    theSubTableView.theDataSourceArray = [_theSecondNameArr objectAtIndex:index];
    if (_theSecondIdArr.count != 0)
    _theSelectedSecondIdArr = [_theSecondIdArr objectAtIndex:index];
    [theSubTableView reloadData];
}

#pragma mark - table view delegate and datasource 左和右tableView的delegate都在这里实现
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 100) {
        [theTableView deselectCurrentRow:indexPath.row];
        [self reloadSubTableViewData:indexPath.row];
    } else {
        MoreViewController *moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        moreVC.categoryIdStr = [_theSelectedSecondIdArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:moreVC animated:YES];
    }
}

- (void)configTheNavBar {
//    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
//    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
//    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
//    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_map.png"]];
//    navLeftIV.frame = CGRectMake(10, 5, 30, 24);
//    [navLeftBtn addSubview:navLeftIV];
//    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
//    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_scan.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;
    
}

- (void)clickNavRightBtn:(id)sender {
    
}

- (void)clickNavTitleBtn:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

@end

