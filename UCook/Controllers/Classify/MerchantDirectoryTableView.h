//二级列表中的左列表 huangrun
#import <UIKit/UIKit.h>

@interface MerchantDirectoryTableView : UITableView <UITableViewDataSource>

@property (nonatomic, retain) NSMutableArray*       theDataSourceArray;
@property (nonatomic, assign) int   theCurrentRow;

- (void)deselectCurrentRow:(int)newRow;
- (void)selectNewRow:(int)newRow;

@end
