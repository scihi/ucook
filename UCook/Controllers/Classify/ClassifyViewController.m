//
//  ClassifyViewController.m
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ClassifyViewController.h"
#import "SearchViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SubClassifyViewController.h"
#import "MapViewController.h"

@interface ClassifyViewController () {
    UITableView *_theTableView;
    NSArray *_theDataArr;
    NSArray *_theFirstDataArr;//下个视图(分类列表)中的第一级分类的数组
    NSArray *_theThirdDataArr;//下个视图(分类列表)中的第二级分类的数组
    
    NSArray *_mapDataArr;//地图数组
}

@end

@implementation ClassifyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64 - 49:568 - 64 - 49 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    [self configTheNavBar];
    
    _theTableView = [[UITableView alloc]initWithFrame:kTableViewFrame];
//    _theTableView.backgroundColor = [UIColor clearColor];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self getTableViewData];
    
    [self getMapData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 地图
- (void)clickNavLeftBtn:(id)sender {
    if (!_mapDataArr) {
        return;
    }
    MapViewController *mapVC = [[MapViewController alloc]init];
    mapVC.mapDataArr = _mapDataArr;
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (void)clickNavRightBtn:(id)sender {
    [GlobalSharedClass showAlertView:@"功能开发中，敬请期待！"];
}

- (void)clickNavTitleBtn:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)configTheNavBar {
    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_map.png"]];
    navLeftIV.frame = CGRectMake(10, 5, 30, 24);
    [navLeftBtn addSubview:navLeftIV];
    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_scan.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _theDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ClassifyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSString *cellImgStr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"pic"];
    [cell.imageView setImageWithURL:[NSURL URLWithString:cellImgStr] placeholderImage:kDefaultImage];
    cell.textLabel.text = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"name"];
    _theFirstDataArr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"sub"];
    NSString *_subNameStr = @"";//_subDataArr里商品的名称
    for (NSDictionary *dictionary in _theFirstDataArr) {
        NSString *nameString = [dictionary objectForKey:@"name"];//_subDataArr里每个商品的名称
        NSString *string = [NSString stringWithFormat:@"%@/",nameString];
        _subNameStr = [_subNameStr stringByAppendingString:string];
    }
    
    if (_subNameStr.length != 0){
    NSString *wantString = [_subNameStr substringToIndex:_subNameStr.length - 1];
            cell.detailTextLabel.text = wantString;
    }

    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    return cell;
}
//列表展示用的数据
- (void)getTableViewData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"category",@"action": @"index",@"level": @2};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _theDataArr = [[responseObject objectForKey:@"data"]mutableCopy];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [_theFirstDataArr removeAllObjects];
    _theFirstDataArr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"sub"];
    SubClassifyViewController *subClassifyVC = [[SubClassifyViewController alloc]init];
    subClassifyVC.theFirstDataArr = _theFirstDataArr;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"category",@"action": @"index",@"level": @2,@"categoryId": [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"id"]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            subClassifyVC.theSecondDataArr = [[responseObject objectForKey:@"data"] mutableCopy];
            subClassifyVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:subClassifyVC animated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)getMapData {
    //获取地图数据
    AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
    manager2.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters2 = @{@"key": APP_KEY,@"controller": @"store",@"action": @"marketList"};
    [manager2 POST:BASIC_URL parameters:parameters2 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            _mapDataArr = [[responseObject objectForKey:@"data"]mutableCopy];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

@end
