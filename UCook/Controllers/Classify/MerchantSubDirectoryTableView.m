
#import "MerchantSubDirectoryTableView.h"
#import "UITableViewCellAdditionals.h"

@implementation MerchantSubDirectoryTableView

@synthesize theDataSourceArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}



#pragma mark - table view delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  theDataSourceArray ?  [theDataSourceArray count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [UITableViewCell getCell:tableView identifier:@"MerchantSubDirectoryTableViewCell"];
        
    cell.accessoryType = UITableViewCellAccessoryNone;
//    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.text = [theDataSourceArray objectAtIndex:indexPath.row];
    cell.backgroundColor = kTableViewBackgroundColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
