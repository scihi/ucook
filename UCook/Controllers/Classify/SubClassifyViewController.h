//
//  SubClassifyViewController.h
//  UCook
//
//  Created by huangrun on 14-7-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "MerchantDirectoryTableView.h"
#import "MerchantSubDirectoryTableView.h"

@interface SubClassifyViewController : GeneralWithBackBtnViewController <UITableViewDelegate> {
    MerchantDirectoryTableView*             theTableView;
    MerchantSubDirectoryTableView*          theSubTableView;
}

@property (nonatomic, retain) NSArray* theFirstDataArr;//左边tableView数据源
@property (nonatomic, retain) NSArray* theSecondDataArr;//右边tableView数据源

@end
