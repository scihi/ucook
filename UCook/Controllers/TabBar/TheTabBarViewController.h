

#import <UIKit/UIKit.h>

#import "HRTabBarButton.h"
#import "HRTabBar.h"

@interface TheTabBarViewController : UITabBarController <HRTabBarDelegate>

/**
 *  设置之前选中的按钮
 */
@property (nonatomic, weak) UIButton *selectedBtn;

@end
