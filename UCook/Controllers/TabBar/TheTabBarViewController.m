

#import "TheTabBarViewController.h"

#import "HomeViewController.h"
#import "GoodsBagViewController.h"
#import "ClassifyViewController.h"
#import "ShoppingCartViewController.h"
#import "MineViewController.h"
#import "HRNavViewController.h"
#import "LoginViewController.h"

@implementation TheTabBarViewController

+ (void)initialize
{
    //the color for the text for unselected tabs
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
//    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} forState:UIControlStateNormal];//设置tabBarItem文字颜色 huangrun
//    }
//    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor colorWithRed:95/255.0 green:160/255.0 blue:40/255.0 alpha:1.0]} forState:UIControlStateSelected];//设置tabBarItem选中文字颜色 huangrun
    
    //the color for selected icon
//    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:95/255.0 green:160/255.0 blue:40/255.0 alpha:1.0]];//改变tabBarItem选择后的颜色 huangrun
}


- (void)viewDidLoad
{
  [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
        [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} forState:UIControlStateNormal];//设置tabBarItem文字颜色 huangrun
//    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6)
        
    {
        
        [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
        
    }//防止tabBar上出现黑线 huangrun
    
    HomeViewController *homeVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
//    homeVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[UIImage imageNamed:@"ico_home.png"] tag:0];
    HRNavViewController *navHomeVC = [[HRNavViewController alloc]initWithRootViewController:homeVC];
    GoodsBagViewController *goodBagVC = [[GoodsBagViewController alloc]initWithNibName:@"GoodsBagViewController" bundle:nil];
//    goodBagVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"商品包" image:[UIImage imageNamed:@"ico_bag.png"] tag:1];
    HRNavViewController *navGoodBagVC = [[HRNavViewController alloc]initWithRootViewController:goodBagVC];
    ClassifyViewController *classifyVC = [[ClassifyViewController alloc]init];
//    classifyVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:nil image:nil tag:2];
    HRNavViewController *navClassifyVC = [[HRNavViewController alloc]initWithRootViewController:classifyVC];
    ShoppingCartViewController *shoppingCartVC =[[ShoppingCartViewController alloc]init];
//    shoppingCartVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"购物车" image:[UIImage imageNamed:@"ico_car.png"] tag:3];
    HRNavViewController *navShoppingCartVC = [[HRNavViewController alloc]initWithRootViewController:shoppingCartVC];
    MineViewController *mineVC = [[MineViewController alloc]initWithNibName:@"MineViewController" bundle:nil];
//    mineVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"我的" image:[UIImage imageNamed:@"ico_mine.png"] tag:4];
    HRNavViewController *navMineVC = [[HRNavViewController alloc]initWithRootViewController:mineVC];

    self.viewControllers = @[navHomeVC,navGoodBagVC,navClassifyVC,navShoppingCartVC,navMineVC];
    
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"public_tab.png"]];

//    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f],
//                                                        NSForegroundColorAttributeName : [UIColor colorWithRed:.5 green:.5 blue:.5 alpha:1]
//                                                        } forState:UIControlStateNormal];
    
//    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor greenColor], UITextAttributeTextColor, nil]
//                                             forState:UIControlStateNormal];
    
//    // set selected and unselected icons
//    UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
//    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
//    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];
//    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:3];
//    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:4];
//    
//
//    if (IS_IOS7) {
////     this icon is used for selected tab and it will get tinted as defined in self.tabBar.tintColor
//        item0.selectedImage = [[UIImage imageNamed:@"ico_home_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        item1.selectedImage = [[UIImage imageNamed:@"ico_bag_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        item2.selectedImage = [[UIImage imageNamed:@"bottom_btn_kind_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        item3.selectedImage = [[UIImage imageNamed:@"ico_car_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        item4.selectedImage = [[UIImage imageNamed:@"ico_mine_on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];//改变tabBarItem未选择时的颜色  huangrun
////
//        for (UITabBarItem *tbi in self.tabBar.items) {
//            tbi.image = [tbi.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        }
//    } else if (IS_IOS6) {
//        [item0 setFinishedSelectedImage:[UIImage imageNamed:@"ico_home_on.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"ico_home.png"]];
//        [item1 setFinishedSelectedImage:[UIImage imageNamed:@"ico_bag_on.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"ico_bag.png"]];
//        [item2 setFinishedSelectedImage:[UIImage imageNamed:@"bottom_btn_kind_on.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"bottom_btn_kind.png"]];
//        [item3 setFinishedSelectedImage:[UIImage imageNamed:@"ico_car_on.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"ico_car.png"]];
//        [item4 setFinishedSelectedImage:[UIImage imageNamed:@"ico_mine_on.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"ico_mine.png"]];
//    }
    
    
    //HRCustomTabBar配置：
    //下面两个方法在开发中是经常会用到的
    //    NSLog(@"%s",__func__);
    //    NSLog(@"%@",self.view.subviews); //能打印出所有子视图,和其frame
    //	LogFun;
    //	LogSubviews(self.view);
	//Hell
    
    
	//删除现有的tabBar
	CGRect rect = self.tabBar.bounds; //这里要用bounds来加, 否则会加到下面去.看不见
    LogFrame(self.tabBar);
	//[self.tabBar removeFromSuperview];  //移除TabBarController自带的下部的条
    
	//测试添加自己的视图
	HRTabBar *myView = [[HRTabBar alloc] init]; //设置代理必须改掉前面的类型,不能用UIView
	myView.delegate = self; //设置代理
	myView.frame = rect;
	[self.tabBar addSubview:myView]; //添加到系统自带的tabBar上, 这样可以用系统的事件方法. 而不必自己去写
    
    //为控制器添加按钮
    for (int i=0; i<self.viewControllers.count; i++) { //根据有多少个子视图控制器来进行添加按钮
        
        NSString *imageName = [NSString stringWithFormat:@"TabBar%d", i + 1];
        NSString *imageNameSel = [NSString stringWithFormat:@"TabBar%dSel", i + 1];
        
        UIImage *image = [UIImage imageNamed:imageName];
        UIImage *imageSel = [UIImage imageNamed:imageNameSel];
        
        [myView addButtonWithImage:image selectedImage:imageSel];
    }
    
    
    //    //添加按钮
    //	for (int i = 0; i < 5; i++) {
    //		//UIButton *btn = [[UIButton alloc] init];
    //        HRTabBarButton *btn = [[HRTabBarButton alloc] init];
    //
    //		NSString *imageName = [NSString stringWithFormat:@"TabBar%d", i + 1];
    //		NSString *imageNameSel = [NSString stringWithFormat:@"TabBar%dSel", i + 1];
    //
    //		[btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    //		[btn setImage:[UIImage imageNamed:imageNameSel] forState:UIControlStateSelected];
    //
    //		CGFloat x = i * myView.frame.size.width / 5;
    //		btn.frame = CGRectMake(x, 0, myView.frame.size.width / 5, myView.frame.size.height);
    //
    //		[myView addSubview:btn];
    //
    //        btn.tag = i;//设置按钮的标记, 方便来索引当前的按钮,并跳转到相应的视图
    //
    //		//带参数的监听方法记得加"冒号"
    //		[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    //
    //		//设置刚进入时,第一个按钮为选中状态
    //		if (0 == i) {
    //			btn.selected = YES;
    //			self.selectedBtn = btn;  //设置该按钮为选中的按钮
    //		}
    //	}

}

/**永远别忘记设置代理*/
- (void)tabBar:(HRTabBar *)tabBar selectedFrom:(NSInteger)from to:(NSInteger)to button:(UIButton *)button {

    if (to == 1) {
        if (![AccountManager isLogged]) {
            LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
            [self presentViewController:navLoginVC animated:YES completion:nil];
            button.selected = NO;
            tabBar.selectedBtn.selected = YES;
        } else {
            tabBar.selectedBtn = button;
            self.selectedIndex = 1;
        }
    } else if (to == 3) {
        if (![AccountManager isLogged]) {
            LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
            [self presentViewController:navLoginVC animated:YES completion:nil];
            button.selected = NO;
            tabBar.selectedBtn.selected = YES;
        } else {
            tabBar.selectedBtn = button;
            self.selectedIndex = 3;
        }
        
    } else if (to == 4) {
        if (![AccountManager isLogged]) {
            LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
            [self presentViewController:navLoginVC animated:YES completion:nil];
            button.selected = NO;
            tabBar.selectedBtn.selected = YES;
        } else {
            tabBar.selectedBtn = button;
            self.selectedIndex = 4;
        }
        
    }else {
        tabBar.selectedBtn = button;
        self.selectedIndex = to;
    }
}//修改过 原代码就只有一行：self.selectedIndex = to; huangrun

/**
 *  自定义TabBar的按钮点击事件
 */
//- (void)clickBtn:(UIButton *)button {
//	//1.先将之前选中的按钮设置为未选中
//	self.selectedBtn.selected = NO;
//	//2.再将当前按钮设置为选中
//	button.selected = YES;
//	//3.最后把当前按钮赋值为之前选中的按钮
//	self.selectedBtn = button;
//
//    //4.跳转到相应的视图控制器. (通过selectIndex参数来设置选中了那个控制器)
//    self.selectedIndex = button.tag;
//}


@end
