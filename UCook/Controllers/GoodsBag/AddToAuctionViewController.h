//
//  AddToAuctionViewController.h
//  UCook
//
//  Created by huangrun on 14-8-3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "TakeDeliveryAddressViewController.h"

@interface AddToAuctionViewController : GeneralWithBackBtnViewController <TakeDeliveryAddressVCDelegate>

@property (nonatomic, strong) NSMutableArray *goodsInfoMutArr;
@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *cellPhoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, copy) NSString *goodsBagNameStr;
@property (nonatomic, copy) NSString *goodsBagIdStr;

- (IBAction)clickDoneBtn:(id)sender;
- (IBAction)clickAddressBtn:(id)sender;

@property (copy, nonatomic) NSArray *goodsPriceMutArr;

@end
