//
//  AddGoodsBagViewController.m
//  UCook
//
//  Created by huangrun on 14-7-28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AddGoodsBagViewController.h"
#import "NSStringAdditions.h"
#import "ShopViewController.h"


@interface AddGoodsBagViewController () {
    NSString *_bagIdStr;
}

@end

@implementation AddGoodsBagViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"新增商品包";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickConfirmBtn:(id)sender {
    
    if ([NSString isEmptyOrWhitespace:_goodsBagNameTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入商品包名"];
        return;
    }
    
    [_goodsBagNameTF resignFirstResponder];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"create",@"uid":[AccountManager getUid],@"name": _goodsBagNameTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _bagIdStr = [[responseObject objectForKey:@"data"] objectForKey:@"id"];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"成功创建商品包,请添加商品" delegate:self cancelButtonTitle:nil otherButtonTitles:@"添加商品", nil];
            [alertView show];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    ShopViewController *shopVC = [[ShopViewController alloc]init];
    shopVC.fromAddGoodsBagClass = YES;
    shopVC.bagIdStr = _bagIdStr;
    [self.navigationController pushViewController:shopVC animated:YES];
    
}

@end
