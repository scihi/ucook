//
//  AddAddressViewController.h
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "SelectAddressViewController.h"

@interface AddAddressViewController : GeneralWithBackBtnViewController <SelectAddressVCDelegate>
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
- (IBAction)clickSelcetAddressBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *streetTF;
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *postcodeTF;
- (IBAction)clickSaveBtn:(id)sender;

@end
