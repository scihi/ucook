//
//  GoodsBagViewController.m
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GoodsBagViewController.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "TSPopoverController.h"
#import "CGRectAdditionals.h"
#import "AuctionViewController.h"
#import "ModifyBagNameViewController.h"

#define NC_ADD_GOODS @"goodsDidAdd"

static NSString *CellIdentifier = @"GoodsBagCell";

@interface GoodsBagViewController () {
    NSArray *_goodsBagInfoArr;//总的dataArray
    NSMutableArray *_selectedDataArr;//选中的商品dataArray
    NSInteger _goodsBagCount;//商品包数量
    TSPopoverController *_popoverController;//点击导航栏弹出的popView
    UIButton *_titleButton;//导航栏中部的button
    NSMutableArray *_titleArray;//导航栏上的标题数组
    UIImageView *_indicateIV;//导航栏上的指示图片
    NSUInteger _index;//popView选中的索引
    UIView *_noDataView;//无数据时的展示背景
    
    NSString *_selectedGoodsBagIdStr;
}

@end

@implementation GoodsBagViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"商品包";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     _theTableView.tableFooterView = [[UIView alloc]init];//遮住多余的分割线
    [self configTheNavBar];
    
    self.modifyBagNameBtn.layer.masksToBounds = YES;
    self.modifyBagNameBtn.layer.borderWidth = 1.0;
    self.modifyBagNameBtn.layer.borderColor = [UIColor blackColor].CGColor;
    self.modifyBagNameBtn.layer.cornerRadius = 15.0;
    
    _addGoodsBagBtn.layer.masksToBounds = YES;
    _addGoodsBagBtn.layer.borderWidth = 1.0;
    _addGoodsBagBtn.layer.borderColor = [UIColor blackColor].CGColor;
    _addGoodsBagBtn.layer.cornerRadius = 15.0;
    
    _participateInBidBtn.layer.masksToBounds = YES;
    _participateInBidBtn.layer.borderWidth = 1.0;
    _participateInBidBtn.layer.borderColor = [UIColor blackColor].CGColor;
    _participateInBidBtn.layer.cornerRadius = 15.0;
    
    
//    [self addTableViewLine];
    [self registerCell];
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        _theTableView.backgroundColor = [UIColor clearColor];
        //设置无数据时的提示视图
        [self configTheNoDataViewForTheTableView];
    } else {
    [self getGoodsBagInfoData];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goodsDidAdd) name:NC_ADD_GOODS object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 删除商品包
- (void)clickNavLeftBtn:(id)sender {
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        [GlobalSharedClass showAlertView:@"经销商不能进行此操作"];
    } else {
    
    if (_goodsBagInfoArr.count != 0) {
    NSString *alertBodyStr = [NSString stringWithFormat:@"确定删除\"%@\"这个商品包?",_titleButton.titleLabel.text];
    [GlobalSharedClass showAlertView:alertBodyStr delegate:self];
    }
    }
}

- (void)clickNavRightBtn:(id)sender {
    
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        [GlobalSharedClass showAlertView:@"经销商不能进行此操作"];
    } else {
    
    if (_goodsBagInfoArr.count == 0) {
        [GlobalSharedClass showAlertView:@"该商品包中没有产品"];
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSString *boxIdStr = [[_goodsBagInfoArr objectAtIndex:_index]objectForKey:@"id"];
    NSArray *idArray = @[@{@"boxId": boxIdStr}];
    NSString *idArrayJsonStr = [self arrayToJson:idArray];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"addCart",@"uid":[AccountManager getUid],@"boxList": idArrayJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
        
    }
}

- (void)configTheNavBar {
    //左item
    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_bar_delete.png"]];
    navLeftIV.frame = CGRectMake(10, 2, 30, 30);
    [navLeftBtn addSubview:navLeftIV];
    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    //右item
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setTitle:@"购买" forState:UIControlStateNormal];
    navRightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    navRightBtn.titleLabel.textColor = [UIColor blackColor];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_scan.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    //    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    //titleView
    _titleButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 200, 20)];
    [_titleButton addTarget:self action:@selector(showPopover:forEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = _titleButton;
}

//获取商品包信息用于展示
- (void)getGoodsBagInfoData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"list",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr firstObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            _selectedGoodsBagIdStr = [[_goodsBagInfoArr firstObject]objectForKey:@"id"];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
            [_titleButton setTitle:[_titleArray firstObject] forState:UIControlStateNormal];
            CGFloat x = _titleButton.titleLabel.frame.origin.x + _titleButton.titleLabel.frame.size.width;
            if (!_indicateIV)
            _indicateIV = [[UIImageView alloc]init];
            _indicateIV.frame = CGRectMake(x, 15, 10, 9.5);
            _indicateIV.image = [UIImage imageNamed:@"ico_select_big"];
            [_titleButton addSubview:_indicateIV];

            [_theTableView reloadData];
            
            if (!_selectedDataArr) {
                _theTableView.backgroundColor = [UIColor clearColor];
                [self configTheNoDataViewForTheTableView];
            }
            
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr firstObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
            [_titleButton setTitle:[_titleArray firstObject] forState:UIControlStateNormal];
            [_indicateIV removeFromSuperview];
            _theTableView.backgroundColor = [UIColor clearColor];
            [_theTableView reloadData];
            
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];
            
//            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

#pragma - mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _selectedDataArr?1:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;
    
    //为section底部添加分割线
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width - 10, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width - 10, 5)];
    lineView.backgroundColor = kThemeColor;
    [headerView addSubview:lineView];
    
    UIImageView *cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(5, 9.5, 25, 25)];
    cellIV.image = [UIImage imageNamed:@"ico_store"];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 0, BOUNDS.size.width - 35, 44)];
    if ([NSArray isValidNSArray:[[_goodsBagInfoArr objectAtIndex:_index]objectForKey:@"stores"]]) {
        titleLabel.text = [[[[_goodsBagInfoArr objectAtIndex:_index]objectForKey:@"stores"]firstObject]objectForKey:@"storeName"];
        [headerView addSubview:cellIV];
        [headerView addSubview:titleLabel];
    }
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _selectedDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    _theTableView.hidden = NO;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [[_selectedDataArr objectAtIndex:indexPath.row]objectForKey:@"image"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = [[_selectedDataArr objectAtIndex:indexPath.row]objectForKey:@"name"];
    UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
    NSString *specificationString = [[_selectedDataArr objectAtIndex:indexPath.row]objectForKey:@"spec"];
    specificationLabel.text = [NSString stringWithFormat:@"规格:%@",specificationString];
    UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];//单位
    NSString *unitString = [specificationString substringFromIndex:specificationString.length - 1];;
    unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitString];
    UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:105];
    NSString *currentPriceStr = [[_selectedDataArr objectAtIndex:indexPath.row]objectForKey:@"discountPrice"];
    currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:106];//原价
    originalPriceLabel.strikeThroughEnabled = YES;
    NSString *originalPriceStr = [[_selectedDataArr objectAtIndex:indexPath.row]objectForKey:@"price"];
    originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",originalPriceStr];
    
    return cell;
}

- (void)registerCell {
    [_theTableView registerNib:[UINib nibWithNibName:@"GoodsBagCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
}

- (IBAction)clickAddGoodsBagBtn:(id)sender {
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        [GlobalSharedClass showAlertView:@"经销商不能进行此操作"];
    } else {
    
    AddGoodsBagViewController *addGoodsBagVC = [[AddGoodsBagViewController alloc]initWithNibName:@"AddGoodsBagViewController" bundle:nil];
    addGoodsBagVC.delegate = self;
    addGoodsBagVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addGoodsBagVC animated:YES];
        
    }
}

- (IBAction)clickParticipatInBidBtn:(id)sender {
    
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        [GlobalSharedClass showAlertView:@"经销商不能进行此操作"];
    } else {
    
        if (_goodsBagInfoArr.count == 0) {
            [GlobalSharedClass showAlertView:@"该商品包中没有产品"];
            return;
        }
  
        
    AuctionViewController *auctionVC = [[AuctionViewController alloc]initWithNibName:@"AuctionViewController" bundle:nil];
    auctionVC.theDataArr = _selectedDataArr;
    auctionVC.goodsBagNameStr = _titleButton.titleLabel.text;
    auctionVC.goodsBagIdStr = _selectedGoodsBagIdStr;
    auctionVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:auctionVC animated:YES];
        
    }
}

//-(void)showPopover:(id)sender
-(void)showPopover:(id)sender forEvent:(UIEvent*)event
{
    if (_goodsBagInfoArr.count != 0) {
    [self getGoodsBagInfoDataForPopView];
    }
}

//选择了导航栏的tableView的cell回调
- (void)didSelectRowAtIndexPath:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    _index = indexPath.row;
        NSString *titleString = [_titleArray objectAtIndex:indexPath.row];
    [_titleButton setTitle:titleString forState:UIControlStateNormal];
    CGFloat x = _titleButton.titleLabel.frame.origin.x + _titleButton.titleLabel.frame.size.width;
    _indicateIV.frame = CGRectMake(x, 15, 10, 9.5);
    
    [_selectedDataArr removeAllObjects];
    _selectedDataArr = [[[[[_goodsBagInfoArr objectAtIndex:indexPath.row]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
    _selectedGoodsBagIdStr = [[_goodsBagInfoArr objectAtIndex:indexPath.row]objectForKey:@"id"];
    if (!_selectedDataArr) {
////        _theTableView.hidden = YES;
        _theTableView.backgroundColor = [UIColor clearColor];
        [self configTheNoDataViewForTheTableView];
        [_theTableView reloadData];
    } else {
        //        _theTableView.hidden = NO;
        
        [_noDataView removeFromSuperview];
        _theTableView.backgroundColor = [UIColor whiteColor];
        [_theTableView reloadData];
    }
    [_popoverController dismissPopoverAnimatd:YES];
}

//在tableView顶部添加一根绿色的线
- (void)addTableViewLine {
//    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(5, 52, 310, 3)];
//    lineView.backgroundColor = kThemeColor;
//    [_theTableView addSubview:lineView];
}

- (void)deleteGoodsBagInServer {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSString *boxIdStr = [[_goodsBagInfoArr objectAtIndex:_index]objectForKey:@"id"];
        NSArray *idArray = @[@{@"boxId": boxIdStr}];
        NSString *idArrayJsonStr = [self arrayToJson:idArray];
        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"delete",@"uid":[AccountManager getUid],@"id": idArrayJsonStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                [GlobalSharedClass showAlertView:MESSAGE];
                _index = 0;
                [self getGoodsBagInfoData];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self deleteGoodsBagInServer];
    }
}

//获取商品包信息用于展示PopView
- (void)getGoodsBagInfoDataForPopView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"list",@"uid":[AccountManager getUid]};
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self stopRequest];
        if (RIGHT) {
//            [_noDataView removeFromSuperview];
//            _theTableView.backgroundColor = [UIColor whiteColor];
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr firstObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
//            [_titleButton setTitle:[_titleArray firstObject] forState:UIControlStateNormal];
//            CGFloat x = _titleButton.titleLabel.frame.origin.x + _titleButton.titleLabel.frame.size.width;
//            if (!_indicateIV)
//                _indicateIV = [[UIImageView alloc]init];
//            _indicateIV.frame = CGRectMake(x, 15, 10, 9.5);
//            _indicateIV.image = [UIImage imageNamed:@"ico_select_big"];
//            [_titleButton addSubview:_indicateIV];
            
//            [_theTableView reloadData];
            SharePopupTableView *tableView= [[SharePopupTableView alloc] init];
            tableView.textColor = [UIColor whiteColor];
            tableView.backgroundColor = [UIColor darkGrayColor];//huangrun增加
            tableView.frame = CGRectMake(0,0, 120, 130);
            tableView.theCustomDelegate = self;
            
            tableView.theTextArray = _titleArray;
            _popoverController = [[TSPopoverController alloc] initWithView:tableView];
            
            //    _popoverController.cornerRadius = 5;
            _popoverController.cornerRadius = 0.1;//huangrun修改
            //    _popoverController.titleText = @"change order";//huangrun注释
            _popoverController.popoverBaseColor = [UIColor darkGrayColor];
            _popoverController.popoverGradient= NO;
            //    _popoverController.arrowPosition = TSPopoverArrowPositionHorizontal;
            [_popoverController showPopoverWithRect:self.navigationController.navigationBar.frame];
            
//            if (!_selectedDataArr) {
//                
//                _theTableView.backgroundColor = [UIColor clearColor];
//                [self configTheNoDataViewForTheTableView];
//            }
            
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
//            [_noDataView removeFromSuperview];
//            _theTableView.backgroundColor = [UIColor whiteColor];
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr firstObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
            //            [_titleButton setTitle:[_titleArray firstObject] forState:UIControlStateNormal];
            //            CGFloat x = _titleButton.titleLabel.frame.origin.x + _titleButton.titleLabel.frame.size.width;
            //            if (!_indicateIV)
            //                _indicateIV = [[UIImageView alloc]init];
            //            _indicateIV.frame = CGRectMake(x, 15, 10, 9.5);
            //            _indicateIV.image = [UIImage imageNamed:@"ico_select_big"];
            //            [_titleButton addSubview:_indicateIV];
            
            //            [_theTableView reloadData];
            SharePopupTableView *tableView= [[SharePopupTableView alloc] init];
            tableView.textColor = [UIColor whiteColor];
            tableView.backgroundColor = [UIColor darkGrayColor];//huangrun增加
            tableView.frame = CGRectMake(0,0, 120, 130);
            tableView.theCustomDelegate = self;
            
            tableView.theTextArray = _titleArray;
            _popoverController = [[TSPopoverController alloc] initWithView:tableView];
            
            //    _popoverController.cornerRadius = 5;
            _popoverController.cornerRadius = 0.1;//huangrun修改
            //    _popoverController.titleText = @"change order";//huangrun注释
            _popoverController.popoverBaseColor = [UIColor darkGrayColor];
            _popoverController.popoverGradient= NO;
            //    _popoverController.arrowPosition = TSPopoverArrowPositionHorizontal;
            [_popoverController showPopoverWithRect:self.navigationController.navigationBar.frame];

        } else {
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
//        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64 - 49 - 44 - 50)];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = [[AccountManager getRole]isEqualToString:@"normal"]?@"没有找到相关数据":@"经销商用户不能使用商品包功能";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];

}

#pragma mark - 以下两项为添加商品包后不添加商品和添加商品后的代理和通知

- (void)dismissBtnDidClick {
    [self getGoodsBagInfoData];
}

- (void)goodsDidAdd {
    [self getGoodsBagInfoDataForAddGoodsBag];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

//获取商品包信息用于展示,用于刚添加完商品包返回回来，取数组最后一个元素(初始化默认取的第一个)
- (void)getGoodsBagInfoDataForAddGoodsBag {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"list",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr lastObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            _selectedGoodsBagIdStr = [[_goodsBagInfoArr lastObject]objectForKey:@"id"];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
            [_titleButton setTitle:[_titleArray lastObject] forState:UIControlStateNormal];
            CGFloat x = _titleButton.titleLabel.frame.origin.x + _titleButton.titleLabel.frame.size.width;
            if (!_indicateIV)
                _indicateIV = [[UIImageView alloc]init];
            _indicateIV.frame = CGRectMake(x, 15, 10, 9.5);
            _indicateIV.image = [UIImage imageNamed:@"ico_select_big"];
            [_titleButton addSubview:_indicateIV];
            
            [_theTableView reloadData];
            
            _index = _titleArray.count - 1;
            
            if (!_selectedDataArr) {
                _theTableView.backgroundColor = [UIColor clearColor];
                [self configTheNoDataViewForTheTableView];
            }
            
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
            _goodsBagInfoArr = [[responseObject objectForKey:@"data"]mutableCopy];
            _selectedDataArr = [[[[[_goodsBagInfoArr lastObject]objectForKey:@"stores"]firstObject]objectForKey:@"products"]mutableCopy];
            
            _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
            for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
                NSString *titleString = [titleDictionary objectForKey:@"name"];
                [_titleArray addObject:titleString];
            }
            [_titleButton setTitle:[_titleArray firstObject] forState:UIControlStateNormal];
            [_indicateIV removeFromSuperview];
            _theTableView.backgroundColor = [UIColor clearColor];
            [_theTableView reloadData];
            
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];
            
            //            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)getDetailBagInfo {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"list",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {

 
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (IBAction)clickModifyBagNameBtn:(id)sender {
    ModifyBagNameViewController *modifyBagNameVC = [[ModifyBagNameViewController alloc]initWithNibName:@"ModifyBagNameViewController" bundle:nil];
    modifyBagNameVC.bagIdStr = [[_goodsBagInfoArr objectAtIndex:_index]objectForKey:@"id"];
    _titleArray = [NSMutableArray arrayWithCapacity:10];//导航栏上的标题数组
    for (NSDictionary *titleDictionary in _goodsBagInfoArr) {
        NSString *titleString = [titleDictionary objectForKey:@"name"];
        [_titleArray addObject:titleString];
    }
    modifyBagNameVC.nameStr = [_titleArray lastObject];
    modifyBagNameVC.delegate = self;
    [self.navigationController pushViewController:modifyBagNameVC animated:YES];
}

- (void)bagNameDidModify:(ModifyBagNameViewController *)modifyBagNameVC {
    [self getGoodsBagInfoData];
}

@end
