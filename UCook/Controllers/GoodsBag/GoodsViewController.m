//
//  GoodsViewController.m
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GoodsViewController.h"
#import "SearchViewController.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"

#import "PrettySearchBar.h"

#define NC_ADD_GOODS @"goodsDidAdd"

static NSString *CellIdentifier = @"GoodsCell";

@interface GoodsViewController () {
    
    NSMutableArray *_theDataMutArr;
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    PrettySearchBar *_searchBar;
    NSString *_searchNameStr;
    
    NSMutableArray *_addedGoodsMutArr;
    NSMutableArray *_indexPathMutArr;
    NSMutableArray *_addBtnTitleMutArr;//添加按钮的标题数组
}

@end

@implementation GoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_searchBar resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTheVars];
    
    //设置导航栏
    [self configTheNavBar];
    
    //获取商品包店铺列表数据
    [self getData];
    
    //配置搜索栏
    [self configTheSearchBar];

    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_ico.png"]];
    navRightIV.frame = CGRectMake(15, 7, 20, 20);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
//    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;
}


- (void)clickNavRightBtn:(id)sender {
    [self getData];
    [_searchBar resignFirstResponder];
}

- (IBAction)clickCheckSelectedBtn:(id)sender {
    SelectedGoodsViewController *selectedGoodsVC = [[SelectedGoodsViewController alloc]init];
    selectedGoodsVC.delegate = self;
    selectedGoodsVC.theDataMutArr = _addedGoodsMutArr;
    selectedGoodsVC.indexPathMutArr = _indexPathMutArr;
    [self.navigationController pushViewController:selectedGoodsVC animated:YES];
}

- (IBAction)clickConfirmAddBtn:(id)sender {
    
    if (_addedGoodsMutArr.count == 0) {
        [GlobalSharedClass showAlertView:@"请添加商品"];
        return;
    }

    NSMutableArray *goodsIdStrArr = [NSMutableArray arrayWithCapacity:10];
    for (NSDictionary *addedGoodsDic in _addedGoodsMutArr) {
    NSInteger goodsIdInteger = [[addedGoodsDic objectForKey:@"id"]integerValue];
        [goodsIdStrArr addObject:@(goodsIdInteger)];
    }
    
    NSString *goodsIdStrArrJsonStr = [self arrayToJson:goodsIdStrArr];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"productBox",@"action": @"addProduct",@"uid": [AccountManager getUid],@"boxId": _bagIdStr,@"productId": goodsIdStrArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [[NSNotificationCenter defaultCenter]postNotificationName:NC_ADD_GOODS object:nil];
            [GlobalSharedClass showAlertView:MESSAGE];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)initTheVars {
    _theDataMutArr = [NSMutableArray arrayWithCapacity:10];
    [_theTableView registerNib:[UINib nibWithNibName:@"GoodsCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    _addedGoodsMutArr  = [NSMutableArray arrayWithCapacity:10];
    _indexPathMutArr = [NSMutableArray arrayWithCapacity:10];
    _addBtnTitleMutArr = [NSMutableArray arrayWithCapacity:10];
    
    _pageNumber = 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (_theDataArr.count >= 10) {
    //        return 10;
    //    } else {
    return _theDataMutArr.count;
    //    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (_theDataMutArr.count != 0) {
            NSDictionary *currentRowDic = [_theDataMutArr objectAtIndex:indexPath.row];
            NSString *imgUrlStr = [currentRowDic objectForKey:@"image"];
            UIImageView *cellIV = (UIImageView*)[cell viewWithTag:101];
            [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
            titleLabel.text = [currentRowDic objectForKey:@"name"];
            
            UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
            specificationLabel.text = [NSString stringWithFormat:@"规格:%@",[currentRowDic objectForKey:@"spec"]];
            
            UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];
            unitLabel.text = [NSString stringWithFormat:@"单位:%@",[currentRowDic objectForKey:@"unit"]];
            
            UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:105];
            currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"discountPrice"]];
            
            StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:106];
            originalPriceLabel.strikeThroughEnabled = YES;
            originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"price"]];
            
            UILabel *noticeLabel = (UILabel *)[cell viewWithTag:107];
            noticeLabel.text = _shopNameStr;
            
            UIButton *addButton = (UIButton *)[cell viewWithTag:108];
            [addButton setTitle:[_addBtnTitleMutArr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        if ([addButton.titleLabel.text isEqualToString:@"已添加"]) {
            [addButton setTitleColor:kThemeColor forState:UIControlStateNormal];
            addButton.userInteractionEnabled = NO;
        } else {
            [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            addButton.userInteractionEnabled = YES;
        }

            [addButton addTarget:self action:@selector(clickAddBtn:) forControlEvents:UIControlEventTouchUpInside];
            addButton.layer.borderColor = kThemeCGColor;
            addButton.layer.borderWidth = 1.0;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        _searchNameStr = _searchBar.text;
        if (!_searchNameStr) {
            _searchNameStr = @"";
        }

        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"product",@"action": @"list",@"page": @(_pageNumber),@"name": _searchNameStr,@"storeId": @(_shopIdInteger)};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [self stopRequest];
            if (RIGHT) {
                //            [_theDataMutArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
                for (int i = 0; i < _theDataMutArr.count; i++) {
                    [_addBtnTitleMutArr addObject:@"添加"];
                }

                [self doneWithView:refreshView];
            } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                    
//                    [_theDataMutArr removeAllObjects];
                    [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                for (int i = 0; i < _theDataMutArr.count; i++) {
                    [_addBtnTitleMutArr addObject:@"添加"];
                }

                [self doneWithView:refreshView];
                
                } else {
                    [GlobalSharedClass showAlertView:MESSAGE];
                }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (void)getData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    _searchNameStr = _searchBar.text;
    if (!_searchNameStr) {
       _searchNameStr = @"";
    }

    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"product",@"action": @"list",@"page": @(_pageNumber),@"name": _searchNameStr,@"storeId": @(_shopIdInteger)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
            for (int i = 0; i < _theDataMutArr.count; i++) {
                [_addBtnTitleMutArr addObject:@"添加"];
            }

            [_theTableView reloadData];
            
        }else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
            for (int i = 0; i < _theDataMutArr.count; i++) {
                [_addBtnTitleMutArr addObject:@"添加"];
            }

            [_theTableView reloadData];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)clickAddBtn:(id)sender {
    UIButton *button = sender;
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[button superview]superview];
    } else {
     cell = (UITableViewCell *)[[[button superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    [_addedGoodsMutArr addObject:[_theDataMutArr objectAtIndex:indexPath.row]];
    [_indexPathMutArr addObject:indexPath];
     [_addBtnTitleMutArr replaceObjectAtIndex:indexPath.row withObject:@"已添加"];
    [_theTableView reloadData];
}

- (void)configTheSearchBar {
    _searchBar = [[PrettySearchBar alloc]initWithFrame:CGRectMake(0, 0, 175, 44)];
    _searchBar.delegate = self;
    //    searchBar.showsCancelButton = YES;
    //    [searchBar sizeToFit];
    [_searchBar setTintColor:[UIColor blackColor]];
    _searchBar.searchBarStyle=UISearchBarStyleMinimal;
    
    _searchBar.placeholder = @"请输入关键字";
    
    [_searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"input"] forState:UIControlStateNormal];
    //    [_searchBar setSearchTextPositionAdjustment:UIOffsetMake(25, 0)];
    //    [[UISearchBar appearance] setSearchFieldBackgroundPositionAdjustment:UIOffsetMake(30, 0)];
//    [_searchBar setImage:[UIImage imageNamed:@"search_replace"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];//去掉searchBar前面的搜索图标 方法二 huangrun
    //    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];//去掉searchBar前面的搜索图标 方法一 huangrun
    //    self.tintColor = [UIColor whiteColor];
    
    self.navigationItem.titleView = _searchBar;

    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self getData];
    [_searchBar resignFirstResponder];
}

#pragma mark - SelectedGoodsVCDelegate cell上的删除委托
- (void)deleteBtnDidClicked:(SelectedGoodsViewController *)selectedGoodsVC index:(NSUInteger)index indexPath:(NSIndexPath *)indexPath {
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
//    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
//    UIButton *addButton = (UIButton *)[cell viewWithTag:108];
//    [addButton setTitle:@"添加" forState:UIControlStateNormal];
//    addButton.userInteractionEnabled = YES;
    [_indexPathMutArr removeObjectAtIndex:indexPath.row];
    [_addBtnTitleMutArr replaceObjectAtIndex:index withObject:@"添加"];
    [_theTableView reloadData];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"_theIndexMutArr"];
}

#pragma mark - SelectedGoodsVCDelegate 导航右上角删除委托
- (void)navRightBtnDidClicked:(SelectedGoodsViewController *)selectedGoodsVC {
    [_indexPathMutArr removeAllObjects];
    for (int i = 0; i < _addBtnTitleMutArr.count; i++) {
        [_addBtnTitleMutArr replaceObjectAtIndex:i withObject:@"添加"];
    }
    [_theTableView reloadData];
}

@end
