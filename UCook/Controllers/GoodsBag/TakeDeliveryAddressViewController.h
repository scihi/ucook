//
//  TakeDeliveryAddressViewController.h
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class TakeDeliveryAddressViewController;

@protocol TakeDeliveryAddressVCDelegate <NSObject>

- (void)addressDidSelect:(TakeDeliveryAddressViewController *)takeDeliveryAddressVC address:(NSString *)aAddress name:(NSString *)aName id:(NSString *)aId;

@end

@interface TakeDeliveryAddressViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) NSString *addressIdStr;
- (IBAction)clickAddAddressBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property id <TakeDeliveryAddressVCDelegate> delegate;
@property (nonatomic) NSInteger vCTag;//vc入口tag值 101表示其他vc进入 102表示我的优厨vc进入

@end
