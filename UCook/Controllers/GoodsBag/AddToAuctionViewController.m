//
//  AddToAuctionViewController.m
//  UCook
//
//  Created by huangrun on 14-8-3.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AddToAuctionViewController.h"
#import "AuctionInitModel.h"
#import "PPiFlatSegmentedControl.h"
#import "NSString+FontAwesome.h"
#import "AddAuctionModel.h"

@interface AddToAuctionViewController () {
    NSInteger _timeInteger;
    AuctionInitModel *_auctionInitModel;
    AddAuctionModel *_addAuctionModel;
}

@end

@implementation AddToAuctionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"加入竞拍";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configTheSegment];
    [self getInitData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//获取竞拍包初始数据
- (void)getInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"bidding",@"action": @"init",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _auctionInitModel = [AuctionInitModel objectWithKeyValues:dictionary];
            [self configTheView];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


- (void)configTheSegment {
    _timeInteger = 4;
    PPiFlatSegmentedControl *segmented=[[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(5, 155, 310, 40) items:@[               @{@"text":@"4H",@"icon":@""},
                                                                                                                                         @{@"text":@"8H",@"icon":@""},
                                                                                                                                         @{@"text":@"12H",@"icon":@""},
                                                                                                                                         @{@"text":@"24H",@"icon":@""}
                                                                                                                                         ]
                                                                         iconPosition:IconPositionRight andSelectionBlock:^(NSUInteger segmentIndex) {
                                                                             switch (segmentIndex) {
                                                                                 case 0:
                                                                                     _timeInteger = 4;
                                                                                     break;
                                                                                 case 1:
                                                                                     _timeInteger = 8;
                                                                                     break;
                                                                                 case 2:
                                                                                     _timeInteger = 12;
                                                                                     break;
                                                                                 case 3:
                                                                                     _timeInteger = 24;
                                                                                 default:
                                                                                     break;
                                                                             }
                                                                         }];
//    segmented.color=[UIColor colorWithRed:88.0f/255.0 green:88.0f/255.0 blue:88.0f/255.0 alpha:1];
    segmented.color = [UIColor lightGrayColor];//huangrun
    segmented.borderWidth=0.5;
    segmented.borderColor=[UIColor darkGrayColor];
//    segmented.selectedColor=[UIColor colorWithRed:0.0f/255.0 green:141.0f/255.0 blue:147.0f/255.0 alpha:1];
    segmented.selectedColor = kThemeColor;//huangrun
    segmented.textAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                               NSForegroundColorAttributeName:[UIColor whiteColor]};
    segmented.selectedTextAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                       NSForegroundColorAttributeName:[UIColor whiteColor]};
    [_theScrollView addSubview:segmented];

}

- (void)configTheView {
    _nameLabel.text = _auctionInitModel.contactName;
    _addressLabel.text = _auctionInitModel.address;
    _cellPhoneLabel.text = [AccountManager getAccount];
    
    CGFloat totalPrice;
    for (int i = 0; i < _goodsInfoMutArr.count; i++) {
        NSInteger integer = [[_goodsPriceMutArr objectAtIndex:i]integerValue] * [[[_goodsInfoMutArr objectAtIndex:i]objectForKey:@"number"]integerValue];
        totalPrice += integer;
    }
    
    _priceLabel.text = [NSString stringWithFormat:@"￥%.1f",totalPrice];
}

//添加商品包
- (IBAction)clickDoneBtn:(id)sender {
    
    if (!_auctionInitModel.id) {
        [GlobalSharedClass showAlertView:@"请设置收货地址"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSString *goodsInfoMutArrJsonStr = [self arrayToJson:_goodsInfoMutArr];
    NSString *priceString = [_priceLabel.text stringByReplacingOccurrencesOfString:@"￥" withString:@""];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"bidding",@"action": @"create",@"uid":[AccountManager getUid],@"name": _goodsBagNameStr, @"productBoxId": _goodsBagIdStr,@"price": priceString,@"endTime": @(_timeInteger),@"productList": goodsInfoMutArrJsonStr,@"recAddressId":_auctionInitModel.id};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _addAuctionModel = [AddAuctionModel objectWithKeyValues:dictionary];
            [GlobalSharedClass showAlertView:MESSAGE];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (IBAction)clickAddressBtn:(id)sender {
    TakeDeliveryAddressViewController *takeDeliveryAddressVC = [[TakeDeliveryAddressViewController alloc]initWithNibName:@"TakeDeliveryAddressViewController" bundle:nil];
    takeDeliveryAddressVC.delegate = self;
    takeDeliveryAddressVC.vCTag = 101;
    takeDeliveryAddressVC.addressIdStr = _auctionInitModel.id;
    [self.navigationController pushViewController:takeDeliveryAddressVC animated:YES];
}

- (void)addressDidSelect:(TakeDeliveryAddressViewController *)takeDeliveryAddressVC address:(NSString *)aAddress name:(NSString *)aName id:(NSString *)aId{
    //修复添加完地址后model里数据没更新的问题 huangrun 20141010
//    _nameLabel.text = aName;
//    _addressLabel.text = aAddress;
    
    [self getInitData];
    
    //结束修改
}

@end
