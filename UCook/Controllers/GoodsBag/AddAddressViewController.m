//
//  AddAddressViewController.m
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AddAddressViewController.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"
#import "AddAddressModel.h"

@interface AddAddressViewController () {
    NSDictionary *_idsDictionary;//省市区id
    AddAddressModel *_addAddressModel;
}

@end

@implementation AddAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"新增收货地址";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSelcetAddressBtn:(id)sender {
    SelectAddressViewController *selectAddressVC = [[SelectAddressViewController alloc]init];
    selectAddressVC.delegate = self;
    [self.navigationController pushViewController:selectAddressVC animated:YES];
}

- (void)addressDidSelect:(SelectAddressViewController *)selectAddressVC address:(NSString *)aAddress idsDictionary:(NSDictionary *)aIdsDictionary{
    _addressLabel.text = aAddress;
    _idsDictionary = [aIdsDictionary copy];
}

- (IBAction)clickSaveBtn:(id)sender {
    if ([_addressLabel.text isEqualToString:@"请选择所在地"]) {
        [GlobalSharedClass showAlertView:@"请选择所在地"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_streetTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入街道地址"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_nameTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入收货人姓名"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_phoneTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入收货人联系电话"];
        return;
    }
    if (![NSString isValidatedMobilePhoneNumber:_phoneTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的联系电话"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_postcodeTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入邮编"];
        return;
    }
    if (_postcodeTF.text.length != 6) {
        [GlobalSharedClass showAlertView:@"请输入正确的邮编"];
        return;
    }
    
    NSString *provinceIdStr = [_idsDictionary objectForKey:@"provinceId"];
    NSString *cityIdStr = [_idsDictionary objectForKey:@"cityId"];
    NSString *districtIdStr = [_idsDictionary objectForKey:@"districtId"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"addRecAddress",@"uid":[AccountManager getUid],@"state": provinceIdStr,@"city": cityIdStr,@"district": districtIdStr, @"zipcode": _postcodeTF.text,@"address": _streetTF.text,@"contactName": _nameTF.text,@"mobilePhone": _phoneTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _addAddressModel = [AddAddressModel objectWithKeyValues:dictionary];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

@end
