//
//  SelectedGoodsViewController.m
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SelectedGoodsViewController.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"

static NSString *CellIdentifier = @"SelectedGoodsCell";

@interface SelectedGoodsViewController () {
    UITableView *_theTableView;
    NSIndexPath *_theIndexPath;
    NSMutableArray *_theIndexMutArr;
}

@end

@implementation SelectedGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"已选";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64 - 49:568 - 64 - 49 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    //初始化变量
    [self initTheVars];
    
    //设置导航栏
    [self configTheNavBar];
    
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerNib:[UINib nibWithNibName:@"GoodsCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    [self.view addSubview:_theTableView];
    
    [self configTheIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_bar_delete.png"]];
    navRightIV.frame = CGRectMake(10, 2, 30, 30);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)clickNavRightBtn:(id)sender {
    [_delegate navRightBtnDidClicked:self];
    NSMutableArray *mutableArray = [NSMutableArray arrayWithCapacity:10];
    for (int i = 0; i < _theDataMutArr.count; i++) {
        [mutableArray addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    [_theDataMutArr removeAllObjects];
    [_theTableView deleteRowsAtIndexPaths:mutableArray withRowAnimation:UITableViewRowAnimationFade];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (_theDataArr.count >= 10) {
    //        return 10;
    //    } else {
    return _theDataMutArr.count;
    //    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (_theDataMutArr.count != 0) {
        
            NSDictionary *currentRowDic = [_theDataMutArr objectAtIndex:indexPath.row];
            NSString *imgUrlStr = [currentRowDic objectForKey:@"image"];
            UIImageView *cellIV = (UIImageView*)[cell viewWithTag:101];
            [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
            titleLabel.text = [currentRowDic objectForKey:@"name"];
            
            UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
            specificationLabel.text = [NSString stringWithFormat:@"规格:%@",[currentRowDic objectForKey:@"spec"]];
            
            UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];
            unitLabel.text = [NSString stringWithFormat:@"单位:%@",[currentRowDic objectForKey:@"unit"]];
            
            UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:105];
            currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"discountPrice"]];
            
            StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:106];
            originalPriceLabel.strikeThroughEnabled = YES;
            originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"price"]];
            
            UILabel *noticeLabel = (UILabel *)[cell viewWithTag:107];
            noticeLabel.text = _shopNameStr;
            
            UIButton *deleteButton = (UIButton *)[cell viewWithTag:108];
            [deleteButton setTitle:@"删除" forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(clickDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
            deleteButton.layer.borderColor = kThemeCGColor;
            deleteButton.layer.borderWidth = 1.0;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)clickDeleteBtn:(id)sender {
    UIButton *button = sender;
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[button superview]superview];
    } else {
    cell = (UITableViewCell *)[[[button superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *theIndexMutArr = [NSMutableArray arrayWithArray:[ud objectForKey:@"_theIndexMutArr"]];

//    [_indexPathMutArr removeObjectAtIndex:indexPath.row];

    NSIndexPath *indexPath2 = [_indexPathMutArr objectAtIndex:indexPath.row];
                              NSUInteger uInteger2 = indexPath2.row;
    [_delegate deleteBtnDidClicked:self index:uInteger2 indexPath:indexPath];
    
   
    [theIndexMutArr removeObjectAtIndex:indexPath.row];
    [ud setObject:theIndexMutArr forKey:@"_theIndexMutArr"];
    
    [_theDataMutArr removeObjectAtIndex:indexPath.row];
    [_theTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    [_theTableView reloadData];

}

- (void)configTheIndex {

       static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        for (int i = 0; i < _theDataMutArr.count; i++) {
            [_theIndexMutArr addObject:@(i)];
        }

        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:_theIndexMutArr forKey:@"_theIndexMutArr"];

    });
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *theIndexMutArr = [NSMutableArray arrayWithArray:[ud objectForKey:@"_theIndexMutArr"]];

    if (theIndexMutArr.count < _theDataMutArr.count) {
        for (int i = _theIndexMutArr.count; i < _theDataMutArr.count; i++) {
            [_theIndexMutArr addObject:@(i)];
        }
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:_theIndexMutArr forKey:@"_theIndexMutArr"];
    }
   }

- (void)initTheVars {
    _theIndexMutArr = [NSMutableArray arrayWithCapacity:10];
}

@end
