//
//  ModifyBagNameViewController.m
//  UCook
//
//  Created by huangrun on 14/10/31.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ModifyBagNameViewController.h"
#import "NSStringAdditions.h"

@interface ModifyBagNameViewController ()

@end

@implementation ModifyBagNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"修改商品包名";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickDoneBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:self.textField.text]) {
        [GlobalSharedClass showAlertView:@"请输入包名"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"update",@"uid":[AccountManager getUid],@"id":self.bagIdStr,@"name":self.textField.text,@"remark":@""};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            if (self.delegate) {
                [self.delegate bagNameDidModify:self];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}
@end
