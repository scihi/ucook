//
//  TakeDeliveryAddressViewController.m
//  UCook
//
//  Created by scihi on 14-8-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "TakeDeliveryAddressViewController.h"
#import "TakeDeliveryAddressModel.h"
#import "AddAddressViewController.h"
#import "ModifyAddressViewController.h"

#import "NSObject+MJKeyValue.h"
#import "NSObject+MJMember.h"

@interface TakeDeliveryAddressViewController () {
    TakeDeliveryAddressModel *_takeDeliveryAddressModel;
    NSArray *_takeDeliveryAddressModelArr;
}

@end

@implementation TakeDeliveryAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"收货地址";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getAddressListData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getAddressListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"recAddress",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _takeDeliveryAddressModelArr = [TakeDeliveryAddressModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];

        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _takeDeliveryAddressModelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TakeDeliveryAddressCell";
    UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    TakeDeliveryAddressModel *takeDeliveryAddressModel = [_takeDeliveryAddressModelArr objectAtIndex:indexPath.row];
    cell.textLabel.text = takeDeliveryAddressModel.contactName;
    cell.detailTextLabel.text = takeDeliveryAddressModel.address;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TakeDeliveryAddressModel *takeDeliveryAddressModel = [_takeDeliveryAddressModelArr objectAtIndex:indexPath.row];
    //其他vc入口进入的处理事件
    if(_vCTag == 101) {
    [_delegate addressDidSelect:self address:takeDeliveryAddressModel.address name:takeDeliveryAddressModel.contactName id:takeDeliveryAddressModel.id];
            [self.navigationController popViewControllerAnimated:YES];
        //我的优厨vc入口的处理事件
    } else if (_vCTag == 102) {
        ModifyAddressViewController *modifyAddressVC = [[ModifyAddressViewController alloc]initWithNibName:@"ModifyAddressViewController" bundle:nil];
        modifyAddressVC.takeDeliveryAddressIdStr = takeDeliveryAddressModel.id;
        [self.navigationController pushViewController:modifyAddressVC animated:YES];
    }
}

- (IBAction)clickAddAddressBtn:(id)sender {
    AddAddressViewController *addAddressVC = [[AddAddressViewController alloc]initWithNibName:@"AddAddressViewController" bundle:nil];
    [self.navigationController pushViewController:addAddressVC animated:YES];
}
@end
