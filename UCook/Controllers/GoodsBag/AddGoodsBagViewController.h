//
//  AddGoodsBagViewController.h
//  UCook
//
//  Created by huangrun on 14-7-28.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

//添加商品包后不添加商品而直接点击返回的delegate
@protocol AddGoodsBagVCDelegate <NSObject>

- (void)dismissBtnDidClick;

@end

@interface AddGoodsBagViewController : GeneralWithBackBtnViewController <UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *goodsBagNameTF;
- (IBAction)clickConfirmBtn:(id)sender;
@property (assign, nonatomic) id <AddGoodsBagVCDelegate> delegate;

@end
