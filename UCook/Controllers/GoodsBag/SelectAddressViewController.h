//
//  SelectAddressViewController.h
//  UCook
//
//  Created by scihi on 14-8-6.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class SelectAddressViewController;

@protocol SelectAddressVCDelegate <NSObject>

- (void)addressDidSelect:(SelectAddressViewController *)selectAddressVC address:(NSString *)aAddress idsDictionary:(NSDictionary *)aIdsDictionary;

@end

@interface SelectAddressViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id <SelectAddressVCDelegate> delegate;

@end
