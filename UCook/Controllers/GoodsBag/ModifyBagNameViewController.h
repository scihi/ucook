//
//  ModifyBagNameViewController.h
//  UCook
//
//  Created by huangrun on 14/10/31.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class ModifyBagNameViewController;

@protocol ModifyBagNameVCDelegate <NSObject>

- (void)bagNameDidModify:(ModifyBagNameViewController *)modifyBagNameVC;

@end

@interface ModifyBagNameViewController : GeneralWithBackBtnViewController

@property (copy, nonatomic) NSString *bagIdStr;
@property (copy, nonatomic) NSString *nameStr;
@property (assign, nonatomic) id <ModifyBagNameVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)clickDoneBtn:(id)sender;

@end
