//
//  ShopViewController.h
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ShopViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, copy) NSString *bagIdStr;

@property (copy, nonatomic) NSString *searchNameStr;

@property (nonatomic) BOOL fromAddGoodsBagClass;


@end
