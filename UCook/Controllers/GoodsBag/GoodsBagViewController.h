//
//  GoodsBagViewController.h
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"
#import "SharePopupTableView.h"
#import "AddGoodsBagViewController.h"
#import "ModifyBagNameViewController.h"

@interface GoodsBagViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate, CustomTableViewDelegate, UIAlertViewDelegate, AddGoodsBagVCDelegate, ModifyBagNameVCDelegate>
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickAddGoodsBagBtn:(id)sender;
- (IBAction)clickParticipatInBidBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *addGoodsBagBtn;
@property (strong, nonatomic) IBOutlet UIButton *participateInBidBtn;
- (IBAction)clickModifyBagNameBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *modifyBagNameBtn;

@end
