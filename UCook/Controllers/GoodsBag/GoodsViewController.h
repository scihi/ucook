//
//  GoodsViewController.h
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "SelectedGoodsViewController.h"

@interface GoodsViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, SelectedGoodsVCDelegate>
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickCheckSelectedBtn:(id)sender;
- (IBAction)clickConfirmAddBtn:(id)sender;

@property (nonatomic) NSInteger shopIdInteger;
@property (nonatomic, copy) NSString *shopNameStr;
@property (nonatomic, copy) NSString *bagIdStr;

@end
