//
//  ShopViewController.m
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ShopViewController.h"
#import "SearchViewController.h"
#import "UIImageView+AFNetworking.h"
#import "MJRefresh.h"
#import "GoodsViewController.h"

#import "PrettySearchBar.h"
#import "CGRectAdditionals.h"

#import "ShopInfoViewController.h"

static NSString *CellIdentifier = @"ShopCell";

@interface ShopViewController () {
    UITableView *_theTableView;
    NSMutableArray *_theDataMutArr;
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    PrettySearchBar *_searchBar;
    
    UIView *_noDataView;//无数据时的展示背景
    
    BOOL _fromOtherVC;//用于区分搜索的内容，翻页时需要
}

@end

@implementation ShopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_searchBar resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64:568 - 64 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    [self initTheVars];

    //设置导航栏
    [self configTheNavBar];
    
    //配置搜索栏
    [self configTheSearchBar];
    
    //获取商品包店铺列表数据
    [self getShopData];
    
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerNib:[UINib nibWithNibName:@"ShopCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    [self.view addSubview:_theTableView];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_ico.png"]];
    navRightIV.frame = CGRectMake(15, 7, 20, 20);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
//    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;
    
    if (_fromAddGoodsBagClass) {
    _theBackButton.hidden = YES;
    } else {
        _theBackButton.hidden = NO;
    }
    
}


- (void)clickNavRightBtn:(id)sender {
    [self getShopDataByClickSearchBtn];
    [_searchBar resignFirstResponder];
}

//获取商品包店铺列表数据，通过从其他页面搜索进入
- (void)getShopData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    if (!_searchNameStr) {
        _searchNameStr = @"";
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"store",@"action": @"list",@"name": _searchNameStr,@"page": @(_pageNumber)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
            [_theTableView reloadData];
            
            _fromOtherVC = YES;
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {

                [_theDataMutArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                [_theTableView reloadData];
            [_noDataView removeFromSuperview];
            
            _theTableView.backgroundColor = [UIColor clearColor];
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];


        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

//获取商品包店铺列表数据,通过点击该页的搜索按钮
- (void)getShopDataByClickSearchBtn {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    _searchNameStr = _searchBar.text;
    if (!_searchNameStr) {
        _searchNameStr = @"";
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"store",@"action": @"list",@"name": _searchNameStr,@"page": @(_pageNumber)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
            [_theTableView reloadData];
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor  = [UIColor whiteColor];
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
            
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
            [_theTableView reloadData];
            
            [_noDataView removeFromSuperview];
            
            _theTableView.backgroundColor = [UIColor clearColor];
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];

            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _theDataMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *cellDictionary = [_theDataMutArr objectAtIndex:indexPath.row];
    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [cellDictionary objectForKey:@"logo"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:102];
    nameLabel.text = [cellDictionary objectForKey:@"name"];
    
    //实体认证状态
    UILabel *authStatusLabel = (UILabel *)[cell viewWithTag:103];
    NSString *authStatusStr;
    if ([[cellDictionary objectForKey:@"realStatus"]integerValue] == 2) {
         authStatusStr = @"实体店认证";
    } else {
        authStatusStr = @"";
    }
    authStatusLabel.text = authStatusStr;
    
    UILabel *addressLabel = (UILabel *)[cell viewWithTag:104];
    addressLabel.text = [cellDictionary objectForKey:@"address"];
    
    UILabel *introLabel = (UILabel *)[cell viewWithTag:105];
    introLabel.text = [cellDictionary objectForKey:@"intro"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_fromAddGoodsBagClass) {
    GoodsViewController *goodsVC = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
    goodsVC.shopIdInteger = [[[_theDataMutArr objectAtIndex:indexPath.row]objectForKey:@"id"]integerValue];
    goodsVC.shopNameStr = [[_theDataMutArr objectAtIndex:indexPath.row]objectForKey:@"name"];
    goodsVC.bagIdStr = _bagIdStr;
    [self.navigationController pushViewController:goodsVC animated:YES];
    } else {
        ShopInfoViewController *shopInfoVC = [[ShopInfoViewController alloc]initWithNibName:@"ShopInfoViewController" bundle:nil];
        shopInfoVC.storeIdStr = [[_theDataMutArr objectAtIndex:indexPath.row]objectForKey:@"id"];
        [self.navigationController pushViewController:shopInfoVC animated:YES];
    }
}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        if (!_fromOtherVC) {
        _searchNameStr = _searchBar.text;
        }
        if (!_searchNameStr) {
            _searchNameStr = @"";
        }

        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"store",@"action": @"list",@"name": _searchNameStr,@"page": @(_pageNumber)};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [self stopRequest];
            if (RIGHT) {
                //            [_theDataMutArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
                [self doneWithView:refreshView];
            } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {

                    //            [_theDataArr removeAllObjects];
                    [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                    [self doneWithView:refreshView];

            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (void)initTheVars {
    _theDataMutArr = [NSMutableArray arrayWithCapacity:10];
    
    _pageNumber = 0;
}

- (void)configTheSearchBar {
    _searchBar = [[PrettySearchBar alloc]initWithFrame:CGRectMake(0, 0, 175, 44)];
    _searchBar.delegate = self;
    //    searchBar.showsCancelButton = YES;
    //    [searchBar sizeToFit];
    [_searchBar setTintColor:[UIColor blackColor]];
    _searchBar.searchBarStyle=UISearchBarStyleMinimal;
    
    _searchBar.placeholder = @"请输入关键字";
    
    [_searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"input"] forState:UIControlStateNormal];
//    [_searchBar setSearchTextPositionAdjustment:UIOffsetMake(25, 0)];
    //    [[UISearchBar appearance] setSearchFieldBackgroundPositionAdjustment:UIOffsetMake(30, 0)];
//    [_searchBar setImage:[UIImage imageNamed:@"search_replace"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];//去掉searchBar前面的搜索图标 方法二 huangrun
    //    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];//去掉searchBar前面的搜索图标 方法一 huangrun
    //    self.tintColor = [UIColor whiteColor];
    
    //将搜索条放在一个UIView上
    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 175, 44)];
    searchView.backgroundColor = [UIColor clearColor];
    [searchView addSubview:_searchBar];
    
    self.navigationItem.titleView = searchView;

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self getShopDataByClickSearchBtn];
    [_searchBar resignFirstResponder];
}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}


@end
