//
//  AuctionViewController.m
//  UCook
//
//  Created by scihi on 14-8-1.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AuctionViewController.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "AddToAuctionViewController.h"

static NSString *CellIdentifier = @"GoodsBagCell";

@interface AuctionViewController () {
    UIView *_noDataView;//无数据时的展示背景
    NSDictionary *_theDataDic;
    UITableViewCell *_cell;
    NSMutableArray *_goodsIdStrMutArr;
    NSMutableArray *_goodsNumMutArr;
    NSMutableArray *_goodsInfoMutArr;
    NSMutableArray *_tempGoodsNumMutArr;
    NSMutableArray *_goodsPriceMutArr;
    BOOL _noZero;//判断数量按钮值是否全不为0
    
    StepperTestViewController *stepperTestVC;
    
    NSArray *_productsArr;
}

@end

@implementation AuctionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"加入竞拍";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerCell];
    [self initTheVars];
    for (int i = 0; i < _theDataArr.count; i++) {
        [_tempGoodsNumMutArr addObject:@"0"];
    }
    
    _productsArr = [NSArray array];
    
    [self getDetailBagInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickNextStepBtn:(id)sender {
    //遍历产品id
    [_goodsInfoMutArr removeAllObjects];
    for (NSDictionary *dictionary in _productsArr) {
        NSString *string = [dictionary objectForKey:@"itemId"];
        [_goodsIdStrMutArr addObject:string];
    }
    
    //遍历产品价格
    [_goodsPriceMutArr removeAllObjects];
    for (NSDictionary *dictionary in _theDataArr) {
        NSString *string = [dictionary objectForKey:@"price"];
        [_goodsPriceMutArr addObject:string];
    }
    
    //遍历cell上的数量按钮的值，也就是产品数量
//    for (int i = 0; i < _theDataArr.count; i++) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//        UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
//        UIButton *countButton = (UIButton *)[cell viewWithTag:107];
//        [_goodsNumMutArr addObject:countButton.titleLabel.text];
//    }
    [_goodsNumMutArr removeAllObjects];
    for (int i = 0; i < _tempGoodsNumMutArr.count; i++) {
        [_goodsNumMutArr addObject:[_tempGoodsNumMutArr objectAtIndex:i]];
    }
    
    
    //把产品id和数量组成字典加入新的数组
    for (int i = 0; i < _theDataArr.count; i++) {
        [_goodsInfoMutArr addObject:@{@"itemId": [_goodsIdStrMutArr objectAtIndex:i ], @"number": [_goodsNumMutArr objectAtIndex:i]}];
    }
    
    NSMutableIndexSet *mutIndexSet = [NSMutableIndexSet indexSet];
    for (NSDictionary *dict in _goodsInfoMutArr) {
        if ([[dict objectForKey:@"number"]isEqualToString:@"0"]) {
            [mutIndexSet addIndex:[_goodsInfoMutArr indexOfObject:dict]];
        }
    }
    

    [_goodsInfoMutArr removeObjectsAtIndexes:mutIndexSet];
    
    //遍历产品数量并判断是否全为0
    for (NSString *string in _goodsNumMutArr) {
        if (![string isEqualToString:@"0"]) {
            _noZero = YES;
        }
    }
    
    if (_noZero) {
        AddToAuctionViewController *addToAuctionVC = [[AddToAuctionViewController alloc]initWithNibName:@"AddToAuctionViewController" bundle:nil];
        addToAuctionVC.goodsInfoMutArr = _goodsInfoMutArr;
        addToAuctionVC.goodsBagNameStr = _goodsBagNameStr;
        addToAuctionVC.goodsBagIdStr = _goodsBagIdStr;
        addToAuctionVC.goodsPriceMutArr = _goodsPriceMutArr;
        [self.navigationController pushViewController:addToAuctionVC animated:YES];
    } else {
        [GlobalSharedClass showAlertView:@"请选择需要竞拍产品的数量"];
    }
}

#pragma - mark - tableView dataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _theDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    _theTableView.hidden = NO;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"image"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"name"];
    UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
    NSString *specificationString = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"spec"];
    specificationLabel.text = [NSString stringWithFormat:@"规格:%@",specificationString];
    UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];//单位
    NSString *unitString = [specificationString substringFromIndex:specificationString.length - 1];;
    unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitString];
    UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:105];
    NSString *currentPriceStr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"discountPrice"];
    currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:106];//原价
    originalPriceLabel.strikeThroughEnabled = YES;
    NSString *originalPriceStr = [[_theDataArr objectAtIndex:indexPath.row]objectForKey:@"price"];
    originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",originalPriceStr];
    
    //数量选择标签
    UIButton *countButton = (UIButton *)[cell viewWithTag:107];
    countButton.layer.borderWidth = 1.0;
    countButton.layer.borderColor = [UIColor colorWithRed:234.00f/255.00f green:234.00f/255.00f blue:234.00f/255.00f alpha:1.00f].CGColor;
    NSString *numberString = [_tempGoodsNumMutArr objectAtIndex:indexPath.row];
    [countButton setTitle:numberString forState:UIControlStateNormal];
    [countButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [countButton addTarget:self action:@selector(setTheGoodsNumber:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)registerCell {
    [_theTableView registerNib:[UINib nibWithNibName:@"ShoppingCartCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
}

- (void)setTheGoodsNumber:(id)sender {
    UIButton *button = sender;
    if (IS_IOS8) {
            _cell = (UITableViewCell *)[[button superview]superview];
    } else {
    _cell = (UITableViewCell *)[[[button superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
    _theDataDic = [_theDataArr objectAtIndex:indexPath.row];
    
    stepperTestVC = [[StepperTestViewController alloc]initWithNibName:@"StepperTestViewController" bundle:nil];
    stepperTestVC.theFloat = [[_tempGoodsNumMutArr objectAtIndex:indexPath.row]floatValue];
    stepperTestVC.Maximum = 10000;
    stepperTestVC.view.frame = CGRectMake(0, 0, 300, 200);
    stepperTestVC.delegate = self;
    stepperTestVC.contador.textField.delegate = self;
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [self addChildViewController:stepperTestVC];
    [view addSubview:stepperTestVC.view];
    stepperTestVC.view.center = view.center;
    [stepperTestVC didMoveToParentViewController:self];
    
    //     stepperTestVC.stepperLabel.text = @"0";
    stepperTestVC.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] * stepperTestVC.theFloat];
    
    [self.view addSubview:view];
    self.navigationController.navigationBar.userInteractionEnabled = NO;

}

#pragma mark - textStepperDelegate
- (void)stepperValueDidChange:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    
    stepperTestViewController.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] * counter];
}

- (void)stepperDidClickConfirmBtn:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
     UIButton *countButton = (UIButton *)[_cell viewWithTag:107];
    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
    [countButton setTitle:[NSString stringWithFormat:@"%d",counter] forState:UIControlStateNormal];
    [_tempGoodsNumMutArr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",counter]];
}

- (void)initTheVars {
    _goodsIdStrMutArr = [NSMutableArray arrayWithCapacity:10];
    _goodsNumMutArr = [NSMutableArray arrayWithCapacity:10];
    _goodsInfoMutArr = [NSMutableArray arrayWithCapacity:10];
    _tempGoodsNumMutArr  = [NSMutableArray arrayWithCapacity:10];
    _goodsPriceMutArr = [NSMutableArray arrayWithCapacity:10];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    stepperTestVC.view.frame = CGRectMake(10, 5, 300, 200);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    stepperTestVC.view.center = view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [stepperTestVC.contador.textField resignFirstResponder];
    return YES;
}

- (void)getDetailBagInfo {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"productBox",@"action": @"boxProduct",@"uid":[AccountManager getUid],@"boxId":self.goodsBagIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)  {
        [self stopRequest];
        if (RIGHT) {
            _productsArr = [[[responseObject objectForKey:@"data"]firstObject] objectForKey:@"products"];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


@end
