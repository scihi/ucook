//
//  AuctionViewController.h
//  UCook
//
//  Created by scihi on 14-8-1.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "StepperTestViewController.h"

@interface AuctionViewController : GeneralWithBackBtnViewController
<TextStepperDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickNextStepBtn:(id)sender;
@property (nonatomic, strong) NSArray *theDataArr;
@property (nonatomic, copy) NSString *goodsBagNameStr;
@property (nonatomic, copy) NSString *goodsBagIdStr;

@end
