//
//  SelectedGoodsViewController.h
//  UCook
//
//  Created by scihi on 14-7-30.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
@class SelectedGoodsViewController;

@protocol SelectedGoodsVCDelegate <NSObject>

- (void)deleteBtnDidClicked:(SelectedGoodsViewController *)selectedGoodsVC index:(NSUInteger)index indexPath:(NSIndexPath *)indexPath;
- (void)navRightBtnDidClicked:(SelectedGoodsViewController *)selectedGoodsVC;

@end

@interface SelectedGoodsViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *theDataMutArr;
@property (nonatomic, strong) NSMutableArray *indexPathMutArr;
@property (nonatomic, assign) id <SelectedGoodsVCDelegate> delegate;
@property (nonatomic, copy) NSString *shopNameStr;

@end