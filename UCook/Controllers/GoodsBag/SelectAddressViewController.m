//
//  SelectAddressViewController.m
//  UCook
//
//  Created by scihi on 14-8-6.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SelectAddressViewController.h"
#import "SelectAddressModel.h"

#import "NSObject+MJKeyValue.h"

static NSString *CellIdentifer = @"SelectAddressCell";

@interface SelectAddressViewController () {
    NSString *_areaIdString;
    SelectAddressModel *_selectAddressModel;
    NSArray *_selectAddressModelArr;
    UITableView *_theTableView;
    NSString *_headerTitleStr0;//省份名字
    NSString *_headerTitleStr1;//市名字
    NSString *_headerTitleStr2;//区名字
    NSString *_provinceIdStr;//省id
    NSString *_cityIdStr;//市id
    NSString *_districtIdStr;//区id
    NSInteger _headerNumTag;//用来标记进入tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath的次数 防止第一次_headerNumber ++
    NSInteger _headerNumber;
}

@end

@implementation SelectAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"新增收货地址";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _areaIdString = @"";
    _headerNumTag = 0;
    _headerNumber = 1;
    
    [self initTheTableView];
    [self getSelectAddressListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getSelectAddressListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"areaList",@"areaId":_areaIdString};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _selectAddressModelArr = [SelectAddressModel objectArrayWithKeyValuesArray:array];
            [_theTableView reloadData];
            if (_selectAddressModelArr.count != 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:_headerNumber - 1];
            [_theTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
            }
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_headerNumber == 1) {
            return _selectAddressModelArr.count;
    } else if (_headerNumber == 2) {
        if (section == 0) {
            return 0;
        } else if (section == 1) {
            return _selectAddressModelArr.count;
        }
    } else if (_headerNumber == 3) {
        if (section == 0) {
            return 0;
        } else if (section == 1) {
            return 0;
        } else {
            return _selectAddressModelArr.count;
        }
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TakeDeliveryAddressCell";
    UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    SelectAddressModel *selectAddressModel = [_selectAddressModelArr objectAtIndex:indexPath.row];
    cell.textLabel.text = selectAddressModel.name;
    
    return cell;
}

- (void)initTheTableView {
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifer];
    [self.view addSubview:_theTableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _areaIdString = [[_selectAddressModelArr objectAtIndex:indexPath.row]id];
    
    if (_headerNumTag != 0) {
        _headerNumber ++;
    }
    if (_headerNumber == 1) {
        _headerTitleStr0 = [[_selectAddressModelArr objectAtIndex:indexPath.row]name];
        _provinceIdStr = [[_selectAddressModelArr objectAtIndex:indexPath.row]id];
    } else if (_headerNumber == 2) {
        _headerTitleStr1 = [[_selectAddressModelArr objectAtIndex:indexPath.row]name];
        _cityIdStr = [[_selectAddressModelArr objectAtIndex:indexPath.row]id];
    } else  if (_headerNumber == 3){
        _headerTitleStr2 = [[_selectAddressModelArr objectAtIndex:indexPath.row]name];
        _districtIdStr = [[_selectAddressModelArr objectAtIndex:indexPath.row]id];
    }

    _headerNumTag++;
    [self getSelectAddressListData];
    
    if (_headerNumber == 3) {
        NSString *addressString = [[_headerTitleStr0 stringByAppendingString:_headerTitleStr1]stringByAppendingString:_headerTitleStr2];
        NSDictionary *idsDictionary = @{@"provinceId": _provinceIdStr,@"cityId": _cityIdStr,@"districtId": _districtIdStr};
        [_delegate addressDidSelect:self address:addressString idsDictionary:idsDictionary];
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([_headerTitleStr0 isEqualToString:@"海外"]) {
        if (_headerNumber == 2) {
            NSString *addressString = [_headerTitleStr0 stringByAppendingString:_headerTitleStr1];
            NSDictionary *idsDictionary = @{@"proviceId": _provinceIdStr,@"cityId": _cityIdStr};
            [_delegate addressDidSelect:self address:addressString idsDictionary:idsDictionary];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _headerTitleStr0;
    } else if (section == 1) {
        return _headerTitleStr1;
    }   else if (section == 2) {
        return _headerTitleStr2;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _headerNumber;
}


@end
