//
//  HRGeneralPickerView.h
//  chronic
//
//  Created by huangrun on 14-1-15.
//  Copyright (c) 2014年 chenhai. All rights reserved.
//解决iOS7中picker可以操作其下层视图的问题 huangrun

#import <UIKit/UIKit.h>

@interface HRGeneralPickerView : UIPickerView

@end
