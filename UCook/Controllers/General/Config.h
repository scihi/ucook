
#ifndef XX_Config_h
#define XX_Config_h

#pragma mark -
#pragma mark notification defined
#define NOTIFICATION_LOGIN_SUCCEEDED            @"NOTIFICATION_LOGIN_SUCCEEDED"
#define NOTIFICATION_REGISTER_SUCCEEDED         @"NOTIFICATION_REGISTER_SUCCEEDED"
#define NOTIFICATION_LOGOUT_SUCCEEDED           @"NOTIFICATION_LOGOUT_SUCCEEDED"
#define NOTIFICATION_USER_GUIDE_SUCCEEDED       @"NOTIFICATION_USER_GUIDE_SUCCEEDED"
#define NOTIFICATION_PUSH_MESSAGE_COMMING       @"NOTIFICATION_PUSH_MESSAGE_COMMING"

#define NOTIFICATION_UPDATE_BASIC_DATA_START    @"NOTIFICATION_UPDATE_BASIC_DATA_START"
#define NOTIFICATION_UPDATE_BASIC_DATA_FINISHED @"NOTIFICATION_UPDATE_BASIC_DATA_FINISHED"
#define NOTIFICATION_DOWNLOAD_AD_LIST_FINISHED  @"NOTIFICATION_DOWNLOAD_AD_LIST_FINISHED"

#define NOTIFICATION_GET_GPS_LOCATION_SUCCEEDED  @"NOTIFICATION_GET_GPS_LOCATION_SUCCEEDED"
#define NOTIFICATION_GET_GPS_LOCATION_FAILED  @"NOTIFICATION_GET_GPS_LOCATION_FAILED"

#define NS_GOTO_DEALER_MODULE @"goToDealerModule" //经销商用户进入经销商模块通知
#define NS_GOTO_REST_MODULE @"goToRestModule" //经销商用户进入餐厅模块通知
#define NS_DEALER_LAST_PLACE @"dealerLastPlace" //经销商用户最后一次进入的模块通知



#pragma mark - 母窗口位置常量定义
#define NAVIGATION_PANEL_HEIGHT                 64
#define NAVIGATION_PANEL_TITLE_Y_OFFSET         64
#define BOTTOM_BAR_HEIGHT                       49

#define WAITTING_VIEW_YOFFSET                   -100
#define DEFAULT_DELAY_SECONDS                   1.2
#define DEFAULT_DELAY_SPLASH                    3.0
#define MBHUD_VIEW_TAG                          100001

#define POPUP_PANEL_HEIGHT                      210

#define INPUT_INDICATOR_STAR_WIDTH              20

#pragma mark - network defined
#define MSG                             @"msg"
#define RIGHT ([[responseObject objectForKey:@"code"]integerValue] == 1)//优厨网HTTP请求成功后结果反馈正确
#define MESSAGE [responseObject objectForKey:@"msg"]//优厨网HTTP请求成功后结果反馈错误信息
#define kAppDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define kTableViewFrame CGRectMake(5, 0, BOUNDS.size.width - 10, BOUNDS.size.height - 64 - 49) //tableview在大多数情况下的frame 两边有间距的风格里面用
#define kTableViewBackgroundColor [UIColor colorWithRed:234.00f/255.00f green:234.00f/255.00f blue:234.00f/255.00f alpha:1.00f] //我喜欢的tableview背景色
#define kThemeColor [UIColor colorWithRed:95.00f/255.00f green:160.00f/255.00f blue:40.00f/255.00f alpha:1.00f] //优厨网的主题色(绿)
#define kThemeCGColor [UIColor colorWithRed:95.00f/255.00f green:160.00f/255.00f blue:40.00f/255.00f alpha:1.00f].CGColor //优厨网的主题色(绿) layer用
#define kDefaultImage [UIImage imageNamed:@"default_pic"] //经销商端默认图片

#define TABLE_VIEW_FRAME CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height -  64) //除去tabBar高度的tableViewFrame 扁平化风格里面用

#define TABLE_VIEW_FRAME_WITHOUT_TOOLBAR CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height -  64 - 49) //除去tabBar高度的tableViewFrame 扁平化风格里面用

#define kThemeColorDealer [UIColor colorWithRed:83.00f/255.00f green:147.00f/255.00f blue:197.00f/255.00f alpha:1.00f] //优厨网的主题色(淡蓝)
#define kThemeCGColorDealer [UIColor colorWithRed:83.00f/255.00f green:147.00f/255.00f blue:197.00f/255.00f alpha:1.00f].CGColor //优厨网的主题色(淡蓝) layer用

#define BASIC_OUTURL                     @"http://192.168.1.16:8010/api.html"//优厨网内网
//#define BASIC_URL                     @"http://s-82207.gotocdn.com/api.html"//优厨网外网
#define BASIC_URL                       @"http://www.youchu100.com/api.html" // 优厨商城外网2

#define APP_KEY                         @"111111" //优厨网临时key
#define COMPANY_TEL                     @"02885363233"

#define PAGE_SIZE_LIMITED               20
#define PAGE_SIZE_LIMITED_STRING        @"20"
#define OperationQueue_Max              6
#define MD5_HASH_MARK                   @"enveesoft"
#define APP_DOWNLOAD_URL                @"http://www.sina.com.cn"

#pragma mark - option button
#define OPTION_ITEM_ID                  @"id"
#define OPTION_ITEM_VALUE               @"value"
#define OPTION_SELECTED_NULL_VALUE      @"99999"

#pragma mark - input limited
#define FEEDBACK_INPUT_LIMITED          200
#define BUDGET_AMOUNT_INPUT_LIMITED     12
#define SHOPPING_LIST_INPUT_LIMITED     10
#define EMAIL_INPUT_LIMITED             100

#define DEFAULT_SCHEME_NAME             @"a"

#define DEFAULT_CELL_LOAING_IMAGE       @"imageAD"//HR修改 默认为空

#define CELL_LABEL_WIDTH                80

#pragma mark - Macro Manager
#define Release(__object) if(__object){[__object release];__object=nil;}
#define InvalidateTimer(__timer) if(__timer){[__timer invalidate];__timer=nil;}
#define LoaderRelease(__loader) if(__loader){__loader.theCallbackObj = nil; [__loader release];__loader=nil;}

#pragma mark - baidu map app
#define BAIDU_MAP_APP_ID        @"555425"
#define BAIDU_MAP_APP_KEY       @"bNfnhI7vBkRxRWVKBTnDd4sHAPI"
#define BAIDU_MAP_SECRET_KEY    @"uPeeGeb2lYAkrUyiYEItUetVzA51Z2GR"
#define BAIDU_MAP_NODE_MERGE_DISTANCE_PIXEL     50      //百度地图， 两个节点合并像素

#define APP_OLD_VERSION         0

#define HEIGHT_IPHONE_5 568
#define HEIGHT_IPHONE_4 480
#define IS_IPHONE_5 ([[ UIScreen mainScreen ] bounds ].size.height == HEIGHT_IPHONE_5)
#define IS_IPHONE_4 ([[ UIScreen mainScreen ] bounds ].size.height == HEIGHT_IPHONE_4)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)

#define BOUNDS [[ UIScreen mainScreen ] bounds]
#define APPLICATION_FRAME [[ UIScreen mainScreen ] applicationFrame]


#define GLOBLE_IMAGE_TOGGLE @"globleImageToggle" //全局图片模式开关 此宏定义ud用了 ns也用了 huangrun

//response type
typedef enum{
    LastResponseType_TextField,
    LastResponseType_TextView,
    LastResponseType_WebView
} LastResponseType;

//merchant entry type
typedef enum{
    MerchantEntryType_More,         //更多详情
    MerchantEntryType_Aound,        //周边商户
    MerchantEntryType_Recommand,    //推荐商户
    MerchantEntryType_Map_Search,   //地图搜索
    MerchantEntryType_Consumer,     //持卡消费
    MerchantEntryType_Go            //去哪儿
} MerchantEntryType;

//merchant list type
typedef enum{
    MerchantListType_Search,        //条件搜索商户
    MerchantListType_Aound,         //周边商户
    MerchantListType_Recommand,     //推荐商户
    MerchantListType_All,           //所有商户
} MerchantListType;

//需要更新的数据
typedef enum{
    OfflineDataType_About,              //关于
    OfflineDataType_Xieyi,              //注册协议
    OfflineDataType_BuyAndJoinin,       //购卡, 加盟
    OfflineDataType_WebSiteBuyCard,     //购卡网点
    OfflineDataType_HXTCommsqs,         //商圈
    OfflineDataType_HXTCommtype,        //商户类别
    OfflineDataType_GetHXTCard,         //和信通卡
    OfflineDataType_BookData            //小册子(商户目录信息)
} OfflineDataType;

//搜索类型 (1 距离 2 收藏量 3 新加盟 4 星级)
typedef enum{
    MerchantSearchType_Distance=1,
    MerchantSearchType_Favorites=2,
    MerchantSearchType_Joinin=3,
    MerchantSearchType_Star
} MerchantSearchType;

typedef enum{
    APPModuleType_MerchantDirectory = 0,
    APPModuleType_CardHolderNotice,
    APPModuleType_Promotions,
    APPModuleType_Balance,
    APPModuleType_Go,
    APPModuleType_Around,
    APPModuleType_Consumer,
    APPModuleType_Member,
    APPModuleType_BuyCard,
    APPModuleType_Normal,
    APPModuleType_Joinin,
    APPModuleType_Search,
    APPModuleType_More    
} APPModuleType;

//typedef enum {
//    TicketFiltrateSelectType_region,
//    TicketFiltrateSelectType_theme,
//    TicketFiltrateSelectType_sort
//} TicketFiltrateSelectTypes;//景点筛选类型

#endif
