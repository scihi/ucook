//
//  UITableView+HRAdditions.m
//  WankaCarSteward
//
//  Created by huangrun on 14-5-29.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "UITableView+HRAdditions.h"

@implementation UITableView (HRAdditions)

- (void)setNeedsDisplay {
    self.tableFooterView = [[UIView alloc]init];
}

//- (void)didAddSubview:(UIView *)subview {
//    
//    self.backgroundColor = kTableViewBackgroundColor;
//}//有后遗症 比如datepicker背景混乱 所以暂取消此方法 改在各自类中实现 huangrun

@end
