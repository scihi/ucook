//
//  GeneralViewController.m
//  chronic
//
//  Created by 黄润 on 13-8-12.
//  Copyright (c) 2013年 chenhai. All rights reserved.
//

#import "GeneralViewController.h"
#import "MBProgressHUDAdditoin.h"
#import "AppDelegate.h"

@interface GeneralViewController ()

@end

@implementation GeneralViewController

@synthesize theStopRequestBlock;
@synthesize theStartRequestBlock;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = kTableViewBackgroundColor;
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];//一行代码改变状态栏为透明背景白色字 huangrun
    
    _theBackButton = [[UIButton alloc] initWithFrame:[self BackButtonRect]];
    [_theBackButton setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [_theBackButton setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    [_theBackButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    _theBackButton.hidden = YES;
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_back.png"]];
    navLeftIV.frame = CGRectMake(10, 5, 30, 24);
    [_theBackButton addSubview:navLeftIV];
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:_theBackButton];
    
    //未考虑到第一次安装APP使用时ud为nil的情况 huangrun
//     if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
//    self.navigationItem.leftBarButtonItem = backBarBtn;
//     } else {
//         //修改iOS7中导航栏的返回标题 1016 huangrun
//         UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:nil];
//         self.navigationItem.backBarButtonItem = item;
//        //修改iOS7中导航栏标题颜色 1016 huangrun
//         self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
//         self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//         [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//         self.navigationController.navigationBar.translucent = NO;
//     }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"dealer"]) {
        
        //修改iOS7中导航栏的返回标题 1016 huangrun
        UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:nil];
        self.navigationItem.backBarButtonItem = item;
        //修改iOS7中导航栏标题颜色 1016 huangrun
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        self.navigationController.navigationBar.translucent = NO;
    } else {
        self.navigationItem.leftBarButtonItem = backBarBtn;
    }

    //结束修改

    if (IS_IOS7) {
//
//        self.automaticallyAdjustsScrollViewInsets = NO;
        [self setEdgesForExtendedLayout:UIRectEdgeNone];//ios7适配iOS6关键代码 使顶部坐标从0，0开始算 但要注意理解 举例：有导航栏、选项卡等 则0，0是从导航栏底部开始的 如4寸屏下self.view的坐标为(0,0,320,504) huangrun
////        self.extendedLayoutIncludesOpaqueBars = NO;
//        
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.extendedLayoutIncludesOpaqueBars = NO;
//        self.modalPresentationCapturesStatusBarAppearance = NO;
//        self.navigationController.navigationBar.translucent = NO;
//        self.tabBarController.tabBar.translucent = NO;
    }
    
    //以下配置已经写在了
    
//    // 1.取出设置主题的对象
//    
//    UINavigationBar *navBar = [UINavigationBar appearance];
//
////    // 2.设置导航栏的背景图片
//    
//    NSString *navBarBg = nil;
//
//    if (IS_IOS7 ) { // iOS7
//        
//        navBarBg = @"public_nav_64.png";
//        
//        navBar.tintColor = [UIColor whiteColor];
//        
//    } else { // 非iOS7
//        
//        navBarBg = @"public_nav_44.png";
//        
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
//        
//    }
//
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:navBarBg] forBarMetrics:UIBarMetricsDefault];
//    
//    NSLog(@"navFrame: %@",NSStringFromCGRect(navBar.frame));
//
//    // 3.标题
//    
//    [navBar setTitleTextAttributes:@{
//                                     
//                                     UITextAttributeTextColor : [UIColor whiteColor]
//                                     
//                                     }];
//


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - public methods(http)

- (void)startRequestWithString:(NSString*)str
{
    [self startRequest:str delegate:nil yOffset:0];
}

- (void)startRequest:(NSString*)str delegate:(id)aDelegate yOffset:(int)yOffset
{
    NSString* strMsg = str;
     [self showWaitingViewWithYOffset:strMsg parentView:self.view yOffset:yOffset delegate:nil];
//    theHttpApi = [[XPHttpApi alloc]init];
//    theHttpApi.delegate = self;
}

- (void)startActionDelay
{
    if(theStartRequestBlock)
        theStartRequestBlock();
}
- (void)startActionWithBlock:(dispatch_block_t)block
{
    if (!block)
        block = ^{};
    [self setTheStartRequestBlock:block];
    [self performSelector:@selector(startActionDelay) withObject:nil afterDelay:DEFAULT_DELAY_SECONDS];
}

- (void)stopRequest
{
    [self hideWatingView];
}
- (void)stopRequestWithDelay:(SEL)aSel withObject:(NSObject*)anObject
{
    [self stopRequest];
    [self performSelector:aSel withObject:anObject afterDelay:DEFAULT_DELAY_SECONDS];
}
- (void)stopRequestDelay
{
    if(theStopRequestBlock)
        theStopRequestBlock();
    
    [self stopRequest];
}
- (void)stopRequestWithBlock:(dispatch_block_t)block
{
    if (!block)
        block = ^{};
    [self setTheStopRequestBlock:block];
    
    [self performSelector:@selector(stopRequestDelay) withObject:nil afterDelay:DEFAULT_DELAY_SECONDS];
}

#pragma mark -
#pragma mark public methods (MBHUD)
- (void)showWaitingViewWithYOffset:(NSString*)str parentView:(UIView*)parentView yOffset:(int)yOffset delegate:(id)aDelegate
{
    [self hideWatingView];
    
    //theHUD = [[MBProgressHUD alloc]initWithView:self.view];
    theHUD = [[MBProgressHUD alloc]initWithFrame:[self BottomViewRect]];
    theHUD.mode = MBProgressHUDModeIndeterminate;
    theHUD.delegate = aDelegate;
    theHUD.labelText = str;
    theHUD.yOffset = yOffset;
    theHUD.tag = MBHUD_VIEW_TAG;
    [self.view addSubview:theHUD];
    [theHUD show:YES];
}

- (void)hideWatingView
{
    if(theHUD)
    {
        [theHUD removeFromSuperview];
        
    }
    
    if (_activityIndicator) {
        [_activityIndicator removeFromSuperview];
    }
}

- (CGRect)BottomViewRect
{
    CGRect rect = [self CustomNavgationBarRect];
    return CGRectMake(5, 0, self.view.frame.size.width - 10, self.view.bounds.size.height - rect.size.height);
}

- (CGRect)CustomNavgationBarRect
{
    return CGRectMake(0, 0, self.view.frame.size.width, NAVIGATION_PANEL_HEIGHT);
}

- (void)changeMBHUDYOffset:(int)yOffset
{
    MBProgressHUD* hud = [MBProgressHUD getCurrentMBProgressHUD:self.view];
    hud.yOffset = yOffset;
}

- (CGRect)BackButtonRect
{
   // CGRect rect = [self CustomNavgationBarRect];
    return CGRectMake(10, 6, 50, 34);
}

- (CGRect)rightButtonRect
{
    CGRect rect = [self CustomNavgationBarRect];
    return CGRectMake(310, 6, 30, rect.size.height-40);
}

- (void)startRequestWithCustomIndicator {
   [self startRequestWithDelegate:nil yOffset:0];
}

- (void)startRequestWithDelegate:(id)aDelegate yOffset:(int)yOffset {
    [self showWaitingViewWithParentView:self.view yOffset:yOffset delegate:nil];

}

- (void)showWaitingViewWithParentView:(UIView*)parentView yOffset:(int)yOffset delegate:(id)aDelegate
{
    _activityIndicator.hidden = YES;
    
    _activityIndicator = [[WDActivityIndicator alloc]initWithFrame:CGRectMake([[UIApplication sharedApplication]keyWindow].center.x - 17, [[UIApplication sharedApplication]keyWindow].center.y + 20 - 17, 40, 40)];
    [[[UIApplication sharedApplication]keyWindow] addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
}

+ (void)resignAllKeyboard:(UIView*)aView
{
    if([aView isKindOfClass:[UITextField class]] ||
       [aView isKindOfClass:[UITextView class]])
    {
        UITextField* tf = (UITextField*)aView;
        if([tf canResignFirstResponder])
            [tf resignFirstResponder];
    }
    
    for (UIView* pView in aView.subviews) {
        [self resignAllKeyboard:pView];
    }
}

#pragma mark - 字典转json
- (NSString *)dictionaryToJson:(NSDictionary *)dictionary

{
    
    NSError *parseError = nil;
    
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}

#pragma mark 数组转json
- (NSString *)arrayToJson:(NSArray *)array

{
    
    NSError *parseError = nil;
    
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
