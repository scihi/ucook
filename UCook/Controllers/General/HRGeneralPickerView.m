//
//  HRGeneralPickerView.m
//  chronic
//
//  Created by huangrun on 14-1-15.
//  Copyright (c) 2014年 chenhai. All rights reserved.
//

#import "HRGeneralPickerView.h"

@implementation HRGeneralPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    id hitview=[super hitTest:point withEvent:event];
    if(!hitview)
    {
        if(point.y<=self.frame.size.height&&point.y>=0)
            return self;
    }
    return hitview;
}

@end
