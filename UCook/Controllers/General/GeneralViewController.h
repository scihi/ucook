//
//  GeneralViewController.h
//  chronic
//
//  Created by 黄润 on 13-8-12.
//  Copyright (c) 2013年 chenhai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WDActivityIndicator.h"

@interface GeneralViewController : UIViewController
{
    MBProgressHUD *theHUD;
    NSOperationQueue*           theOperationQueue;
    BOOL _isDidPop;
    UIButton *_theBackButton;
} 

@property (nonatomic, copy) dispatch_block_t              theStopRequestBlock;
@property (nonatomic, copy) dispatch_block_t              theStartRequestBlock;
@property (strong, nonatomic) WDActivityIndicator *activityIndicator;


- (void)startRequestWithString:(NSString*)str;
- (void)stopRequest;
- (void)changeMBHUDYOffset:(int)yOffset;
- (CGRect)BackButtonRect;
- (CGRect)rightButtonRect;
- (CGRect)BottomViewRect;
- (void)stopRequestWithBlock:(dispatch_block_t)block;
- (void)startRequestWithCustomIndicator;

+ (void)resignAllKeyboard:(UIView*)aView;

- (NSString *)dictionaryToJson:(NSDictionary *)dictionary;
- (NSString *)arrayToJson:(NSArray *)array;

@end
