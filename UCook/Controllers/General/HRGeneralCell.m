//
//  HRGeneralCell.m
//  chronic
//
//  Created by chenhai on 14-1-14.
//  Copyright (c) 2014年 chenhai. All rights reserved.
//

#import "HRGeneralCell.h"

@implementation HRGeneralCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews {
    [super layoutSubviews];
    if(IS_IOS7)
    self.backgroundColor = [UIColor clearColor];
//    self.backgroundView.frame = CGRectMake(10, 0, 300, self.frame.size.height);
//    self.selectedBackgroundView.frame = CGRectMake(10, 0, 300, self.frame.size.height);
}

@end
