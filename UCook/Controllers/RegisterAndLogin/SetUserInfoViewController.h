//
//  SetUserInfoViewController.h
//  UCook
//
//  Created by scihi on 14-8-25.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface SetUserInfoViewController : GeneralWithBackBtnViewController
@property (strong, nonatomic) IBOutlet UITextField *cellPhoneNumTF;
@property (strong, nonatomic) IBOutlet UITextField *passwardTF;
@property (strong, nonatomic) IBOutlet UITextField *confirmPsdTF;
- (IBAction)clickDoneBtn:(id)sender;

@property (copy, nonatomic) NSString *cellPhoneNumStr;
@property (copy, nonatomic) NSString *uidStr;

@end
