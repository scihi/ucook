//
//  ModifyViewController.m
//  UCook
//
//  Created by scihi on 14-8-25.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ModifyViewController.h"
#import "NSStringAdditions.h"
#import "LoginViewController.h"

@interface ModifyViewController ()

@end

@implementation ModifyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"设置密码";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickDoneBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_passwardTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入密码"];
        return;
    }
    if (_passwardTF.text.length < 6 || _passwardTF.text.length > 16) {
        [GlobalSharedClass showAlertView:@"请输入符合要求的密码"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"user",@"action":@"forgotPwd", @"uid": _uidStr,@"newPwd": _passwardTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:@"设置成功"];
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [AccountManager logout];
                LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
                UINavigationController *navLoginVC = [[UINavigationController alloc]initWithRootViewController:loginVC];
                [self presentViewController:navLoginVC animated:YES completion:nil];
            }
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


@end
