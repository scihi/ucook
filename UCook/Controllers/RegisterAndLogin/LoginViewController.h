//
//  LoginViewController.h
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface LoginViewController : GeneralWithBackBtnViewController
@property (strong, nonatomic) IBOutlet UITextField *accountTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)clickRegisterBtn:(id)sender;
- (IBAction)clickLoginBtn:(id)sender;
- (IBAction)clickForgetPsdBtn:(id)sender;

@end
