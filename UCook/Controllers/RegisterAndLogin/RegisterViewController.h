//
//  RegisterViewController.h
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface RegisterViewController : GeneralWithBackBtnViewController
@property (strong, nonatomic) IBOutlet UITextField *cellPhoneNumTF;
- (IBAction)clickVeriCodeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *veriCodeTF;
- (IBAction)clickNextStepBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *veriCodeBtn;

@end
