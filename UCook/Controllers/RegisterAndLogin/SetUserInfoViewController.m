//
//  SetUserInfoViewController.m
//  UCook
//
//  Created by scihi on 14-8-25.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SetUserInfoViewController.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"

@interface SetUserInfoViewController ()

@end

@implementation SetUserInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"完善用户名和密码";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _cellPhoneNumTF.text = _cellPhoneNumStr;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickDoneBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_cellPhoneNumTF.text] || ![NSString isValidatedMobilePhoneNumber:_cellPhoneNumTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的手机号码"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_passwardTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入密码"];
        return;
    }
    if (_passwardTF.text.length < 6 || _passwardTF.text.length > 16) {
        [GlobalSharedClass showAlertView:@"请输入符合要求的密码"];
        return;
    }
    if (![_confirmPsdTF.text isEqualToString:_passwardTF.text]) {
        [GlobalSharedClass showAlertView:@"两次密码输入不一致"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"user",@"action":@"setAccount", @"uid": _uidStr, @"username": _cellPhoneNumTF.text,@"password": _passwardTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:@"设置成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}


@end
