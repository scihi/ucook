//
//  LoginViewController.m
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "UserModel.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"
#import "FindPsdViewController.h"

#import "TypeSelectingViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"用户登录";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"public_nav_64"] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickRegisterBtn:(id)sender {
    RegisterViewController *registerVC = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)clickLoginBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_accountTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入手机号码或邮箱"];
        return;
    }
    if (![NSString isValidatedMobilePhoneNumber:_accountTF.text]&&![NSString  isValidatedEmail:_accountTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的手机号码或邮箱"];
        return;
    }
    
    if ([NSString isEmptyOrWhitespace:_passwordTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入密码"];
        return;
    }
    if (_passwordTF.text.length < 6) {
        [GlobalSharedClass showAlertView:@"密码长度不能少于6个字符"];
        return;
    }
    
    [self startRequestWithString:@"登录中..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"user",@"action":@"login", @"username": _accountTF.text, @"password": _passwordTF.text};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            UserModel *model = [[UserModel alloc] initWithDictionary:[responseObject objectForKey:@"data"]];
            AccountManager* manager = [AccountManager sharedInstance];
            [manager setThePassword:_passwordTF.text];
            [manager setTheUid:model.theUid];
            [manager setTheAccount:_accountTF.text];
            
            [manager setRole:model.role];
            
//            manager.theIsRememberLoginInfo = _rememberPsdBtn.selected;
            [manager WriteDataToFile];
            //        [delegate loginDidSucceed];
            
            if ([[AccountManager getRole]isEqualToString:@"normal"]) {
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            } else {
                
                [self configTheTypeSelectingVC];
                
            }
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (IBAction)clickForgetPsdBtn:(id)sender {
    FindPsdViewController *findPsdVC = [[FindPsdViewController alloc]initWithNibName:@"FindPsdViewController" bundle:nil];
    [self.navigationController pushViewController:findPsdVC animated:YES];
}

- (void)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 类型选择页面

- (void)configTheTypeSelectingVC {
    TypeSelectingViewController *tsvc = [[TypeSelectingViewController alloc]initWithNibName:@"TypeSelectingViewController" bundle:nil];
    [self.navigationController pushViewController:tsvc animated:YES];
}

@end
