//
//  RegisterViewController.m
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "RegisterViewController.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"
#import "SetUserInfoViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"用户注册";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickVeriCodeBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_cellPhoneNumTF.text] || ![NSString isValidatedMobilePhoneNumber:_cellPhoneNumTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的手机号码"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"common",@"action":@"sendSmsc", @"role": @"normal", @"type": @"register",@"mobile": _cellPhoneNumTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            __block int timeout=60; //倒计时时间
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout<=0){ //倒计时结束，关闭
                    dispatch_source_cancel(_timer);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        
                        _veriCodeBtn.userInteractionEnabled = YES;
                        [_veriCodeBtn setTitle:@"短信获取验证码" forState:UIControlStateNormal];
                        
                    });
                }else{
                    
                    int seconds = timeout % 60;
                    NSString *strTime = [NSString stringWithFormat:@"%.2d秒后重新获取", seconds];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        _veriCodeBtn.userInteractionEnabled = NO;
                        NSLog(@"%@",strTime);
                        [_veriCodeBtn setTitle:strTime forState:UIControlStateNormal];
                    });
                    timeout--;
                    
                }
            });
            dispatch_resume(_timer);
    
    } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}


- (IBAction)clickNextStepBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_cellPhoneNumTF.text] || ![NSString isValidatedMobilePhoneNumber:_cellPhoneNumTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的手机号码"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_veriCodeTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入验证码"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"common",@"action":@"verifySmsc", @"type": @"register", @"smsCaptcha": _veriCodeTF.text,@"mobile": _cellPhoneNumTF.text};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            SetUserInfoViewController *setUserInfoVC = [[SetUserInfoViewController alloc]initWithNibName:@"SetUserInfoViewController" bundle:nil];
            setUserInfoVC.cellPhoneNumStr = _cellPhoneNumTF.text;
            setUserInfoVC.uidStr = [[responseObject objectForKey:@"data"]objectForKey:@"uid"];
            [self.navigationController pushViewController:setUserInfoVC animated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}


@end
