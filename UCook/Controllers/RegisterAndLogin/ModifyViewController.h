//
//  ModifyViewController.h
//  UCook
//
//  Created by scihi on 14-8-25.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ModifyViewController : GeneralWithBackBtnViewController

@property (strong, nonatomic) IBOutlet UITextField *passwardTF;
- (IBAction)clickDoneBtn:(id)sender;

@property (copy, nonatomic) NSString *uidStr;

@end
