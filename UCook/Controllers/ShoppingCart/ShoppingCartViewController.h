//
//  ShoppingCartViewController.h
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"
#import "StepperTestViewController.h"
#import "ConfirmOrderViewController.h"

@interface ShoppingCartViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate, TextStepperDelegate, UITextFieldDelegate, clearTheShoppingCartDelegate>

@end
