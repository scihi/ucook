//
//  ShoppingCartViewController.m
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "CGRectAdditionals.h"

#import "ConfirmOrderModel.h"

static NSString *CellIdentifier = @"ShoppingCartCell";

@interface ShoppingCartViewController () {
    UITableView *_theTableView;
    NSMutableArray *_theDataMutArr;
    UIView *_noDataView;//无数据时的展示背景
    UITableViewCell *_cell;//用于sepper
    NSDictionary *_theDataDic;//用于steppter
    NSMutableArray *_goodsIdStrMutArr;//cell上的产品id数组
    NSMutableArray *_goodsNumMutArr;//cell上的产品数量数组
    NSMutableArray *_goodsPriceMutArr;//cell上的产品价格数组
    NSMutableArray *_goodsInfoMutArr;//包含id、价格和数量的产品信息数组
    BOOL _noZero;//判断数量里有没有0
    
    StepperTestViewController *stepperTestVC;
}

@end

@implementation ShoppingCartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"购物车";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self getShoppingCartData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64 - 49:568 - 64 - 49 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
        [self initTheVars];
    
    [self configTheNavBar];
    
    _theTableView = [[UITableView alloc]initWithFrame:kTableViewFrame];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self registerCell];
        _theTableView.tableFooterView = [[UIView alloc]init];//遮住多余的分割线
//    [self getShoppingCartData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navLeftBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_bar_delete.png"]];
    navLeftIV.frame = CGRectMake(10, 2, 30, 30);
    [navLeftBtn addSubview:navLeftIV];
    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setTitle:@"付款" forState:UIControlStateNormal];
    navRightBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    navRightBtn.titleLabel.textColor = [UIColor blackColor];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_scan.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    //    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)clickNavLeftBtn:(id)sender {
    if (_theDataMutArr.count == 0) {
        [GlobalSharedClass showAlertView:@"购物车为空"];
        return;
    }
    
    NSDictionary *cartIdDic = [NSDictionary dictionary];
    NSMutableArray *cartIdMutArr = [NSMutableArray arrayWithCapacity:10];
    for (NSDictionary *dictionary in _theDataMutArr) {
        for (NSDictionary *dictionary2 in [dictionary objectForKey:@"products"]) {
            cartIdDic = @{@"cartId": [dictionary2 objectForKey:@"cartId"]};
            [cartIdMutArr addObject:cartIdDic];
        }
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString *cartIdArrJsonStr = [self arrayToJson:cartIdMutArr];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"cart",@"action": @"delete",@"uid":[AccountManager getUid],@"id": cartIdArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getShoppingCartData];
//            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)clickNavRightBtn:(id)sender {
    
    if (_theDataMutArr.count == 0) {
        [GlobalSharedClass showAlertView:@"购物车为空"];
        return;
    }
    [self buyInServer];
}


- (void)registerCell {
    [_theTableView registerNib:[UINib nibWithNibName:@"ShoppingCartCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
}

//获取购物车信息用于展示
- (void)getShoppingCartData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"cart",@"action": @"list",@"uid":[AccountManager getUid]};
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
            [_theDataMutArr removeAllObjects];
            _theDataMutArr = [[responseObject objectForKey:@"data"]mutableCopy];
            [_theTableView reloadData];
        } else {
//            [GlobalSharedClass showAlertView:MESSAGE];
            [_theDataMutArr removeAllObjects];
            _theTableView.backgroundColor = [UIColor clearColor];
            [_theTableView reloadData];
            
            if (!_noDataView)
            _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
            UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
            noDataIV.image = [UIImage imageNamed:@"ico_tip"];
            UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
            noDataLabel.text = @"购物车已空，快去添加吧！";
            noDataLabel.font = [UIFont systemFontOfSize:13];
            noDataLabel.textAlignment = NSTextAlignmentCenter;
            noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
            noDataLabel.numberOfLines = 0;
            
            [_noDataView addSubview:noDataIV];
            [_noDataView addSubview:noDataLabel];
            
            noDataIV.center = _noDataView.center;
            UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
            noDataLabel.center = view.center;
            
            [_theTableView addSubview:_noDataView];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

#pragma - mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _theDataMutArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;
    
    //为section底部添加分割线
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width - 10, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width - 10, 5)];
    lineView.backgroundColor = kThemeColor;
    [headerView addSubview:lineView];
    
    UIImageView *cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(5, 9.5, 25, 25)];
    cellIV.image = [UIImage imageNamed:@"ico_store"];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 0, BOUNDS.size.width - 35, 44)];
    titleLabel.text = [[_theDataMutArr objectAtIndex:section]objectForKey:@"storeName"];
    [headerView addSubview:cellIV];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[_theDataMutArr objectAtIndex:section]objectForKey:@"products" ]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSArray *goodsInfoArr = [[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"products"];
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"image"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"name"];
    UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
    NSString *specificationString = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"spec"];
    specificationLabel.text = [NSString stringWithFormat:@"规格:%@",specificationString];
    UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];//单位
    NSString *unitString = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"unit"];;
    unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitString];
    UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:105];
    NSString *currentPriceStr = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"discountPrice"];
    currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:106];//原价
    originalPriceLabel.strikeThroughEnabled = YES;
    NSString *originalPriceStr = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"price"];
    originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",originalPriceStr];
    //数量选择按钮
    UIButton *countButton = (UIButton *)[cell viewWithTag:107];
    countButton.layer.borderWidth = 1.0;
    countButton.layer.borderColor = [UIColor colorWithRed:234.00f/255.00f green:234.00f/255.00f blue:234.00f/255.00f alpha:1.00f].CGColor;
    NSString *numberString = [[goodsInfoArr objectAtIndex:indexPath.row]objectForKey:@"number"];
    [countButton setTitle:numberString forState:UIControlStateNormal];
    [countButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [countButton addTarget:self action:@selector(setTheGoodsNumber:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setTheGoodsNumber:(id)sender {
    UIButton *button = sender;
    if (IS_IOS8) {
        _cell = (UITableViewCell *)[[button superview]superview];
    } else {
    _cell = (UITableViewCell *)[[[button superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
    _theDataDic = [[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"products"]objectAtIndex:indexPath.row];
    stepperTestVC = [[StepperTestViewController alloc]initWithNibName:@"StepperTestViewController" bundle:nil];
    stepperTestVC.theFloat = [[_theDataDic objectForKey:@"number"]floatValue];
    stepperTestVC.Maximum = 10000;
    stepperTestVC.view.frame = CGRectMake(0, 0, 300, 200);
    stepperTestVC.delegate = self;
    stepperTestVC.contador.textField.delegate = self;
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [self addChildViewController:stepperTestVC];
    [view addSubview:stepperTestVC.view];
    stepperTestVC.view.center = view.center;
    [stepperTestVC didMoveToParentViewController:self];
    stepperTestVC.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] * [[_theDataDic objectForKey:@"number"]floatValue]];
    [self.view addSubview:view];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
}

#pragma mark - textStepperDelegate
- (void)stepperValueDidChange:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    
    stepperTestViewController.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] * counter];
}

- (void)stepperDidClickConfirmBtn:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    UIButton *countButton = (UIButton *)[_cell viewWithTag:107];
    [countButton setTitle:[NSString stringWithFormat:@"%ld",(long)counter] forState:UIControlStateNormal];
    
//    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
    
    NSString *cartIdStr = [_theDataDic objectForKey:@"cartId"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"cart",@"action": @"update",@"uid":[AccountManager getUid],@"id": cartIdStr,@"number": @(counter)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
//            [GlobalSharedClass showAlertView:MESSAGE];
            [self getShoppingCartData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

#pragma mark - 立即购买
- (void)buyInServer {
    
//    //遍历cell上的产品id
//    [_goodsIdStrMutArr removeAllObjects];
//    for (int i = 0; i < _theDataMutArr.count; i++) {
//        
//        for (NSDictionary *dictionary in [[_theDataMutArr objectAtIndex:i]objectForKey:@"products"]) {
//            NSString *string = [dictionary objectForKey:@"id"];
//            [_goodsIdStrMutArr addObject:string];
//        }
//    }
//
//    //遍历cell上的数量按钮的值，也就是产品数量
//    [_goodsNumMutArr removeAllObjects];
//    for (int i = 0; i < _theDataMutArr.count; i++) {
//        for (int j = 0; j < [[[_theDataMutArr objectAtIndex:i]objectForKey:@"products"]count]; j++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
//            UITableViewCell *cell = [_cellMutArr objectAtIndex:indexPath.row];
//            UIButton *countButton = (UIButton *)[cell viewWithTag:107];
//            [_goodsNumMutArr addObject:countButton.titleLabel.text];
//        }
//    }
//    //遍历cell上的价格按钮的值，也就是产品价格
//    [_goodsPriceMutArr removeAllObjects];
//    for (int i = 0; i < _theDataMutArr.count; i++) {
//        for (int j = 0; j < [[[_theDataMutArr objectAtIndex:i]objectForKey:@"products"]count]; j++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
//            UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
//            UILabel *priceLabel = (UILabel *)[cell viewWithTag:105];
//            [_goodsPriceMutArr addObject:[priceLabel.text stringByReplacingOccurrencesOfString:@"￥" withString:@""]];
//        }
//    }
//    
//    //把产品id、数量和价格组成字典加入新的数组
//    [_goodsInfoMutArr removeAllObjects];
//    for (int i = 0; i < _goodsIdStrMutArr.count; i++) {
//        [_goodsInfoMutArr addObject:@{@"id": @([[_goodsIdStrMutArr objectAtIndex:i]integerValue]), @"number": @([[_goodsNumMutArr objectAtIndex:i]integerValue]),@"price": [_goodsPriceMutArr objectAtIndex:i]}];
//    }
//    //遍历产品数量并判断是否全为0
//    for (NSString *string in _goodsNumMutArr) {
//        if (![string isEqualToString:@"0"]) {
//            _noZero = YES;
//        }
//    }
    
    [_goodsInfoMutArr removeAllObjects];
    for (NSDictionary *dictionary in _theDataMutArr) {
        for (NSDictionary *dictionary2 in [dictionary objectForKey:@"products"]) {
            if ([[dictionary2 objectForKey:@"number"]integerValue] != 0) {
                [_goodsInfoMutArr addObject:dictionary2];
            }
        }
    }
    if (_goodsInfoMutArr.count == 0) {
        
        [GlobalSharedClass showAlertView:@"请选择数量"];
        return;
        
    }

    
    NSMutableArray *wantMutArr = [NSMutableArray arrayWithCapacity:10];
    [wantMutArr removeAllObjects];
    for (NSDictionary *dictionary in _goodsInfoMutArr) {
        [wantMutArr addObject:@{@"id": @([[dictionary objectForKey:@"id"]integerValue]),@"number": @([[dictionary objectForKey:@"number"]integerValue]),@"price": [dictionary objectForKey:@"price"]}];
    }
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSString *productArrJsonStr = [self arrayToJson:wantMutArr];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"order",@"action": @"confirm",@"uid": [AccountManager getUid],@"products":productArrJsonStr};
        
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                
                NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
               
                ConfirmOrderModel *confirmOrderModel = [ConfirmOrderModel objectWithKeyValues:dictionary];
                ConfirmOrderViewController *confirmOrderVC = [[ConfirmOrderViewController alloc]initWithNibName:@"ConfirmOrderViewController" bundle:nil];
                confirmOrderVC.delegate = self;
                confirmOrderVC.confirmOrderModel = confirmOrderModel;
                confirmOrderVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:confirmOrderVC animated:YES];
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    }

- (void)initTheVars {
    _goodsIdStrMutArr = [NSMutableArray arrayWithCapacity:10];
    _goodsNumMutArr = [NSMutableArray arrayWithCapacity:10];
    _goodsInfoMutArr = [NSMutableArray arrayWithCapacity:10];
    _goodsPriceMutArr = [NSMutableArray arrayWithCapacity:10];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    //关闭键盘
//    [self.view endEditing:YES];
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    stepperTestVC.view.frame = CGRectMake(10, 5, 300, 200);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    stepperTestVC.view.center = view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [stepperTestVC.contador.textField resignFirstResponder];
    return YES;
}

- (void)clearTheShoppingCart:(ConfirmOrderViewController *)confirmOrderVC {
    [self clickNavLeftBtn:nil];
}

@end
