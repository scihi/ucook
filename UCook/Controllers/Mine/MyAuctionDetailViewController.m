//
//  MyAuctionDetailViewController.m
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyAuctionDetailViewController.h"
#import "MyAuctionDetailModel.h"
#import "UIImageView+AFNetworking.h"

#import "AllOrdersDetailViewController.h"

@interface MyAuctionDetailViewController () {
    UITableView *_theTableView;
    MyAuctionDetailModel *_myAuctionDetailModel;
    NSString *_auctionRecordIdStr;
}

@end

@implementation MyAuctionDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"我的竞拍详情";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getTheInitData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
//    [self getTheInitData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"bidding",@"action": @"detail",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _myAuctionDetailModel = [MyAuctionDetailModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 200.f;
    } else {
        return 80.f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return _myAuctionDetailModel.record.count;
    } else {
        return _myAuctionDetailModel.products.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyAuctionDetailCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        
        if (indexPath.section == 0) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MyAuctionDetailCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 1) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AuctionRecordCell" owner:self options:nil]firstObject];
        } else {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
    }
    
    if (indexPath.section == 0) {
        UILabel *statusLabel = (UILabel *)[cell viewWithTag:101];
//        NSString *statusString;
//        if ([_myAuctionDetailModel.status isEqualToString:@"0"]) {
//            statusString = @"未发布";
//        } else if ([_myAuctionDetailModel.status isEqualToString:@"1"]) {
//            statusString = @"已发布";
//        } else if ([_myAuctionDetailModel.status isEqualToString:@"2"]) {
//            statusString = @"已结束";
//        } else if ([_myAuctionDetailModel.status isEqualToString:@"-2"]) {
//            statusString = @"已删除";
//        }
//        statusLabel.text = statusString;
        
        statusLabel.text = _myAuctionDetailModel.statusName;
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:102];
        nameLabel.text = _myAuctionDetailModel.name;
        
        UILabel *lowestPriceLabel = (UILabel *)[cell viewWithTag:103];
        NSString *lowestPriceStr = _myAuctionDetailModel.storePrice;
        if (lowestPriceStr) {
            lowestPriceLabel.text = [NSString stringWithFormat:@"￥%@",lowestPriceStr];
        } else {
            lowestPriceLabel.text = @"无";
        }
        
        UILabel *auctionShopLabel = (UILabel *)[cell viewWithTag:104];
        auctionShopLabel.text = _myAuctionDetailModel.storeName?_myAuctionDetailModel.storeName:@"无";
        

        if (_myAuctionDetailModel.leftTime == 0) {
            UILabel *leftTimeLabel = (UILabel *)[cell viewWithTag:105];
            leftTimeLabel.text = @"已结束";
        } else {
        
        __block int timeout=[_myAuctionDetailModel.leftTime intValue]; //倒计时时间
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
        dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
        dispatch_source_set_event_handler(_timer, ^{
            if(timeout<=0){ //倒计时结束，关闭
                dispatch_source_cancel(_timer);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置

                    
                });
            }else{
                int hours = timeout / 3600;
                int minutes = timeout / 60 % 60;
                int seconds = timeout % 60;
                NSString *strTime = [NSString stringWithFormat:@"%.2d:%.2d:%.2d", hours, minutes, seconds];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置

                    UILabel *leftTimeLabel = (UILabel *)[cell viewWithTag:105];
                    leftTimeLabel.text = strTime;
                    
                });
                timeout--;
                
            }
        });
        dispatch_resume(_timer);
            
        }
        
        UILabel *beginPriceLabel = (UILabel *)[cell viewWithTag:106];
        NSString *beginPriceStr = _myAuctionDetailModel.price;
        beginPriceLabel.text = [NSString stringWithFormat:@"￥%@",beginPriceStr];
        
        UILabel *cycleLabel = (UILabel *)[cell viewWithTag:107];
        cycleLabel.text = _myAuctionDetailModel.cycleTime;
        
    } else if (indexPath.section == 1) {
        NSDictionary *currentDic = [_myAuctionDetailModel.record objectAtIndex:indexPath.row];
        
        UILabel *lowestPriceLabel = (UILabel *)[cell viewWithTag:101];
        NSString *lowestPriceStr =[currentDic objectForKey:@"price"];
        lowestPriceLabel.text = [NSString stringWithFormat:@"￥%@",lowestPriceStr];
        
        UILabel *timeLabel = (UILabel *)[cell viewWithTag:102];
        NSString *timeString = [currentDic objectForKey:@"time"];
        timeLabel.text = timeString;
        
        UILabel *auctionShopLabel = (UILabel *)[cell viewWithTag:103];
        auctionShopLabel.text = [currentDic objectForKey:@"storeName"];
        
        UIButton *dealButton = (UIButton *)[cell viewWithTag:104];
        dealButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        dealButton.layer.borderWidth = 1.f;
        if ([_myAuctionDetailModel.status isEqualToString:@"2"]||[_myAuctionDetailModel.status isEqualToString:@"0"]||[_myAuctionDetailModel.status isEqualToString:@"-2"]) {
            dealButton.hidden = YES;
        } else if ([_myAuctionDetailModel.status isEqualToString:@"1"]){
            dealButton.hidden = NO;
            
            [dealButton addTarget:self action:@selector(clickDealBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        //星星
        CGFloat score1 = [[[_myAuctionDetailModel.record objectAtIndex:indexPath.row]objectForKey:@"rating"]integerValue];
        NSInteger score2;
        if (score1 == 0 ) {
            score2 = 0;
        } else if (score1 > 0 && score1 <= 1) {
            score2 = 1;
        } else if (score1 > 1 && score1 <= 2) {
            score2 = 2;
        } else if (score1 > 2 && score1 <= 3) {
            score2 = 3;
        } else if (score1 > 3 && score1 <= 4) {
            score2 = 4;
        } else {
            score2 = 5;
        }
        
        UIImageView *starIV1 = (UIImageView *)[cell viewWithTag:105];
        UIImageView *starIV2 = (UIImageView *)[cell viewWithTag:106];
        UIImageView *starIV3 = (UIImageView *)[cell viewWithTag:107];
        UIImageView *starIV4 = (UIImageView *)[cell viewWithTag:108];
        UIImageView *starIV5 = (UIImageView *)[cell viewWithTag:109];
        
        switch (score2) {
            case 0:
                break;
            case 1:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 2:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 3:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 4:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            case 5:
                starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
                starIV5.image = [UIImage imageNamed:@"ico_star_big_light"];
                break;
            default:
                break;
        }

        
    }
    else if (indexPath.section == 2)
    {
        NSDictionary *currentDic = [_myAuctionDetailModel.products objectAtIndex:indexPath.row];
        
        NSString *imgUrlStr = [currentDic objectForKey:@"image"];
        [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        cell.textLabel.text = [currentDic objectForKey:@"name"];
        
        NSString *specificationString = [currentDic objectForKey:@"spec"];
        NSString *unitString = [currentDic objectForKey:@"unit"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"规格:%@ 单位:%@",specificationString, unitString];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return @"竞价记录";
    } else if (section == 2) {
        return @"商品包产品";
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.f;
    } else {
        return 44.f;
    }
}


- (void)clickDealBtn:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[sender superview]superview];
    } else {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    NSDictionary *currentDic = [_myAuctionDetailModel.record objectAtIndex:indexPath.row];
    _auctionRecordIdStr = [currentDic objectForKey:@"recordId"];
    
    NSString *noteString = [NSString stringWithFormat:@"出价商家:%@\n当前价格:￥%@",[currentDic objectForKey:@"storeName"], [currentDic objectForKey:@"price"]];
    
//    [GlobalSharedClass showAlertView:noteString delegate:self];
    
    //                [self.navigationController popViewControllerAnimated:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:noteString delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"优厨币",@"支付宝",@"货到付款", nil];
    [actionSheet showInView:self.view];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    } else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"bidding",@"action": @"deal",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr,@"recordId": _auctionRecordIdStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                //改为跳订单详情



            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];

    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 3) {
        [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    } else {
        NSString *payWayStr;
        if (buttonIndex == 0) {
            payWayStr = @"3";
        } else if (buttonIndex == 1) {
            payWayStr = @"1";
        } else if (buttonIndex == 2) {
            payWayStr = @"0";
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"bidding",@"action": @"deal",@"uid":[AccountManager getUid],@"id": _auctionBagIdStr,@"recordId": _auctionRecordIdStr,@"payWay": payWayStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                //改为跳订单详情
                //订单详情
                NSString *orderIdStr = [[responseObject objectForKey:@"data"]objectForKey:@"orderId"];
                
                AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
                allOrdersDetailVC.orderIdStr = orderIdStr;
                
                [self.navigationController pushViewController:allOrdersDetailVC animated:YES];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];

    }
    
}

@end
