//
//  MineViewController.h
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralViewController.h"

@interface MineViewController : GeneralViewController
@property (strong, nonatomic) IBOutlet UIButton *headImgBtn;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *uCookCoinLabel;
@property (strong, nonatomic) IBOutlet UILabel *waitingForPaymentLabel;
@property (strong, nonatomic) IBOutlet UILabel *waitingForDeliveryLabel;//待发货
@property (strong, nonatomic) IBOutlet UILabel *waitingForTakeDeliveryLabel;//待收货
@property (strong, nonatomic) IBOutlet UILabel *waitingForEvaluateLabel;//待评价
@property (strong, nonatomic) IBOutlet UILabel *refundLabel;//退款
- (IBAction)clickAllOrderBtn:(id)sender;
- (IBAction)clickMyCollectionBtn:(id)sender;//我的收藏
- (IBAction)clickTakeDeliveryAddressBtn:(id)sender;//收货地址
- (IBAction)clickMyDiscountCouponBtn:(id)sender;//我的优惠券
- (IBAction)clickMyMessageBtn:(id)sender;
- (IBAction)clickMyReportFormsBtn:(id)sender;//我的报表

- (IBAction)clickWaitingForPaymentBtn:(id)sender;
- (IBAction)clickWaitingForDeliveryBtn:(id)sender;
- (IBAction)clickWaitingForTakeDeliveryBtn:(id)sender;
- (IBAction)clickWaitingForEvaluateBtn:(id)sender;
- (IBAction)clickRefundBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *chargeButton;
@property (strong, nonatomic) IBOutlet UIButton *withdrawButton;
- (IBAction)clickChargeBtn:(id)sender;
- (IBAction)clickWithdrawBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
- (IBAction)clickMyAuctionBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *menuView;

@property (strong, nonatomic) IBOutlet UIView *menuView2;
@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
- (IBAction)clickDealRecordBtn:(id)sender;
- (IBAction)clickManageMyStoreBtn:(id)sender;

@end
