//
//  AllOrdersViewController.m
//  UCook
//
//  Created by huangrun on 14-8-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AllOrdersViewController.h"
#import "AllOrdersModel.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"
#import "CGRectAdditionals.h"
#import "AllOrdersDetailViewController.h"
#import "AllOrdersDetailModel.h"
#import "AlipayModel.h"

#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"

#import "RechargeViewController.h"

#import "SalesReturnViewController.h"

#import "EvaluateViewController.h"

@interface AllOrdersViewController () {
    UITableView *_theTableView;
    AllOrdersModel *_allOrderModel;
    NSMutableArray *_allOrderModelMutArr;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    UIView *_noDataView;//无数据时的展示背景
    
    NSIndexPath *_indexPath;//点击付款按钮时需要的indexPath
    
    AllOrdersDetailModel *_allOrderDetailModel;//为了取出里面的product的id和number，本是详情页的数据
    
    UIAlertView *_alertView;
    
    AlipayModel *_alipayModel;
}

@end

@implementation AllOrdersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
      [self getTheInitData];
}

#pragma mark - 为了解决支付宝WAP支付取消返回回来界面偏移问题
- (void)viewDidLayoutSubviews {

    if (self.view.frame.size.height != (BOUNDS.size.height - 64)) {

        self.tabBarController.tabBar.hidden = YES;
        _theTableView.frame =CGRectMake(_theTableView.frame.origin.x, _theTableView.frame.origin.y,_theTableView.frame.size.width, BOUNDS.size.height - 64);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _titleString;
    
    _result = @selector(paymentResult:);//支付宝的应用内(wap方式)回调方法
    
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64:568 - 64 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];

    
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _allOrderModelMutArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section, indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AllOrdersCell" owner:self options:nil]firstObject];
    }
    
    
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:indexPath.section];
    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = _allOrderModel.image;
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = _allOrderModel.title;
    
    UILabel *amountLabel = (UILabel *)[cell viewWithTag:103];
    NSString *amountString = [NSString stringWithFormat:@"共%d件商品",_allOrderModel.amount];
    amountLabel.text = amountString;
    
    UILabel *priceLabel = (UILabel *)[cell viewWithTag:104];
    NSString *priceString = [NSString stringWithFormat:@"￥%.2f",_allOrderModel.payFee];
    priceLabel.text = priceString;
    
    UIButton *leftButton = (UIButton *)[cell viewWithTag:105];
    leftButton.layer.masksToBounds = YES;
    leftButton.layer.borderWidth = 1.f;
    leftButton.layer.borderColor = kThemeCGColor;
    leftButton.layer.cornerRadius = 12.f;
    [leftButton addTarget:self action:@selector(clickLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rightButton = (UIButton *)[cell viewWithTag:106];
    rightButton.layer.masksToBounds = YES;
    rightButton.layer.borderWidth = 1.f;
    rightButton.layer.borderColor = kThemeCGColor;
    rightButton.layer.cornerRadius = 12.f;
    [rightButton addTarget:self action:@selector(clickRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (_allOrderModel.status == 32 || _allOrderModel.status == 33) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 20) {
        leftButton.hidden = NO;
        rightButton.hidden = NO;
        [leftButton setTitle:@"付款" forState:UIControlStateNormal];
        [rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 23) {
        //2表示经销商已评
        if ([_allOrderModel.commentStatus isEqualToString:@"0"] || [_allOrderModel.commentStatus isEqualToString:@"2"]) {
            leftButton.hidden = NO;
            [leftButton setTitle:@"评价" forState:UIControlStateNormal];
        } else if ([_allOrderModel.commentStatus isEqualToString:@"1"]) {
            leftButton.hidden = YES;
        }
        
        if (_allOrderModel.refundStatus == 2 || !_allOrderModel.refundStatus ||_allOrderModel.refundStatus == 0 || _allOrderModel.refundStatus == 3) {
            rightButton.hidden = NO;
            [rightButton setTitle:@"申请退货" forState:UIControlStateNormal];
        } else if (_allOrderModel.refundStatus == 1) {
            rightButton.hidden = NO;
            [rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];
            rightButton.userInteractionEnabled = NO;
        }
    } else if (_allOrderModel.status == 21 ) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"申请退款" forState:UIControlStateNormal];
    }  else if (_allOrderModel.status == 22 || _allOrderModel.status == 11) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 30) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"删除订单" forState:UIControlStateNormal];
    } else if (_allOrderModel.status == 31) {
        leftButton.hidden = NO;
        rightButton.hidden = NO;
        [leftButton setTitle:@"交易成功" forState:UIControlStateNormal];
        if (_allOrderModel.refundStatus == 1) {
            [rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];                       rightButton.userInteractionEnabled = NO;
        } else {
        [rightButton setTitle:@"申请退货" forState:UIControlStateNormal];
        }
    }  else if (_allOrderModel.status == 24) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"申请退款中" forState:UIControlStateNormal];
        rightButton.userInteractionEnabled = NO;
    } else if (_allOrderModel.status == 25) {
        leftButton.hidden = YES;
        rightButton.hidden = YES;
    } else if (_allOrderModel.status == 10) {
        leftButton.hidden = YES;
        rightButton.hidden = NO;
        [rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } 


    UILabel *statusNameLabel = (UILabel *)[cell viewWithTag:107];
    statusNameLabel.text = _allOrderModel.statusName;
    
    return cell;
}


- (void)getTheInitData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"list",@"uid":[AccountManager getUid],@"page": @(_pageNumber),@"status":_statusString};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _allOrderModelMutArr = [NSMutableArray arrayWithArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
            [_theTableView reloadData];
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                [_allOrderModelMutArr removeAllObjects];
                [_theTableView reloadData];
                [_noDataView removeFromSuperview];
                _theTableView.backgroundColor = [UIColor clearColor];
                //设置无数据时的提示视图
                [self configTheNoDataViewForTheTableView];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;
    
    //为section底部添加分割线
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width - 10, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width - 10, 5)];
    lineView.backgroundColor = kThemeColor;
    [headerView addSubview:lineView];
    
    UIImageView *cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(5, 9.5, 25, 25)];
    cellIV.image = [UIImage imageNamed:@"ico_store"];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 0, BOUNDS.size.width - 35, 44)];
    titleLabel.text = [[_allOrderModelMutArr objectAtIndex:section]storeName];
    [headerView addSubview:cellIV];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //已废弃 huangrun
//    [self getOrderDetailData:indexPath];
    
    AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
    allOrdersDetailVC.orderIdStr = [[_allOrderModelMutArr objectAtIndex:indexPath.section]orderId];
    [self.navigationController pushViewController:allOrdersDetailVC animated:YES];
}


- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
         NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"list",@"uid":[AccountManager getUid],@"page": @(_pageNumber),@"status":_statusString};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [self stopRequest];
            if (RIGHT) {
                //            [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_allOrderModelMutArr addObjectsFromArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
                
                [self doneWithView:refreshView];
            } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                
                //                    [_theDataMutArr removeAllObjects];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_allOrderModelMutArr addObjectsFromArray:[AllOrdersModel objectArrayWithKeyValuesArray:array]];
                [self doneWithView:refreshView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}

- (void)clickLeftBtn:(id)sender {
    UITableViewCell *cell;
     if (IS_IOS8) {
             cell = (UITableViewCell *)[[sender superview]superview];
     } else {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
     }
    _indexPath = [_theTableView indexPathForCell:cell];
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:_indexPath.section];
    
    //付款
    if (_allOrderModel.status == 20) {
        //优厨币支付
        if ([_allOrderModel.payMethod isEqualToString:@"3"]) {
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",_allOrderModel.orderId, _allOrderModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"优厨币支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 101;
            [_alertView show];
            
        } else if ([_allOrderModel.payMethod isEqualToString:@"1"]) {
            //支付宝支付
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",_allOrderModel.orderId, _allOrderModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"支付宝支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 103;
            [_alertView show];

        } else if ([_allOrderModel.payMethod isEqualToString:@"0"]) {
            [GlobalSharedClass showAlertView:@"该商品为货到付款,请收到货再支付"];
            return;
        }
    } else if (_allOrderModel.status == 23) {
        EvaluateViewController *evaluateVC = [[EvaluateViewController alloc]initWithNibName:@"EvaluateViewController" bundle:nil];
        evaluateVC.idInteger = [_allOrderModel.storeId integerValue];
        evaluateVC.orderIdStr = _allOrderModel.orderId;
        [self.navigationController pushViewController:evaluateVC animated:YES];
    }
}

- (void)clickRightBtn:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[sender superview]superview];
    } else {
        cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    _indexPath = [_theTableView indexPathForCell:cell];
    _allOrderModel = [_allOrderModelMutArr objectAtIndex:_indexPath.section];
    
    if (_allOrderModel.status == 21) {
        //申请退款
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定申请退款?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 102;
        [_alertView show];
    } else if (_allOrderModel.status == 23 || _allOrderModel.status == 31) {
        if (_allOrderModel.refundStatus == 2 || !_allOrderModel.refundStatus || _allOrderModel.refundStatus == 0 || _allOrderModel.refundStatus == 3) {
            
            if (_allOrderModel.refundTimes == 3) {
                [GlobalSharedClass showAlertView:@"超出申请退货次数限制!"];
                return;
            }
            
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定申请退货?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 104;
        [_alertView show];
        }
    } else if (_allOrderModel.status == 22 || _allOrderModel.status == 11) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请您在收到货后再确认收货，否则有可能钱货两空。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 105;
        [_alertView show];
    } else if (_allOrderModel.status == 20 || _allOrderModel.status == 32 || _allOrderModel.status == 10 || _allOrderModel.status == 33) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要放弃此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 106;
        [_alertView show];

    } else if (_allOrderModel.status == 30) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要删除此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 107;
        [_alertView show];

    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 101) {
        //优厨币支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            [self payByUcookb];
        }
    } else if (alertView.tag == 102) {
        //申请退款
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
//            [self getOrderDetailData];
            [self orderStatusUpdate];
        }
        
    } else if (alertView.tag == 103) {
        //支付宝支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self getAlipayParameters];
        }
    } else if (alertView.tag == 104) {
            //申请退货
            if (buttonIndex == 0) {
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
            } else {
                
                SalesReturnViewController *salesReturnVC = [[SalesReturnViewController alloc]init];
                salesReturnVC.orderIdStr = _allOrderModel.orderId;
                [self.navigationController pushViewController:salesReturnVC animated:YES];
            }

    } else if (alertView.tag == 105) {
        //确认收货
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self orderStatusUpdate];
        }

    } else if (alertView.tag == 106) {
        //放弃订单
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self orderStatusUpdate];
        }

    } else if (alertView.tag == 107) {
        //删除订单
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self deleteTheOrder];
        }
    }
}

#pragma mark - 优厨币付款
- (void)payByUcookb {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"ucookbPay",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId,@"total_fee": @(_allOrderModel.payFee)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [_allOrderModelMutArr removeObjectAtIndex:_indexPath.section];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:_indexPath.section];
            [_theTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

#pragma mark - 请求支付宝参数
- (void)getAlipayParameters {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSArray *orderIdArr = @[@{@"orderId": _allOrderModel.orderId}];
    NSString *orderIdArrJsonStr = [self arrayToJson:orderIdArr];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"payString",@"uid":[AccountManager getUid], @"orderIds": orderIdArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _alipayModel = [AlipayModel objectWithKeyValues:dictionary];
            
            [self pay];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

#pragma mark - 支付宝付款
- (void)pay {
    /*
	 *生成订单信息及签名
	 *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
	 */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
	
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];
    
}

- (NSString *)getOrderInfo {
    /*
	 *点击获取prodcut实例并初始化订单信息
	 */
	Product *product = [[Product alloc]init];
    product.subject = [_alipayModel.info objectForKey:@"subject"];
    product.body = [_alipayModel.info objectForKey:@"body"];
    product.price = [[_alipayModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
    //    order.partner = PartnerID;
    //    order.seller = SellerID;
    order.partner = [_alipayModel.info objectForKey:@"partner"];
    order.seller = [_alipayModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_alipayModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
	order.productName = product.subject; //商品标题
	order.productDescription = product.body; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
	order.notifyURL = [_alipayModel.info objectForKey:@"notify_url"]; //回调URL
	
	return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
			id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
			if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
			}
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}

#pragma mark - 退款更新状态为退款申请中(21->24)、退货更新为申请退货中(23->25)、退款更新为申请退款中(22->23)、放弃交易更新为结束交易 (20->30、32->30、10->30)
- (void)orderStatusUpdate {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSInteger wantStatus;//需要更新到的状态值
    if (_allOrderModel.status == 21) {
        wantStatus = 24;
    } else if (_allOrderModel.status == 23) {
        wantStatus = 25;
    } else if (_allOrderModel.status == 22) {
        wantStatus = 23;
    } else if (_allOrderModel.status == 20 || _allOrderModel.status == 32 || _allOrderModel.status == 10) {
        wantStatus = 30;
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"statusUpdate",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId,@"type": @"status",@"status": @(wantStatus)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)deleteTheOrder {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"delete",@"uid":[AccountManager getUid], @"orderId": _allOrderModel.orderId};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

@end
