//
//  AllOrdersDetailViewController.m
//  UCook
//
//  Created by huangrun on 14-8-17.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AllOrdersDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AllOrdersDetailModel.h"
#import "EvaluateViewController.h"
#import "SalesReturnViewController.h"
#import "AlipayModel.h"

#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"

#import "RechargeViewController.h"

@interface AllOrdersDetailViewController () {
    AllOrdersDetailModel *_allOrdersDetailModel;
    UIAlertView *_alertView;
    AlipayModel *_alipayModel;
}

@end

@implementation AllOrdersDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"订单详情";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getTheInitData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64 - 49:568 - 64 - 49 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    [self getTheInitData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickPhoneBtn:(id)sender {
    UIWebView *phoneWV = [[UIWebView alloc]init];
    NSString *phoneNumStr = [NSString stringWithFormat:@"tel://%@",_allOrdersDetailModel.storeTel];
    [phoneWV loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:phoneNumStr]]];
    [self.view addSubview:phoneWV];
}
- (IBAction)clickLeftBtn:(id)sender {
    
    //付款
    if (_allOrdersDetailModel.status == 20) {
        //优厨币支付
        if ([_allOrdersDetailModel.payMethod isEqualToString:@"3"]) {
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%@\n 3.确认支付?",_allOrdersDetailModel.orderId, _allOrdersDetailModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"优厨币支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 101;
            [_alertView show];
            
        } else if ([_allOrdersDetailModel.payMethod isEqualToString:@"1"]) {
            //支付宝支付
            NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%@\n 3.确认支付?",_allOrdersDetailModel.orderId, _allOrdersDetailModel.payFee];
            
            _alertView = [[UIAlertView alloc]initWithTitle:@"支付宝支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 103;
            [_alertView show];
            
        } else if ([_allOrdersDetailModel.payMethod isEqualToString:@"0"]) {
            [GlobalSharedClass showAlertView:@"该商品为货到付款,请收到货再支付"];
            return;
        }
    } else if (_allOrdersDetailModel.status == 23) {
        EvaluateViewController *evaluateVC = [[EvaluateViewController alloc]initWithNibName:@"EvaluateViewController" bundle:nil];
        evaluateVC.idInteger = [_allOrdersDetailModel.storeId integerValue];
        evaluateVC.orderIdStr = _allOrdersDetailModel.orderId;
        [self.navigationController pushViewController:evaluateVC animated:YES];
    }

}
- (IBAction)clickRightBtn:(id)sender {
    if (_allOrdersDetailModel.status == 21) {
        //申请退款
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定申请退款?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 102;
        [_alertView show];
    } else if (_allOrdersDetailModel.status == 23 || _allOrdersDetailModel.status == 31) {
        if (_allOrdersDetailModel.refundStatus == 2 || !_allOrdersDetailModel.refundStatus || _allOrdersDetailModel.refundStatus == 0 || _allOrdersDetailModel.refundStatus == 3) {
            
            if ([_allOrdersDetailModel.refundTimes isEqualToString:@"3"]) {
                [GlobalSharedClass showAlertView:@"超出申请退货次数限制!"];
                return;
            }
            
            _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定申请退货?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            _alertView.tag = 104;
            [_alertView show];
        }
    } else if (_allOrdersDetailModel.status == 22 || _allOrdersDetailModel.status == 11) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"请您在收到货后再确认收货，否则有可能钱货两空。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 105;
        [_alertView show];
    } else if (_allOrdersDetailModel.status == 20 || _allOrdersDetailModel.status == 32 || _allOrdersDetailModel.status == 10 || _allOrdersDetailModel.status == 33) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要放弃此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 106;
        [_alertView show];
        
    } else if (_allOrdersDetailModel.status == 30) {
        _alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定要删除此订单？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        _alertView.tag = 107;
        [_alertView show];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 80.f;
    }else if (indexPath.section == 1) {
        return 90.f;
    } else if (indexPath.section == 2) {
        return 50.f;
    } else {
        if (indexPath.row == 0) {
            return 230.f;
        } else {
            return 30.0;
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _allOrdersDetailModel.products.count;
    } else if (section == 3) {
        return 1 + (_allOrdersDetailModel.refund.count != 0 ? _allOrdersDetailModel.refund.count + 1:0);
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section, indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        if (indexPath.section == 0) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"OrdersStatusCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 1) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AddressCell" owner:self options:nil]firstObject];
        } else if (indexPath.section == 2) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ShopInfoCell" owner:self options:nil]firstObject];
        } else {
            if (indexPath.row == 0) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"OrderInfoCell" owner:self options:nil]firstObject];
            } else {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
        }
    }
    if (indexPath.section == 0) {
        //订单状态
        NSDictionary *goodsInfoDic = [_allOrdersDetailModel.products objectAtIndex:indexPath.row];
        
        UIImageView *goodsIV = (UIImageView *)[cell viewWithTag:101];
        NSString *imgUrlStr = [goodsInfoDic objectForKey:@"image"];
        [goodsIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        
        UILabel *goodsNameLabel = (UILabel *)[cell viewWithTag:102];
        goodsNameLabel.text = [goodsInfoDic objectForKey:@"name"];
        
        NSString *amountString = [goodsInfoDic objectForKey:@"number"];
        UILabel *goodsAmountLabel = (UILabel *)[cell viewWithTag:103];
        goodsAmountLabel.text = [NSString stringWithFormat:@"共%@件商品",amountString];
        
        NSString *goodsPriceStr = [goodsInfoDic objectForKey:@"total"];
        UILabel *goodsPriceLabel = (UILabel *)[cell viewWithTag:104];
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",goodsPriceStr];
    } else if (indexPath.section == 1) {
        //收货地址
        UILabel *addressLabel = (UILabel *)[cell viewWithTag:101];
        addressLabel.text = _allOrdersDetailModel.receiverAddress;
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:102];
        nameLabel.text = _allOrdersDetailModel.receiverName;
        
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:103];
        phoneLabel.text = _allOrdersDetailModel.receiverMobile;
    } else if (indexPath.section == 2) {
        //商铺信息
        UILabel *shopNameLabel = (UILabel *)[cell viewWithTag:101];
        shopNameLabel.text = _allOrdersDetailModel.storeName;
        
        UILabel *shopPhoneLabel = (UILabel *)[cell viewWithTag:102];
        shopPhoneLabel.text = _allOrdersDetailModel.storeTel;
    } else {
        //订单信息
        if (indexPath.row == 0) {
        UILabel *orderIdLabel = (UILabel *)[cell viewWithTag:101];
        orderIdLabel.text = _allOrdersDetailModel.orderId;
        
        UILabel *orderDateLabel = (UILabel *)[cell viewWithTag:102];
        orderDateLabel.text = _allOrdersDetailModel.createTime;
        
        UILabel *payTimeLabel = (UILabel *)[cell viewWithTag:103];
        payTimeLabel.text = _allOrdersDetailModel.payTime;
        
        UILabel *payMethodLabel = (UILabel *)[cell viewWithTag:104];
        payMethodLabel.text = _allOrdersDetailModel.payMethodName;
        
        UILabel *deliveryTimeLabel = (UILabel *)[cell viewWithTag:105];
        deliveryTimeLabel.text = _allOrdersDetailModel.shipTime;
        
        UILabel *evaluateStatusLabel = (UILabel *)[cell viewWithTag:106];
        evaluateStatusLabel.text = _allOrdersDetailModel.commentStatusName;
        
        UILabel *salesReturnAmountLabel = (UILabel *)[cell viewWithTag:107];
        salesReturnAmountLabel.text = _allOrdersDetailModel.refundTimes;
        
        UILabel *salesReturnStatusLabel = (UILabel *)[cell viewWithTag:108];
        NSString *salesReturnStatusStr;
        if (_allOrdersDetailModel.refundStatus == 0) {
            salesReturnStatusStr = @"不用退货";
        } else if (_allOrdersDetailModel.refundStatus == 1) {
            salesReturnStatusStr = @"申请退货中";
        }
        salesReturnStatusLabel.text = salesReturnStatusStr;
    
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"退款记录";
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.font = [UIFont systemFontOfSize:13.0];
    } else {
        NSDictionary *currentDic = [_allOrdersDetailModel.refund objectAtIndex:indexPath.row - 2];
        NSString *refundStatusStr;
        if ([[currentDic objectForKey:@"status"] isEqualToString:@"1"]) {
            refundStatusStr = @"申请退货中";
        } else if ([[currentDic objectForKey:@"status"] isEqualToString:@"0"]) {
            refundStatusStr = @"不用退货";
        } else if ([[currentDic objectForKey:@"status"] isEqualToString:@"2"]) {
            refundStatusStr = @"同意退货";
        }

        cell.textLabel.text = [NSString stringWithFormat:@"退款金额：%@元       退款状态：%@",[currentDic objectForKey:@"totalFee"],refundStatusStr];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, BOUNDS.size.width - 10, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;
    
    //为section底部添加分割线
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width - 10, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width - 10, 5)];
    lineView.backgroundColor = kThemeColor;
    [headerView addSubview:lineView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 9.5, 80, 25)];
    if (section == 0) {
            titleLabel.text = @"订单状态";
        UILabel *goodsStatusLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, BOUNDS.size.width - 100 - 20, 44)];
        goodsStatusLabel.font = [UIFont systemFontOfSize:17];
        goodsStatusLabel.textColor = kThemeColor;
        goodsStatusLabel.textAlignment = NSTextAlignmentRight;
        goodsStatusLabel.text = _allOrdersDetailModel.statusName;
        [headerView addSubview:goodsStatusLabel];
    } else if (section == 1) {
        titleLabel.text = @"收货地址";
    } else if (section == 2) {
        titleLabel.text = @"商铺信息";
    } else {
        titleLabel.text = @"订单信息";
    }

    [headerView addSubview:titleLabel];

    
    return headerView;
}

- (void)configTheView {
    //商品总价
    _goodsTotalPriceLabel.text = [NSString stringWithFormat:@"￥%@",_allOrdersDetailModel.totalFee];
    //按钮状态值
    
    if (_allOrdersDetailModel.status == 32) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status  == 20) {
        _leftButton.hidden = NO;
        [_leftButton setTitle:@"付款" forState:UIControlStateNormal];
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status == 23) {
        //2表示经销商已评
        if ([_allOrdersDetailModel.commentStatus isEqualToString:@"0"]|| [_allOrdersDetailModel.commentStatus isEqualToString:@"2"]) {
            _leftButton.hidden = NO;
            [_leftButton setTitle:@"评价" forState:UIControlStateNormal];
        } else if ([_allOrdersDetailModel.commentStatus isEqualToString:@"1"]) {
            _leftButton.hidden = YES;
        }
        if (_allOrdersDetailModel.refundStatus == 2 || !_allOrdersDetailModel.refundStatus  || _allOrdersDetailModel.refundStatus == 0 || _allOrdersDetailModel.refundStatus == 3) {
            _rightButton.hidden = NO;
            [_rightButton setTitle:@"申请退货" forState:UIControlStateNormal];
                        _rightButton.userInteractionEnabled = YES;
        } else if (_allOrdersDetailModel.refundStatus == 1) {
            _rightButton.hidden = NO;
            [_rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];
            _rightButton.userInteractionEnabled = NO;
        }

    } else if (_allOrdersDetailModel.status == 21) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"申请退款" forState:UIControlStateNormal];
    }  else if (_allOrdersDetailModel.status == 22 || _allOrdersDetailModel.status  == 11) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status == 30) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"删除订单" forState:UIControlStateNormal];
    } else if (_allOrdersDetailModel.status == 31) {
        _leftButton.hidden = NO;
        _rightButton.hidden = NO;
        [_leftButton setTitle:@"交易成功" forState:UIControlStateNormal];
        if (_allOrdersDetailModel.refundStatus == 1) {
        [_rightButton setTitle:@"申请退货中" forState:UIControlStateNormal];
        } else {
        [_rightButton setTitle:@"申请退货" forState:UIControlStateNormal];
        }
    }  else if (_allOrdersDetailModel.status == 24) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"申请退款中" forState:UIControlStateNormal];
        _rightButton.userInteractionEnabled = NO;
    } else if (_allOrdersDetailModel.status == 25) {
        //在线 退货中
        _leftButton.hidden = YES;
        _rightButton.hidden = YES;
    } else if (_allOrdersDetailModel.status == 10) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
        [_rightButton setTitle:@"放弃交易" forState:UIControlStateNormal];
    }

    
    [_theTableView reloadData];

}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"detail",@"orderId":_orderIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _allOrdersDetailModel = [AllOrdersDetailModel objectWithKeyValues:dictionary];
            
            [self configTheView];
            [_theTableView reloadData];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 101) {
        //优厨币支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            [self payByUcookb];
        }
    } else if (alertView.tag == 102) {
        //申请退货
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            //            [self getOrderDetailData];
            [self orderStatusUpdate];
        }
        
    } else if (alertView.tag == 103) {
        //支付宝支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self getAlipayParameters];
        }
    } else if (alertView.tag == 104) {
        //申请退货
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            SalesReturnViewController *salesReturnVC = [[SalesReturnViewController alloc]init];
            salesReturnVC.orderIdStr = _allOrdersDetailModel.orderId;
            [self.navigationController pushViewController:salesReturnVC animated:YES];
        }
        
    } else if (alertView.tag == 105) {
        //确认收货
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self orderStatusUpdate];
        }
        
    } else if (alertView.tag == 106) {
        //放弃订单
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self orderStatusUpdate];
        }
        
    } else if (alertView.tag == 107) {
        //删除订单
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self deleteTheOrder];
        }
    }
}

#pragma mark - 优厨币付款
- (void)payByUcookb {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"ucookbPay",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId,@"total_fee": @([_allOrdersDetailModel.payFee integerValue])};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

#pragma mark - 请求支付宝参数
- (void)getAlipayParameters {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSArray *orderIdArr = @[@{@"orderId": _allOrdersDetailModel.orderId}];
    NSString *orderIdArrJsonStr = [self arrayToJson:orderIdArr];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"payString",@"uid":[AccountManager getUid], @"orderIds": orderIdArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _alipayModel = [AlipayModel objectWithKeyValues:dictionary];
            
            [self pay];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

#pragma mark - 支付宝付款
- (void)pay {
    /*
	 *生成订单信息及签名
	 *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
	 */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
	
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];
    
}

- (NSString *)getOrderInfo {
    /*
	 *点击获取prodcut实例并初始化订单信息
	 */
	Product *product = [[Product alloc]init];
    product.subject = [_alipayModel.info objectForKey:@"subject"];
    product.body = [_alipayModel.info objectForKey:@"body"];
    product.price = [[_alipayModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
    //    order.partner = PartnerID;
    //    order.seller = SellerID;
    order.partner = [_alipayModel.info objectForKey:@"partner"];
    order.seller = [_alipayModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_alipayModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
	order.productName = product.subject; //商品标题
	order.productDescription = product.body; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
	order.notifyURL = [_alipayModel.info objectForKey:@"notify_url"]; //回调URL
	
	return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
			id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
			if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
			}
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}

#pragma mark - 退款更新状态为退款申请中(21->24)、退货更新为申请退货中(23->25)、退款更新为申请退款中(22->23)、放弃交易更新为结束交易 (20->30、32->30、10->30)
- (void)orderStatusUpdate {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSInteger wantStatus;//需要更新到的状态值
    if (_allOrdersDetailModel.status  == 21) {
        wantStatus = 24;
    } else if (_allOrdersDetailModel.status  == 23) {
        wantStatus = 25;
    } else if (_allOrdersDetailModel.status == 22) {
        wantStatus = 23;
    } else if (_allOrdersDetailModel.status  == 20 || _allOrdersDetailModel.status  == 32 || _allOrdersDetailModel.status == 10) {
        wantStatus = 30;
    }
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"statusUpdate",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId,@"type": @"status",@"status": @(wantStatus)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self getTheInitData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)deleteTheOrder {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"delete",@"uid":[AccountManager getUid], @"orderId": _allOrdersDetailModel.orderId};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


@end
