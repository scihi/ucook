//
//  RechargeViewController.h
//  UCook
//
//  Created by scihi on 14-8-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AlixLibService.h"

//huangrun备注:下面的Product类是model类，其实此项目用不着，因为一次性处理订单就一个，这个主要用于多个订单，数据多的时候，不过我还是用按这个标准做吧，毕竟更规范
@interface Product : NSObject

@property (nonatomic, assign) float price;//商品价格
@property (copy, nonatomic) NSString *subject;//商品标题
@property (copy, nonatomic) NSString *body;//商品内容
@property (copy, nonatomic) NSString *orderId;//商品订单号

@end


@interface RechargeViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickPayBtn:(id)sender;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调
@property (strong, nonatomic) IBOutlet UIImageView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *payButton;


@end
