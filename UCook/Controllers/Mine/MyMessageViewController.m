//
//  MyMessageViewController.m
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyMessageViewController.h"
#import "MyMessageModel.h"
#import "AllOrdersDetailViewController.h"
#import "PromotionListViewController.h"

#import "DealerOrderDetailViewController.h"
#import "CRequestManager.h"
#import "AccountManager.h"

//商品竞拍详情
#import "DealerAuctionDetailViewController.h"

@interface MyMessageViewController ()
{
    UITableView *_theTableView;
    MyMessageModel *_myMessageModel;
}

@end

@implementation MyMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"我的消息";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectio
{
    return _myMessageModel.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyMessageCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSDictionary *currentDic = [_myMessageModel.list objectAtIndex:indexPath.row];
    cell.textLabel.text = [currentDic objectForKey:@"title"];
    cell.detailTextLabel.text = [currentDic objectForKey:@"message"];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger codeInteger = [[[_myMessageModel.list objectAtIndex:indexPath.row]objectForKey:@"type"]integerValue];
    //urlString可能为连接、订单id或活动id huangrun
    NSString *urlString = [[_myMessageModel.list objectAtIndex:indexPath.row]objectForKey:@"uri"];
    
    if (urlString && ![urlString isKindOfClass:[NSNull class]])
    {
        
        if (codeInteger == 1)
        {
            //网页
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
        else if (codeInteger == 2)
        {
            //订单详情
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"dealer"])
                {
                    DealerOrderDetailViewController *dodVc = [[DealerOrderDetailViewController alloc]initWithNibName:@"DealerOrderDetailViewController" bundle:nil];
                    dodVc.orderIdStr = urlString;
                    [self.navigationController pushViewController:dodVc animated:YES];
                }
                else
                {
            AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
            allOrdersDetailVC.orderIdStr = urlString;
                
            [self.navigationController pushViewController:allOrdersDetailVC animated:YES];
                }
        }
        else if (codeInteger == 3)
        {
            //活动列表
            PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
            promotionListVC.promotionIdStr = urlString;
            promotionListVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:promotionListVC animated:YES];
        }
        
        //竞拍包消息
        else if(codeInteger == 4)
        {

            NSDictionary *parameters = @{@"key": APP_KEY,
                                         @"controller": @"biddingAgency",
                                         @"action": @"list",
                                         @"uid":[AccountManager getUid],
                                         @"page":@"0"};
            
            [CRequestManager requestWith:parameters method:BASIC_OUTURL
                           requestSucess:^(id object)
             {
                               NSLog(@"%@",[object[@"data"][0] objectForKey:@"id"]);
               NSString  *idStr = [object[@"data"][0] objectForKey:@"id"];
                 
                 DealerAuctionDetailViewController *vc = [[DealerAuctionDetailViewController alloc]init];
                 vc.auctionBagIdStr =[ _myMessageModel.list[9] objectForKey:@"id"];
                 [self.navigationController pushViewController:vc animated:YES];
            }
                          requestFailure:^
            {
                NSLog(@"网络请求失败");
                           }];

        }
    }
    else
    {
        [GlobalSharedClass showAlertView:@"uri字段内容为空"];
    }
}

- (void)getTheInitData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,
                                 @"controller": @"system",
                                 @"action": @"push",
                                 @"uid":[AccountManager getUid],
                                 @"last_time": @""};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self stopRequest];
        if (RIGHT)
        {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _myMessageModel = [MyMessageModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        }
        else
        {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}




@end
