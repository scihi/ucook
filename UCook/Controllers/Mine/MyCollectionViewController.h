//
//  MyCollectionViewController.h
//  UCook
//
//  Created by scihi on 14-8-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MyCollectionViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>
- (IBAction)clickGoodsCollectBtn:(id)sender;
- (IBAction)clickShopCollectBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UIButton *goodsCollectBtn;
@property (strong, nonatomic) IBOutlet UIButton *shopCollectBtn;

@end
