//
//  ModifyAddressViewController.m
//  UCook
//
//  Created by huangrun on 14-8-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ModifyAddressViewController.h"
#import "NSStringAdditions.h"
#import "RegexKitLiteAdditions.h"
#import "ModifyAddressModel.h"

@interface ModifyAddressViewController () {
    NSDictionary *_idsDictionary;//省市区id
    ModifyAddressModel *_modifyAddressModel;
}

@end

@implementation ModifyAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"修改收货地址";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSelcetAddressBtn:(id)sender {
    SelectAddressViewController *selectAddressVC = [[SelectAddressViewController alloc]init];
    selectAddressVC.delegate = self;
    [self.navigationController pushViewController:selectAddressVC animated:YES];
}

- (void)addressDidSelect:(SelectAddressViewController *)selectAddressVC address:(NSString *)aAddress idsDictionary:(NSDictionary *)aIdsDictionary{
    _addressLabel.text = aAddress;
    _idsDictionary = [aIdsDictionary copy];
}

- (IBAction)clickSaveBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_addressLabel.text]) {
        [GlobalSharedClass showAlertView:@"请选择所在地"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_streetTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入街道地址"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_nameTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入收货人姓名"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_phoneTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入收货人联系电话"];
        return;
    }
    if (![NSString isValidatedMobilePhoneNumber:_phoneTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入正确的联系电话"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_postcodeTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入邮编"];
        return;
    }
    if (_postcodeTF.text.length != 6) {
        [GlobalSharedClass showAlertView:@"请输入正确的邮编"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"editRecAddress",@"uid":[AccountManager getUid],@"state": _modifyAddressModel.state,@"city": _modifyAddressModel.city,@"district": _modifyAddressModel.district, @"zipcode": _postcodeTF.text,@"address": _streetTF.text,@"contactName": _nameTF.text,@"mobilePhone": _phoneTF.text,@"id": _modifyAddressModel.id};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _modifyAddressModel = [ModifyAddressModel objectWithKeyValues:dictionary];
            [GlobalSharedClass showAlertView:MESSAGE];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)clickDeleteBtn:(id)sender {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"delRecAddress",@"uid":[AccountManager getUid],@"id": _takeDeliveryAddressIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
           [GlobalSharedClass showAlertView:MESSAGE];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"detailRecAddress",@"uid":[AccountManager getUid],@"id": _takeDeliveryAddressIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _modifyAddressModel = [ModifyAddressModel objectWithKeyValues:dictionary];
            [self setTheViewValue];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)setTheViewValue {
    _addressLabel.text = [NSString stringWithFormat:@"%@%@%@",_modifyAddressModel.stateName,_modifyAddressModel.cityName,_modifyAddressModel.districtName];
    _streetTF.text = _modifyAddressModel.address;
    _nameTF.text = _modifyAddressModel.contactName;
    _phoneTF.text = _modifyAddressModel.mobilePhone;
    _postcodeTF.text = _modifyAddressModel.zipcode;
}

@end
