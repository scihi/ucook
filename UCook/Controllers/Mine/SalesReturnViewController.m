//
//  SalesReturnViewController.m
//  UCook
//
//  Created by scihi on 14-8-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SalesReturnViewController.h"
#import "UIImageView+AFNetworking.h"

#import "AllOrdersDetailModel.h"
#import "NSObject+MJKeyValue.h"
#import "NSObject+MJMember.h"

static NSString *CellIdentifier = @"SalesReturnCell";


@interface SalesReturnViewController () {
    UITableView *_theTableView;
    UITableViewCell *_cell;//用于sepper
    NSMutableDictionary *_theDataMutDic;//用于steppter
    
    NSMutableArray *_goodsIdStrMutArr;//cell上的产品id数组
    NSMutableArray *_goodsNumMutArr;//cell上的产品数量数组
//    NSMutableArray *_goodsInfoMutArr;//包含id和数量的产品信息数组
    BOOL _noZero;//判断数量里有没有0
    
    AllOrdersDetailModel *_allOrderDetailModel;
    NSMutableArray *_productsMutArr;
    NSMutableArray *_selectNumMutArr;
    
    StepperTestViewController *stepperTestVC;
}

@end

@implementation SalesReturnViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"退货";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64:568 - 64 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
//    _productsMutArr = [NSMutableArray arrayWithCapacity:10];
//    _theDataMutDic = [NSMutableDictionary dictionaryWithCapacity:10];
    _selectNumMutArr = [NSMutableArray arrayWithCapacity:10];
    
    [self configTheNavBar];
    
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64)];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self registerCell];
    _theTableView.tableFooterView = [[UIView alloc]init];//遮住多余的分割线
    
    [self getTheInitData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerCell {
    [_theTableView registerNib:[UINib nibWithNibName:@"SalesReturnCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
}

#pragma - mark - tableView dataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _productsMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *currentDictionary = [_productsMutArr objectAtIndex:indexPath.row];
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [currentDictionary objectForKey:@"image"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = [currentDictionary objectForKey:@"name"];
   
    UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:103];
    NSString *currentPriceStr = [currentDictionary objectForKey:@"total"];
    currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
  
    UILabel *residueTimesLabel = (UILabel *)[cell viewWithTag:104];
    NSInteger numberInteger = [[currentDictionary objectForKey:@"number"]integerValue] - [[currentDictionary objectForKey:@"refundSucNum"]integerValue];
    residueTimesLabel.text = [NSString stringWithFormat:@"%d", numberInteger];
    
    //数量选择按钮
    UIButton *countButton = (UIButton *)[cell viewWithTag:105];
    countButton.layer.borderWidth = 1.0;
    countButton.layer.borderColor = [UIColor colorWithRed:234.00f/255.00f green:234.00f/255.00f blue:234.00f/255.00f alpha:1.00f].CGColor;
//    NSInteger canRefundNum = [[currentDictionary objectForKey:@"number"]floatValue] - [[currentDictionary objectForKey:@"refundSucNum"]floatValue];
    NSInteger canRefundNum = [[_selectNumMutArr objectAtIndex:indexPath.row]integerValue];
    NSString *numberString = [NSString stringWithFormat:@"%d",canRefundNum];
    [countButton setTitle:numberString forState:UIControlStateNormal];
    [countButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [countButton addTarget:self action:@selector(setTheGoodsNumber:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setTheGoodsNumber:(id)sender {
    UIButton *button = sender;
    if (IS_IOS8) {
        _cell = (UITableViewCell *)[[button superview]superview];
    } else {
    _cell = (UITableViewCell *)[[[button superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
    _theDataMutDic = [NSMutableDictionary dictionaryWithDictionary:[_productsMutArr objectAtIndex:indexPath.row]];
    stepperTestVC = [[StepperTestViewController alloc]initWithNibName:@"StepperTestViewController" bundle:nil];
     stepperTestVC.Maximum = [[_theDataMutDic objectForKey:@"number"]floatValue] - [[_theDataMutDic objectForKey:@"refundSucNum"]floatValue];
    stepperTestVC.theFloat = [[_selectNumMutArr objectAtIndex:indexPath.row]floatValue];
    stepperTestVC.view.frame = CGRectMake(0, 0, 300, 200);
    stepperTestVC.delegate = self;
    stepperTestVC.contador.textField.delegate = self;
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [self addChildViewController:stepperTestVC];
    [view addSubview:stepperTestVC.view];
    stepperTestVC.view.center = view.center;
    [stepperTestVC didMoveToParentViewController:self];
    stepperTestVC.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataMutDic objectForKey:@"price"]floatValue] * ([[_theDataMutDic objectForKey:@"number"]floatValue] - [[_theDataMutDic objectForKey:@"refundSucNum"]floatValue])];
    //可以自定义按钮的标题，但要记得在上个view传进来 huangrun
//    [stepperTestVC.confirmButton setTitle:@"退货" forState:UIControlStateNormal];
        [stepperTestVC.confirmButton setTitle:@"确定" forState:UIControlStateNormal];
   
    [self.view addSubview:view];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
}

#pragma mark - textStepperDelegate
- (void)stepperValueDidChange:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    
    stepperTestViewController.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataMutDic objectForKey:@"price"]floatValue] * counter];
}

- (void)stepperDidClickConfirmBtn:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    UIButton *countButton = (UIButton *)[_cell viewWithTag:105];
    [countButton setTitle:[NSString stringWithFormat:@"%ld",(long)counter] forState:UIControlStateNormal];
    
    NSIndexPath *indexPath = [_theTableView indexPathForCell:_cell];
//    _theDataMutDic = [_productsMutArr objectAtIndex:indexPath.row];
//    [_theDataMutDic setObject:@(counter) forKey:@"number"];
//    
//    [_productsMutArr replaceObjectAtIndex:indexPath.row withObject:_theDataMutDic];
    [_selectNumMutArr replaceObjectAtIndex:indexPath.row withObject:@(counter)];
    
//    [_theTableView reloadData];
    
    
    
}

//用的订单详情接口
- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"detail",@"orderId":_orderIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _allOrderDetailModel = [AllOrdersDetailModel objectWithKeyValues:dictionary];
            _productsMutArr = [NSMutableArray arrayWithArray:_allOrderDetailModel.products];
            
            for (NSDictionary *dictionary in _productsMutArr) {
                NSInteger numberInteger = [[dictionary objectForKey:@"number"]floatValue] - [[dictionary objectForKey:@"refundSucNum"]floatValue];
                [_selectNumMutArr addObject:@(numberInteger)];
            }
            
            [_theTableView reloadData];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)refund {
    NSMutableArray *goodsInfoMutArr = [NSMutableArray arrayWithCapacity:10];
    
    for (int i = 0; i < _productsMutArr.count; i++) {
        [goodsInfoMutArr addObject:@{@"productId": [[_productsMutArr objectAtIndex:i]objectForKey:@"id"],@"number": [NSString stringWithFormat:@"%d",[[_selectNumMutArr objectAtIndex:i]integerValue]]}];
    }
    
    NSString *goodsInfoMutArrJsonStr = [self arrayToJson:goodsInfoMutArr];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"refund",@"uid":[AccountManager getUid], @"orderId": _allOrderDetailModel.orderId,@"products": goodsInfoMutArrJsonStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self getTheInitData];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    [navRightBtn setTitle:@"退货" forState:UIControlStateNormal];
    navRightBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}


- (void)clickNavRightBtn:(id)sender {
    [self refund];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    stepperTestVC.view.frame = CGRectMake(10, 5, 300, 200);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    stepperTestVC.view.center = view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [stepperTestVC.contador.textField resignFirstResponder];
    return YES;
}

@end
