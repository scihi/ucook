//
//  RechargeViewController.m
//  UCook
//
//  Created by scihi on 14-8-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "RechargeViewController.h"
#import "RechargeModel.h"
#import "NSStringAdditions.h"
#import "AlipayModel.h"

#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"

@implementation Product

@end

static NSString *CellIdentifier = @"RechargeCell";

@interface RechargeViewController () {
    UITableView *_theTableView;
    RechargeModel *_rechargeModel;
    NSString *_payMethodIdStr;
    UITableViewCell *_cell;
    UITextField *_rechargeTF;
    AlipayModel *_alipayModel;
}

@end

@implementation RechargeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"充值";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
        if (self.view.frame.size.height != (BOUNDS.size.height - 64))
        self.view.frame = CGRectMake(0, 64, self.view.frame.size.width, (BOUNDS.size.height - 64));
}

#pragma mark - 为了解决支付宝WAP支付取消返回回来界面偏移问题
- (void)viewDidLayoutSubviews {
    self.tabBarController.tabBar.hidden = YES;
    if (self.view.frame.size.height != (BOUNDS.size.height - 64)) {
                self.view.frame = CGRectMake(0, 64, self.view.frame.size.width, (BOUNDS.size.height - 64));
//    _bottomView.frame = CGRectMake(_bottomView.frame.origin.x, _bottomView.frame.origin.y + 49, _bottomView.frame.size.width, _bottomView.frame.size.height);
//    _payButton.frame = CGRectMake(_payButton.frame.origin.x, _payButton.frame.origin.y + 49, _payButton.frame.size.width, _payButton.frame.size.height);
//    _theTableView.frame =CGRectMake(_theTableView.frame.origin.x, _theTableView.frame.origin.y,_theTableView.frame.size.width, _theTableView.frame.size.height + 49);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _result = @selector(paymentResult:);//支付宝的应用内(wap方式)回调方法

    [_theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    _theTableView.tableFooterView = [[UIView alloc]init];
    
    [self getTheInitData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 充值、提现
- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"ucookb",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _rechargeModel = [RechargeModel objectWithKeyValues:dictionary];
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return _rechargeModel.payMethods.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyMessageCell";
    
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    if (indexPath.section == 0) {
        CGFloat theFloat = [_rechargeModel.ucookbAccount floatValue];
        cell.textLabel.text = [NSString stringWithFormat:@"余额:￥%.2f",theFloat];
    } else if (indexPath.section == 1) {
        _rechargeTF = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, BOUNDS.size.width - 30, 44)];
        _rechargeTF.delegate = self;
        _rechargeTF.placeholder = @"请输入充值金额";
        _rechargeTF.keyboardType = UIKeyboardTypeDecimalPad;
        [cell.contentView addSubview:_rechargeTF];
    } else {
        cell.textLabel.text = [[_rechargeModel.payMethods objectAtIndex:indexPath.row]objectForKey:@"name"];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2) {
    return 44.f;
    } else {
        return 0.f;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        return @"选择充值方式";
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSUInteger _oldRow;
    if (indexPath.section == 2) {
    if (_oldRow >= 0)
    {
        // 清除上次一选中的钩子
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_oldRow inSection:indexPath.section];
        _cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
        [_cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    // 添加当前行的小勾子
    _cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [_cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    // 标记当前选中的行数
    _oldRow = indexPath.row;
        _payMethodIdStr = [[_rechargeModel.payMethods objectAtIndex:indexPath.row]objectForKey:@"id"];
    }
}

- (IBAction)clickPayBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_rechargeTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入充值金额"];
        return;
    }
    
    if (_cell.accessoryType != UITableViewCellAccessoryCheckmark) {
        [GlobalSharedClass showAlertView:@"请选择支付方式"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"voucher",@"uid":[AccountManager getUid],@"payMethodId": _payMethodIdStr,@"money": _rechargeTF.text};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _alipayModel = [AlipayModel objectWithKeyValues:dictionary];
            
            NSLog([NSString stringWithFormat:@"菲菲接招~~~~~~~~~~~~~~~~~~~~~~订单号：%@，回调URL：%@",[_alipayModel.info objectForKey:@"orderId"],[_alipayModel.info objectForKey:@"notify_url"]],nil);
            
            [self pay];
            
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)pay {
    /*
	 *生成订单信息及签名
	 *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
	 */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
	
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];

}

- (NSString *)getOrderInfo {
    /*
	 *点击获取prodcut实例并初始化订单信息
	 */
	Product *product = [[Product alloc]init];
    product.subject = [_alipayModel.info objectForKey:@"subject"];
    product.body = [_alipayModel.info objectForKey:@"body"];
    product.price = [[_alipayModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
//    order.partner = PartnerID;
//    order.seller = SellerID;
    order.partner = [_alipayModel.info objectForKey:@"partner"];
    order.seller = [_alipayModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_alipayModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
	order.productName = product.subject; //商品标题
	order.productDescription = product.body; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
	order.notifyURL = [_alipayModel.info objectForKey:@"notify_url"]; //回调URL
	
	return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
			id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
			if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                
                
			}
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}


@end
