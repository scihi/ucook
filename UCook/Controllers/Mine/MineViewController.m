//
//  MineViewController.m
//  UCook
//
//  Created by scihi on 14-7-2.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MineViewController.h"
#import "SettingViewController.h"
#import "UIButton+AFNetworking.h"
#import "MyCollectionViewController.h"
#import "TakeDeliveryAddressViewController.h"
#import "MyMessageViewController.h"
#import "MyCouponViewController.h"
#import "MyReportFormsViewController.h"
#import "MyAuctionViewController.h"
#import "RechargeViewController.h"
#import "WithdrawViewController.h"
#import "AllOrdersViewController.h"
#import "RechargeModel.h"
#import "WithdrawWithAccountViewController.h"

#import "DealRecordViewController.h"

@interface MineViewController () {
    NSDictionary *_theDataDic;
    RechargeModel *_rechargeModel;
    
    NSArray *_orderStatusArr;//顶部的五个按钮的名字和状态 之前写的代码 没用model 维护性差 huangrun
}

@end

@implementation MineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"我的";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
        [super viewWillAppear:animated];
    //让后面视图隐藏了的tabBar显现
    self.tabBarController.tabBar.hidden = NO;
    
       //显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    //   显示navigationBar
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    if (![[AccountManager getRole]isEqualToString:@"normal"]) {
        self.menuView.hidden = YES;
        self.menuView2.frame = CGRectMake(5, 155, BOUNDS.size.width - 10, 278);
        [self.theScrollView addSubview:self.menuView2];
    } else {
        self.menuView2.hidden = YES;
        self.menuView.hidden = NO;
    }
    

    [self getPersonalInfoData];//获取个人数据并展示
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configTheNavBar];
    _chargeButton.layer.cornerRadius = 12.0;
    _withdrawButton.layer.cornerRadius = 12.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_set.png"]];
    navRightIV.frame = CGRectMake(10, 2, 30, 30);
    [navRightBtn addSubview:navRightIV];
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)clickNavRightBtn:(id)sender {
    SettingViewController *settingVC =[[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:nil];
    settingVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:settingVC animated:YES];
}

- (void)getPersonalInfoData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user", @"action": @"center",@"uid": [AccountManager getUid]};
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self stopRequest];
        if (RIGHT) {
            NSLog(@"MESSAGE: %@",MESSAGE);
            _theDataDic = [[responseObject objectForKey:@"data"]mutableCopy];
            //以下展示数据
            NSString *imgUrlStr = [_theDataDic objectForKey:@"headimg"];
            [_headImgBtn setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            _usernameLabel.text = [_theDataDic objectForKey:@"username"];
            NSString *uCookCoinStr = [_theDataDic objectForKey:@"points"];
            _uCookCoinLabel.text = [NSString stringWithFormat:@"积分: %@",uCookCoinStr];
            NSString *balanceString = [_theDataDic objectForKey:@"currency"];
            CGFloat theFloat = [balanceString floatValue];
            _balanceLabel.text = [NSString stringWithFormat:@"余额: %.2f",theFloat];
            _orderStatusArr = [_theDataDic objectForKey:@"orderStatus"];//订单状态数组
            if(_orderStatusArr.count == 5) {
                NSInteger waitingForPaymentCountInteger = [[[_orderStatusArr firstObject]objectForKey:@"number"]integerValue];
                NSInteger waitingForDeliveryInteger = [[[_orderStatusArr objectAtIndex:1]objectForKey:@"number"]integerValue];
                NSInteger waitingForTakeDeliveryInteger = [[[_orderStatusArr objectAtIndex:2]objectForKey:@"number"]integerValue];
                NSInteger waitingForEvaluateInteger = [[[_orderStatusArr objectAtIndex:3]objectForKey:@"number"]integerValue];
                NSInteger refundInteger = [[[_orderStatusArr objectAtIndex:4]objectForKey:@"number"]integerValue];
                _waitingForPaymentLabel.text = [NSString stringWithFormat:@"%ld",(long)waitingForPaymentCountInteger];
                _waitingForDeliveryLabel.text = [NSString stringWithFormat:@"%ld",(long)waitingForDeliveryInteger];
                _waitingForTakeDeliveryLabel.text = [NSString stringWithFormat:@"%ld",(long)waitingForTakeDeliveryInteger];
                _waitingForEvaluateLabel.text = [NSString stringWithFormat:@"%ld",(long)waitingForEvaluateInteger];
                _refundLabel.text = [NSString stringWithFormat:@"%ld",(long)refundInteger];
            }
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}
//全部订单
- (IBAction)clickAllOrderBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"全部订单";
    allOrdersVC.statusString = @"";
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//我的收藏
- (IBAction)clickMyCollectionBtn:(id)sender {
    MyCollectionViewController *myCollectionVC = [[MyCollectionViewController alloc]initWithNibName:@"MyCollectionViewController" bundle:nil];
    myCollectionVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myCollectionVC animated:YES];
}
//收货地址
- (IBAction)clickTakeDeliveryAddressBtn:(id)sender {
    TakeDeliveryAddressViewController *takeDeliveryAddressVC = [[TakeDeliveryAddressViewController alloc]initWithNibName:@"TakeDeliveryAddressViewController" bundle:nil];
    takeDeliveryAddressVC.hidesBottomBarWhenPushed = YES;
    takeDeliveryAddressVC.vCTag = 102;
    [self.navigationController pushViewController:takeDeliveryAddressVC animated:YES];
}
//我的优惠券
- (IBAction)clickMyDiscountCouponBtn:(id)sender {
    MyCouponViewController *myCouponVC = [[MyCouponViewController alloc]initWithNibName:@"MyCouponViewController" bundle:nil];
    myCouponVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myCouponVC animated:YES];
}
//我的消息
- (IBAction)clickMyMessageBtn:(id)sender {
    MyMessageViewController *myMessageVC = [[MyMessageViewController alloc]init];
    myMessageVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myMessageVC animated:YES];
}
//我的报表
- (IBAction)clickMyReportFormsBtn:(id)sender {
    MyReportFormsViewController *myReportFormsVC = [[MyReportFormsViewController alloc]init];
    myReportFormsVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myReportFormsVC animated:YES];
}


//待付款
- (IBAction)clickWaitingForPaymentBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"待付款";
    allOrdersVC.statusString = [[_orderStatusArr firstObject]objectForKey:@"status"];
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//待发货
- (IBAction)clickWaitingForDeliveryBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"待发货";
    allOrdersVC.statusString = [[_orderStatusArr objectAtIndex:1]objectForKey:@"status"];
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//待收货
- (IBAction)clickWaitingForTakeDeliveryBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"待收货";
    allOrdersVC.statusString = [[_orderStatusArr objectAtIndex:2]objectForKey:@"status"];
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//待评价
- (IBAction)clickWaitingForEvaluateBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"待评价";
    allOrdersVC.statusString = [[_orderStatusArr objectAtIndex:3]objectForKey:@"status"];
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//退款
- (IBAction)clickRefundBtn:(id)sender {
    AllOrdersViewController *allOrdersVC = [[AllOrdersViewController alloc]init];
    allOrdersVC.titleString = @"退款";
    allOrdersVC.statusString =  [[_orderStatusArr objectAtIndex:4]objectForKey:@"status"];
    allOrdersVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allOrdersVC animated:YES];
}
//充值
- (IBAction)clickChargeBtn:(id)sender {
    RechargeViewController *rechargeVC =[[RechargeViewController alloc]init];
    rechargeVC.hidesBottomBarWhenPushed  = YES;
    [self.navigationController pushViewController:rechargeVC animated:YES];
}
//提现
- (IBAction)clickWithdrawBtn:(id)sender {
    
   [self getTheInitData];
    
}

//我的竞拍
- (IBAction)clickMyAuctionBtn:(id)sender {
    MyAuctionViewController *myAuctionVC = [[MyAuctionViewController alloc]init];
    myAuctionVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myAuctionVC animated:YES];
}

#pragma mark - 充值、提现
- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"ucookb",@"uid":[AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _rechargeModel = [RechargeModel objectWithKeyValues:dictionary];
            
            if (!_rechargeModel.account) {
                WithdrawViewController *withdrawVC = [[WithdrawViewController alloc]initWithNibName:@"WithdrawViewController" bundle:nil];
                withdrawVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:withdrawVC animated:YES];

            } else {
                WithdrawWithAccountViewController *withdrawWithAccountVC = [[WithdrawWithAccountViewController alloc]initWithNibName:@"WithdrawWithAccountViewController" bundle:nil];
                withdrawWithAccountVC.theDataDic = _rechargeModel.account;
                withdrawWithAccountVC.isCashInteger = [_rechargeModel.isCash integerValue];
                withdrawWithAccountVC.ucookbAccountStr = [NSString stringWithFormat:@"%.2f元",[_rechargeModel.ucookbAccount floatValue]];
                withdrawWithAccountVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:withdrawWithAccountVC animated:YES];
            }
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


- (IBAction)clickDealRecordBtn:(id)sender {
    
    DealRecordViewController *dealRecordVC = [[DealRecordViewController alloc]init];
    dealRecordVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dealRecordVC animated:YES];
}

- (IBAction)clickManageMyStoreBtn:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"dealer" forKey:NS_DEALER_LAST_PLACE];//餐厅模块
        [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:NS_GOTO_DEALER_MODULE object:nil];
}
@end
