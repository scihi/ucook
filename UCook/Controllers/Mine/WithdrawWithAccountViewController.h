//
//  WithdrawWithAccountViewController.h
//  UCook
//
//  Created by huangrun on 14-8-19.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface WithdrawWithAccountViewController : GeneralWithBackBtnViewController
@property (strong, nonatomic) IBOutlet UILabel *namelabel;
@property (strong, nonatomic) IBOutlet UILabel *cardNumLabel;
@property (strong, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UITextField *withdrawMoneyTF;
@property (strong, nonatomic) IBOutlet UIButton *withdrawButton;
- (IBAction)clickWithdrawBtn:(id)sender;

@property (strong, nonatomic) NSDictionary *theDataDic;
@property (nonatomic) NSInteger isCashInteger;//提现是否在处理中
@property (copy, nonatomic) NSString *ucookbAccountStr;//账户余额

@end
