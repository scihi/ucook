//
//  AllOrdersViewController.h
//  UCook
//
//  Created by huangrun on 14-8-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

#import "AlixLibService.h"


@interface AllOrdersViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (copy, nonatomic) NSString *titleString;
@property (copy, nonatomic) NSString *statusString;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调

@end
