//
//  DealRecordViewController.h
//  UCook
//
//  Created by huangrun on 14-9-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface DealRecordViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@end
