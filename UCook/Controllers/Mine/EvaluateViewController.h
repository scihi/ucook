//
//  EvaluateViewController.h
//  UCook
//
//  Created by huangrun on 14-8-24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "RatingView.h"

@interface EvaluateViewController : GeneralWithBackBtnViewController <RatingViewDelegate>
@property (strong, nonatomic) IBOutlet RatingView *starView;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UITextView *evaluateTV;
- (IBAction)clickSubmitBtn:(id)sender;

@property (nonatomic) NSInteger idInteger;
@property (copy, nonatomic) NSString *orderIdStr;

@end
