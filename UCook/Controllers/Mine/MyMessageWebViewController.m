//
//  MyMessageWebViewController.m
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyMessageWebViewController.h"

@interface MyMessageWebViewController ()

@end

@implementation MyMessageWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _titleString;
    
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64)];
    [self.view addSubview:webView];
  
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
