//
//  MyCouponViewController.m
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyCouponViewController.h"
#import "MyCouponModel.h"
#import "UIImageView+AFNetworking.h"
#import "CGRectAdditionals.h"

#import "NSObject+MJKeyValue.h"
#import "NSObject+MJMember.h"

static NSString *CellIdentifier = @"MyCouponCell";

@interface MyCouponViewController () {
    MyCouponModel *_myCouponModel;
    NSMutableArray *_myCouponMutArr;
    NSString *_typeString;
    UIView *_noDataView;//无数据时的展示背景
}

@end

@implementation MyCouponViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"优惠券";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_theTableView registerNib:[UINib nibWithNibName:@"MyCouponCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
//    _theTableView.tableFooterView = [[UIView alloc]init];//遮住多余的分割线
    _theTableView.separatorStyle = UITableViewCellSeparatorStyleNone;//不需要分割线 所以上句多余
    [self clickUnusedBtn:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _myCouponMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    MyCouponModel *myCouponModel = [_myCouponMutArr objectAtIndex:indexPath.row];
    NSString *imgUrlStr = myCouponModel.storeLogo;
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = myCouponModel.name;
    
    UILabel *timeLabel = (UILabel *)[cell viewWithTag:103];
    timeLabel.text = [NSString stringWithFormat:@"有效期:%@至%@",myCouponModel.startTime,myCouponModel.endTime];
    
    UILabel *moneyLabel = (UILabel *)[cell viewWithTag:104];
    moneyLabel.text = [NSString stringWithFormat:@"￥%@",myCouponModel.money];
    
    UILabel *conditionLabel = (UILabel *)[cell viewWithTag:105];
    conditionLabel.text = [NSString stringWithFormat:@"订单满￥%@起",myCouponModel.condition];
    
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:106];
    numberLabel.text = [NSString stringWithFormat:@"x%@",myCouponModel.number];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (IBAction)clickUnusedBtn:(id)sender {
    _unusedButton.selected = YES;
    _usedButton.selected = NO;
    _expiryButton.selected = NO;
    _typeString = @"normal";
    [self getData];
}

- (IBAction)clickUsedBtn:(id)sender {
    _unusedButton.selected = NO;
    _usedButton.selected = YES;
    _expiryButton.selected = NO;
    _typeString = @"used";
    [self getData];
}

- (IBAction)clickExpiryBtn:(id)sender {
    _unusedButton.selected = NO;
    _usedButton.selected = NO;
    _expiryButton.selected = YES;
    _typeString = @"outDate";
    [self getData];
}

- (void)getData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"user",@"action": @"coupons",@"uid":[AccountManager getUid],@"type": _typeString};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            NSArray *array = [responseObject objectForKey:@"data"];
            _myCouponMutArr = [NSMutableArray arrayWithArray:[MyCouponModel objectArrayWithKeyValuesArray:array]];
            [_theTableView reloadData];
            if (_myCouponMutArr.count == 0) {
                [self configTheNoDataViewForTheTableView];
            }
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                [_myCouponMutArr removeAllObjects];
                [_theTableView reloadData];
                [_noDataView removeFromSuperview];
                _theTableView.backgroundColor = [UIColor clearColor];
                //设置无数据时的提示视图
                [self configTheNoDataViewForTheTableView];
                

        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}


@end
