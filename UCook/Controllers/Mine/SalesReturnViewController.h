//
//  SalesReturnViewController.h
//  UCook
//
//  Created by scihi on 14-8-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "StepperTestViewController.h"

@interface SalesReturnViewController : GeneralWithBackBtnViewController <UITableViewDataSource,
UITableViewDelegate, TextStepperDelegate, UITextFieldDelegate>

@property (copy, nonatomic) NSString *orderIdStr;

@end
