﻿//
//  PartnerConfig.h
//  AlipaySdkDemo
//
//  Created by ChaoGanYing on 13-5-3.
//  Copyright (c) 2013年 RenFei. All rights reserved.
//
//  提示：如何获取安全校验码和合作身份者id
//  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
//  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
//  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
//

#ifndef MQPDemo_PartnerConfig_h
#define MQPDemo_PartnerConfig_h

//由服务器返回以下两个值，所以本地取消

//////合作身份者id，以2088开头的16位纯数字
//#define PartnerID @"2088501822952617"
//////收款支付宝账号
//#define SellerID  @"xiaosu_zhang@126.com"

//安全校验码（MD5）密钥，以数字和字母组成的32位字符
#define MD5_KEY @""

//商户私钥，自助生成
#define PartnerPrivKey @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKI7Dx9abTCkgbY0awz5lUaifz3g6z/eh8zjaAebSAtXLvUYvcXeC4dl4BGbR6bWsHDCHJwY/oR05OO13MmXdSpE5pNtUMiaszE+y+wCMj5/yGNAJAm6ayqJNv8KISplu8COSRilF6hmD8ZEwyLkeubtdrrecxnBhYhfrrvEpH6tAgMBAAECgYBO8NIQ/Z14N/RuRt94dtIuhs/HunR+pbgK+PUcDnFmI0xUM2/UopGVlvsLVjXtdzZwB+SICJmAtqjSLSJ8bdiyrFmLovNJrqQFk2eqGb5EBVfvHXnEk0621bI7DPmtouHAPOQQlja7hcLMNXch7aSUbtVQ+zb3qqU/UAuoQCZbeQJBANJqKr4SsGhvWOWYA7RUw1iuCHnilZN33WuMERrcYjcIBWlvdJooBUXq4XlRTl4o+4Jfft0OJiZx65E8pCdfft8CQQDFYI2jvVfGhWFyxMse3mHOA1gEhtPpqEpNjUDh0xs3Is7SjH/yy2cbeDA0LY1k214SuZod4GzLH1lleBQVcA/zAkA/9+7OojfnSBKqmjBmHm7VAvOoum3my0YRcB+zHtTtDg83Ip88TnZr7yBlFYbRsXvlAss6wRZwEaogGWN+ZNXZAkEAtwwrb55KyC6syE5f/hnJZNDPESulXj4X4sJMWOJ6i/C5FKT+iEJBNkWJlupkBhQ3s2/z0R9wZhSFyMfLAHK0JwJAI4mUlLPmLPKM+40GdAu31hx+0FGZbEaHdpdikjT0Ljs4u28WHHwbzNVRo713jMSInWwI9Ws2ODTbqvdNuEXc4Q=="



//支付宝公钥
#define AlipayPubKey @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB"

#endif
