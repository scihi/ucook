//
//  WithdrawWithAccountViewController.m
//  UCook
//
//  Created by huangrun on 14-8-19.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "WithdrawWithAccountViewController.h"
#import "NSStringAdditions.h"

@interface WithdrawWithAccountViewController () {
    NSString *_accountIdStr;//帐号id 用于提现
}

@end

@implementation WithdrawWithAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"提现";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _withdrawButton.layer.cornerRadius = 6.0;
    
    _namelabel.text = [_theDataDic objectForKey:@"receiverName"];
    _cardNumLabel.text = [_theDataDic objectForKey:@"accounts"];
    _bankNameLabel.text = [_theDataDic objectForKey:@"bankName"];
    _balanceLabel.text = _ucookbAccountStr;
    _accountIdStr = [_theDataDic objectForKey:@"id"];
    
    if (_isCashInteger == 1) {
        [_withdrawButton setTitle:@"请等待提现审核通过..." forState:UIControlStateNormal];
        _withdrawButton.userInteractionEnabled = NO;
        _withdrawMoneyTF.userInteractionEnabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickWithdrawBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_withdrawMoneyTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入提现金额"];
        return;
    }
    
    [_withdrawMoneyTF resignFirstResponder];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"withdraw",@"uid":[AccountManager getUid],@"money": _withdrawMoneyTF.text,@"accountId": _accountIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            
            [GlobalSharedClass showAlertView:MESSAGE];
            [_withdrawButton setTitle:@"请等待提现审核通过..." forState:UIControlStateNormal];
            _withdrawButton.userInteractionEnabled = NO;
            _withdrawMoneyTF.userInteractionEnabled = NO;
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}
@end
