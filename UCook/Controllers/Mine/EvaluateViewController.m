//
//  EvaluateViewController.m
//  UCook
//
//  Created by huangrun on 14-8-24.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "EvaluateViewController.h"
#import "NSStringAdditions.h"

@interface EvaluateViewController ()

@end

@implementation EvaluateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"评价";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_starView setImagesDeselected:@"ico_star_big_dark" partlySelected:@"1.png" fullSelected:@"ico_star_big_light" andDelegate:self];
	[_starView displayRating:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ratingChanged:(float)newRating {
	_ratingLabel.text = [NSString stringWithFormat:@"%1.f", newRating];
}

- (IBAction)clickSubmitBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_evaluateTV.text]) {
        [GlobalSharedClass showAlertView:@"请输入评价内容"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"review",@"action": @"create",@"uid":[AccountManager getUid],@"content": _evaluateTV.text,@"entityId": @3,@"entityPk": @(_idInteger),@"rating":@([_ratingLabel.text floatValue]), @"orderId": _orderIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [self.navigationController popViewControllerAnimated:YES];
            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}
@end
