//
//  MyReportFormsViewController.m
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyReportFormsViewController.h"
#import "MyReportFormsModel.h"
#import "MyMessageWebViewController.h"
#import "MJRefresh.h"

#import "DealerMyReportFormsModel.h"

@interface MyReportFormsViewController () {
    UITableView *_theTableView;
    MyReportFormsModel *_myReportFormsModel;
    
    NSArray *_dmrfModelArr;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
}

@end

@implementation MyReportFormsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"我的报表";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [self getTheInitData];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
    return _myReportFormsModel.products.count;
    } else {
        return _dmrfModelArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyReportFormsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
    NSDictionary *currentDic = [_myReportFormsModel.products objectAtIndex:indexPath.row];
    cell.textLabel.text = [currentDic objectForKey:@"name"];
    } else {
        DealerMyReportFormsModel *dmrfModel = [_dmrfModelArr objectAtIndex:indexPath.row];
        cell.textLabel.text = dmrfModel.name;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MyMessageWebViewController *myMessageWebVC = [[MyMessageWebViewController alloc]init];
        myMessageWebVC.titleString = @"我的报表详情";
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
    myMessageWebVC.urlString = [[_myReportFormsModel.products objectAtIndex:indexPath.row]objectForKey:@"url"];
    } else {
        DealerMyReportFormsModel *dmrfModel = [_dmrfModelArr objectAtIndex:indexPath.row];
        myMessageWebVC.urlString = dmrfModel.url;
    }
    [self.navigationController pushViewController:myMessageWebVC animated:YES];
}

- (void)getTheInitData {
    _pageNumber = 6;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString *roleStr;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
    roleStr = @"restaurant";
    } else {
        roleStr = @"agency";
    }
        
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": roleStr,@"action": @"report",@"uid":[AccountManager getUid],@"monthNum": @(_pageNumber)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _myReportFormsModel = [MyReportFormsModel objectWithKeyValues:dictionary];
            } else {
                NSArray *array = [responseObject objectForKey:@"data"];
                _dmrfModelArr = [DealerMyReportFormsModel objectArrayWithKeyValuesArray:array];
            }
            [_theTableView reloadData];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber += 6;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSString *roleStr;
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
            roleStr = @"restaurant";
        } else {
            roleStr = @"agency";
        }
        
          NSDictionary *parameters = @{@"key": APP_KEY,@"controller": roleStr,@"action": @"report",@"uid":[AccountManager getUid],@"monthNum": @(_pageNumber)};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [self stopRequest];
            if (RIGHT) {
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {

                //            [_theDataMutArr removeAllObjects];
                NSDictionary *dictionary = [responseObject objectForKey:@"data"];
                _myReportFormsModel = [MyReportFormsModel objectWithKeyValues:dictionary];
                } else {
                    NSArray *array = [responseObject objectForKey:@"data"];
                    _dmrfModelArr = [DealerMyReportFormsModel objectArrayWithKeyValuesArray:array];
                }
                [self doneWithView:refreshView];
            } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"rest"]) {
                //                    [_theDataMutArr removeAllObjects];
                NSDictionary *dictionary = [responseObject objectForKey:@"data"];
                _myReportFormsModel = [MyReportFormsModel objectWithKeyValues:dictionary];
                } else {
                    NSArray *array = [responseObject objectForKey:@"data"];
                    _dmrfModelArr = [DealerMyReportFormsModel objectArrayWithKeyValuesArray:array];
                }
                [self doneWithView:refreshView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

@end
