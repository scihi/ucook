//
//  MyCouponViewController.h
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MyCouponViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *unusedButton;
@property (strong, nonatomic) IBOutlet UIButton *usedButton;
@property (strong, nonatomic) IBOutlet UIButton *expiryButton;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickUnusedBtn:(id)sender;
- (IBAction)clickUsedBtn:(id)sender;
- (IBAction)clickExpiryBtn:(id)sender;

@end
