//
//  MyReportFormsViewController.h
//  UCook
//
//  Created by scihi on 14-8-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MyReportFormsViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@end
