//
//  MyMessageWebViewController.h
//  UCook
//
//  Created by scihi on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MyMessageWebViewController : GeneralWithBackBtnViewController

@property (copy, nonatomic) NSString *urlString;

@property (copy, nonatomic) NSString *titleString;

@end
