//
//  SettingViewController.h
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "Harpy.h"

@interface SettingViewController : GeneralWithBackBtnViewController <UIAlertViewDelegate, HarpyDelegate>
- (IBAction)clickLogoutBtn:(id)sender;
- (IBAction)clickClearCacheBtn:(id)sender;
- (IBAction)clickGoodsImgQualityBtn:(id)sender;
- (IBAction)clickCheckUpdateBtn:(id)sender;

@end
