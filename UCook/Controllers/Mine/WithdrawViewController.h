//
//  WithdrawViewController.h
//  UCook
//
//  Created by huangrun on 14-8-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface WithdrawViewController : GeneralWithBackBtnViewController

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIButton *bankSelectBtn;
@property (strong, nonatomic) IBOutlet UIButton *provinceSelectBtn;
@property (strong, nonatomic) IBOutlet UIButton *citySelectBtn;
- (IBAction)clickBankSelectBtn:(id)sender;
- (IBAction)clickProvinceSelectBtn:(id)sender;
- (IBAction)clickCitySelectBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *cardNumTF;
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *iDCardNumTF;
@property (strong, nonatomic) IBOutlet UIButton *bindButton;
- (IBAction)clickBindBtn:(id)sender;

@end