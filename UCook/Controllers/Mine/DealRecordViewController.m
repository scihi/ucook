//
//  DealRecordViewController.m
//  UCook
//
//  Created by huangrun on 14-9-14.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "DealRecordViewController.h"
#import "MJRefresh.h"
#import "DealRecordModel.h"
#import "CGRectAdditionals.h"

static NSString *CellIdentifier = @"DealRecordCell";

@interface DealRecordViewController () {
    UITableView *_theTableView;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    UIView *_noDataView;//无数据时的展示背景
    
    NSMutableArray *_dealRecordMutArr;
}

@end

@implementation DealRecordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"我的竞拍记录";
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
//    //    //隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//    //隐藏navigationController
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.frame = CGRectMake(0, 0, 320, IS_IPHONE_5?568:568-88);
    self.view.backgroundColor = [UIColor whiteColor];
    //设置应用程序的状态栏到指定的方向
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
    
    //view旋转
    [self.view setTransform:CGAffineTransformMakeRotation(M_PI/2)];

    _theBackButton = [[UIButton alloc] initWithFrame:[self BackButtonRect]];
    [_theBackButton setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [_theBackButton setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    [_theBackButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_back_dealer.png"]];
    navLeftIV.frame = CGRectMake(10, 5, 30, 24);
    [_theBackButton addSubview:navLeftIV];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:_theBackButton];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(IS_IPHONE_5?220:220 - 48, 10, 150, 20)];
    titleLabel.text = @"我的竞拍记录";
    titleLabel.textColor = [UIColor whiteColor];
    
    UIToolbar *toobar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.height, 44)];
    [toobar setBackgroundImage:[UIImage imageNamed:@"public_nav_landscape"] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    
    [toobar addSubview:titleLabel];
    
    [toobar setItems:@[backItem,spacer]];
    [self.view addSubview:toobar];
    
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, BOUNDS.size.height, 320) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerNib:[UINib nibWithNibName:@"DealRecordCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    [self.view addSubview:_theTableView];
    
    [self getTheInitData];
    _pageNumber = 1;
    [self addFooter];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dealRecordMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    DealRecordModel *dealRecordModel = [_dealRecordMutArr objectAtIndex:indexPath.row];
    //时间
    UILabel *timeLabel1 = (UILabel *)[cell viewWithTag:101];
    timeLabel1.text = dealRecordModel.createTime;
    
    //名称/交易号
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:103];
    nameLabel.text = dealRecordModel.type;
    numberLabel.text = dealRecordModel.entity;
    numberLabel.numberOfLines = 2;
//    numberLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    //对方
    UILabel *receiverLabel = (UILabel *)[cell viewWithTag:104];
    receiverLabel.text = dealRecordModel.receiver?dealRecordModel.receiver:@"无";
    
    //支付方式
    UILabel *payMethonLabel = (UILabel *)[cell viewWithTag:105];
    payMethonLabel.text = dealRecordModel.pay?dealRecordModel.pay:@"无";
    
    //余额
    UILabel *balanceLabel = (UILabel *)[cell viewWithTag:106];
    balanceLabel.text = dealRecordModel.ucookb;
    
    //状态
    UILabel *statusLabel = (UILabel *)[cell viewWithTag:107];
    statusLabel.text = dealRecordModel.status;
    
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (IS_IPHONE_5) {
    return @" 创建时间              名称/交易号                     对方          支付方式          余额          状态";
    } else {
    return @" 创建时间       名称/交易号        对方         支付方式          余额         状态";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"transaction",@"uid":[AccountManager getUid],@"page":@(_pageNumber)};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _dealRecordMutArr  = [NSMutableArray arrayWithArray:[DealRecordModel objectArrayWithKeyValuesArray:array]];
            [_theTableView reloadData];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
    //UIStatusBarStyleDefault = 0 黑色文字，浅色背景时使用
    //UIStatusBarStyleLightContent = 1 白色文字，深色背景时使用
}

- (BOOL)prefersStatusBarHidden
{
    return YES; //返回NO表示要显示，返回YES将hiden
}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"transaction",@"uid":[AccountManager getUid],@"page":@(_pageNumber)};
        //        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [self stopRequest];
            if (RIGHT) {
                //            [_theDataMutArr removeAllObjects];
                [_noDataView removeFromSuperview];
                NSArray *array = [responseObject objectForKey:@"data"];
                [_dealRecordMutArr  addObjectsFromArray:[DealRecordModel objectArrayWithKeyValuesArray:array]];
                
                [self doneWithView:refreshView];
            } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                
                _theTableView.backgroundColor = [UIColor clearColor];
                [self configTheNoDataViewForTheTableView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}


@end
