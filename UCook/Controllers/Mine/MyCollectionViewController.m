//
//  MyCollectionViewController.m
//  UCook
//
//  Created by scihi on 14-8-12.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MyCollectionViewController.h"
#import "MyCollectModel.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "CGRectAdditionals.h"
#import "GoodsDetailViewController.h"

#import "ShopInfoViewController.h"

//cell的类型 商品和店铺
typedef NS_ENUM(NSInteger, CellTypes) {
    CellTypes_goods,
    CellTypes_shop
};

@interface MyCollectionViewController () {
    MyCollectModel *_myCollectModel;
    NSMutableArray *_myCollectModelMutArr;
    UIView *_noDataView;//无数据时的展示背景
    
    CellTypes _cellTypes;
}

@end

@implementation MyCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"我的收藏";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _goodsCollectBtn.selected = YES;
    _theTableView.tableFooterView = [[UIView alloc]init];//遮住多余的分割线

    _cellTypes = CellTypes_goods;
    [self getTheInitData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickGoodsCollectBtn:(id)sender {
    _cellTypes = CellTypes_goods;
    _goodsCollectBtn.selected = YES;
    _shopCollectBtn.selected = NO;
    [self getTheInitData];
}

- (IBAction)clickShopCollectBtn:(id)sender {
    _cellTypes = CellTypes_shop;

    _shopCollectBtn.selected = YES;
    _goodsCollectBtn.selected = NO;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"collect",@"action": @"list",@"uid":[AccountManager getUid],@"uid": [AccountManager getUid],@"type": @2};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
            [_myCollectModelMutArr removeAllObjects];
            _myCollectModelMutArr = [NSMutableArray arrayWithArray:[MyCollectModel objectArrayWithKeyValuesArray:[responseObject objectForKey:@"data"]]];
            [_theTableView reloadData];
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
            [_myCollectModelMutArr removeAllObjects];
            [_theTableView reloadData];
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor clearColor];
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _myCollectModelMutArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier;
    if (_cellTypes == CellTypes_goods) {
        CellIdentifier = @"MyCollectionCell";
                [_theTableView registerNib:[UINib nibWithNibName:@"MyCollectionCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        
        UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        MyCollectModel *myCollectModel = [_myCollectModelMutArr objectAtIndex:indexPath.row];
        
        UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
        NSString *imgUrlStr = myCollectModel.image;
        [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
        titleLabel.text = myCollectModel.name;
        UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
        NSString *specificationString = myCollectModel.spec;
        specificationLabel.text = [NSString stringWithFormat:@"规格:%@",specificationString];
        UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];//单位
        NSString *unitString = myCollectModel.unit;
        unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitString];
        
        UILabel *introLabel = (UILabel *)[cell viewWithTag:105];
        introLabel.text = myCollectModel.introduction;
        introLabel.textColor = [UIColor lightGrayColor];
        
        UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:106];
        NSString *currentPriceStr = myCollectModel.discountPrice;
        currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
        StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:107];//原价
        originalPriceLabel.strikeThroughEnabled = YES;
        NSString *originalPriceStr = myCollectModel.price;
        originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",originalPriceStr];
        
        UILabel *collectNumLabel = (UILabel *)[cell viewWithTag:108];
        NSString *collectNumStr = myCollectModel.hot;
        collectNumLabel.text = [NSString stringWithFormat:@"收藏人气:%@",collectNumStr];

    return cell;
        
    } else {
        CellIdentifier = @"MyCollectionOfShopCell";
                [_theTableView registerNib:[UINib nibWithNibName:@"MyCollectionOfShopCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        
        UITableViewCell *cell = [_theTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        MyCollectModel *myCollectModel = [_myCollectModelMutArr objectAtIndex:indexPath.row];
        
        UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
        NSString *imgUrlStr = myCollectModel.image;
        [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
        titleLabel.text = myCollectModel.name;
        
        //地址
        UILabel *addressLabel = (UILabel *)[cell viewWithTag:104];
        addressLabel.text = myCollectModel.address;
        
        //介绍
        UILabel *introductionLabel = (UILabel *)[cell viewWithTag:105];
        introductionLabel.text = myCollectModel.introduction;
        
    return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_cellTypes == CellTypes_goods) {
    GoodsDetailViewController *goodsDetailVC =[[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    //item系统关键字冲突 用下面方法解决
//    NSInteger goodsIdInteger = [[_myCollectModelMutArr objectAtIndex:indexPath.row]item];
    NSInteger goodsIdInteger = [(MyCollectModel *)([_myCollectModelMutArr objectAtIndex:indexPath.row])item];
    goodsDetailVC.goodsIdStr = [NSString stringWithFormat:@"%d",goodsIdInteger];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
        
    } else {
        ShopInfoViewController *shopInfoVC =[[ShopInfoViewController alloc]initWithNibName:@"ShopInfoViewController" bundle:nil];
            NSInteger storeIdInteger = [(MyCollectModel *)([_myCollectModelMutArr objectAtIndex:indexPath.row])item];
        shopInfoVC.storeIdStr = [NSString stringWithFormat:@"%d",storeIdInteger];
        [self.navigationController pushViewController:shopInfoVC animated:YES];
    }
}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"collect",@"action": @"list",@"uid":[AccountManager getUid],@"uid": [AccountManager getUid],@"type": @1};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
           _myCollectModelMutArr = [NSMutableArray arrayWithArray:[MyCollectModel objectArrayWithKeyValuesArray:[responseObject objectForKey:@"data"]]];
            [_theTableView reloadData];
        } else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                [_myCollectModelMutArr removeAllObjects];
                [_theTableView reloadData];
                [_noDataView removeFromSuperview];
                _theTableView.backgroundColor = [UIColor clearColor];
                //设置无数据时的提示视图
                [self configTheNoDataViewForTheTableView];

        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}

@end
