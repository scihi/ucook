//
//  WithdrawViewController.m
//  UCook
//
//  Created by huangrun on 14-8-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "WithdrawViewController.h"
#import "SGActionView.h"
#import "SelectAddressModel.h"
#import "NSStringAdditions.h"

typedef void(^SGMenuActionHandler)(NSInteger index);

@interface WithdrawViewController () {
    
    NSArray *_bankInfoArr;//弹出列表用
    SelectAddressModel *_selectAddressModel;
    NSArray *_selectAddressModelArr1;//省份数组
    NSArray *_selectAddressModelArr2;//城市数组
    NSString *_areaIdString;
    NSString *_cityIdStr;
    NSString *_cityNameStr;
}

@end

@implementation WithdrawViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"绑定银行卡";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _areaIdString = @"";
    _bankSelectBtn.layer.cornerRadius = 6.0;
    _provinceSelectBtn.layer.cornerRadius = 6.0;
    _citySelectBtn.layer.cornerRadius = 6.0;
    _bindButton.layer.cornerRadius = 6.0;
    
    [self getTheBankInfoData];
    [self getSelectAddressListData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheBankInfoData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"bankList"};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _bankInfoArr = [[responseObject objectForKey:@"data"]copy];
            NSMutableArray *bankNameMutArr = [NSMutableArray arrayWithCapacity:10];
            for (NSDictionary *dictionary in _bankInfoArr) {
                [bankNameMutArr addObject:[dictionary objectForKey:@"bankName"]];
            }
            
            [_bankSelectBtn setTitle:[bankNameMutArr objectAtIndex:0] forState:UIControlStateNormal];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)getSelectAddressListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"areaList",@"areaId":_areaIdString};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _selectAddressModelArr1 = [SelectAddressModel objectArrayWithKeyValuesArray:array];
            
            NSMutableArray *provinceNameMutArr = [NSMutableArray arrayWithCapacity:10];
            for (SelectAddressModel *selectAddressModel in _selectAddressModelArr1) {
                [provinceNameMutArr addObject:selectAddressModel.name];
            }
            [_provinceSelectBtn setTitle:[provinceNameMutArr objectAtIndex:0] forState:UIControlStateNormal];
            
            _areaIdString = [[_selectAddressModelArr1 objectAtIndex:0]id];
            [self getSelectAddressListDataOfCity];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)getSelectAddressListDataOfCity {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"areaList",@"areaId":_areaIdString};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSArray *array = [responseObject objectForKey:@"data"];
            _selectAddressModelArr2 = [SelectAddressModel objectArrayWithKeyValuesArray:array];
            
            NSMutableArray *cityNameMutArr = [NSMutableArray arrayWithCapacity:10];
            for (SelectAddressModel *selectAddressModel in _selectAddressModelArr2) {
                [cityNameMutArr addObject:selectAddressModel.name];
            }
            NSMutableArray *cityIdMutArr = [NSMutableArray arrayWithCapacity:10];
            for (SelectAddressModel *selectAddressModel in _selectAddressModelArr2) {
                [cityIdMutArr addObject:selectAddressModel.id];
            }
            
            [_citySelectBtn setTitle:[cityNameMutArr objectAtIndex:0] forState:UIControlStateNormal];
            _cityIdStr = [cityIdMutArr objectAtIndex:0];
            _cityNameStr = [cityNameMutArr objectAtIndex:0];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)clickBankSelectBtn:(id)sender {
    NSMutableArray *bankNameMutArr = [NSMutableArray arrayWithCapacity:10];
    for (NSDictionary *dictionary in _bankInfoArr) {
        [bankNameMutArr addObject:[dictionary objectForKey:@"bankName"]];
    }
    
    
    [SGActionView showSheetWithTitle:@"请选择银行"
                          itemTitles:bankNameMutArr
     
                       selectedIndex:0
                      selectedHandle:^(NSInteger index){
                          NSLog(@"选择了%d",index);
                          [_bankSelectBtn setTitle:[bankNameMutArr objectAtIndex:index] forState:UIControlStateNormal];
                      }];
    
}

- (IBAction)clickProvinceSelectBtn:(id)sender {
    NSMutableArray *provinceNameMutArr = [NSMutableArray arrayWithCapacity:10];
    for (SelectAddressModel *selectAddressModel in _selectAddressModelArr1) {
        [provinceNameMutArr addObject:selectAddressModel.name];
    }
    
    [SGActionView showSheetWithTitle:@"请选择省份"
                          itemTitles:provinceNameMutArr
     
                       selectedIndex:0
                      selectedHandle:^(NSInteger index){
                          NSLog(@"选择了%d",index);
                          _areaIdString = [[_selectAddressModelArr1 objectAtIndex:index]id];
                          [self getSelectAddressListDataOfCity];
                          
                          [_provinceSelectBtn setTitle:[provinceNameMutArr objectAtIndex:index] forState:UIControlStateNormal];
                          
                      }];
    
}

- (IBAction)clickCitySelectBtn:(id)sender {
    
    NSMutableArray *provinceNameMutArr = [NSMutableArray arrayWithCapacity:10];
    for (SelectAddressModel *selectAddressModel in _selectAddressModelArr2) {
        [provinceNameMutArr addObject:selectAddressModel.name];
    }
    
    [SGActionView showSheetWithTitle:@"请选择城市"
                          itemTitles:provinceNameMutArr
     
                       selectedIndex:0
                      selectedHandle:^(NSInteger index){
                          NSLog(@"选择了%d",index);
                          _cityIdStr = [[_selectAddressModelArr2 objectAtIndex:index]id];
                          _cityNameStr = [[_selectAddressModelArr2 objectAtIndex:index]name];
                          
                          [_citySelectBtn setTitle:[provinceNameMutArr objectAtIndex:index] forState:UIControlStateNormal];
                      }];
    
}
- (IBAction)clickBindBtn:(id)sender {
    if ([NSString isEmptyOrWhitespace:_cardNumTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入银行卡卡号"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_nameTF.text]) {
        [GlobalSharedClass showAlertView:@"请输入开户姓名"];
        return;
    }
    if ([NSString isEmptyOrWhitespace:_iDCardNumTF.text]||_iDCardNumTF.text.length != 18) {
        [GlobalSharedClass showAlertView:@"请输入18位有效生份证号"];
        return;
    }
    
    [_cardNumTF resignFirstResponder];
    [_nameTF resignFirstResponder];
    [_iDCardNumTF resignFirstResponder];
    
    NSString *opiningNameStr = [NSString stringWithFormat:@"%@%@支行",_bankSelectBtn.titleLabel.text,_cityNameStr];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"common",@"action": @"account",@"uid":[AccountManager getUid],@"bankName": _bankSelectBtn.titleLabel.text,@"opiningName": opiningNameStr,@"receiverName": _nameTF.text,    @"accounts": _cardNumTF.text,@"bankDistrict": _cityIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            
            [GlobalSharedClass showAlertView:MESSAGE];
            
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


@end