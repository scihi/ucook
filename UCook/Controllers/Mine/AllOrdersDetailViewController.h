//
//  AllOrdersDetailViewController.h
//  UCook
//
//  Created by huangrun on 14-8-17.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "AlixLibService.h"

@interface AllOrdersDetailViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel *goodsTotalPriceLabel;

- (IBAction)clickPhoneBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
- (IBAction)clickLeftBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)clickRightBtn:(id)sender;

@property (copy, nonatomic) NSString *orderIdStr;

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调

@end
