//
//  SettingViewController.m
//  UCook
//
//  Created by scihi on 14-7-10.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SettingViewController.h"
#import "HomeViewController.h"
#import "HRNavViewController.h"
#import "TheTabBarViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"设置";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickLogoutBtn:(id)sender {
    [GlobalSharedClass showAlertView:@"确定注销登录?" delegate:self];
}

- (IBAction)clickClearCacheBtn:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定清空缓存?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 102;
    [alertView show];
}

- (IBAction)clickGoodsImgQualityBtn:(id)sender {
    
}

- (IBAction)clickCheckUpdateBtn:(id)sender {
    [self startRequestWithString:@"检查更新中..."];
    [Harpy sharedInstance].delegate = self;
    [Harpy sharedInstance].alertType = HarpyAlertTypeOption;
    [[Harpy sharedInstance]checkVersion];    
}

#pragma mark - harpyDelegate
- (void)harpyDidShowUpdateDialog {
    [self stopRequest];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if (alertView.tag != 102) {
            [self logout];
        } else {
            [self clearCache];
        }
    } else {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

- (void)logout {
    [AccountManager logout];
//    RegisterAndLoginViewController *ralvc = [[RegisterAndLoginViewController alloc]initWithNibName:@"RegisterAndLoginViewController" bundle:nil];
//    UINavigationController *navralvc = [[UINavigationController alloc]initWithRootViewController:ralvc];
//    ralvc.isFromMyCarStewardVC = YES;
//    [self presentViewController:navralvc animated:YES completion:nil];
    
//    HomeViewController *homeVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    TheTabBarViewController *theTabBarVC = [[TheTabBarViewController alloc]init];
    self.view.window.rootViewController = theTabBarVC;
}

- (void)clearCache {
    
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                   , ^{
                       NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
                       
                       NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
                       NSLog(@"files :%d",[files count]);
                       for (NSString *p in files) {
                           NSError *error;
                           NSString *path = [cachPath stringByAppendingPathComponent:p];
                           if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                               [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                           }
                       }
                       [self performSelectorOnMainThread:@selector(clearCacheSuccess) withObject:nil waitUntilDone:YES];});
    
}

-(void)clearCacheSuccess
{
//    NSLog(@"清理成功");
    [GlobalSharedClass showAlertView:@"清理成功"];
}

@end
