//
//  GridViewController.h
//  UCook
//
//  Created by scihi on 14-7-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "UIGridView.h"
#import "UIGridViewDelegate.h"

@interface GridViewController : GeneralWithBackBtnViewController <UIGridViewDelegate>

@property (nonatomic, copy) NSString *marketAreaIdStr;//市场区域id
@property (nonatomic, copy) NSString *titleString;

@end
