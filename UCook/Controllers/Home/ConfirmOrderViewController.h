//
//  ConfirmOrderViewController.h
//  UCook
//
//  Created by huangrun on 14-7-27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "ConfirmOrderModel.h"
#import "TakeDeliveryAddressViewController.h"
#import "LCTableViewPickerControl.h"

#import "AlixLibService.h"
#import "RechargeViewController.h"

@class ConfirmOrderViewController;

@protocol clearTheShoppingCartDelegate <NSObject>

- (void)clearTheShoppingCart:(ConfirmOrderViewController *)confirmOrderVC;

@end

@interface ConfirmOrderViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, TakeDeliveryAddressVCDelegate, LCItemPickerDelegate>

@property (nonatomic, strong) ConfirmOrderModel *confirmOrderModel;

@property (nonatomic, copy) NSArray *payMethodModelArr;
@property (strong, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
- (IBAction)clickConfirmBtn:(id)sender;

@property (nonatomic,assign) SEL result;//这里声明为属性方便在于外部传入。支付宝支付接口
-(void)paymentResult:(NSString *)result;//支付宝回调
@property (strong, nonatomic) IBOutlet UIImageView *buttomIV;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UIImageView *totalPriceBgView;
@property (strong, nonatomic) IBOutlet UILabel *fixedTotalPriceLabel;

@property (assign, nonatomic) id <clearTheShoppingCartDelegate> delegate;

@end
