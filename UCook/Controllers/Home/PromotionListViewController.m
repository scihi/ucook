//
//  PromotionListViewController.m
//  UCook
//
//  Created by scihi on 14-8-22.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "PromotionListViewController.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "PromotionListModel.h"
#import "GoodsDetailViewController.h"

static NSString *CellIdentifier = @"PromotionListCell";

@interface PromotionListViewController () {
    UITableView *_theTableView;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
    
    PromotionListModel *_promotionListModel;
    
    NSMutableArray *_listMutArr;//创建列表数组，因为model的列表数组不可变，不好处理
}

@end

@implementation PromotionListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"活动产品";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _theTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [self.view addSubview:_theTableView];
    
    [_theTableView registerNib:[UINib nibWithNibName:@"PromotionListCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    
    [self getTheInitData];
    
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark - tableView dataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _promotionListModel.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    _theTableView.hidden = NO;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *currentDictionary = [_listMutArr objectAtIndex:indexPath.row];
    
    UIImageView *cellIV = (UIImageView *)[cell viewWithTag:101];
    NSString *imgUrlStr = [currentDictionary objectForKey:@"image"];
    [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    titleLabel.text = [currentDictionary objectForKey:@"name"];
    UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
    NSString *specificationString = [currentDictionary objectForKey:@"spec"];
    specificationLabel.text = [NSString stringWithFormat:@"规格:%@",specificationString];
    UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];//单位
    if (![specificationString isEqualToString:@""]) {
    NSString *unitString = [specificationString substringFromIndex:specificationString.length - 1];;
    unitLabel.text = [NSString stringWithFormat:@"单位:%@",unitString];
    }
    
    UILabel *descriptionLabel = (UILabel *)[cell viewWithTag:105];//产品介绍
    NSString *descriptionString = [currentDictionary objectForKey:@"desc"];
    if (![descriptionString isEqualToString:@""]) {
            descriptionLabel.text = [NSString stringWithFormat:@"产品介绍：%@",descriptionString];
    }
    
    UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:106];//现价
    NSString *currentPriceStr = [currentDictionary objectForKey:@"discountPrice"];
    currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:107];//原价
    originalPriceLabel.strikeThroughEnabled = YES;
    NSString *originalPriceStr = [currentDictionary objectForKey:@"price"];
    if (![originalPriceStr isKindOfClass:[NSNull class]])
    originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",originalPriceStr];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[_listMutArr objectAtIndex:indexPath.row]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (void)getTheInitData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dictionary = @{@"key": APP_KEY, @"controller": @"product",@"action": @"promotionPro",@"promotionId": _promotionIdStr,@"page": @(_pageNumber)};
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            _promotionListModel = [PromotionListModel objectWithKeyValues:dictionary];
            _listMutArr = [NSMutableArray arrayWithArray:_promotionListModel.list];
            [_theTableView reloadData];
            
            } else {
                
            [GlobalSharedClass showAlertView:MESSAGE];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;
    
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        NSDictionary *dictionary = @{@"key": APP_KEY, @"controller": @"product",@"action": @"promotionPro",@"promotionId": _promotionIdStr,@"page": @(_pageNumber)};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:dictionary];
//        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            [self stopRequest];
            if (RIGHT) {
                [_listMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[[responseObject objectForKey:@"data"]objectForKey:@"list" ]mutableCopy]]];
                [self doneWithView:refreshView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}


@end
