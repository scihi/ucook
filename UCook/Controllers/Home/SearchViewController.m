//
//  SearchViewController.m
//  UCook
//
//  Created by huangrun on 14-7-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "SearchViewController.h"
#import "PrettySearchBar.h"
#import "MoreViewController.h"

#import "ShopViewController.h"

@interface SearchViewController () {
    PrettySearchBar *_searchBar;
    
    NSArray* textArray;
    MerchantTopBar *_theTopBar;
    SharePopupTableView *_theShareView1;
    BOOL _isTapped;//记录是否触碰,如果在顶部列表展开时触碰视图内容则做return处理，而不需要跳转 huangrun
    NSString *_typeStr;
    
    NSInteger _pageNumber;
}

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_searchBar resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64)];
    bgIV.image = [UIImage imageNamed:@"public_bg.png"];
    [self.view addSubview:bgIV];//尤其注意背景图片的坐标 如果设置不恰当 会使底部留白 会误认为tabBar没隐藏等 huangrun
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_ico.png"]];
    navRightIV.frame = CGRectMake(15, 7, 20, 20);
    [navRightBtn addSubview:navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;

    
    _searchBar = [[PrettySearchBar alloc]initWithFrame:CGRectMake(0, 0, 175, 44)];
    _searchBar.delegate = self;
//    searchBar.showsCancelButton = YES;
//    [searchBar sizeToFit];
    [_searchBar setTintColor:[UIColor blackColor]];
    UIView *barWrapper = [[UIView alloc]initWithFrame:_searchBar.bounds];
    _searchBar.searchBarStyle=UISearchBarStyleMinimal;
    
    _searchBar.placeholder = @"请输入关键字";
    
    [_searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"input"] forState:UIControlStateNormal];
    [_searchBar setSearchTextPositionAdjustment:UIOffsetMake(25, 0)];
    //    [[UISearchBar appearance] setSearchFieldBackgroundPositionAdjustment:UIOffsetMake(30, 0)];
        [_searchBar setImage:[UIImage imageNamed:@"search_replace"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];//去掉searchBar前面的搜索图标 方法二 huangrun
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];//去掉searchBar前面的搜索图标 方法一 huangrun
    //    self.tintColor = [UIColor whiteColor];
    
    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [testButton setTitle:@"test" forState:UIControlStateNormal];
    //    testButton.backgroundColor = [UIColor whiteColor];
    testButton.frame = CGRectMake(10, 5, 40, 30);
//    [searchBar addSubview:testButton];

    [barWrapper addSubview:_searchBar];

    self.navigationItem.titleView = barWrapper;
    
    UIImageView *indicateIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_select_small.9.png"]];
    indicateIV.frame = CGRectMake(11, 13, 40, 20);
    [_searchBar addSubview:indicateIV];
    
    [_searchBar becomeFirstResponder];
    
    _typeStr = @"0";
    
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height - NAVIGATION_PANEL_HEIGHT);
    //顶部搜索区域
    _theTopBar = [[MerchantTopBar alloc] initWithFrame:CGRectMake(15, 3, 50, 34) itemsCount:1];
    _theTopBar.hidden = NO;
    _theTopBar.delegate = self;
    [_searchBar addSubview:_theTopBar];
    
    UITapGestureRecognizer* myTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//    [self.view addGestureRecognizer:myTap];
    myTap.delegate = self;
    myTap.cancelsTouchesInView = NO;
    
    UIPanGestureRecognizer *mytap2 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
//    [self.view addGestureRecognizer:mytap2];
    mytap2.delegate = self;
    mytap2.cancelsTouchesInView = NO;
    
    
    _theShareView1 = [[SharePopupTableView alloc] initWithFrame:CGRectZero];
    _theShareView1.theCustomDelegate = self;
    _theShareView1.hidden = YES;
    
    _pageNumber = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickNavRightBtn:(id)sender {
    if ([_typeStr isEqualToString: @"0"]) {
        MoreViewController *moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        moreVC.searchNameStr = _searchBar.text;
        [self.navigationController pushViewController:moreVC animated:YES];
    } else {
//        [GlobalSharedClass showAlertView:@"木有参考,未开发"];
        ShopViewController *shopVC = [[ShopViewController alloc]init];
        shopVC.searchNameStr = _searchBar.text;
        [self.navigationController pushViewController:shopVC animated:YES];
    }
}

- (void)merchantSearchTopViewButtonPressed:(id)sender index:(NSInteger)buttonIndex {
    _theShareView1.alpha = 1.0;
    if(!_theTopBar.hasButtonSelected)
    {
        //        [UIView animateWithDuration:0.4
        //                              delay:0.0
        //                            options: UIViewAnimationOptionCurveEaseInOut
        //                         animations:^{
        //
        //                             _theShareView.frame = CGRectMake(_theShareView.frame.origin.x, _theShareView.frame.origin.y, _theShareView.frame.size.width, 0);
        //
        //                         }
        //                         completion:^(BOOL finished){
        //
        //                             for (UIView *view in self.view.subviews) {
        //                                 if ([view isKindOfClass:[_theShareView class]]) {
        //                                     view.hidden = YES;
        //                                 }
        //                             }
        //
        //                         }];//取消点击button的关闭动画 因为我觉得不需要这个动画 huangrun
        _theShareView1.hidden = YES;
        
    } else {
        
        CGRect rect = [self BottomViewRect];
        CGFloat ftYOffset = rect.origin.y - 40;
        CGFloat ftWidth = 100;
        
        if (buttonIndex == 0) {
            textArray = @[@"产品",@"店铺"];
            _theShareView1.hidden = NO;
            _theShareView1.theTextArray = [textArray mutableCopy];
            
            ftYOffset += 32;
            //            for (UIView *view in self.view.subviews) {
            //                if ([view isKindOfClass:[_theShareView1 class]]) {
            //                    //                [view removeFromSuperview];
            //
            //                }
            //            }
            
            
            
        } else if (buttonIndex == 1) {
            
            
        } else {
            
            
        }
        //        _theShareView = [[SharePopupTableView alloc] initWithFrame:CGRectZero];
        
        if (buttonIndex == 0) {
            _theShareView1.frame = CGRectMake(80, ftYOffset, ftWidth, 0);
            _theShareView1.rowHeight = 40;
            _theShareView1.layer.masksToBounds = YES;
            _theShareView1.layer.borderColor = [[UIColor grayColor] CGColor];
            _theShareView1.layer.borderWidth = 1.0f;
            _theShareView1.opaque = YES;
            
            _theShareView1.theCustomDelegate = self;
            [_theShareView1 reloadData];
            [self.view addSubview:_theShareView1];
            
        } else if (buttonIndex == 1) {
            
        } else if (buttonIndex == 2) {
        }
        
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             _theShareView1.frame = CGRectMake(80, ftYOffset, ftWidth, 80);
                         }
         
         
                         completion:^(BOOL finished){
                             
                             
                         }];
        
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)handleSingleTap:(UITapGestureRecognizer *)sender{
    //    CGPoint gesturePoint = [sender locationInView:self.view];
    //    NSLog(@"handleSingleTap!gesturePoint:%f,y:%f",gesturePoint.x,gesturePoint.y);
    //
    
    if (_theShareView1.hidden == NO) {
        _isTapped = YES;
    }
    
    [_theTopBar restoreToInitStateForHide];
    _theShareView1.hidden = YES;
    
    
}

- (void)handlePan:(UIPanGestureRecognizer *)sender{
    [_theTopBar restoreToInitStateForHide];
    _theShareView1.hidden = YES;
}


- (void)didSelectRowAtIndexPath:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    [_theTopBar restoreToInitStateForHide];
    [UIView beginAnimations:@"hide view" context:nil];
    [UIView setAnimationDuration:0.4f];
    [UIView setAnimationDidStopSelector:@selector(hideView)];
    [UIView setAnimationDelegate:self];
    
    _theShareView1.alpha = 0.0;
    
    [UIView commitAnimations];//添加这个动画 在选择过后很有必要有这种效果 huangrun
    
    if (_theShareView1.hidden == NO) {
        _typeStr = [NSString stringWithFormat:@"%d",indexPath.row];
    }
}

- (void)hideView {
    _theShareView1.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_theTopBar restoreToInitStateForHide];
    _theShareView1.hidden = YES;
    [_searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if ([_typeStr isEqualToString: @"0"]) {
        MoreViewController *moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        moreVC.searchNameStr = searchBar.text;
        [self.navigationController pushViewController:moreVC animated:YES];
    } else {
//        [GlobalSharedClass showAlertView:@"木有参考,未开发"];
        ShopViewController *shopVC = [[ShopViewController alloc]init];
        shopVC.searchNameStr = _searchBar.text;
        [self.navigationController pushViewController:shopVC animated:YES];
    }

}

@end
