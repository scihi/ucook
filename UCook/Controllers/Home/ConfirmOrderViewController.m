//
//  ConfirmOrderViewController.m
//  UCook
//
//  Created by huangrun on 14-7-27.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ConfirmOrderViewController.h"
#import "PayMethodModel.h"
#import "TouchTableView.h"

#import "AddOrderModel.h"
#import "PartnerConfig.h"
#import "DataSigner.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "AlixPayOrder.h"

#import "AllOrdersDetailViewController.h"

#define UD_COUPONINDEX @"couponIndex"

@interface ConfirmOrderViewController () {
    NSInteger _oldRow;
    UITableViewCell *_cell;
    NSString *_addressIdStr;
    NSString *_payMethodIdStr;
    NSMutableArray *_goodsInfoMutArr;
    NSMutableArray *_couponNameMutArr;//其元素是名字数组
    NSIndexPath *_theIndexPath;
    NSMutableDictionary *_couponNameDic;//为了防止拖动tableview时数据重置
    
    AddOrderModel *_addOrderModel;
    UIAlertView *_alertView;
    
    NSMutableArray *_couponMutArr;//用于装载请求回来的优惠券数组
    NSMutableArray *_selectedCouponMutArr;//最终选择的优惠券数组
    NSMutableArray *_wantCouponMutArr;
    
    NSMutableArray *_indexArray;
    NSMutableArray *_couponTempArray;
    
    NSUserDefaults *_userDefaults;
}

@end

@implementation ConfirmOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"确认订单";
    }
    return self;
}

#pragma mark - 为了解决支付宝WAP支付取消返回回来界面偏移问题
- (void)viewDidLayoutSubviews {
    
    if (self.view.frame.size.height != (BOUNDS.size.height - 64)) {
        
        self.tabBarController.tabBar.hidden = YES;
        _theTableView.frame =CGRectMake(_theTableView.frame.origin.x, _theTableView.frame.origin.y,_theTableView.frame.size.width, BOUNDS.size.height - 64 - 49 - 49);
        
        _totalPriceBgView.frame = CGRectMake(_totalPriceBgView.frame.origin.x, _theTableView.frame.size.height + 1, _totalPriceBgView.frame.size.width, _totalPriceBgView.frame.size.height);
        
        _fixedTotalPriceLabel.frame = CGRectMake(_fixedTotalPriceLabel.frame.origin.x, _theTableView.frame.size.height + 10, _fixedTotalPriceLabel.frame.size.width, _fixedTotalPriceLabel.frame.size.height);
        
        _totalPriceLabel.frame = CGRectMake(_totalPriceLabel.frame.origin.x, _theTableView.frame.size.height + 10, _totalPriceLabel.frame.size.width, _totalPriceLabel.frame.size.height);
        
        _buttomIV.frame = CGRectMake(_buttomIV.frame.origin.x, _theTableView.frame.size.height + 41, _buttomIV.frame.size.width, _buttomIV.frame.size.height);
        
        _confirmButton.frame = CGRectMake(_confirmButton.frame.origin.x, _theTableView.frame.size.height + 45, _confirmButton.frame.size.width, _confirmButton.frame.size.height);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _goodsInfoMutArr = [NSMutableArray arrayWithCapacity:10];
    _couponNameMutArr = [NSMutableArray arrayWithCapacity:10];
    _couponNameDic = [NSMutableDictionary dictionaryWithCapacity:10];
    
    _couponNameDic = [NSMutableDictionary dictionaryWithObject:@"不使用" forKey:@"couponName"];
    
    _couponMutArr = [NSMutableArray arrayWithArray:_confirmOrderModel.orders];
    
    _selectedCouponMutArr = [NSMutableArray arrayWithCapacity:10];
    
    _wantCouponMutArr = [NSMutableArray arrayWithCapacity:10];
    
    _indexArray = [NSMutableArray arrayWithCapacity:10];
    _couponTempArray = [NSMutableArray arrayWithCapacity:10];
    
    CGFloat wantFloatValue;
    for (NSDictionary *dictionary in _couponMutArr) {
        CGFloat floatValue = [[dictionary objectForKey:@"storeTotal"]floatValue];
        wantFloatValue += floatValue;
        _totalPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",wantFloatValue];
        
        for (NSDictionary *dictionary2 in [dictionary objectForKey:@"products"]) {
            NSDictionary *dictionary = @{@"id": [dictionary2 objectForKey:@"id"], @"number": [dictionary2 objectForKey:@"number"]};
            [_goodsInfoMutArr addObject:dictionary];
        }
        
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
        NSMutableArray *array2 = [NSMutableArray arrayWithCapacity:10];
        for (NSDictionary *dictionary3 in [dictionary objectForKey:@"coupons"]) {
            
            [array addObject:[dictionary3 objectForKey:@"name"]];
            [array2 addObject:dictionary3];
            
        }
        [array insertObject:@"未使用" atIndex:0];
        [_couponNameMutArr addObject:array];
        
        [array2 insertObject:@{@"name": @"未使用"} atIndex:0];
        [_selectedCouponMutArr addObject:array2];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _confirmOrderModel.orders.count + 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == _confirmOrderModel.orders.count + 1) {
        return _confirmOrderModel.payMothed.count + 1;
    } else {
        return 4 + [[[_confirmOrderModel.orders objectAtIndex:section - 1]objectForKey:@"products"]count] + ([[[_confirmOrderModel.orders objectAtIndex:section - 1]objectForKey:@"coupons"]count] == 0?+0:+1);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 60;
    } else {
        return 44;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        if (indexPath.section == 0) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        } else if (indexPath.section == _confirmOrderModel.orders.count + 1) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        } else {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
    }
    if (indexPath.section == 0) {
        _addressIdStr = _confirmOrderModel.recAddress.id;
        cell.textLabel.text = _confirmOrderModel.recAddress.contactName;
        cell.detailTextLabel.text = _confirmOrderModel.recAddress.address;
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if (indexPath.section == _confirmOrderModel.orders.count + 1) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"选择支付方式";
        } else {
            
            NSDictionary *dictionary = [_confirmOrderModel.payMothed objectAtIndex:indexPath.row - 1];
            cell.textLabel.text = [dictionary objectForKey:@"name"];
            //            _payMethodIdStr = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"id"]];
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *dictionary = [_confirmOrderModel.orders objectAtIndex:indexPath.section - 1];
        NSInteger fixedNumber = [[dictionary objectForKey:@"coupons"]count] == 0?4:5;
        
        if (fixedNumber == 5) {
            
            if (indexPath.row == 0) {
                cell.textLabel.text = [dictionary objectForKey:@"storeName"];
                NSString *detailString = [dictionary objectForKey:@"miniAmount"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"起送价:￥%@",detailString];
                cell.detailTextLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 1) {
                cell.textLabel.text = @"未满足店铺起送金额或第一次在店铺下单，需经销商审核";
                cell.textLabel.numberOfLines = 2;
                cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
                cell.textLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 2) {
                NSString *detailString = [dictionary objectForKey:@"storeTotal"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"合计:￥%@",detailString];
                cell.detailTextLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 3) {
                cell.textLabel.text = @"付款信息";
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 4) {
                cell.textLabel.text = @"优惠券";
                //待修改 huangrun
                //                cell.detailTextLabel.text = [_couponNameDic objectForKey:@"couponName"];
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
            } else {
                NSDictionary *goodsInfoDic = [[dictionary objectForKey:@"products"]objectAtIndex:indexPath.row - 1];
                NSString *string = [goodsInfoDic objectForKey:@"name"];
                cell.textLabel.text = string;
                cell.textLabel.textColor = [UIColor lightGrayColor];
                
                NSString *detailString = [NSString stringWithFormat:@"￥%@x%@%@",[goodsInfoDic objectForKey:@"price"], [goodsInfoDic objectForKey:@"number"], [goodsInfoDic objectForKey:@"unit"]];
                cell.detailTextLabel.text = detailString;
                cell.detailTextLabel.textColor = [UIColor lightGrayColor];
            }
            
        } else {
            
            if (indexPath.row == 0) {
                cell.textLabel.text = [dictionary objectForKey:@"storeName"];
                NSString *detailString = [dictionary objectForKey:@"miniAmount"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"起送价:￥%@",detailString];
                cell.detailTextLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 1) {
                cell.textLabel.text = @"未满足店铺起送金额或第一次在店铺下单，需经销商审核";
                cell.textLabel.numberOfLines = 2;
                cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
                cell.textLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 2) {
                NSString *detailString = [dictionary objectForKey:@"storeTotal"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"合计:￥%@",detailString];
                cell.detailTextLabel.textColor = kThemeColor;
            } else if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 3) {
                cell.textLabel.text = @"付款信息";
            } else {
                NSDictionary *goodsInfoDic = [[dictionary objectForKey:@"products"]objectAtIndex:indexPath.row - 1];
                NSString *string = [goodsInfoDic objectForKey:@"name"];
                cell.textLabel.text = string;
                cell.textLabel.textColor = [UIColor lightGrayColor];
                
                NSString *detailString = [NSString stringWithFormat:@"￥%@x%@%@",[goodsInfoDic objectForKey:@"price"], [goodsInfoDic objectForKey:@"number"], [goodsInfoDic objectForKey:@"unit"]];
                cell.detailTextLabel.text = detailString;
                cell.detailTextLabel.textColor = [UIColor lightGrayColor];
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        TakeDeliveryAddressViewController *takeDeliveryAddressVC = [[TakeDeliveryAddressViewController alloc]initWithNibName:@"TakeDeliveryAddressViewController" bundle:nil];
        takeDeliveryAddressVC.delegate = self;
        takeDeliveryAddressVC.vCTag = 101;
        [self.navigationController pushViewController:takeDeliveryAddressVC animated:YES];
    } else if (indexPath.section == _confirmOrderModel.orders.count + 1) {
        
        if (indexPath.row == 0) {
            return;
        }
        
        if (_oldRow >= 0)
        {
            // 清除上次一选中的钩子
            NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_oldRow inSection:indexPath.section];
            _cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
            [_cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        // 添加当前行的小勾子
        _cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        [_cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        // 标记当前选中的行数
        _oldRow = indexPath.row;
        
        _payMethodIdStr = [[_confirmOrderModel.payMothed objectAtIndex:indexPath.row - 1]objectForKey:@"id"];
        
    } else {
        NSDictionary *dictionary = [_confirmOrderModel.orders objectAtIndex:indexPath.section - 1];
        NSInteger fixedNumber = [[dictionary objectForKey:@"coupons"]count] == 0?4:5;
        
        if (fixedNumber == 5) {
            if (indexPath.row == fixedNumber + [[dictionary objectForKey:@"products"]count] - 4) {
                
                _theIndexPath = indexPath;
                
                for (UIView *view in self.view.subviews) {
                    if ([view isKindOfClass:[LCTableViewPickerControl class]]) {
                        [((LCTableViewPickerControl *)view) dismiss];
                    }
                }
                
                LCTableViewPickerControl *pickerView = [[LCTableViewPickerControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, kPickerControlWidth, kPickerControlAgeHeight) title:@"请选择优惠券" value:nil items:[_couponNameMutArr objectAtIndex:indexPath.section - 1 ]];
                [pickerView setDelegate:self];
                
                [self.view addSubview:pickerView];
                
                [pickerView show];
            }
        }
    }
}

- (void)addressDidSelect:(TakeDeliveryAddressViewController *)takeDeliveryAddressVC address:(NSString *)aAddress name:(NSString *)aName id:(NSString *)aId{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = aName;
    cell.detailTextLabel.text = aAddress;
    _addressIdStr = aId;
}

- (IBAction)clickConfirmBtn:(id)sender {
    if (_cell.accessoryType != UITableViewCellAccessoryCheckmark) {
        [GlobalSharedClass showAlertView:@"请选择支付方式"];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString *memoString = @"未满足店铺起送金额或第一次在店铺下单，需经销商审核";
    
    NSMutableArray *selectedCouponMutArr = [NSMutableArray arrayWithCapacity:10];
    for (int i = 0; i < _selectedCouponMutArr.count; i++) {
        NSUInteger index = [[_userDefaults objectForKey:[NSString stringWithFormat:@"%d",i]]integerValue];
        NSDictionary *dictionary;
        if ([[[_selectedCouponMutArr objectAtIndex:i]objectAtIndex:index]objectForKey:@"id"] ) {
        dictionary = @{@"id": [[[_selectedCouponMutArr objectAtIndex:i]objectAtIndex:index]objectForKey:@"id"],@"number": @"1",@"storeId": [[[_selectedCouponMutArr objectAtIndex:i]objectAtIndex:index]objectForKey:@"storeId"]};
        }
 
        if (dictionary && index != 0) {
        [selectedCouponMutArr addObject:dictionary];
        }
    }

    
    NSString *goodsInfoMutArrJson = [self arrayToJson:_goodsInfoMutArr];
    
    NSDictionary *parameters;
    if (_selectedCouponMutArr.count == 0) {
        parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"create",@"uid":[AccountManager getUid],@"receiverAddressId": _addressIdStr,@"memo":memoString,@"paymentMethodId": _payMethodIdStr,@"productId": goodsInfoMutArrJson};
    } else {
        
        
            NSString *selectedCouponMutArrJsonStr = [self arrayToJson:selectedCouponMutArr];
            parameters = @{@"key": APP_KEY,@"controller": @"order",@"action": @"create",@"uid":[AccountManager getUid],@"receiverAddressId": _addressIdStr,@"memo":memoString,@"paymentMethodId": _payMethodIdStr,@"productId": goodsInfoMutArrJson,@"coupons": selectedCouponMutArrJsonStr};
        
    }
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [responseObject objectForKey:@"data"];
            _addOrderModel = [AddOrderModel objectWithKeyValues:dictionary];
            
            if (_addOrderModel.isPay == 1) {
                
                if ([_addOrderModel.paymentMethodId isEqualToString:@"3"]) {
                    //优厨币支付
                    NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",[[_addOrderModel.orderIds firstObject]objectForKey:@"orderId"], [[[_confirmOrderModel.orders firstObject]objectForKey:@"storeTotal"]floatValue]];
                    _alertView = [[UIAlertView alloc]initWithTitle:@"优厨币支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    _alertView.tag = 101;
                    [_alertView show];
                    
                    
                } else if ([_addOrderModel.paymentMethodId isEqualToString:@"1"]) {
                    //支付宝支付
                    NSString *message = [NSString stringWithFormat:@"1.订单号为:%@\n 2.订单总额:￥%.2f\n 3.确认支付?",[[_addOrderModel.orderIds firstObject]objectForKey:@"orderId"], [[[_confirmOrderModel.orders firstObject]objectForKey:@"storeTotal"]floatValue]];
                    
                    _alertView = [[UIAlertView alloc]initWithTitle:@"支付宝支付" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    _alertView.tag = 102;
                    [_alertView show];
                } else if ([_addOrderModel.paymentMethodId isEqualToString:@"2"]) {
                    [GlobalSharedClass showAlertView:MESSAGE];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            } else {
                [GlobalSharedClass showAlertView:memoString];
//                [GlobalSharedClass showAlertView:MESSAGE];
                [self.delegate clearTheShoppingCart:self];
//                [self.navigationController popViewControllerAnimated:YES];
                
                AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
                allOrdersDetailVC.orderIdStr = [_addOrderModel.info objectForKey:@"orderId"];
                [self.navigationController pushViewController:allOrdersDetailVC animated:YES];
                
            }
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

#pragma mark - LCTableViewPickerDelegate

- (void)selectControl:(LCTableViewPickerControl*)view didSelectWithItem:(id)item indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [_theTableView cellForRowAtIndexPath:_theIndexPath];
    
    
    NSIndexPath *cellIndexPath = [_theTableView indexPathForCell:cell];
    NSIndexPath *wantIndexPath = [NSIndexPath indexPathForRow:cellIndexPath.row + 2 inSection:cellIndexPath.section];
    UITableViewCell *totalPriceCell = [_theTableView cellForRowAtIndexPath:wantIndexPath];
    NSString *totalPriceStr = [[[_selectedCouponMutArr objectAtIndex:_theIndexPath.section - 1] objectAtIndex:indexPath.row] objectForKey:@"money"];

    NSDictionary *dictionary = [_confirmOrderModel.orders objectAtIndex:_theIndexPath.section - 1];
    CGFloat originalPriceFloat = [[dictionary objectForKey:@"storeTotal"]floatValue];
    CGFloat wantFloat = originalPriceFloat - [totalPriceStr floatValue];

    if (indexPath.row != 0) {

        if (wantFloat < 0) {
            [GlobalSharedClass showAlertView:@"优惠券金额超过总价，不能使用，请联系经销商"];
            [self dismissPickerControl:view];
            return;
        }
        
        totalPriceCell.detailTextLabel.text = [NSString stringWithFormat:@"合计:￥%f",wantFloat];
        
        CGFloat wantFloatValue;
        for (NSDictionary *dictionary in _couponMutArr) {
            CGFloat floatValue = [[dictionary objectForKey:@"storeTotal"]floatValue];
          wantFloatValue += floatValue;
        
        CGFloat wantPriceFloat = wantFloatValue - [totalPriceStr floatValue];
        _totalPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",wantPriceFloat];
        }
        
    } else {

        totalPriceCell.detailTextLabel.text = [NSString stringWithFormat:@"合计:￥%.2f",originalPriceFloat];
        _totalPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",originalPriceFloat];

    }
    


    
    
    
    cell.detailTextLabel.text = item;
    
    //    [_couponNameDic setObject:item forKey:@"couponName"];
    _userDefaults = [NSUserDefaults standardUserDefaults];
    for (int i = 0; i < _selectedCouponMutArr.count; i++) {
        [_userDefaults removeObjectForKey:[NSString stringWithFormat:@"%d",i]];
    }
    [_userDefaults setObject:@(indexPath.row) forKey:[NSString stringWithFormat:@"%d",_theIndexPath.section - 1]];
    
    
    [self dismissPickerControl:view];
}

- (void)selectControl:(LCTableViewPickerControl *)view didCancelWithItem:(id)item
{
    
    [self dismissPickerControl:view];
}

- (void)dismissPickerControl:(LCTableViewPickerControl*)view
{
    [view dismiss];
}

#pragma mark - 优厨币付款
- (void)payByUcookb {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"orderRefund",@"action": @"ucookbPay",@"uid":[AccountManager getUid], @"orderId": [[_addOrderModel.orderIds firstObject]objectForKey:@"orderId"],@"total_fee": @([[[_confirmOrderModel.orders firstObject]objectForKey:@"storeTotal"]integerValue])};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
            [self.delegate clearTheShoppingCart:self];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}


#pragma mark - 支付宝付款
- (void)pay {
    /*
	 *生成订单信息及签名
	 *由于demo的局限性，采用了将私钥放在本地签名的方法，商户可以根据自身情况选择签名方法(为安全起见，在条件允许的前提下，我们推荐从商户服务器获取完整的订单信息)
	 */
    
    NSString *appScheme = @"HRAlipay";
    NSString* orderInfo = [self getOrderInfo];
    NSString* signedStr = [self doRsa:orderInfo];
    
    NSLog(@"%@",signedStr);
    
    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                             orderInfo, signedStr, @"RSA"];
	
    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:_result target:self];
    
}

- (NSString *)getOrderInfo {
    /*
	 *点击获取prodcut实例并初始化订单信息
	 */
	Product *product = [[Product alloc]init];
    product.subject = [_addOrderModel.info objectForKey:@"subject"];
    product.body = [_addOrderModel.info objectForKey:@"body"];
    product.price = [[_addOrderModel.info objectForKey:@"total_fee"]floatValue];
    AlixPayOrder *order = [[AlixPayOrder alloc] init];
    //    order.partner = PartnerID;
    //    order.seller = SellerID;
    order.partner = [_addOrderModel.info objectForKey:@"partner"];
    order.seller = [_addOrderModel.info objectForKey:@"seller_id"];
    
    order.tradeNO = [_addOrderModel.info objectForKey:@"orderId"]; //订单ID（由商家自行制定）
	order.productName = product.subject; //商品标题
	order.productDescription = product.body; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",product.price]; //商品价格
	order.notifyURL = [_addOrderModel.info objectForKey:@"notify_url"]; //回调URL
	
	return [order description];
}

-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}

//wap回调函数
-(void)paymentResult:(NSString *)resultd
{
    //结果处理
#if ! __has_feature(objc_arc)
    AlixPayResult* result = [[[AlixPayResult alloc] initWithString:resultd] autorelease];
#else
    AlixPayResult* result = [[AlixPayResult alloc] initWithString:resultd];
#endif
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            NSString* key = AlipayPubKey;//签约帐户后获取到的支付宝公钥
			id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
			if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                //验证签名成功，交易结果无篡改
                [self.delegate clearTheShoppingCart:self];
			}
        }
        else
        {
            //交易失败
        }
    }
    else
    {
        //失败
        
    }
    
}

-(void)paymentResultDelegate:(NSString *)result
{
    NSLog(@"%@",result);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 101) {
        //优厨币支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            [self payByUcookb];
        }
    } else if (alertView.tag == 102) {
        //支付宝支付
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        } else {
            
            [self pay];
        }
    }
}


@end
