//
//  MoreViewController.m
//  UCook
//
//  Created by huangrun on 14-7-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "MoreViewController.h"
#import "ITTSegement.h"
#import "UIImageView+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "SearchViewController.h"
#import "HRCoreAnimationEffect.h"
#import "GoodsDetailViewController.h"
#import "MJRefresh.h"

static NSString *CellIdentifier = @"MoreCell";

@interface MoreViewController () {
    NSMutableArray *_theDataMutArr;
    NSString *_orderingString;//排序方式 默认 销量 还是价格
    NSString *_sortordString;//排序方式 升还是降
    
    NSInteger _selectedItemTag;//当前已选中的排序方式 默认 销量 价格
    UIView *_filtrateBgView;
    UIView *_filtrateView;
    UITextField *_maximumPriceTF;
    UITextField *_minimumPriceTF;
    NSInteger _minPriceInteger;//最低价格
    NSInteger _maxPriceInteger;//最高价格
    
    UIButton *_navRightBtn;
    
    //自动加载更多
    NSInteger _pageNumber; //页码
    MJRefreshFooterView *_footer;
}

@end

@implementation MoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_theTableView registerNib:[UINib nibWithNibName:@"MoreCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    
    _theDataMutArr = [NSMutableArray arrayWithCapacity:10];
    _selectedItemTag = 2013;
    [self configTheNavBar];
//    [_theTableView registerNib:[UINib nibWithNibName:@"MoreCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    NSArray *items = @[@"默认", @"价格", @"销量"];
    ITTSegement *segment = [[ITTSegement alloc] initWithItems:items];
    segment.frame = CGRectMake(0, 0, 320, 40);
    segment.selectedIndex = 0;
    [segment addTarget:self action:@selector(sgAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:segment];
    _orderingString = @"default";
    _sortordString = @"asc";//初始升
    _minPriceInteger = 0;
    _maxPriceInteger = 1000000;
    [self getData];
    [self addFooter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sgAction:(ITTSegement *)sg
{
    NSLog(@"%d %d", sg.selectedIndex, sg.currentState);
    
    if (sg.selectedIndex == 0 && _selectedItemTag != 2013) {
        _orderingString = @"default";
        _sortordString = @"";
        [self getData];
        
        _selectedItemTag = 2013;
      
    } else if (sg.selectedIndex == 1) {
        _orderingString = @"price";
        if (sg.currentState == 1) {
            _sortordString = @"asc";
        } else {
            _sortordString = @"desc";
        }
        [self getData];
        
        _selectedItemTag =2014;
    } else if (sg.selectedIndex == 2 && _selectedItemTag != 2015){
        _orderingString = @"sales";
        _sortordString = @"";
        [self getData];
        
        _selectedItemTag = 2015;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (_theDataArr.count >= 10) {
//        return 10;
//    } else {
        return _theDataMutArr.count;
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    if (_theDataMutArr.count != 0) {
        
            NSDictionary *currentRowDic = [_theDataMutArr objectAtIndex:indexPath.row];
            NSString *imgUrlStr = [currentRowDic objectForKey:@"image"];
            UIImageView *cellIV = (UIImageView*)[cell viewWithTag:101];
            [cellIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
            titleLabel.text = [currentRowDic objectForKey:@"name"];
            
            UILabel *specificationLabel = (UILabel *)[cell viewWithTag:103];//规格
            specificationLabel.text = [NSString stringWithFormat:@"规格:%@",[currentRowDic objectForKey:@"spec"]];
            
            UILabel *unitLabel = (UILabel *)[cell viewWithTag:104];
            unitLabel.text = [NSString stringWithFormat:@"单位:%@",[currentRowDic objectForKey:@"unit"]];
            
            UILabel *descriptionLabel = (UILabel*)[cell viewWithTag:105];
            descriptionLabel.text = [currentRowDic objectForKey:@"desc"];
            
            UILabel *currentPriceLabel = (UILabel *)[cell viewWithTag:106];
            currentPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"discountPrice"]];
            
            StrikeThroughLabel *originalPriceLabel = (StrikeThroughLabel *)[cell viewWithTag:107];
            originalPriceLabel.strikeThroughEnabled = YES;
            originalPriceLabel.text = [NSString stringWithFormat:@"￥%@",[currentRowDic objectForKey:@"price"]];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[_theDataMutArr objectAtIndex:indexPath.row]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (void)getData {
    _pageNumber = 0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSString *shopArrStr = @"";
    if (_shopIdInteger) {
    shopArrStr = [NSString stringWithFormat:@"[%d]",_shopIdInteger];
    }
    if (!_searchNameStr) {
        _searchNameStr = @"";
    }
    
    NSDictionary *dictionary = @{@"key": APP_KEY, @"controller": @"product",@"action": @"storeList",@"stores": shopArrStr,@"ordering": _orderingString,@"sortord": _sortordString,@"minPrice": @(_minPriceInteger),@"maxPrice": @(_maxPriceInteger),@"page": @(_pageNumber),@"name": _searchNameStr};
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    if (_categoryIdStr) {
        [parameters setObject:@([_categoryIdStr integerValue]) forKey:@"categoryId"];
    }
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _minPriceInteger = 0;
            _maximumPriceTF = 0;
            [_theDataMutArr removeAllObjects];
            [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
            [_theTableView reloadData];
            
        }else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                //                [GlobalSharedClass showAlertView:MESSAGE];
                _minPriceInteger = 0;
                _maximumPriceTF = 0;
                [_theDataMutArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                [_theTableView reloadData];

        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configTheNavBar {
    _navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [_navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [_navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_select.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    [_navRightBtn addSubview:navRightIV];
    
    [_navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:_navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;
    
}

- (void)clickNavRightBtn:(id)sender {
    
    ((UIButton *)sender).selected = !((UIButton *)sender).selected;
    
    if (!((UIButton *)sender).selected) {
        [_minimumPriceTF resignFirstResponder];
        [_maximumPriceTF resignFirstResponder];
    
        _filtrateBgView.frame = CGRectMake(0, BOUNDS.size.height, BOUNDS.size.width, 180);
        //    [HRCoreAnimationEffect animationPushDown:_filtrateView];
        [HRCoreAnimationEffect animationMoveDown:_filtrateView duration:0.35];//两个方法效果一样 huangrun
        _theTableView.userInteractionEnabled = YES;
        
        return;
    }

    
    [_filtrateBgView removeFromSuperview];
    _filtrateBgView = [[UIView alloc]initWithFrame:self.view.window.frame];
    _filtrateView = [[UIView alloc]initWithFrame:CGRectMake(0, BOUNDS.size.height - 200  - 40 - 5, BOUNDS.size.width, 180)];
    _filtrateView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_filtrateBgView addSubview:_filtrateView];
    UIView *titleBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 40)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(120, 0, 100, 40)];
    titleLabel.text = @"产品筛选";
    [titleBgView addSubview:titleLabel];
    
    UIView *minimumPriceBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 30, BOUNDS.size.width, 60)];
    UILabel *minimumPriceTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 100, 20)];
    minimumPriceTitleLabel.text = @"最低价格";
    [minimumPriceBgView addSubview:minimumPriceTitleLabel];
    
    _minimumPriceTF = [[UITextField alloc]initWithFrame:CGRectMake(115, 10, 100, 30)];
    _minimumPriceTF.keyboardType = UIKeyboardTypeNumberPad;
    _minimumPriceTF.delegate = self;
    _minimumPriceTF.layer.masksToBounds = YES;
    _minimumPriceTF.layer.cornerRadius = 6.0;
    _minimumPriceTF.layer.borderColor = kThemeCGColor;
    _minimumPriceTF.layer.borderWidth = 1.0;
    [minimumPriceBgView addSubview:_minimumPriceTF];
    
    UILabel *unitLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(270, 15, 20, 20)];
    unitLabel0.text = @"元";
    [minimumPriceBgView addSubview:unitLabel0];
    
    UIView *maximumPriceBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 70, BOUNDS.size.width, 60)];
    UILabel *maximumPriceTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 100, 20)];
    maximumPriceTitleLabel.text = @"最高价格";
    [maximumPriceBgView addSubview:maximumPriceTitleLabel];
    
    _maximumPriceTF = [[UITextField alloc]initWithFrame:CGRectMake(115, 10, 100, 30)];
    _maximumPriceTF.keyboardType = UIKeyboardTypeNumberPad;
    _maximumPriceTF.delegate = self;
    _maximumPriceTF.layer.masksToBounds = YES;
    _maximumPriceTF.layer.cornerRadius = 6.0;
    _maximumPriceTF.layer.borderColor = kThemeCGColor;
    _maximumPriceTF.layer.borderWidth = 1.0;
    [maximumPriceBgView addSubview:_maximumPriceTF];
    
    UILabel *unitLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(270, 15, 20, 20)];
    unitLabel1.text = @"元";
    [maximumPriceBgView addSubview:unitLabel1];
    
    UIView *doneBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 120, BOUNDS.size.width, 35)];
    
    UIButton *doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(115, 0, 100, 35)];
    [doneBtn addTarget:self action:@selector(clickDoneBtn:) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [doneBtn setTitleColor:kThemeColor forState:UIControlStateNormal];
    doneBtn.layer.masksToBounds = YES;
    doneBtn.layer.cornerRadius = 6.0;
    doneBtn.layer.borderColor = kThemeCGColor;
    doneBtn.layer.borderWidth = 1.0;
    [doneBgView addSubview:doneBtn];
    
    [_filtrateView addSubview:titleBgView];
    [_filtrateView addSubview:minimumPriceBgView];
    [_filtrateView addSubview:maximumPriceBgView];
    [_filtrateView addSubview:doneBgView];
    
    [self.view addSubview:_filtrateBgView];
    
//    [HRCoreAnimationEffect animationPushUp:_filtrateView];
    [HRCoreAnimationEffect animationMoveUp:_filtrateView duration:0.35];//两个方法效果一样 huangrun
    _theTableView.userInteractionEnabled = NO;
}

- (void)clickNavTitleBtn:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];UIView *view = [touch view];
    [_minimumPriceTF resignFirstResponder];
    [_maximumPriceTF resignFirstResponder];
    
    _filtrateBgView.frame = CGRectMake(0, BOUNDS.size.height, BOUNDS.size.width, 180);
//    [HRCoreAnimationEffect animationPushDown:_filtrateView];
    [HRCoreAnimationEffect animationMoveDown:_filtrateView duration:0.35];//两个方法效果一样 huangrun
    _theTableView.userInteractionEnabled = YES;
    _navRightBtn.selected = NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _filtrateView.frame = CGRectMake(0, -30, 320, 180);
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

- (void)clickDoneBtn:(id)sender {
    if (![_minimumPriceTF.text isEqualToString:@""])
    _minPriceInteger = [_minimumPriceTF.text integerValue];
    if (![_maximumPriceTF.text isEqualToString:@""])
    _maxPriceInteger = [_maximumPriceTF.text integerValue];
    
    if (_minPriceInteger > _maxPriceInteger) {
        [GlobalSharedClass showAlertView:@"最低价不能大于最高价,请重新输入"];
        return;
    }
    
    [self getData];
    
    [_minimumPriceTF resignFirstResponder];
    [_maximumPriceTF resignFirstResponder];
    
    _filtrateBgView.frame = CGRectMake(0, BOUNDS.size.height, BOUNDS.size.width, 180);
    //    [HRCoreAnimationEffect animationPushDown:_filtrateView];
    [HRCoreAnimationEffect animationMoveDown:_filtrateView duration:0.35];//两个方法效果一样 huangrun
    _theTableView.userInteractionEnabled = YES;
    _navRightBtn.selected = NO;
}

- (void)addFooter
{
    MJRefreshFooterView *footer;
    if(!_footer)
        footer = [MJRefreshFooterView footer];
    footer.scrollView = _theTableView;

    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        _pageNumber ++;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSString *shopArrStr = @"";
        if (_shopIdInteger) {
            shopArrStr = [NSString stringWithFormat:@"[%d]",_shopIdInteger];
        }
        if (!_searchNameStr) {
            _searchNameStr = @"";
        }

        NSDictionary *dictionary = @{@"key": APP_KEY, @"controller": @"product",@"action": @"storeList",@"stores": shopArrStr,@"ordering": _orderingString,@"sortord": _sortordString,@"minPrice": @(_minPriceInteger),@"maxPrice": @(_maxPriceInteger),@"page": @(_pageNumber),@"name": _searchNameStr};
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        if (_categoryIdStr) {
            [parameters setObject:@([_categoryIdStr integerValue]) forKey:@"categoryId"];
        }
        
//        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            [self stopRequest];
            if (RIGHT) {
                _minPriceInteger = 0;
                _maximumPriceTF = 0;
                //            [_theDataArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"data"]mutableCopy]]];
                [self doneWithView:refreshView];
                
            }else if ([[responseObject objectForKey:@"code"]integerValue] == -2) {
                //                [GlobalSharedClass showAlertView:MESSAGE];
                _minPriceInteger = 0;
                _maximumPriceTF = 0;
                //            [_theDataArr removeAllObjects];
                [_theDataMutArr addObjectsFromArray:[[responseObject objectForKey:@"data"]mutableCopy]];
                [self doneWithView:refreshView];
                
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    };
    _footer = footer;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_theTableView  reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    NSLog(@"MJTableViewController--dealloc---");
    [_footer free];
}


@end
