
#import "MerchantTopBar.h"
#import "GeneralViewController.h"

@interface MerchantTopBar ()
- (void)buttonPressed:(id)sender;
@end

@implementation MerchantTopBar


@synthesize delegate;
@synthesize theCurrentIndex;
@synthesize hasButtonSelected;

- (id)initWithFrame:(CGRect)frame itemsCount:(NSInteger)itemsCount
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(itemsIsSelected:) name:@"selectItemsNotification" object:nil];
        
        CGFloat ftXOffset = 0;
        CGFloat ftWidth = frame.size.width / itemsCount;
        CGFloat ftYOffset = 0;
        CGFloat ftHeight = [MerchantTopBar getTopPanelHeight];
        
        UIImageView* bkgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        bkgImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height - 19);
        bkgImageView.image = [UIImage imageNamed:@"merchant_search_background_dep.png"];
        [self addSubview:bkgImageView];

        NSArray* titleArray = [NSArray arrayWithObjects:@"产品", @"店铺", nil];
        
        for(int i=0;i<itemsCount;i++)
        {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
            button.tag = 100+i;
            button.frame = CGRectMake(ftXOffset, ftYOffset, ftWidth, ftHeight);
            button.backgroundColor = [UIColor clearColor];
            [button setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:13];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 24)];
            [button setImageEdgeInsets:UIEdgeInsetsMake(0, ftWidth-24, 5, 0)];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            [button setImage:[UIImage imageNamed:@"merchant_items_arrow_dep"] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"merchant_search_selected"] forState:UIControlStateHighlighted];
            [button setBackgroundImage:[UIImage imageNamed:@"merchant_search_selected"] forState:UIControlStateSelected];
            [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            
            ftXOffset += ftWidth;
        }
        
        theCurrentIndex = 0;
        hasButtonSelected = NO;
    }
    
    return self;
}

#pragma mark -
#pragma mark button event
- (void)buttonPressed:(id)sender
{
    UIButton* button = (UIButton*)sender;
    UIButton* currentButton = (UIButton*)[self viewWithTag:100+theCurrentIndex];
    
    //把上一个按钮设置为不选中
    if((button.tag-100)!=theCurrentIndex)
        currentButton.selected = NO;
    
    theCurrentIndex = button.tag - 100;
    button.selected = !button.selected;
    hasButtonSelected = button.selected;
    
//    NSLog(@"selected:%i", hasButtonSelected);//0627最后版本优化注销注释
    if(delegate)
        [delegate merchantSearchTopViewButtonPressed:sender index:theCurrentIndex];
}

#pragma mark -
#pragma mark public methods
+ (CGFloat)getTopPanelHeight
{
    return 44;
}
- (UIButton*)getCurrentButton
{
    return (UIButton*)[self viewWithTag:100+theCurrentIndex];
}
- (UIButton*)getButtonByIndex:(int)index
{
    return (UIButton*)[self viewWithTag:100+index];
}


//0621HR添加 用来隐藏各试图 但主视图不隐藏
- (void)restoreToInitStateForHide
{

    
    [GeneralViewController resignAllKeyboard:self];
    
    UIButton* currentButton = (UIButton*)[self viewWithTag:100+theCurrentIndex];
    currentButton.selected = NO;
}

- (void)itemsIsSelected:(NSNotification *)notification {
    NSInteger index = [[[notification userInfo]objectForKey:@"index"]integerValue];
    NSString *titleString = [[notification object]objectAtIndex:index];
    [[self getCurrentButton] setTitle:titleString forState:UIControlStateNormal];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
