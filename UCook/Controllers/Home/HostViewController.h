//
//  HostViewController.h
//  ICViewPager
//
//  Created by Ilter Cengiz on 28/08/2013.
//  Copyright (c) 2013 Ilter Cengiz. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ViewPagerController.h"

#import "StrikeThroughLabel.h"

@interface HostViewController : ViewPagerController

@property (strong, nonatomic) UILabel *brandLabel;
@property (strong, nonatomic) UILabel *specificationLabel;
@property (strong, nonatomic) UILabel *classifyLabel;
@property (strong, nonatomic) UILabel *unitLabel;
@property (strong, nonatomic) UILabel *expirationDateLabel;
@property (strong, nonatomic) UILabel *goodsIntroductionLabel;
@property (strong, nonatomic) UIButton *goodsButton0;
@property (strong, nonatomic) UIButton *goodsButton1;
@property (strong, nonatomic) UIButton *goodsButton2;
@property (strong, nonatomic) UIImageView *goodsIV0;
@property (strong, nonatomic) UIImageView *goodsIV1;
@property (strong, nonatomic) UIImageView *goodsIV2;
@property (strong, nonatomic) UILabel *goodsNameLabel0;
@property (strong, nonatomic) UILabel *goodsNameLabel1;
@property (strong, nonatomic) UILabel *goodsNameLabel2;
@property (strong, nonatomic) UILabel *goodsCurrentPriceLabel0;
@property (strong, nonatomic) UILabel *goodsCurrentPriceLabel1;
@property (strong, nonatomic) UILabel *goodsCurrentPriceLabel2;
@property (strong, nonatomic) StrikeThroughLabel *goodsOriginalPriceLabel0;
@property (strong, nonatomic) StrikeThroughLabel *goodsOriginalPriceLabel1;
@property (strong, nonatomic) StrikeThroughLabel *goodsOriginalPriceLabel2;

@end
