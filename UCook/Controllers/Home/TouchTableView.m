//
//  TouchTableView.m
//  UCook
//
//  Created by huangrun on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "TouchTableView.h"
#import "LCTableViewPickerControl.h"

@implementation TouchTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event


{
    
    
    [super touchesBegan:touches withEvent:event];
    for (UIView *view in self.superview.subviews) {
        if ([view isKindOfClass:[LCTableViewPickerControl class]]) {
            [((LCTableViewPickerControl *)view) dismiss];
        }
    }
//关闭键盘
   [self endEditing:YES];
}






- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event


{
    
    
    [super touchesMoved:touches withEvent:event];
    
    
}






- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event


{
    
    
    [super touchesEnded:touches withEvent:event];
    
    
}






- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event


{
    
    
    [super touchesCancelled:touches withEvent:event];
    
    
}


@end
