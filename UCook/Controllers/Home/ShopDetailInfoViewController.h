//
//  ShopDetailInfoViewController.h
//  UCook
//
//  Created by huangrun on 14-7-18.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ShopDetailInfoViewController : GeneralWithBackBtnViewController
@property (strong, nonatomic) IBOutlet UIImageView *shopIV;
@property (strong, nonatomic) IBOutlet UILabel *shopNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *setUpTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *shopIntroductionLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UILabel *collectPopularityLabel;
@property (strong, nonatomic) IBOutlet UILabel *merchantNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;

@property (copy, nonatomic) NSString *storeIdStr;

@end
