//
//  ShopDetailInfoViewController.m
//  UCook
//
//  Created by huangrun on 14-7-18.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ShopDetailInfoViewController.h"
#import "UIImageView+AFNetworking.h"

@interface ShopDetailInfoViewController () {
    NSDictionary *_theDataDic;
}

@end

@implementation ShopDetailInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"店铺信息";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"store",@"action": @"infomation",@"id": _storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _theDataDic = [responseObject objectForKey:@"data"];
            [self configTheView];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configTheView {
    NSString *imgUrlStr = [_theDataDic objectForKey:@"logo"];
    [_shopIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    _shopNameLabel.text = [_theDataDic objectForKey:@"name"];
    _setUpTimeLabel.text = [_theDataDic objectForKey:@"createTime"];
    NSString *introductionString;
    if ([[_theDataDic objectForKey:@"introduction"]isKindOfClass:[NSNull class]]) {
        introductionString = @"";
    } else {
        introductionString = [_theDataDic objectForKey:@"introduction"];
    }
    _shopIntroductionLabel.text = introductionString;
    //以下两行用于让uilabel顶部顶格
    for(int i = 0; i < 4; i++)
    _shopIntroductionLabel.text = [_shopIntroductionLabel.text stringByAppendingString:@"\n "];
    _countLabel.text = [_theDataDic objectForKey:@"productTotal"];
    _collectPopularityLabel.text = [_theDataDic objectForKey:@"collection"];
    _merchantNameLabel.text = [_theDataDic objectForKey:@"agencyName"];
    _phoneLabel.text = [_theDataDic objectForKey:@"tel"];
}

@end
