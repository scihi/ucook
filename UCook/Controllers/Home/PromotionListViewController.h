//
//  PromotionListViewController.h
//  UCook
//
//  Created by scihi on 14-8-22.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface PromotionListViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (copy, nonatomic) NSString *promotionIdStr;

@end
