//
//  ViewController.m
//  WankaCarSteward
//
//  Created by huangrun on 14-5-15.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import "HomeViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AccountManager.h"
#import "UserModel.h"
#import "ADModel.h"
#import "SearchViewController.h"
#import "UIButton+AFNetworking.h"
#import "TheTabBarViewController.h"
#import "StrikeThroughLabel.h"
#import "MapViewController.h"
#import "GoodsDetailViewController.h"

#import "ActivityListModel.h"
#import "PromotionListViewController.h"
#import "ShopInfoViewController.h"
#import "PromotionListViewController.h"

#import "CityListViewController.h"
#import "HRNavViewController.h"

#define UD_AREA_ID @"areaId"
#define UD_AREA_NAME @"areaName"

@interface HomeViewController () {
    
    NSArray *_mapDataArr;//地图数据
    
    NSArray *_adModelArr;
    ADModel *_adModel;
    UITableView *_theTableView;

    ActivityListModel *_activityListModel;
    NSArray *_activityListModelArr;
    
    NSString *_promotionIdStr0;
    NSString *_promotionIdStr1;
    NSString *_promotionIdStr2;
    
    HRImageADView *_imageAdScrollView;
    
    NSMutableArray *_theDataMutArr;//如果用model 里面的数组会成为不可变数组 导致不方便控制 huangrun
    NSMutableDictionary *_marketMutDic;
    NSMutableArray *_shopInfoMutArr;
}

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"首页";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //设置导航栏
    [self configTheNavBar];
    
    //请求市场列表数据
    [self getTheMarketListData];
    
    [self getTheMapData];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //背景图片
    self.view.backgroundColor = [UIColor whiteColor];
    
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64 - 49:568 - 64 - 49 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    _marketMutDic = [NSMutableDictionary dictionaryWithCapacity:10];
    _shopInfoMutArr = [NSMutableArray arrayWithCapacity:10];
    
    //配置首页控件
    [self configTheViews];
    
    //设置导航栏
    [self configTheNavBar];
    
    //请求广告数据
    [self getTheAdData];
    
    //请求活动列表数据
    [self getTheActivityListData];
    
    //请求市场列表数据
    [self getTheMarketListData];
    
    //请求地图数据
//    [self getTheMapData];
    
    //自动登录
    [self autoLogin];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)imageADButtonPressed:(id)sender index:(int)index {
    //    [GlobalSharedClass showAlertView:[NSString stringWithFormat:@"我是老%d,哈哈",index]];
    _adModel = [_adModelArr objectAtIndex:index];
    if ([_adModel.type isEqualToString:@"1"]) {
        //网页
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:_adModel.link]];
    } else if ([_adModel.type isEqualToString:@"2"]) {
        //活动
        if (_adModel.itemId) {
            PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
            promotionListVC.promotionIdStr = _adModel.itemId;
            promotionListVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:promotionListVC animated:YES];
        } else {
            [GlobalSharedClass showAlertView:@"请后台把itemId字段值加上，谢谢！"];
        }
    } else if ([_adModel.type isEqualToString:@"3"]) {
        if (_adModel.itemId) {
            GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
            goodsDetailVC.goodsIdStr = _adModel.itemId;
            goodsDetailVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:goodsDetailVC animated:YES];
        } else {
            [GlobalSharedClass showAlertView:@"请后台把itemId字段值加上，谢谢！"];
        }
    }
}

#pragma mark - 地图
- (void)clickNavLeftBtn:(id)sender {
    CityListViewController *cityListVC = [[CityListViewController alloc]init];
    HRNavViewController *navCityListVC = [[HRNavViewController alloc]initWithRootViewController:cityListVC];
    [self presentViewController:navCityListVC animated:YES completion:nil];
   }

- (void)clickNavRightBtn:(id)sender {
    
//    [self getTheMapData];
    if (!_mapDataArr) {
        return;
    }
    MapViewController *mapVC = [[MapViewController alloc]init];
    mapVC.mapDataArr = _mapDataArr;
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];

}

- (void)clickNavTitleBtn:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)configTheNavBar {
    UIButton *navLeftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 6, 50, 34)];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *cityNameStr = [userDefaults objectForKey:UD_AREA_NAME];
    
    NSString *leftBtnTitleStr = cityNameStr?[cityNameStr stringByReplacingOccurrencesOfString:@"市" withString:@"" ]:@"成都";
    
    [navLeftBtn setTitle:leftBtnTitleStr forState:UIControlStateNormal];
    navLeftBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [navLeftBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    UIImageView *navLeftIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow_down"]];
    navLeftIV.frame = CGRectMake(35, 11, 16, 14);
    [navLeftBtn addSubview:navLeftIV];
    
    [navLeftBtn addTarget:self action:@selector(clickNavLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:navLeftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_map.png"]];
    navRightIV.frame = CGRectMake(10, 5, 30, 24);
    [navRightBtn addSubview:navRightIV];
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 200, 34);
    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;

}

//自动登录
- (void)autoLogin {
    if (![[AccountManager getUid]isEqualToString:@""]&&![[AccountManager getUid]isEqualToString:@"0"]) {//还可能为"0"是因为后台返回的uid是int型，本来应该字符型
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"user",@"action":@"login", @"username": [AccountManager getAccount], @"password": [AccountManager getPassword]};
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (RIGHT) {
                UserModel *model = [[UserModel alloc] initWithDictionary:[responseObject objectForKey:@"data"]];
                AccountManager* manager = [AccountManager sharedInstance];
                [manager setTheUid:model.theUid];
                
                [manager setRole:model.role];
                
                [manager WriteDataToFile];
                //            [delegate loginDidSucceed];
            } else {
                //                [GlobalSharedClass showAlertView:MESSAGE];//自动登录失败不需要提示
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }

}

#pragma mark - 广告
- (void)getTheAdData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"home",@"action":@"adList"};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            //广告
            NSArray *array = [[responseObject objectForKey:@"data"]copy];
            _adModelArr = [ADModel objectArrayWithKeyValuesArray:array];
            _imageAdScrollView.thePageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(BOUNDS.size.width - 105 * _adModelArr.count, _imageAdScrollView.frame.origin.y + 200 - 20, 105 * _adModelArr.count, 20)];
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(_imageAdScrollView.frame.origin.x, _imageAdScrollView.frame.origin.x + 80, _imageAdScrollView.frame.size.width, _imageAdScrollView.frame.size.height)];
            _imageAdScrollView.thePageControl.center = view.center;
            [_theTableView addSubview:_imageAdScrollView.thePageControl];
            [_imageAdScrollView changeDataSourceArray:_adModelArr];
        } else {

        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - 市场列表
- (void)getTheMarketListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *cityIdStr = [userDefaults objectForKey:UD_AREA_ID];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"home",@"action":@"marketList",@"city": cityIdStr?cityIdStr:@""};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            [self stopRequest];
            [_theDataMutArr removeAllObjects];
            _theDataMutArr = [[responseObject objectForKey:@"data"]mutableCopy];
            
            [_theTableView reloadData];
        } else {
            [self stopRequest];
            _theDataMutArr = nil;
            
            [_theTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - 死的活动列表
- (void)getTheActivityListData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key":APP_KEY,@"controller":@"home",@"action":@"promotion"};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            NSArray *array = [[responseObject objectForKey:@"data"]copy];
            _activityListModelArr = [ActivityListModel objectArrayWithKeyValuesArray:array];
            
            
            //配置活动视图
            NSString *imgUrlStr = [(ActivityListModel *)([_activityListModelArr firstObject])image];
            [_activityButton0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
            _promotionIdStr0 = [(ActivityListModel *)([_activityListModelArr firstObject])id];
            
            NSString *imgUrlStr1 = [(ActivityListModel *)([_activityListModelArr objectAtIndex:1])image];
            [_activityButton1 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr1] placeholderImage:kDefaultImage];
            _promotionIdStr1 = [(ActivityListModel *)([_activityListModelArr objectAtIndex:1])id];
            
            NSString *imgUrlStr2 = [(ActivityListModel *)([_activityListModelArr objectAtIndex:2])image];
            [_activityButton2 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr2] placeholderImage:kDefaultImage];
            _promotionIdStr2 = [(ActivityListModel *)([_activityListModelArr objectAtIndex:2])id];
            
                _theTableView.tableHeaderView = _headerView;
            
        } else {
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


#pragma mark - tableView dataSource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _theDataMutArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[[_theDataMutArr objectAtIndex:section]objectForKey:@"stores"]isKindOfClass:[NSNull class]]) {
        return 0;
    } else {
        return [[[_theDataMutArr objectAtIndex:section]objectForKey:@"stores"]count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //当产品个数为0时，要让视图高度变小 所以修改 0827 huangrun
//    NSLog(@"高度为%f",[[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"activity"]isKindOfClass:[NSNull class]]?(310.0 - 140.0):310.0);
//
//    return [[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"activity"]isKindOfClass:[NSNull class]]?(310.0 - 140.0):310.0;
    
//    NSLog(@"高度为%f",([[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"activity"]isKindOfClass:[NSNull class]]?(310.0 - 140.0):310.0) - ([[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"products"]isKindOfClass:[NSNull class]]?110:0));
    
    return ([[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"activity"]isKindOfClass:[NSNull class]]?(310.0 - 140.0):310.0) - ([[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"]objectAtIndex:indexPath.row]objectForKey:@"products"]isKindOfClass:[NSNull class]]?110:0);
//结束修改
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"HomeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"HomeCell" owner:self options:nil]firstObject];
    }
    
    _marketMutDic = [NSMutableDictionary dictionaryWithDictionary:[_theDataMutArr objectAtIndex:indexPath.section]];
    _shopInfoMutArr = [NSMutableArray arrayWithArray:[_marketMutDic objectForKey:@"stores"]];
    
    NSDictionary *currentDictionary = [_shopInfoMutArr objectAtIndex:indexPath.row];
    
    UIButton *shopButton = (UIButton *)[cell viewWithTag:110];
    [shopButton addTarget:self action:@selector(clickShopBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *shopIV = (UIImageView *)[cell viewWithTag:111];
    shopIV.layer.cornerRadius = 15.0;
    NSString *shopImgUrlStr = [currentDictionary objectForKey:@"image"];
    [shopIV setImageWithURL:[NSURL URLWithString:shopImgUrlStr] placeholderImage:kDefaultImage];
    
    UILabel *shopNameLabel = (UILabel *)[cell viewWithTag:101];
    shopNameLabel.text = [currentDictionary objectForKey:@"name"];
    
    if (![[currentDictionary objectForKey:@"activity"] isKindOfClass:[NSNull class]]) {
        UIButton *activityButton = (UIButton *)[cell viewWithTag:102];
        activityButton.hidden = NO;
        NSString *actImgUrlStr = [[[currentDictionary objectForKey:@"activity"]firstObject]objectForKey:@"image"];
        [activityButton setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:actImgUrlStr] placeholderImage:kDefaultImage];
        [activityButton addTarget:self action:@selector(clickCellActivityBtn:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        UIView *goodsBgView = (UIView *)[cell viewWithTag:109];
        goodsBgView.frame = CGRectMake(goodsBgView.frame.origin.x, goodsBgView.frame.origin.y - 140, goodsBgView.frame.size.width, goodsBgView.frame.size.height);
    }
    
    if (![[currentDictionary objectForKey:@"products"]isKindOfClass:[NSNull class]]) {
    
    if ([[currentDictionary objectForKey:@"products"]count] == 1) {
        UIButton *goodsBtn0 = (UIButton *)[cell viewWithTag:103];
        goodsBtn0.hidden = NO;
        NSString *goodsImgUrlStr0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"image"];
        [goodsBtn0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr0] placeholderImage:kDefaultImage];
        [goodsBtn0 addTarget:self action:@selector(clickGoodsBtn0:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel0 = (UILabel *)[cell viewWithTag:104];
        NSString *priceString0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"price"];
        goodsNameLabel0.text = [NSString stringWithFormat:@"￥%@",priceString0];
        goodsNameLabel0.hidden = NO;
    } else if ([[currentDictionary objectForKey:@"products"]count] == 2) {
        UIButton *goodsBtn0 = (UIButton *)[cell viewWithTag:103];
        goodsBtn0.hidden = NO;
        NSString *goodsImgUrlStr0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"image"];
        [goodsBtn0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr0] placeholderImage:kDefaultImage];
        [goodsBtn0 addTarget:self action:@selector(clickGoodsBtn0:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel0 = (UILabel *)[cell viewWithTag:104];
        NSString *priceString0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"price"];
        goodsNameLabel0.text = [NSString stringWithFormat:@"￥%@",priceString0];
        goodsNameLabel0.hidden = NO;
        
        UIButton *goodsBtn1 = (UIButton *)[cell viewWithTag:105];
        goodsBtn1.hidden = NO;
        NSString *goodsImgUrlStr1 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:1]objectForKey:@"image"];
        [goodsBtn1 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr1] placeholderImage:kDefaultImage];
        [goodsBtn1 addTarget:self action:@selector(clickGoodsBtn1:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel1 = (UILabel *)[cell viewWithTag:106];
        goodsNameLabel1.hidden = NO;
        NSString *priceString1 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:1]objectForKey:@"price"];
        goodsNameLabel1.text = [NSString stringWithFormat:@"￥%@",priceString1];
    } else if ([[currentDictionary objectForKey:@"products"]count] == 3) {
        UIButton *goodsBtn0 = (UIButton *)[cell viewWithTag:103];
        goodsBtn0.hidden = NO;
        NSString *goodsImgUrlStr0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"image"];
        [goodsBtn0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr0] placeholderImage:kDefaultImage];
        [goodsBtn0 addTarget:self action:@selector(clickGoodsBtn0:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel0 = (UILabel *)[cell viewWithTag:104];
        goodsNameLabel0.hidden = NO;
        NSString *priceString0 = [[[currentDictionary objectForKey:@"products"]firstObject]objectForKey:@"price"];
        goodsNameLabel0.text = [NSString stringWithFormat:@"￥%@",priceString0];
        
        UIButton *goodsBtn1 = (UIButton *)[cell viewWithTag:105];
        goodsBtn1.hidden = NO;
        NSString *goodsImgUrlStr1 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:1]objectForKey:@"image"];
        [goodsBtn1 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr1] placeholderImage:kDefaultImage];
        [goodsBtn1 addTarget:self action:@selector(clickGoodsBtn1:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel1 = (UILabel *)[cell viewWithTag:106];
        goodsNameLabel1.hidden = NO;
        NSString *priceString1 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:1]objectForKey:@"price"];
        goodsNameLabel1.text = [NSString stringWithFormat:@"￥%@",priceString1];
        
        UIButton *goodsBtn2 = (UIButton *)[cell viewWithTag:107];
        goodsBtn2.hidden = NO;
        NSString *goodsImgUrlStr2 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:2]objectForKey:@"image"];
        [goodsBtn2 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:goodsImgUrlStr2] placeholderImage:kDefaultImage];
        [goodsBtn2 addTarget:self action:@selector(clickGoodsBtn2:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *goodsNameLabel2 = (UILabel *)[cell viewWithTag:108];
        goodsNameLabel2.hidden = NO;
        NSString *priceString2 = [[[currentDictionary objectForKey:@"products"]objectAtIndex:2]objectForKey:@"price"];
        goodsNameLabel2.text = [NSString stringWithFormat:@"￥%@",priceString2];
    }
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
//    goodsDetailVC.goodsIdStr = [[_theDataMutArr objectAtIndex:indexPath.row]objectForKey:@"id"];
//    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 44)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.alpha = 0.9;

    
    //为section底部添加分割线
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, BOUNDS.size.width, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];
    
    //为section顶部添加分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headerView.frame.origin.y, BOUNDS.size.width, 2)];
    lineView.backgroundColor = kThemeColor;
    [headerView addSubview:lineView];
    
    UIButton *changeABatchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    changeABatchBtn.frame = CGRectMake(250, 9.5, 65, 25);
    [changeABatchBtn addTarget:self action:@selector(clickChangeABatchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [changeABatchBtn setTitle:@"换一批" forState:UIControlStateNormal];
        changeABatchBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [changeABatchBtn setTitleColor:kThemeColor forState:UIControlStateNormal];


    //设置换一批的圆角
    changeABatchBtn.layer.masksToBounds = YES;
    changeABatchBtn.layer.cornerRadius = 12.0;
    changeABatchBtn.layer.borderWidth = 1.0;
    changeABatchBtn.layer.borderColor = kThemeCGColor;

    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 200, 44)];
    titleLabel.text = [[_theDataMutArr objectAtIndex:section]objectForKey:@"name"];
    [headerView addSubview:changeABatchBtn];
    [headerView addSubview:titleLabel];
    
    headerView.tag = section;
    return headerView;
}

- (void)configTheViews {
    
    //给首页button添加边框
    _activityButton0.layer.borderWidth = 0.3;
    _activityButton0.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _activityButton1.layer.borderWidth = 0.3;
    _activityButton1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _activityButton2.layer.borderWidth = 0.3;
    _activityButton2.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //首页广告
    _imageAdScrollView = [[HRImageADView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width,180)];
    _imageAdScrollView.delegate = self;
    [_headerView addSubview:_imageAdScrollView];
}

- (void)getTheMapData {
    //获取地图数据
    AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
    manager2.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSString *areaIdStr = [[NSUserDefaults standardUserDefaults]objectForKey:UD_AREA_ID];
    if (!areaIdStr) {
        areaIdStr = @"510100";
    }
    NSDictionary *parameters2 = @{@"key": APP_KEY,@"controller": @"store",@"action": @"marketList",@"areaId":areaIdStr};
    [manager2 POST:BASIC_URL parameters:parameters2 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            _mapDataArr = [[responseObject objectForKey:@"data"]mutableCopy];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (IBAction)clickActivityBtn0:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = _promotionIdStr0;
    promotionListVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:promotionListVC animated:YES];
    
}

- (IBAction)clickActivityBtn1:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = _promotionIdStr1;
    promotionListVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

- (IBAction)clickActivityBtn2:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = _promotionIdStr2;
    promotionListVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

- (void)clickChangeABatchBtn:(id)sender {
    AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
    manager2.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSUInteger uInteger = ((UIButton *)[sender superview]).tag;
    
    NSString *marketIdStr =[[_theDataMutArr objectAtIndex:uInteger]objectForKey:@"id"];
    
    NSDictionary *parameters2 = @{@"key": APP_KEY,@"controller": @"home",@"action": @"changeMarket",@"marketId": marketIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager2 POST:BASIC_URL parameters:parameters2 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (RIGHT) {
            [self stopRequest];
            NSMutableArray *marketInfoMutArr = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"data"]];
            
            [_shopInfoMutArr removeAllObjects];
            [_shopInfoMutArr addObjectsFromArray:marketInfoMutArr];
            
            _marketMutDic = [NSMutableDictionary dictionaryWithDictionary:[_theDataMutArr objectAtIndex:uInteger]];
            [_marketMutDic setObject:_shopInfoMutArr forKey:@"stores"];
            [_theDataMutArr replaceObjectAtIndex:uInteger withObject:_marketMutDic];
            
            [_theTableView reloadData];
        } else {
            [self stopRequest];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
    }];

}

- (void)clickCellActivityBtn:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[sender superview]superview];
    } else {
        cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];

    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = [[[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"] objectAtIndex:indexPath.row]objectForKey:@"activity"]firstObject]objectForKey:@"id"];
    promotionListVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

- (void)clickShopBtn:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[sender superview]superview];
    } else {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    
    ShopInfoViewController *shopInfoVC =[[ShopInfoViewController alloc]initWithNibName:@"ShopInfoViewController" bundle:nil];
    shopInfoVC.storeIdStr = [[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"] objectAtIndex:indexPath.row]objectForKey:@"id"];
    shopInfoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shopInfoVC animated:YES];

}

- (void)clickGoodsBtn0:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
    cell = (UITableViewCell *)[[[sender superview]superview]superview];
    } else {
    cell = (UITableViewCell *)[[[[sender superview]superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"] objectAtIndex:indexPath.row]objectForKey:@"products"]firstObject]objectForKey:@"id"];
    goodsDetailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
    
}

- (void)clickGoodsBtn1:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[[sender superview]superview]superview];
    } else {
    cell = (UITableViewCell *)[[[[sender superview]superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"] objectAtIndex:indexPath.row]objectForKey:@"products"]objectAtIndex:1]objectForKey:@"id"];
    goodsDetailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
    
}

- (void)clickGoodsBtn2:(id)sender {
    UITableViewCell *cell;
    if (IS_IOS8) {
        cell = (UITableViewCell *)[[[sender superview]superview]superview];
    } else {
    cell = (UITableViewCell *)[[[[sender superview]superview]superview]superview];
    }
    NSIndexPath *indexPath = [_theTableView indexPathForCell:cell];
    
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[[[[_theDataMutArr objectAtIndex:indexPath.section]objectForKey:@"stores"] objectAtIndex:indexPath.row]objectForKey:@"products"]objectAtIndex:2]objectForKey:@"id"];
    goodsDetailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
    
}

@end
