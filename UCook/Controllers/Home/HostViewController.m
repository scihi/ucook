//
//  HostViewController.m
//  ICViewPager
//
//  Created by Ilter Cengiz on 28/08/2013.
//  Copyright (c) 2013 Ilter Cengiz. All rights reserved.
//

#import "HostViewController.h"
#import "ContentViewController.h"

@interface HostViewController () <ViewPagerDataSource, ViewPagerDelegate> {
    ContentViewController *_cvc0;
    ContentViewController *_cvc1;
    ContentViewController *_cvc2;
    
    NSString *_theTestStr;
}


@end

@implementation HostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad {
    
    self.dataSource = self;
    self.delegate = self;
    
    self.title = @"View Pager";
    
    // Keeps tab bar below navigation bar on iOS 7.0+
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    _cvc0 = [[ContentViewController alloc]init];
    _cvc0.view.frame = CGRectMake(5, 0, 310, 400);
    _cvc1 = [[ContentViewController alloc]init];
    _cvc1.view.frame = CGRectMake(5, 0, 310, 400);
    _cvc2 = [[ContentViewController alloc]init];
    _cvc2.view.frame = CGRectMake(5, 0, 310, 400);
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return 3;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:13.0];
    label.text = [NSString stringWithFormat:@"View %i", index];
    if (index == 0) {
        label.text = @"商品参数";
    } else if (index == 1) {
        label.text = @"商品介绍";
    } else {
        label.text = @"相关推荐";
    }
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    if (index == 0) {
        
        UILabel *fixedBrandLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 40, 40)];
        fixedBrandLabel.textColor = [UIColor lightGrayColor];
        fixedBrandLabel.text = @"品牌";
        UILabel *fixedSpecificationLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 40, 40, 40)];
        fixedSpecificationLabel.textColor = [UIColor lightGrayColor];
        fixedSpecificationLabel.text = @"规格";
        UILabel *fixedClassifyLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 80, 40, 40)];
        fixedClassifyLabel.textColor = [UIColor lightGrayColor];
        fixedClassifyLabel.text = @"分类";
        UILabel *fixedUnitLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 120, 40, 40)];
        fixedUnitLabel.textColor = [UIColor lightGrayColor];
        fixedUnitLabel.text = @"单位";
        UILabel *fixExpirationDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 160, 60, 40)];
        fixExpirationDateLabel.textColor = [UIColor lightGrayColor];
        fixExpirationDateLabel.text = @"保质期";
        [_cvc0.view addSubview:fixedBrandLabel];
        [_cvc0.view addSubview:fixedSpecificationLabel];
        [_cvc0.view addSubview:fixedClassifyLabel];
        [_cvc0.view addSubview:fixedUnitLabel];
        [_cvc0.view addSubview:fixExpirationDateLabel];
        
        _brandLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 0, 105, 40)];
        _brandLabel.textColor = [UIColor lightGrayColor];
        _brandLabel.textAlignment = NSTextAlignmentRight;
        _specificationLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 40, 105, 40)];
        _specificationLabel.textColor = [UIColor lightGrayColor];
        _specificationLabel.textAlignment = NSTextAlignmentRight;
        _classifyLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 80, 105, 40)];
        _classifyLabel.textColor = [UIColor lightGrayColor];
        _classifyLabel.textAlignment = NSTextAlignmentRight;
        _unitLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 120, 105, 40)];
        _unitLabel.textColor = [UIColor lightGrayColor];
        _unitLabel.textAlignment = NSTextAlignmentRight;
        _expirationDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 160, 105, 40)];
        _expirationDateLabel.textColor = [UIColor lightGrayColor];
        _expirationDateLabel.textAlignment = NSTextAlignmentRight;
        [_cvc0.view addSubview:_brandLabel];
        [_cvc0.view addSubview:_specificationLabel];
        [_cvc0.view addSubview:_classifyLabel];
        [_cvc0.view addSubview:_unitLabel];
        [_cvc0.view addSubview:_expirationDateLabel];
        
        //index2的初始化 只有在此处初始化才能获取到值
        _goodsIntroductionLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 305, 200)];
        _goodsIntroductionLabel.numberOfLines = 0;
        _goodsIntroductionLabel.font = [UIFont systemFontOfSize:14];
        _goodsIntroductionLabel.textColor = [UIColor lightGrayColor];
        
        //index3的初始化 只有在此处初始化才能获取到值
        _goodsButton0 = [[UIButton alloc]initWithFrame:CGRectMake(10, 0, 100, 120)];
        _goodsButton1 = [[UIButton alloc]initWithFrame:CGRectMake(105, 0, 100, 120)];
        _goodsButton2 = [[UIButton alloc]initWithFrame:CGRectMake(210, 0, 100, 100)];
        _goodsIV0 = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 90, 90)];
        _goodsIV1 = [[UIImageView alloc]initWithFrame:CGRectMake(110, 5, 90, 90)];
        _goodsIV2 = [[UIImageView alloc]initWithFrame:CGRectMake(210, 5, 90, 90)];
        _goodsNameLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(10, 100, 100, 10)];
        _goodsNameLabel0.font = [UIFont systemFontOfSize:12];
        _goodsNameLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(110, 100, 100, 10)];
        _goodsNameLabel1.font = [UIFont systemFontOfSize:12];
        _goodsNameLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(210, 100, 100, 10)];
        _goodsNameLabel2.font = [UIFont systemFontOfSize:12];
        _goodsCurrentPriceLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(10, 115, 50, 15)];
        _goodsCurrentPriceLabel0.font = [UIFont systemFontOfSize:12];
        _goodsCurrentPriceLabel0.textColor = kThemeColor;
        _goodsCurrentPriceLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(110, 115, 50, 15)];
        _goodsCurrentPriceLabel1.font = [UIFont systemFontOfSize:12];
        _goodsCurrentPriceLabel1.textColor = kThemeColor;
        _goodsCurrentPriceLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(210, 115, 50, 15)];
        _goodsCurrentPriceLabel2.font = [UIFont systemFontOfSize:12];
        _goodsCurrentPriceLabel2.textColor = kThemeColor;
        _goodsOriginalPriceLabel0 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(50, 118, 50, 10)];
        _goodsOriginalPriceLabel0.font = [UIFont systemFontOfSize:12];
        _goodsOriginalPriceLabel0.textColor = [UIColor lightGrayColor];
        _goodsOriginalPriceLabel0.strikeThroughEnabled = YES;
        _goodsOriginalPriceLabel1 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(150, 118, 50, 10)];
        _goodsOriginalPriceLabel1.font = [UIFont systemFontOfSize:12];
        _goodsOriginalPriceLabel1.textColor = [UIColor lightGrayColor];
        _goodsOriginalPriceLabel1.strikeThroughEnabled = YES;
        _goodsOriginalPriceLabel2 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(250, 118, 50, 10)];
        _goodsOriginalPriceLabel2.font = [UIFont systemFontOfSize:12];
        _goodsOriginalPriceLabel2.textColor = [UIColor lightGrayColor];
        _goodsOriginalPriceLabel2.strikeThroughEnabled = YES;
        
        return _cvc0;
    } else if (index == 1) {
        [_cvc1.view addSubview:_goodsIntroductionLabel];
        return _cvc1;
    } else if (index == 2) {
        [_cvc2.view addSubview:_goodsButton0];
        [_cvc2.view addSubview:_goodsButton1];
        [_cvc2.view addSubview:_goodsButton2];
        [_cvc2.view addSubview:_goodsIV0];
        [_cvc2.view addSubview:_goodsIV1];
        [_cvc2.view addSubview:_goodsIV2];
        [_cvc2.view addSubview:_goodsNameLabel0];
        [_cvc2.view addSubview:_goodsNameLabel1];
        [_cvc2.view addSubview:_goodsNameLabel2];
        [_cvc2.view addSubview:_goodsCurrentPriceLabel0];
        [_cvc2.view addSubview:_goodsCurrentPriceLabel1];
        [_cvc2.view addSubview:_goodsCurrentPriceLabel2];
        [_cvc2.view addSubview:_goodsOriginalPriceLabel0];
        [_cvc2.view addSubview:_goodsOriginalPriceLabel1];
        [_cvc2.view addSubview:_goodsOriginalPriceLabel2];
        return _cvc2;
    }
    return nil;
}

#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
            break;
        case ViewPagerOptionCenterCurrentTab:
            return 0.0;
            break;
        case ViewPagerOptionTabLocation:
            return 1.0;
            break;
        default:
            break;
    }
    
    return value;
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [[UIColor redColor] colorWithAlphaComponent:0.64];
            break;
        default:
            break;
    }
    
    return color;
}

@end
