//
//  ShopEvaluateViewController.h
//  UCook
//
//  Created by scihi on 14-9-9.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface ShopEvaluateViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate>

@property (copy, nonatomic) NSString *storeIdStr;


@end
