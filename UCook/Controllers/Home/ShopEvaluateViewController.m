//
//  ShopEvaluateViewController.m
//  UCook
//
//  Created by scihi on 14-9-9.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ShopEvaluateViewController.h"
#import "ShopEvaluateModel.h"
#import "CGRectAdditionals.h"

static NSString *CellIdentifier  = @"ShopEvaluateCell";

@interface ShopEvaluateViewController () {
    ShopEvaluateModel *_shopEvaluateModel;
    UITableView *_theTableView;
    NSArray *_shopEvaluateModelArr;
    
    UIView *_noDataView;//无数据时的展示背景
}

@end

@implementation ShopEvaluateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"用户评论";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景图片
    UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?568 - 64:568 - 64 - 88)];
    bgIV.image = [UIImage imageNamed:@"public_bg"];
    [self.view addSubview:bgIV];
    [self.view sendSubviewToBack:bgIV];
    
    _theTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, BOUNDS.size.height - 64) style:UITableViewStylePlain];
    _theTableView.dataSource = self;
    _theTableView.delegate = self;
    [_theTableView registerNib:[UINib nibWithNibName:@"ShopEvaluateCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    [self.view addSubview:_theTableView];
    
    [self getTheInitData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getTheInitData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"review",@"action": @"list",@"uid": [AccountManager getUid],@"entityId": @"3",@"entityPk": self.storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [_noDataView removeFromSuperview];
            _theTableView.backgroundColor = [UIColor whiteColor];
            
            NSArray *array = [[responseObject objectForKey:@"data"]copy];
            _shopEvaluateModelArr = [ShopEvaluateModel objectArrayWithKeyValuesArray:array];
            
            [_theTableView reloadData];
            
        } else {
            
            _theTableView.backgroundColor = [UIColor clearColor];
            //设置无数据时的提示视图
            [self configTheNoDataViewForTheTableView];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _shopEvaluateModelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    _shopEvaluateModel = [_shopEvaluateModelArr objectAtIndex:indexPath.row];
    
    //名字
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
    nameLabel.text = _shopEvaluateModel.customerName;
    
    //星星
    UIImageView *stareIV1 = (UIImageView *)[cell viewWithTag:102];
    UIImageView *stareIV2 = (UIImageView *)[cell viewWithTag:103];
    UIImageView *stareIV3 = (UIImageView *)[cell viewWithTag:104];
    UIImageView *stareIV4 = (UIImageView *)[cell viewWithTag:105];
    UIImageView *stareIV5 = (UIImageView *)[cell viewWithTag:106];
    
    
    switch ([_shopEvaluateModel.rating integerValue]) {
        case 0:
            break;
            
        case 1:
            stareIV1.highlighted = YES;
            break;
            
        case 2:
            stareIV1.highlighted = YES;
            stareIV2.highlighted = YES;
            break;
            
        case 3:
            stareIV1.highlighted = YES;
            stareIV2.highlighted = YES;
            stareIV3.highlighted = YES;
            break;
            
        case 4:
            stareIV1.highlighted = YES;
            stareIV2.highlighted = YES;
            stareIV3.highlighted = YES;
            stareIV4.highlighted = YES;
            break;
            
        case 5:
            stareIV1.highlighted = YES;
            stareIV2.highlighted = YES;
            stareIV3.highlighted = YES;
            stareIV4.highlighted = YES;
            stareIV5.highlighted = YES;
            break;
            
        default:
            break;
    }
    
    //内容
    UILabel *contentLabel = (UILabel *)[cell viewWithTag:107];
    contentLabel.text = _shopEvaluateModel.content;
    
    return cell;
}


- (void)configTheNoDataViewForTheTableView {
    if (!_noDataView)
        _noDataView = [[UIView alloc]initWithFrame:self.view.bounds];
    UIImageView *noDataIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 90)];
    noDataIV.image = [UIImage imageNamed:@"ico_tip"];
    UILabel *noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
    noDataLabel.text = @"没有找到相关数据";
    noDataLabel.font = [UIFont systemFontOfSize:13];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.textColor = [UIColor colorWithRed:170.00f/255.00f green:66.00f/255.00f blue:33.00f/255.00f alpha:1.00f];
    noDataLabel.numberOfLines = 0;
    
    [_noDataView addSubview:noDataIV];
    [_noDataView addSubview:noDataLabel];
    
    noDataIV.center = _noDataView.center;
    UIView *view = [[UIView alloc]initWithFrame:[CGRectAdditionals rectAddHeight:_noDataView.frame height:120]];
    noDataLabel.center = view.center;
    
    [_theTableView addSubview:_noDataView];
    
}

@end
