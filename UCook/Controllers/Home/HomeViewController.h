//
//  ViewController.h
//  WankaCarSteward
//
//  Created by huangrun on 14-5-15.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HRImageADView.h"

@interface HomeViewController : GeneralViewController <UITableViewDataSource, UITableViewDelegate, ImageADViewDelegate>


@property (strong, nonatomic) HRImageADView *imageAdScrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *activityButton0;
@property (strong, nonatomic) IBOutlet UIButton *activityButton1;
@property (strong, nonatomic) IBOutlet UIButton *activityButton2;

- (IBAction)clickActivityBtn0:(id)sender;
- (IBAction)clickActivityBtn1:(id)sender;
- (IBAction)clickActivityBtn2:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (strong, nonatomic) IBOutlet UIView *headerView;

@end
