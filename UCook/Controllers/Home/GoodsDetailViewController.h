//
//  GoodsDetailViewController.h
//  UCook
//
//  Created by scihi on 14-7-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "HRImageADView.h"
#import "UMSocialControllerService.h"

#import "LCTableViewPickerControl.h"

#import "StepperTestViewController.h"

@interface GoodsDetailViewController : GeneralWithBackBtnViewController <UMSocialUIDelegate,LCItemPickerDelegate, TextStepperDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) HRImageADView *imageAdScrollView;
@property (nonatomic, copy) NSString *goodsIdStr;//产品id
@property (strong, nonatomic) IBOutlet UILabel *goodsNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *marketPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *inventoryLabel;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIImageView *starIV0;
@property (strong, nonatomic) IBOutlet UIImageView *starIV1;
@property (strong, nonatomic) IBOutlet UIImageView *starIV2;
@property (strong, nonatomic) IBOutlet UIImageView *starIV3;
@property (strong, nonatomic) IBOutlet UIImageView *starIV4;
@property (strong, nonatomic) IBOutlet UILabel *starCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *evaluateCountLabel;
@property (strong, nonatomic) IBOutlet UIImageView *shopIV;
@property (strong, nonatomic) IBOutlet UILabel *shopNameLabel;
- (IBAction)contactSellerBtn:(id)sender;
- (IBAction)enterShopBtn:(id)sender;

- (IBAction)clickAddToGoodsBagBtn:(id)sender;
- (IBAction)clickAddToShoppingCartBtn:(id)sender;
- (IBAction)clickBuyBtn:(id)sender;

- (IBAction)clickShareBtn:(id)sender;

@property (weak) id pickValue;
- (IBAction)clickEvaluateBtn:(id)sender;

@end
