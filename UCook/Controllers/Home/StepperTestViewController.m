//
//  StepperTestViewController.m
//  stepperTest
//
//  Created by Алексеев Владислав on 11.09.11.
//  Copyright 2011 beefon software. All rights reserved.
//

#import "StepperTestViewController.h"
#import "TextStepperField.h"

@implementation StepperTestViewController
@synthesize changeText;
@synthesize stepperLabel;

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    counter = 1;
    //huangrun 0812 增加_theFloat != 0?_theFloat:
    _contador.Current = _theFloat != 0.0?_theFloat:0.0;
    
    //结束修改
    _contador.Step = 1;
    _contador.Minimum = 0;
    _contador.Maximum = _Maximum;
    _contador.NumDecimals = 0;
    _contador.IsEditableTextField = YES;
    
    // don't care about width and height - they are always fixed
//    TextStepperField *stepper = [[TextStepperField alloc] initWithFrame:CGRectMake(50, 100, 150, 40)];
//    [stepper addTarget:self
//                action:@selector(programmaticallyCreatedStepperDidStep:) 
//      forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:stepper];
    _cancelButton.layer.cornerRadius = 6.0;
    _confirmButton.layer.cornerRadius = 6.0;
    
}


- (void)viewDidUnload {
    [self setStepperLabel:nil];
    _contador = nil;
    [self setChangeText:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

//- (void)programmaticallyCreatedStepperDidStep:(TextStepperField *)stepper {
//    if (stepper.TypeChange == TextStepperFieldChangeKindNegative) {
//        counter -= 10;
//    }
//    else {
//        counter += 5;
//    }
//    
//    self.stepperLabel.text = [NSString stringWithFormat:@"%d PR", counter];
//}

- (IBAction)ibStepperDidStep:(id)sender {
//    if (contador.TypeChange == TextStepperFieldChangeKindNegative) {
//        
//        counter--;
//    }
//    else {
//
//        counter++;
//    }

    [_delegate stepperValueDidChange:self counter:_contador.Current];
}

- (IBAction)changeTexto:(id)sender {
}

- (IBAction)cambio:(id)sender {
}
- (IBAction)clickCancelBtn:(id)sender {
    [self.view.superview removeFromSuperview];
    self.parentViewController.navigationController.navigationBar.userInteractionEnabled = YES;
}
- (IBAction)clickConfirmBtn:(id)sender {

    [_delegate stepperDidClickConfirmBtn:self counter:_contador.Current];
    [self.view.superview removeFromSuperview];
    self.parentViewController.navigationController.navigationBar.userInteractionEnabled = YES;
}
@end
