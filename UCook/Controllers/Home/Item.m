//
//  Item.m
//  CustomMKAnnotationView
//
//  Created by JianYe on 14-2-8.
//  Copyright (c) 2014年 Jian-Ye. All rights reserved.
//

#import "Item.h"

@implementation Item
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.latitude = [dictionary objectForKey:@"latitude"];
        self.longitude = [dictionary objectForKey:@"logitude"];
        self.title = [dictionary objectForKey:@"name"];
        self.subtitle = [dictionary objectForKey:@"address"];
        
        self.logo = [dictionary objectForKey:@"logo"];//huangrun
    }
    return self;
}
@end
