//
//  ShopInfoViewController.h
//  UCook
//
//  Created by huangrun on 14-7-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "UMSocialControllerService.h"

@interface ShopInfoViewController : GeneralWithBackBtnViewController <UMSocialUIDelegate>

@property (nonatomic, strong) NSDictionary *theDataDic;
@property (nonatomic, copy) NSString *storeIdStr;//店铺id
@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIButton *moreButton;
- (IBAction)clickMoreBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *shopIV;
@property (strong, nonatomic) IBOutlet UIButton *activityButton0;
@property (strong, nonatomic) IBOutlet UIButton *activityButton1;
@property (strong, nonatomic) IBOutlet UIButton *activityButton2;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *hotProductBtn0;
@property (strong, nonatomic) IBOutlet UIButton *hotProductBtn1;
@property (strong, nonatomic) IBOutlet UIButton *hotProductBtn2;
@property (strong, nonatomic) IBOutlet UIButton *hotProductBtn3;
//以下两个用于调整frame
@property (strong, nonatomic) IBOutlet UIView *theMoveView0;
@property (strong, nonatomic) IBOutlet UIView *theMoveView1;
@property (strong, nonatomic) IBOutlet UIImageView *collectIV;

- (IBAction)clickInfoBtn:(id)sender;
- (IBAction)clickLocationBtn:(id)sender;
- (IBAction)clickShareBtn:(id)sender;
- (IBAction)clickCollectBtn:(id)sender;
- (IBAction)clickHotProductBtn0:(id)sender;
- (IBAction)clickHotProductBtn1:(id)sender;
- (IBAction)clickHotProductBtn2:(id)sender;
- (IBAction)clickHotProductBtn3:(id)sender;

- (IBAction)clickActivityBtn0:(id)sender;
- (IBAction)clickActivityBtn1:(id)sender;
- (IBAction)clickActivityBtn2:(id)sender;

@end
