//
//  MoreViewController.h
//  UCook
//
//  Created by huangrun on 14-7-20.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"

@interface MoreViewController : GeneralWithBackBtnViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic) NSInteger shopIdInteger;

@property (copy, nonatomic) NSString *searchNameStr;
@property (copy, nonatomic) NSString *categoryIdStr;

@end
