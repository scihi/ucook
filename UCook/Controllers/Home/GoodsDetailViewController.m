//
//  GoodsDetailViewController.m
//  UCook
//
//  Created by scihi on 14-7-21.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GoodsDetailViewController.h"
#import "ADModel.h"
#import "NSDictionaryAdditions.h"
#import "UIImageView+AFNetworking.h"
#import "HostViewController.h"

#import "UMSocial.h"
#import "ShopInfoViewController.h"
#import "HRNavViewController.h"
#import "LoginViewController.h"

#import "ConfirmOrderViewController.h"

#import "ConfirmOrderModel.h"

#import "ShopEvaluateViewController.h"

@interface GoodsDetailViewController () {
    NSDictionary *_theDataDic;
    NSMutableArray *_adArray;
    NSArray *_goodsArray;//该页数据的商品数组 而不是商品包里的商品 下面会声明一个商品包的局部变量商品数组 注意区分
    UIImageView *_navRightIV;//收藏按钮
    NSArray *_goodsBagArr;//商品包数组
    
    StepperTestViewController *stepperTestVC;
}

@end

@implementation GoodsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"商品详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTheVar];
    [self configTheNavBar];
    [self getData];
    //滚动广告
    _imageAdScrollView = [[HRImageADView alloc]initWithFrame:CGRectMake(0, 0, BOUNDS.size.width, 200)];
//    _imageAdScrollView.delegate = self;
    [_theScrollView addSubview:_imageAdScrollView];
}

- (void)didReceiveMemoryWarning
{
    // Dispose of any resources that can be recreated.
    [super didReceiveMemoryWarning];//即使没有显示在window上，也不会自动的将self.view释放。
//    // Add code to clean up any of your own resources that are no longer necessary.
//    
//    // 此处做兼容处理需要加上ios6.0的宏开关，保证是在6.0下使用的,6.0以前屏蔽以下代码，否则会在下面使用self.view时自动加载viewDidLoad
//    if ([self.view window] == nil)// 是否是正在使用的视图
//    {
//        // Add code to preserve data stored in the views that might be
//        // needed later.
//        
//        // Add code to clean up other strong references to the view in
//        // the view hierarchy.
//        self.view = nil;// 目的是再次进入时能够重新加载调用viewDidLoad函数。
//    }
}

- (void)initTheVar {
    _adArray = [NSMutableArray arrayWithCapacity:10];
}

- (void)configTheNavBar {
    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
    _navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_collect"]];
    _navRightIV.frame = CGRectMake(10, 5, 30, 24);
    [navRightBtn addSubview:_navRightIV];
    
    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;

}

- (void)clickNavRightBtn:(id)sender {
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
        return;
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"collect",@"action": @"add",@"type": @"1",@"uid": [AccountManager getUid],@"item": [_theDataDic objectForKey:@"id"]};//type 1为产品 2为店铺
//        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            [self stopRequest];
            if (RIGHT) {
                if ([[responseObject objectForKey:@"total"]integerValue] == 1) {
                    _navRightIV.image = [UIImage imageNamed:@"ico_collect_on"];
                } else if ([[responseObject objectForKey:@"total"]integerValue] == 0) {
                    _navRightIV.image = [UIImage imageNamed:@"ico_collect"];
                }
                [GlobalSharedClass showAlertView:MESSAGE];
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    }

}

- (void)getData {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"product",@"action": @"detail",@"id": _goodsIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _theDataDic = [[responseObject objectForKey:@"data"]mutableCopy];
            _goodsArray = [_theDataDic objectForKey:@"recommend"];
            [self configTheView];
        } else {
            if([[responseObject objectForKey:@"code"]integerValue] == -2)
                [GlobalSharedClass showAlertViewWithOneBtn:MESSAGE delegate:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configTheView {
    //广告
    for(NSDictionary* adDictionary in [_theDataDic objectForKey:@"images"])
    {
//        ADModel *model = [[ADModel alloc] initWithNSDictionary:adDictionary];
        //首页已用。so只能在当前页赋值了
        
        ADModel *model = [[ADModel alloc]init];
        [model setId:[adDictionary getStringValueForKey:@"id" defaultValue:@""]];
        [model setImage:[adDictionary getStringValueForKey:@"url" defaultValue:@""]];
        [model setLink:[adDictionary getStringValueForKey:@"link" defaultValue:@""]];
        [_adArray addObject:model];
    }
    _imageAdScrollView.thePageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(BOUNDS.size.width - 105 * _adArray.count, _imageAdScrollView.frame.origin.y + 200 - 5, 105 * _adArray.count, 20)];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(_imageAdScrollView.frame.origin.x, _imageAdScrollView.frame.origin.x + 185, _imageAdScrollView.frame.size.width, _imageAdScrollView.frame.size.height - 185)];
    _imageAdScrollView.thePageControl.center = view.center;
    [self.view addSubview:_imageAdScrollView.thePageControl];
    [_imageAdScrollView changeDataSourceArray:_adArray];
    
    //杂项
    _goodsNameLabel.text = [_theDataDic objectForKey:@"name"];
    _currentPriceLabel.text = [NSString stringWithFormat:@"￥%@/件",[_theDataDic objectForKey:@"discountPrice"]];
    _marketPriceLabel.text = [NSString stringWithFormat:@"￥%@/件",[_theDataDic objectForKey:@"price"]];
    _inventoryLabel.text = [NSString stringWithFormat:@"%@件",[_theDataDic objectForKey:@"stock"]];
    
    //星星
    CGFloat score1 = [[_theDataDic objectForKey:@"scores"]floatValue];
    NSInteger score2;
    if (score1 == 0 ) {
        score2 = 0;
    } else if (score1 > 0 && score1 <= 1) {
        score2 = 1;
    } else if (score1 > 1 && score1 <= 2) {
        score2 = 2;
    } else if (score1 > 2 && score1 <= 3) {
        score2 = 3;
    } else if (score1 > 3 && score1 <= 4) {
        score2 = 4;
    } else {
        score2 = 5;
    }
    
    switch (score2) {
        case 0:
            break;
        case 1:
            _starIV0.image = [UIImage imageNamed:@"ico_star_big_light"];
            break;
        case 2:
            _starIV0.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
            break;
        case 3:
            _starIV0.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
            break;
        case 4:
            _starIV0.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
            break;
        case 5:
            _starIV0.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV1.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV2.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV3.image = [UIImage imageNamed:@"ico_star_big_light"];
            _starIV4.image = [UIImage imageNamed:@"ico_star_big_light"];
            break;
        default:
            break;
    }
    
    _starCountLabel.text = [NSString stringWithFormat:@"%d分",[[_theDataDic objectForKey:@"scores"]integerValue]];
    _evaluateCountLabel.text = [NSString stringWithFormat:@"%@人评价",[_theDataDic objectForKey:@"scoreNum"]];
    
    //shop
    NSString *imgUrlStr = [_theDataDic objectForKey:@"storeLogo"];
    [_shopIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    _shopNameLabel.text = [_theDataDic objectForKey:@"storeName"];
    
    //滚动列表
    HostViewController *hostVC = [[HostViewController alloc]init];
    hostVC.view.frame = CGRectMake(5, 459, 310, 400);
    hostVC.brandLabel.text = [_theDataDic objectForKey:@"brand"];
    hostVC.specificationLabel.text = [_theDataDic objectForKey:@"spec"];
    hostVC.classifyLabel.text = [_theDataDic objectForKey:@"category"];
    hostVC.unitLabel.text = [_theDataDic objectForKey:@"unit"];
    hostVC.expirationDateLabel.text = [_theDataDic objectForKey:@"storagelife"];
    
    hostVC.goodsIntroductionLabel.text = [_theDataDic objectForKey:@"desc"];
    for(int i = 0; i < 4; i++)
        hostVC.goodsIntroductionLabel.text = [hostVC.goodsIntroductionLabel.text stringByAppendingString:@"\n "];
    

    
    if (_goodsArray.count != 0) {
//        [hostVC.goodsButton0 addTarget:self action:@selector(clickGoodsButton0) forControlEvents:UIControlEventTouchUpInside];
//        [hostVC.goodsButton1 addTarget:self action:@selector(clickGoodsButton1) forControlEvents:UIControlEventTouchUpInside];
//        [hostVC.goodsButton2 addTarget:self action:@selector(clickGoodsButton2) forControlEvents:UIControlEventTouchUpInside];
        
        if (_goodsArray.count == 1) {
            NSString *imgUrlStr0 = [[_goodsArray firstObject]objectForKey:@"img"];
            [hostVC.goodsIV0 setImageWithURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
             hostVC.goodsNameLabel0.text = [[_goodsArray firstObject]objectForKey:@"name"];
            NSString *goodsCurrentPriceStr0 = [[_goodsArray firstObject]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr0];
             NSString *goodsOriginalPriceStr0 = [[_goodsArray firstObject]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr0];
            
            [hostVC.goodsButton0 addTarget:self action:@selector(clickGoodsButton0) forControlEvents:UIControlEventTouchUpInside];
            [hostVC.goodsButton1 removeFromSuperview];
            [hostVC.goodsButton2 removeFromSuperview];
            
        } else if (_goodsArray.count == 2) {
            NSString *imgUrlStr0 = [[_goodsArray firstObject]objectForKey:@"img"];
            [hostVC.goodsIV0 setImageWithURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
            hostVC.goodsNameLabel0.text = [[_goodsArray firstObject]objectForKey:@"name"];
            NSString *goodsCurrentPriceStr0 = [[_goodsArray firstObject]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr0];
            NSString *goodsOriginalPriceStr0 = [[_goodsArray firstObject]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr0];
            
            NSString *imgUrlStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"img"];
            [hostVC.goodsIV1 setImageWithURL:[NSURL URLWithString:imgUrlStr1] placeholderImage:kDefaultImage];
            hostVC.goodsNameLabel1.text = [[_goodsArray objectAtIndex:1]objectForKey:@"name"];
             NSString *goodsCurrentPriceStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel1.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr1];

            NSString *goodsOriginalPriceStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel1.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr1];
            
            [hostVC.goodsButton0 addTarget:self action:@selector(clickGoodsButton0) forControlEvents:UIControlEventTouchUpInside];
            [hostVC.goodsButton1 addTarget:self action:@selector(clickGoodsButton1) forControlEvents:UIControlEventTouchUpInside];
            [hostVC.goodsButton1 removeFromSuperview];
            
        } else if (_goodsArray.count >= 3) {
            NSString *imgUrlStr0 = [[_goodsArray firstObject]objectForKey:@"img"];
            [hostVC.goodsIV0 setImageWithURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
            NSString *imgUrlStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"img"];
            [hostVC.goodsIV1 setImageWithURL:[NSURL URLWithString:imgUrlStr1] placeholderImage:kDefaultImage];
            NSString *imgUrlStr2 = [[_goodsArray objectAtIndex:2]objectForKey:@"img"];
            [hostVC.goodsIV2 setImageWithURL:[NSURL URLWithString:imgUrlStr2] placeholderImage:kDefaultImage];
            hostVC.goodsNameLabel0.text = [[_goodsArray firstObject]objectForKey:@"name"];
            hostVC.goodsNameLabel1.text = [[_goodsArray objectAtIndex:1]objectForKey:@"name"];
            hostVC.goodsNameLabel2.text = [[_goodsArray objectAtIndex:2]objectForKey:@"name"];
            NSString *goodsCurrentPriceStr0 = [[_goodsArray firstObject]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr0];
            NSString *goodsCurrentPriceStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel1.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr1];
            NSString *goodsCurrentPriceStr2 = [[_goodsArray objectAtIndex:2]objectForKey:@"discountPrice"];
            hostVC.goodsCurrentPriceLabel2.text = [NSString stringWithFormat:@"￥%@",goodsCurrentPriceStr2];
            NSString *goodsOriginalPriceStr0 = [[_goodsArray firstObject]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel0.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr0];
            NSString *goodsOriginalPriceStr1 = [[_goodsArray objectAtIndex:1]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel1.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr1];
            NSString *goodsOriginalPriceStr2 = [[_goodsArray objectAtIndex:2]objectForKey:@"price"];
            hostVC.goodsOriginalPriceLabel2.text = [NSString stringWithFormat:@"￥%@",goodsOriginalPriceStr2];
            
            [hostVC.goodsButton0 addTarget:self action:@selector(clickGoodsButton0) forControlEvents:UIControlEventTouchUpInside];
            [hostVC.goodsButton1 addTarget:self action:@selector(clickGoodsButton1) forControlEvents:UIControlEventTouchUpInside];
            [hostVC.goodsButton2 addTarget:self action:@selector(clickGoodsButton2) forControlEvents:UIControlEventTouchUpInside];
        }
        
   
    } else {
        [hostVC.goodsButton0 removeFromSuperview];
        [hostVC.goodsButton1 removeFromSuperview];
        [hostVC.goodsButton2 removeFromSuperview];
    }
    
    [self addChildViewController:hostVC];
    [_theScrollView addSubview:hostVC.view];
    [hostVC didMoveToParentViewController:self];
    
    //进入该页的收藏状态
    if ([[_theDataDic objectForKey:@"isCollection"]integerValue] == 1) {
        _navRightIV.image = [UIImage imageNamed:@"ico_collect_on"];
    } else if ([[_theDataDic objectForKey:@"isCollection"]integerValue] == 0) {
        _navRightIV.image = [UIImage imageNamed:@"ico_collect"];
    }

}

- (void)clickGoodsButton0 {
    NSString *goodsIdStr = [[_goodsArray firstObject]objectForKey:@"id"];
    GoodsDetailViewController *goodsDetailVC =[[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = goodsIdStr;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (void)clickGoodsButton1 {
    NSString *goodsIdStr = [[_goodsArray objectAtIndex:1]objectForKey:@"id"];
    GoodsDetailViewController *goodsDetailVC =[[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = goodsIdStr;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (void)clickGoodsButton2 {
    NSString *goodsIdStr = [[_goodsArray objectAtIndex:2]objectForKey:@"id"];
    GoodsDetailViewController *goodsDetailVC =[[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = goodsIdStr;
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (IBAction)clickShareBtn:(id)sender {
    UIImage *shareImage = [UIImage imageNamed:@"public_logo.png"];
    
    [UMSocialSnsService presentSnsIconSheetView:self appKey:UmengAppkey shareText:@"有优厨,无厨忧" shareImage:shareImage shareToSnsNames:@[UMShareToSina,UMShareToWechatSession,UMShareToWechatTimeline, UMShareToQzone] delegate:self];
}
- (IBAction)contactSellerBtn:(id)sender {
    UIWebView *phoneWV = [[UIWebView alloc]init];
    NSString *phoneUrlStr = [NSString stringWithFormat:@"tel://%@",[_theDataDic objectForKey:@"storeTel"]];
    [phoneWV loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:phoneUrlStr]]];
    [self.view addSubview:phoneWV];
}

- (IBAction)enterShopBtn:(id)sender {
    NSString *storeIdStr = [_theDataDic objectForKey:@"storeId"];
    if (![storeIdStr isKindOfClass:[NSNull class]]) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"store",@"action": @"infomation",@"id": storeIdStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                ShopInfoViewController *shopInfoVC = [[ShopInfoViewController alloc]initWithNibName:@"ShopInfoViewController" bundle:nil];
                //                shopInfoVC.theDataDic = [[responseObject objectForKey:@"data"]mutableCopy];
                shopInfoVC.storeIdStr = storeIdStr;
                [self.navigationController pushViewController:shopInfoVC animated:YES];
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    } else {
        [GlobalSharedClass showAlertView:@"尚未有商家入驻"];
    }

}

- (IBAction)clickAddToGoodsBagBtn:(id)sender {
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
        return;
    } else {

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"productBox",@"action": @"list",@"uid": [AccountManager getUid]};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _goodsBagArr = [responseObject objectForKey:@"data"];
            NSMutableArray *goodsBagNameArr = [NSMutableArray arrayWithCapacity:10];
            for (NSDictionary *goodsBagDic in _goodsBagArr) {
                
                    NSString *goodsBagNamestr = [goodsBagDic objectForKey:@"name"];
                    [goodsBagNameArr addObject:goodsBagNamestr];
                }
            LCTableViewPickerControl *pickerView = [[LCTableViewPickerControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, kPickerControlWidth, kPickerControlAgeHeight) title:@"请选择商品包" value:_pickValue items:goodsBagNameArr];
            [pickerView setDelegate:self];
            [pickerView setTag:0];
            
            [self.view addSubview:pickerView];
            
            [pickerView show];
            

        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    }
}

- (IBAction)clickAddToShoppingCartBtn:(id)sender {
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
        return;
    } else {
        
        stepperTestVC = [[StepperTestViewController alloc]initWithNibName:@"StepperTestViewController" bundle:nil];
        stepperTestVC.Maximum = 10000;
        stepperTestVC.view.frame = CGRectMake(0, 0, 300, 200);
        stepperTestVC.view.tag = 101;
        stepperTestVC.delegate = self;
        UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
        [self addChildViewController:stepperTestVC];
        [view addSubview:stepperTestVC.view];
        stepperTestVC.view.center = view.center;
        [stepperTestVC didMoveToParentViewController:self];
//        stepperTestVC.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] ];
                stepperTestVC.stepperLabel.text = @"￥0.0";
        [self.view addSubview:view];
        self.navigationController.navigationBar.userInteractionEnabled = NO;
    }
}

- (IBAction)clickBuyBtn:(id)sender {
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
//        return;//多余 huangrun
    } else {
        
        stepperTestVC = [[StepperTestViewController alloc]initWithNibName:@"StepperTestViewController" bundle:nil];
        stepperTestVC.Maximum = 10000;
        stepperTestVC.view.frame = CGRectMake(0, 0, 300, 200);
        stepperTestVC.view.tag = 102;
        stepperTestVC.delegate = self;
        stepperTestVC.contador.textField.delegate = self;
        UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [self addChildViewController:stepperTestVC];
    [view addSubview:stepperTestVC.view];
    stepperTestVC.view.center = view.center;
    [stepperTestVC didMoveToParentViewController:self];
//    stepperTestVC.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] ];
        stepperTestVC.stepperLabel.text = @"￥0.0";
    [self.view addSubview:view];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    }
}

- (void)dismissPickerControl:(LCTableViewPickerControl*)view
{
    [view dismiss];
}


#pragma mark - LCTableViewPickerDelegate

- (void)selectControl:(LCTableViewPickerControl*)view didSelectWithItem:(id)item indexPath:(NSIndexPath *)indexPath
{
    /*
     Check item is NSString or NSNumber , if it is necessary
     */
    if (view.tag == 0)
    {
//        if ([item isKindOfClass:[NSString class]])
//        {
//            
//        }
//        else if ([item isKindOfClass:[NSNumber class]])
//        {
//            
//        }
        NSString *goodsBoxIdStr = [[_goodsBagArr objectAtIndex:indexPath.row]objectForKey:@"id"];
//        NSArray *storeArray = [[_goodsBagArr objectAtIndex:indexPath.row]objectForKey:@"stores"];
//        NSArray *goodsArray = [[storeArray firstObject]objectForKey:@"products"];
//        NSMutableArray *goodsIdStrArr = [NSMutableArray arrayWithCapacity:10];
//        for (NSDictionary *goodsInfoDic in goodsArray) {
//            [goodsIdStrArr addObject:[goodsInfoDic objectForKey:@"id"]];
//        }
        NSString *goodsIdStr = [_theDataDic objectForKey:@"id"];
        NSString *wantGoodsIdStr = [NSString stringWithFormat:@"[%@]",goodsIdStr];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"productBox",@"action": @"addProduct",@"uid": [AccountManager getUid],@"boxId": goodsBoxIdStr,@"productId": wantGoodsIdStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                [GlobalSharedClass showAlertView:MESSAGE];
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];

        
    }
    
    self.pickValue = item;
//    [_resultLabel setText:[NSString stringWithFormat:@"%@", item]];
    
    [self dismissPickerControl:view];
}

- (void)selectControl:(LCTableViewPickerControl *)view didCancelWithItem:(id)item
{
    
    [self dismissPickerControl:view];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[LCTableViewPickerControl class]]) {
            [((LCTableViewPickerControl *)view) dismiss];
        }
    }
    
//        [self.view endEditing:YES];
    
}//huangrun添加

#pragma mark - 加入购物车
- (void)addToShoppingCartInServer:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)goodsNumber {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *cartProductDic = @{@"productId": @([[_theDataDic objectForKey:@"id"]integerValue]),@"number": @(goodsNumber)};
    NSArray *cartProductArr = @[cartProductDic];
    NSString *cartProductArrJsonStr = [self arrayToJson:cartProductArr];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"cart",@"action": @"add",@"uid": [AccountManager getUid],@"cartProduct":cartProductArrJsonStr};
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

#pragma mark - textStepperDelegate
- (void)stepperValueDidChange:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    
     stepperTestViewController.stepperLabel.text = [NSString stringWithFormat:@"￥%.1f",[[_theDataDic objectForKey:@"discountPrice"]floatValue] * counter];
}

- (void)stepperDidClickConfirmBtn:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)counter {
    if (stepperTestViewController.view.tag == 101) {
         [self addToShoppingCartInServer:stepperTestViewController counter:counter];
    } else {
        
        if ([stepperTestVC.stepperLabel.text isEqualToString:@"0"]) {
            [GlobalSharedClass showAlertView:@"请选择购买数量"];
            return;
        }
        [self buyInServer:stepperTestViewController counter:counter];
    }
   
}
  
#pragma mark - 立即购买
- (void)buyInServer:(StepperTestViewController *)stepperTestViewController counter:(NSInteger)goodsNumber {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSString *unitPriceStr = [stepperTestViewController.stepperLabel.text stringByReplacingOccurrencesOfString:@"￥" withString:@""];//单价
    CGFloat priceFloat = [unitPriceStr floatValue] * goodsNumber;
    NSString *priceString = [NSString stringWithFormat:@"%.1f",priceFloat];
    NSDictionary *productDic = @{@"id":@([[_theDataDic objectForKey:@"id"]integerValue]),@"number": @(goodsNumber),@"price": priceString};
    NSArray *productArr = @[productDic];
    NSString *productArrJsonStr = [self arrayToJson:productArr];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"order",@"action": @"confirm",@"uid": [AccountManager getUid],@"products":productArrJsonStr};
    
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            NSDictionary *dictionary = [[responseObject objectForKey:@"data"]copy];
            
            ConfirmOrderViewController *confirmOrderVC = [[ConfirmOrderViewController alloc]initWithNibName:@"ConfirmOrderViewController" bundle:nil];
            ConfirmOrderModel *confirmOrderModel = [ConfirmOrderModel objectWithKeyValues:dictionary];
            confirmOrderVC.confirmOrderModel = confirmOrderModel;
            [self.navigationController pushViewController:confirmOrderVC animated:YES];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    stepperTestVC.view.frame = CGRectMake(10, 5, 300, 200);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UIView *view = [[UIView alloc]initWithFrame:self.view.bounds];
    stepperTestVC.view.center = view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [stepperTestVC.contador.textField resignFirstResponder];
    return YES;
}

- (IBAction)clickEvaluateBtn:(id)sender {
    ShopEvaluateViewController *shopEvaluateVC = [[ShopEvaluateViewController alloc]init];
    shopEvaluateVC.storeIdStr = [_theDataDic objectForKey:@"storeId"];
    [self.navigationController pushViewController:shopEvaluateVC animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
