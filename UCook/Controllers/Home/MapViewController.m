//
//  ViewController.m
//  CustomMKAnnotationView
//
//  Created by Jian-Ye on 12-11-22.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "MapViewController.h"
#import "MapView.h"
#import "Item.h"
#import "TestMapCell.h"
#import "GridViewController.h"

@interface MapViewController ()<MapViewDelegate>


@property (nonatomic,strong)MapView *mapView;
@property (nonatomic,strong)NSArray *annotations;
@end

@implementation MapViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"地图";
    

    self.annotations = _mapDataArr;
	self.mapView = [[MapView alloc] initWithDelegate:self];
    [self.view addSubview:_mapView];
    [_mapView setFrame:self.view.bounds];
    
    _mapView.mapDataArr = _mapDataArr;
    [_mapView beginLoad];
}


#pragma mark -
#pragma mark delegate

- (NSInteger)numbersWithCalloutViewForMapView
{
    return _annotations.count;
}

- (CLLocationCoordinate2D)coordinateForMapViewWithIndex:(NSInteger)index
{
    Item *item = [[Item alloc] initWithDictionary:[_annotations objectAtIndex:index]];
    CLLocationCoordinate2D coordinate;
	coordinate.latitude = [item.latitude doubleValue];
	coordinate.longitude = [item.longitude doubleValue];
    return coordinate;
}

- (UIImage *)baseMKAnnotationViewImageWithIndex:(NSInteger)index
{
//    return [UIImage imageNamed:@"pin"];
        UIImage* image=nil;
        NSString *urlString = [[_mapDataArr objectAtIndex:index]objectForKey:@"logo"];
    
    if (![urlString isEqualToString:@""]) {
        NSURL* url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
//        NSData* data = [NSData dataWithContentsOfURL:url];//获取网咯图片数据
    NSData* data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:NULL error:NULL];
        
        UIImage *actualImage = [UIImage imageWithData:data];
        UIGraphicsBeginImageContext(CGSizeMake(50, 50));
                                    [actualImage drawInRect:CGRectMake(0,0,50, 50)];
                                    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                                    UIGraphicsEndImageContext();
                                    NSData *smallData = UIImagePNGRepresentation(newImage);
        
        if(smallData!=nil)
        {
            image = [[UIImage alloc] initWithData:smallData];//根据图片数据流构造image
        }
    }
    
        return image;
}

- (UIView *)mapViewCalloutContentViewWithIndex:(NSInteger)index
{
    Item *item = [[Item alloc] initWithDictionary:[_annotations objectAtIndex:index]];
    TestMapCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TestMapCell" owner:self options:nil] objectAtIndex:0];
    cell.title.text = item.title;
    cell.subtitle.text = item.subtitle;
    
    UIImage* image=nil;
    NSString *urlString = [[_mapDataArr objectAtIndex:index]objectForKey:@"logo"];
    NSURL* url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
//    NSData* data = [NSData dataWithContentsOfURL:url];//获取网咯图片数据
    NSData* data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:NULL error:NULL];
    
    if(data!=nil)
    {
        image = [[UIImage alloc] initWithData:data];//根据图片数据流构造image
    }
    cell.cellImage.image = image;
    
    return cell;
}

- (void)calloutViewDidSelectedWithIndex:(NSInteger)index
{
//    NSLog(@"%@",[_annotations objectAtIndex:index]);
    GridViewController *gridVC = [[GridViewController alloc]init];
    gridVC.marketAreaIdStr = [[_mapDataArr objectAtIndex:index]objectForKey:@"id"];
    gridVC.titleString = [[_mapDataArr objectAtIndex:index]objectForKey:@"name"];
    [self.navigationController pushViewController:gridVC animated:YES];
}
@end
