
#import <UIKit/UIKit.h>
#import "SharePopupTableView.h"

@protocol MerchantTopBarDelegate <NSObject>
- (void)merchantSearchTopViewButtonPressed:(id)sender index:(NSInteger)buttonIndex;
@end



@interface MerchantTopBar : UIView 
@property (nonatomic, assign) BOOL hasButtonSelected;
@property (nonatomic, assign) id <MerchantTopBarDelegate> delegate;
@property (nonatomic, assign) NSInteger theCurrentIndex;

- (id)initWithFrame:(CGRect)frame itemsCount:(NSInteger)itemsCount;

- (UIButton*)getCurrentButton;
- (UIButton*)getButtonByIndex:(int)index;

+ (CGFloat)getTopPanelHeight;

- (void)restoreToInitStateForHide;//0621HR添加


@end
