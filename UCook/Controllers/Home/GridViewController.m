//
//  GridViewController.m
//  UCook
//
//  Created by scihi on 14-7-15.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GridViewController.h"
#import "DiyCell.h"
#import "ShopInfoViewController.h"

@interface GridViewController ()
{
    UIGridView *_theTableView;
    NSArray *_theDataArr;
}

@end

@implementation GridViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = _titleString;
    
    //示意view
    UIView *motionView1 = [[UIView alloc]initWithFrame:CGRectMake(5, 5, 30, 20)];
    motionView1.backgroundColor = [UIColor colorWithRed:249.00f/255.00f green:233.00f/255.00f blue:233.00f/255.00f alpha:1.00f];//偏粉
    UILabel *motionLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(40, 5, 60, 20)];
    motionLabel1.text = @"已入驻";
    UIView *motionView2 = [[UIView alloc]initWithFrame:CGRectMake(105, 5, 30, 20)];
    motionView2.backgroundColor = [UIColor colorWithRed:237.00f/255.00f green:249.00f/255.00f blue:255.00f/255.00f alpha:1.00f];//偏蓝
    UILabel *motionLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(140, 5, 60, 20)];
    motionLabel2.text = @"未入驻";
    [self.view addSubview:motionView1];
    [self.view addSubview:motionLabel1];
    [self.view addSubview:motionView2];
    [self.view addSubview:motionLabel2];
    
    _theTableView = [[UIGridView alloc]initWithFrame:CGRectMake(0, 30, BOUNDS.size.width, BOUNDS.size.height - 30 - 64)];
    //    }
    _theTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _theTableView.uiGridViewDelegate = self;
    [self.view addSubview:_theTableView];
    
    [self startRequestWithString:@"请稍候..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY,@"controller": @"store", @"action": @"marketBooth",@"marketId": _marketAreaIdStr};
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _theDataArr = [[responseObject objectForKey:@"data"]mutableCopy];
            [_theTableView reloadData];
        }
        else
        {
            
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 62;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 40;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 5;
}

- (NSInteger) numberOfSectionsOfGridView:(UIGridView *) grid {
    return _theDataArr.count;
}

- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid section:(NSInteger)section
{
//	return [[[_theDataArr firstObject]objectForKey:@"booth"]count];
    
    return ![[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]isKindOfClass:[NSNull class]]?[[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]count]:0;
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex AndSection:(NSInteger)section
{
	DiyCell *cell = (DiyCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[DiyCell alloc] init];
	}
	
    //	cell.label.text = [NSString stringWithFormat:@"(%d,%d)", rowIndex, columnIndex];
//    [cell.thumbnail setImageWithURL:[[_theDataArr objectAtIndex:rowIndex * 2 +columnIndex]objectForKey:@"image"] placeholderImage:[UIImage imageNamed:@"cell_img_default.png"]];
    if (![[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]isKindOfClass:[NSNull class]]) {
    cell.label.text = [[[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]objectAtIndex:rowIndex * 5 + columnIndex]objectForKey:@"name"];
    if ([[[[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]objectAtIndex:rowIndex * 5 + columnIndex]objectForKey:@"store_id"]isKindOfClass:[NSNull class]]) {
        cell.label.backgroundColor = [UIColor colorWithRed:237.00f/255.00f green:249.00f/255.00f blue:255.00f/255.00f alpha:1.00f];//偏蓝
        cell.tag = 101;
    } else {
        cell.label.backgroundColor = [UIColor colorWithRed:249.00f/255.00f green:233.00f/255.00f blue:233.00f/255.00f alpha:1.00f];//偏粉
    }
    } else {
        return nil;
    }
	return cell;
}

- (NSString *)gridView:(UIGridView *)gridView titleForHeaderInSection:(NSInteger)section {
    return [[_theDataArr objectAtIndex:section]objectForKey:@"name"];
}

//- (void) gridView:(UIGridView *)grid didSelectRowAt:(int)rowIndex AndColumnAt:(int)colIndex
//{
////	NSLog(@"%d, %d clicked", rowIndex, colIndex);
//    
//}

//huagnrun修改为以下方法
- (void) gridView:(UIGridView *)grid didSelectRowAt:(int)rowIndex AndColumnAt:(int)colIndex AndSection:(NSInteger)section  {
    NSString *storeIdStr = [[[[_theDataArr objectAtIndex:section]objectForKey:@"booth"]objectAtIndex:rowIndex * 5 + colIndex]objectForKey:@"store_id"];
    if (![storeIdStr isKindOfClass:[NSNull class]]) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"store",@"action": @"infomation",@"id": storeIdStr};
        [self startRequestWithString:@"请稍候..."];
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self stopRequest];
            if (RIGHT) {
                ShopInfoViewController *shopInfoVC = [[ShopInfoViewController alloc]initWithNibName:@"ShopInfoViewController" bundle:nil];
//                shopInfoVC.theDataDic = [[responseObject objectForKey:@"data"]mutableCopy];
                shopInfoVC.storeIdStr = storeIdStr;
                [self.navigationController pushViewController:shopInfoVC animated:YES];
            } else {
                [GlobalSharedClass showAlertView:MESSAGE];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self stopRequest];
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];
    } else {
        [GlobalSharedClass showAlertView:@"尚未有商家入驻"];
    }
}



@end
