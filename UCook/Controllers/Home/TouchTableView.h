//
//  TouchTableView.h
//  UCook
//
//  Created by huangrun on 14-8-13.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//


//子类化uitableview 用于解决tableview中不响应touch事件的问题 huangrun

#import <UIKit/UIKit.h>

@interface TouchTableView : UITableView

@end
