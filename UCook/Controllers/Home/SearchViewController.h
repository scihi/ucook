//
//  SearchViewController.h
//  UCook
//
//  Created by huangrun on 14-7-5.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "GeneralWithBackBtnViewController.h"
#import "MerchantTopBar.h"

@interface SearchViewController : GeneralWithBackBtnViewController <MerchantTopBarDelegate,CustomTableViewDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate> {
}


@end
