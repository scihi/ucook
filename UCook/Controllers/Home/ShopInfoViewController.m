//
//  ShopInfoViewController.m
//  UCook
//
//  Created by huangrun on 14-7-16.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "ShopInfoViewController.h"
#import "SearchViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "StrikeThroughLabel.h"
#import "ShopDetailInfoViewController.h"

#import "UMSocial.h"

#import "CGRectAdditionals.h"
#import "MoreViewController.h"

#import "HRNavViewController.h"
#import "LoginViewController.h"
#import "GoodsDetailViewController.h"

#import "PromotionListViewController.h"

#import "GridViewController.h"

@interface ShopInfoViewController () {
    NSDictionary *_theDataDic;
    
    UIImageView *_productIV0;
    UILabel *_titleLabel0;
    UILabel *_currentPriceLabel0;
    StrikeThroughLabel *_originalPriceLabel0;
    
    UIImageView *_productIV1;
    UILabel *_titleLabel1;
    UILabel *_currentPriceLabel1;
    StrikeThroughLabel *_originalPriceLabel1;
    
    UIImageView *_productIV2;
    UILabel *_titleLabel2;
    UILabel *_currentPriceLabel2;
    StrikeThroughLabel *_originalPriceLabel2;
    
    UIImageView *_productIV3;
    UILabel *_titleLabel3;
    UILabel *_currentPriceLabel3;
    StrikeThroughLabel *_originalPriceLabel3;
    
    BOOL _firstTime;//是否第一次运行
}

@end

@implementation ShopInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_firstTime) {
//        _theScrollView.frame = CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?504:504 - 88);
//        _theScrollView.contentSize = CGSizeMake(BOUNDS.size.width, 1090);
        _firstTime = NO;
    }
}//在autoLayout模式下无效 huangrun

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _firstTime = YES;
    _moreButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _moreButton.layer.borderWidth = 1.0;
    [self configTheNavBar];
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configTheNavBar {
    
    //需求需要隐藏 huangrun
//    UIButton *navRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 6, 50, 34)];
//    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg.png"] forState:UIControlStateNormal];
//    [navRightBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_bg_on.png"] forState:UIControlStateHighlighted];
//    UIImageView *navRightIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_kind.png"]];
//    navRightIV.frame = CGRectMake(10, 5, 30, 24);
//    [navRightBtn addSubview:navRightIV];
//    
//    [navRightBtn addTarget:self action:@selector(clickNavRightBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:navRightBtn];
//    self.navigationItem.rightBarButtonItem = rightItem;
    
    //用于占位（导航栏右边的item），不然titleView不会居中 huangrun
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:[[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 30)]];
    
    UIButton *navTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navTitleBtn.frame = CGRectMake(0, 0, 160, 34);
    [navTitleBtn addTarget:self action:@selector(clickNavTitleBtn:) forControlEvents:UIControlEventTouchUpInside];
    navTitleBtn.adjustsImageWhenHighlighted = NO;//取消button的点击效果(button有图片)
    [navTitleBtn setBackgroundImage:[UIImage imageNamed:@"search_bg.png"] forState:UIControlStateNormal];
    //    UIImageView *navTitleIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_bg.png"]];
    //    navTitleIV.frame = CGRectMake(0, 0, 200, 34);
    UILabel *navTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 160, 34)];
    navTitleLabel.textColor = [UIColor lightGrayColor];
    navTitleLabel.font = [UIFont systemFontOfSize:13];
    navTitleLabel.text = @"请输入搜索关键字";
    [navTitleBtn addSubview:navTitleLabel];
    self.navigationItem.titleView = navTitleBtn;
}

- (void)clickNavRightBtn:(id)sender {
    [GlobalSharedClass showAlertView:@"功能开发中，敬请期待！"];
}

- (void)clickNavTitleBtn:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (IBAction)clickMoreBtn:(id)sender {
    MoreViewController *moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
    moreVC.shopIdInteger = [[_theDataDic objectForKey:@"id"]integerValue];
    [self.navigationController pushViewController:moreVC animated:YES];
}
- (IBAction)clickInfoBtn:(id)sender {
    ShopDetailInfoViewController *shopDetailInfoVC = [[ShopDetailInfoViewController alloc]initWithNibName:@"ShopDetailInfoViewController" bundle:nil];
    shopDetailInfoVC.storeIdStr = [_theDataDic objectForKey:@"id"];
    [self.navigationController pushViewController:shopDetailInfoVC animated:YES];
}

- (IBAction)clickLocationBtn:(id)sender {
//    [GlobalSharedClass showAlertView:@"功能开发中，敬请期待！"];
    GridViewController *gridVC = [[GridViewController alloc]init];
    gridVC.titleString = [_theDataDic objectForKey:@"marketName"];
    gridVC.marketAreaIdStr = [_theDataDic objectForKey:@"marketId"];
    [self.navigationController pushViewController:gridVC animated:YES];
}

- (IBAction)clickShareBtn:(id)sender {
    UIImage *shareImage = [UIImage imageNamed:@"public_logo.png"];
    
    [UMSocialSnsService presentSnsIconSheetView:self appKey:UmengAppkey shareText:@"有优厨,无厨忧" shareImage:shareImage shareToSnsNames:@[UMShareToSina,UMShareToWechatSession,UMShareToWechatTimeline, UMShareToQzone] delegate:self];
}

- (IBAction)clickCollectBtn:(id)sender {
    if (![AccountManager isLogged]) {
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        HRNavViewController *navLoginVC = [[HRNavViewController alloc]initWithRootViewController:loginVC];
        [self presentViewController:navLoginVC animated:YES completion:nil];
        return;
    } else {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"collect",@"action": @"add",@"type": @"2",@"uid": [AccountManager getUid],@"item": [_theDataDic objectForKey:@"id"]};//type 1为产品 2为店铺
//    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self stopRequest];
        if (RIGHT) {
            if ([[responseObject objectForKey:@"total"]integerValue] == 1) {
                  _collectIV.image = [UIImage imageNamed:@"ico_store_collect_on"];
            } else if ([[responseObject objectForKey:@"total"]integerValue] == 0) {
                _collectIV.image = [UIImage imageNamed:@"ico_store_collect"];
            }
            [GlobalSharedClass showAlertView:MESSAGE];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
    }
}

- (IBAction)clickHotProductBtn0:(id)sender {
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[_theDataDic objectForKey:@"hotProduct"]firstObject]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (IBAction)clickHotProductBtn1:(id)sender {
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[_theDataDic objectForKey:@"hotProduct"]objectAtIndex:1]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (IBAction)clickHotProductBtn2:(id)sender {
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[_theDataDic objectForKey:@"hotProduct"]objectAtIndex:2]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (IBAction)clickHotProductBtn3:(id)sender {
    GoodsDetailViewController *goodsDetailVC = [[GoodsDetailViewController alloc]initWithNibName:@"GoodsDetailViewController" bundle:nil];
    goodsDetailVC.goodsIdStr = [[[_theDataDic objectForKey:@"hotProduct"]objectAtIndex:3]objectForKey:@"id"];
    [self.navigationController pushViewController:goodsDetailVC animated:YES];
}

- (IBAction)clickActivityBtn0:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = [[[_theDataDic objectForKey:@"promotion"]firstObject]objectForKey:@"id"];
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

- (IBAction)clickActivityBtn1:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = [[[_theDataDic objectForKey:@"promotion"]objectAtIndex:1]objectForKey:@"id"];
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

- (IBAction)clickActivityBtn2:(id)sender {
    PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
    promotionListVC.promotionIdStr = [[[_theDataDic objectForKey:@"promotion"]objectAtIndex:2]objectForKey:@"id"];
    [self.navigationController pushViewController:promotionListVC animated:YES];
}


- (void)getData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"store",@"action": @"index",@"id": _storeIdStr};
    [self startRequestWithString:@"请稍候..."];
    [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self stopRequest];
        if (RIGHT) {
            _theDataDic = [[responseObject objectForKey:@"data"]mutableCopy];
            [self configTheView];
        } else {
            [GlobalSharedClass showAlertView:MESSAGE];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self stopRequest];
        [GlobalSharedClass showAlertView:@"请检查网络连接"];
    }];
}

- (void)configTheView {
    NSString *imgUrlStr = [_theDataDic objectForKey:@"logo"];
    [_shopIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:kDefaultImage];
    _nameLabel.text = [_theDataDic objectForKey:@"name"];
    if ([[_theDataDic objectForKey:@"isCollection"]integerValue] == 1) {
        _collectIV.image = [UIImage imageNamed:@"ico_store_collect_on"];
    } else if ([[_theDataDic objectForKey:@"isCollection"]integerValue] == 0) {
        _collectIV.image = [UIImage imageNamed:@"ico_store_collect"];
    }

    //优惠活动
    NSArray *promotionArray = [_theDataDic objectForKey:@"promotion"];
    if (promotionArray.count == 0) {
        _theMoveView0.frame = [CGRectAdditionals rectAddHeight:_theMoveView0.frame height:-395];
        _theMoveView1.frame = [CGRectAdditionals rectAddHeight:CGRectMake(_theMoveView1.frame.origin.x, _theMoveView1.frame.origin.y - 400, _theMoveView1.frame.size.width, _theMoveView1.frame.size.height) height:0];
        _theScrollView.frame = CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?504:504 - 88);
        _theScrollView.contentSize = CGSizeMake(BOUNDS.size.width, 1090 - 400);

    } else if (promotionArray.count == 1) {
        NSString *imgUrlStr0 = [[promotionArray firstObject]objectForKey:@"image"];
        [_activityButton0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
        
        _theMoveView0.frame = [CGRectAdditionals rectAddHeight:_theMoveView0.frame height:-265];
        _theMoveView1.frame = [CGRectAdditionals rectAddHeight:CGRectMake(_theMoveView1.frame.origin.x, _theMoveView1.frame.origin.y - 270, _theMoveView1.frame.size.width, _theMoveView1.frame.size.height) height:0];
        _theScrollView.frame = CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?504:504 - 88);
        _theScrollView.contentSize = CGSizeMake(BOUNDS.size.width, 1090 - 270);
    } else if (promotionArray.count == 2) {
        NSString *imgUrlStr0 = [[promotionArray firstObject]objectForKey:@"image"];
        [_activityButton0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
        NSString *imgUrlStr1 = [[promotionArray objectAtIndex:1]objectForKey:@"image"];
        [_activityButton1 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr1] placeholderImage:kDefaultImage];
        _theMoveView0.frame = [CGRectAdditionals rectAddHeight:_theMoveView0.frame height:-135];
        _theMoveView1.frame = [CGRectAdditionals rectAddHeight:CGRectMake(_theMoveView1.frame.origin.x, _theMoveView1.frame.origin.y - 140, _theMoveView1.frame.size.width, _theMoveView1.frame.size.height) height:0];
        _theScrollView.frame = CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?504:504 - 88);
         _theScrollView.contentSize = CGSizeMake(BOUNDS.size.width, 1090 - 140);
        
    } else if (promotionArray.count == 3) {
        NSString *imgUrlStr0 = [[promotionArray firstObject]objectForKey:@"image"];
        [_activityButton0 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
        NSString *imgUrlStr1 = [[promotionArray objectAtIndex:1]objectForKey:@"image"];
        [_activityButton1 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr1] placeholderImage:kDefaultImage];
        NSString *imgUrlStr2 = [[promotionArray objectAtIndex:2]objectForKey:@"image"];
        [_activityButton2 setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:imgUrlStr2] placeholderImage:kDefaultImage];
        _theScrollView.frame = CGRectMake(0, 0, BOUNDS.size.width, IS_IPHONE_5?504:504 - 88);
        _theScrollView.contentSize = CGSizeMake(BOUNDS.size.width, 1090);
    }
    
    //热销商品
    NSArray *hotProductArr = [_theDataDic objectForKey:@"hotProduct"];
    //给热销商品按钮添加子控件
    _productIV0 = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 135, 120)];
    _titleLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(5, 120, 135, 40)];
    _titleLabel0.numberOfLines = 2;
    _titleLabel0.font = [UIFont systemFontOfSize:13];
    _currentPriceLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(5, 155, 75, 20)];
    _currentPriceLabel0.textColor = kThemeColor;
    _currentPriceLabel0.font = [UIFont systemFontOfSize:13];
    _originalPriceLabel0 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(80, 155, 65, 20)];
    _originalPriceLabel0.strikeThroughEnabled = YES;
    _originalPriceLabel0.font = [UIFont systemFontOfSize:11];
    _originalPriceLabel0.textColor = [UIColor lightGrayColor];
    
    [_hotProductBtn0 addSubview:_productIV0];
    [_hotProductBtn0 addSubview:_titleLabel0];
    [_hotProductBtn0 addSubview:_currentPriceLabel0];
    [_hotProductBtn0 addSubview:_originalPriceLabel0];
    
    _productIV1 = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 135, 120)];
    _titleLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(5, 120, 135, 40)];
    _titleLabel1.numberOfLines = 2;
    _titleLabel1.font = [UIFont systemFontOfSize:13];
    _currentPriceLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(5, 155, 75, 20)];
    _currentPriceLabel1.textColor = kThemeColor;
    _currentPriceLabel1.font = [UIFont systemFontOfSize:13];
    _originalPriceLabel1 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(80, 155, 65, 20)];
    _originalPriceLabel1.strikeThroughEnabled = YES;
    _originalPriceLabel1.font = [UIFont systemFontOfSize:11];
    _originalPriceLabel1.textColor = [UIColor lightGrayColor];
    
    [_hotProductBtn1 addSubview:_productIV1];
    [_hotProductBtn1 addSubview:_titleLabel1];
    [_hotProductBtn1 addSubview:_currentPriceLabel1];
    [_hotProductBtn1 addSubview:_originalPriceLabel1];
    
    _productIV2 = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 135, 120)];
    _titleLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(5, 120, 135, 40)];
    _titleLabel2.numberOfLines = 2;
    _titleLabel2.font = [UIFont systemFontOfSize:13];
    _currentPriceLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(5, 155, 75, 20)];
    _currentPriceLabel2.textColor = kThemeColor;
    _currentPriceLabel2.font = [UIFont systemFontOfSize:13];
    _originalPriceLabel2 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(80, 155, 65, 20)];
    _originalPriceLabel2.strikeThroughEnabled = YES;
    _originalPriceLabel2.font = [UIFont systemFontOfSize:11];
    _originalPriceLabel2.textColor = [UIColor lightGrayColor];
    
    [_hotProductBtn2 addSubview:_productIV2];
    [_hotProductBtn2 addSubview:_titleLabel2];
    [_hotProductBtn2 addSubview:_currentPriceLabel2];
    [_hotProductBtn2 addSubview:_originalPriceLabel2];
    
    _productIV3 = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 135, 120)];
    _titleLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(5, 120, 135, 40)];
    _titleLabel3.numberOfLines = 2;
    _titleLabel3.font = [UIFont systemFontOfSize:13];
    _currentPriceLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(5, 155, 75, 20)];
    _currentPriceLabel3.textColor = kThemeColor;
    _currentPriceLabel3.font = [UIFont systemFontOfSize:13];
    _originalPriceLabel3 = [[StrikeThroughLabel alloc]initWithFrame:CGRectMake(80, 155, 65, 20)];
    _originalPriceLabel3.strikeThroughEnabled = YES;
    _originalPriceLabel3.font = [UIFont systemFontOfSize:11];
    _originalPriceLabel3.textColor = [UIColor lightGrayColor];
    
    [_hotProductBtn3 addSubview:_productIV3];
    [_hotProductBtn3 addSubview:_titleLabel3];
    [_hotProductBtn3 addSubview:_currentPriceLabel3];
    [_hotProductBtn3 addSubview:_originalPriceLabel3];
    
    if (hotProductArr.count == 0) {
        
    } else if (hotProductArr.count == 1) {
        _hotProductBtn0.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
    } else if (hotProductArr.count == 2) {
        _hotProductBtn0.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
    } else if (hotProductArr.count == 3) {
        _hotProductBtn0.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn2.backgroundColor = [UIColor groupTableViewBackgroundColor];
        NSString *imgUrlStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"img"];
        NSString *titleStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"name"];
        NSString *currentPriceStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"discountPrice"];
        NSString *originalPriceStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"price"];
        [_productIV0 setImageWithURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
        _titleLabel0.text = titleStr0;
        _currentPriceLabel0.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr0];
        _originalPriceLabel0.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr0];
        //        _firstFoodMallBtn2.hidden = YES;
        
        NSString *imgUrlStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"img"];
        NSString *titleStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"name"];
        NSString *currentPriceStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"discountPrice"];
        NSString *originalPriceStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"price"];
        [_productIV1 setImageWithURL:[NSURL URLWithString:imgUrlStr1]placeholderImage:kDefaultImage];
        _titleLabel1.text = titleStr1;
        _currentPriceLabel1.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr1];
        _originalPriceLabel1.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr1];
        //        _firstFoodMallBtn2.hidden = YES;
        
        NSString *imgUrlStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"img"];
        NSString *titleStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"name"];
        NSString *currentPriceStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"discountPrice"];
        NSString *originalPriceStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"price"];
        [_productIV2 setImageWithURL:[NSURL URLWithString:imgUrlStr2]placeholderImage:kDefaultImage];
        _titleLabel2.text = titleStr2;
        _currentPriceLabel2.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr2];
        _originalPriceLabel2.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr2];
        //        _firstFoodMallBtn2.hidden = YES;


        
    } else if (hotProductArr.count >= 4) {
        _hotProductBtn0.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn2.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _hotProductBtn3.backgroundColor = [UIColor groupTableViewBackgroundColor];
        NSString *imgUrlStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"img"];
        NSString *titleStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"name"];
        NSString *currentPriceStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"discountPrice"];
        NSString *originalPriceStr0 = [[hotProductArr objectAtIndex:0]objectForKey:@"price"];
        [_productIV0 setImageWithURL:[NSURL URLWithString:imgUrlStr0] placeholderImage:kDefaultImage];
        _titleLabel0.text = titleStr0;
        _currentPriceLabel0.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr0];
        _originalPriceLabel0.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr0];
        //        _firstFoodMallBtn2.hidden = YES;

        NSString *imgUrlStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"img"];
        NSString *titleStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"name"];
        NSString *currentPriceStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"discountPrice"];
        NSString *originalPriceStr1 = [[hotProductArr objectAtIndex:1]objectForKey:@"price"];
        [_productIV1 setImageWithURL:[NSURL URLWithString:imgUrlStr1]placeholderImage:kDefaultImage];
        _titleLabel1.text = titleStr1;
        _currentPriceLabel1.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr1];
        _originalPriceLabel1.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr1];
        //        _firstFoodMallBtn2.hidden = YES;

        NSString *imgUrlStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"img"];
        NSString *titleStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"name"];
        NSString *currentPriceStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"discountPrice"];
        NSString *originalPriceStr2 = [[hotProductArr objectAtIndex:2]objectForKey:@"price"];
        [_productIV2 setImageWithURL:[NSURL URLWithString:imgUrlStr2]placeholderImage:kDefaultImage];
        _titleLabel2.text = titleStr2;
        _currentPriceLabel2.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr2];
        _originalPriceLabel2.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr2];
        //        _firstFoodMallBtn2.hidden = YES;

        NSString *imgUrlStr3 = [[hotProductArr objectAtIndex:3]objectForKey:@"img"];
        NSString *titleStr3 = [[hotProductArr objectAtIndex:3]objectForKey:@"name"];
        NSString *currentPriceStr3 = [[hotProductArr objectAtIndex:3]objectForKey:@"discountPrice"];
        NSString *originalPriceStr3 = [[hotProductArr objectAtIndex:3]objectForKey:@"price"];
        [_productIV3 setImageWithURL:[NSURL URLWithString:imgUrlStr3]placeholderImage:kDefaultImage];
        _titleLabel3.text = titleStr3;
        _currentPriceLabel3.text = [NSString stringWithFormat:@"￥%@/件",currentPriceStr3];
        _originalPriceLabel3.text = [NSString stringWithFormat:@"￥%@/件",originalPriceStr3];
        //        _firstFoodMallBtn2.hidden = YES;

    }
}

@end
