//
//  AppDelegate.h
//  UCook
//
//  Created by scihi on 14-7-1.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheTabBarViewController.h"

#define UmengAppkey @"53c94cd056240b0ceb063e60"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    TheTabBarViewController *theTabBarVC;
    //UITabBarController *_tbc;//经销商端的tabBar
}
@property(nonatomic,strong) UITabBarController *tbc;

@property (strong, nonatomic) UIWindow *window;

@end
