//
//  AppDelegate.m
//  UCook
//
//  Created by scihi on 14-7-1.
//  Copyright (c) 2014年 huangrun. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"

#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"

#import "Harpy.h"

#import "AlixPayResult.h"
#import "DataVerifier.h"
//百度推送
#import "BPush.h"
#import "JSONKit.h"
#import "OpenUDID.h"

#import "AllOrdersDetailViewController.h"
#import "PromotionListViewController.h"
#import "MyAuctionDetailViewController.h"

#import "DealerSettingViewController.h"
#import "StoreManageViewController.h"
#import "AddGoodsViewController.h"
#import "ActivityManageViewController.h"
#import "OrderManageViewController.h"
#import "DealerNavViewController.h"
#import "LoginViewController.h"


//dsvsdbbdsbsdb

@implementation AppDelegate {
    NSDictionary *_userInfoDic;//推送返回回来的数据
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configTheDealerRootVC:) name:NS_GOTO_DEALER_MODULE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(configTheRestRootVC:) name:NS_GOTO_REST_MODULE object:nil];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES; //开启状态栏在数据请求时的菊花展示 huangrun
    
    [application setStatusBarStyle:UIStatusBarStyleLightContent];//设置全局状态栏颜色
    [application setStatusBarHidden:NO]; //启动的时候设置显示, 启动后要打开
    
    //版本更新
    [[Harpy sharedInstance] setAppID:@"928348547"];
    [[Harpy sharedInstance] setAppName:@"优厨商城"];
    [Harpy sharedInstance].alertType = HarpyAlertTypeOption;//有两个选项的风格
    [[Harpy sharedInstance]checkVersion];

    
    theTabBarVC = [[TheTabBarViewController alloc]init];
    
    //默认是进入餐厅 ud为空时也是 在第一次安装并进入的时候会有提现 不然会出错 1021 huangrun
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"dealer"]) {
//        self.window.rootViewController = theTabBarVC;
//    } else {
//        [self configTheDealerTabBar];
//        [self configTheDealerRootVC:nil];
//    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"dealer"]) {
        [self configTheDealerTabBar];
        [self configTheDealerRootVC:nil];
    } else {
        self.window.rootViewController = theTabBarVC;
    }
    
    //结束修改

    
    [UMSocialData openLog:YES];
    
    //如果你要支持不同的屏幕方向，需要这样设置，否则在iPhone只支持一个竖屏方向
    [UMSocialConfig setSupportedInterfaceOrientations:UIInterfaceOrientationMaskAll];
    
    //设置友盟社会化组件appkey
    [UMSocialData setAppKey:UmengAppkey];
    
    //设置微信AppId，设置分享url，默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:@"wxc365ef869ab0bcd0" url:@"http://www.umeng.com/social"];
    
    //设置手机QQ的AppId，指定你的分享url，若传nil，将使用友盟的网址
    [UMSocialQQHandler setQQWithAppId:@"1101815558" appKey:@"kbrVaxLBUD65Pb9e" url:@"http://www.umeng.com/social"];
    
    
    //打开新浪微博的SSO开关
    [UMSocialConfig setSupportSinaSSO:YES appRedirectUrl:@"http://sns.whalecloud.com/sina2/callback"];
    

    //    [UMSocialData defaultData].extConfig.wechatSessionData.wxMessageType = UMSocialWXMessageTypeImage;  //设置微信好友分享纯图片 huangrun添加
    [UMSocialData defaultData].extConfig.qzoneData.title = @"分享到QQ空间";
    [UMSocialData defaultData].extConfig.wechatSessionData.title = @"分享到微信";
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = @"分享到朋友圈";

    

    //百度推送
    [BPush setupChannel:launchOptions]; // 必须
    
    [BPush setDelegate:self]; // 必须。参数对象必须实现onMethod: response:方法，本示例中为self
    
    // [BPush setAccessToken:@"3.ad0c16fa2c6aa378f450f54adb08039.2592000.1367133742.282335-602025"];  // 可选。api key绑定时不需要，也可在其它时机调用
    
//    //不需要badge 而且在此方法针对iOS7 0827 huangrun
//    [application registerForRemoteNotificationTypes:
//     UIRemoteNotificationTypeAlert
//     | UIRemoteNotificationTypeSound];
    
    // IOS8 新系统需要使用新的代码咯
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings
                                                                             settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert)
                                                                             categories:nil]];
        
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        //网页分享的代码是这段 但经测试是错误的 应该改为下面那段 huangrun
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        
        [application registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeAlert
         | UIRemoteNotificationTypeSound];
    }
    //结束修改
    
    [self.window makeKeyAndVisible];

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UMSocialSnsService  applicationDidBecomeActive];
    
    //for apns remove badge
    int badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    if(badge > 0)
    {
        badge--;
        [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.友盟回调
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    return  [UMSocialSnsService handleOpenURL:url wxApiDelegate:nil];
//} //如果不显式调用该方法，则默认都执行handOpenURL:方法 huangrun

//支付宝独立客户端回调函数
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
	
    NSString *string =[url absoluteString];
    
    if ([string hasPrefix:@"wxfd9fe45ffe6294fc"])
    {
        return  [UMSocialSnsService handleOpenURL:url wxApiDelegate:nil];
    }
    else if ([string hasPrefix:@"HRAlipay"])
    {
        [self parse:url application:application];
        return YES;
    }
    return  [UMSocialSnsService handleOpenURL:url wxApiDelegate:nil];
}//由于友盟微信和支付宝都要调用这个方法，因此要判断 huangrun

- (void)parse:(NSURL *)url application:(UIApplication *)application {
    
    //结果处理
    AlixPayResult* result = [self handleOpenURL:url];
    
	if (result)
    {
		
		if (result.statusCode == 9000)
        {
			/*
			 *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
			 */
            
            //交易成功
            //            NSString* key = @"签约帐户后获取到的支付宝公钥";
            //			id<DataVerifier> verifier;
            //            verifier = CreateRSADataVerifier(key);
            //
            //			if ([verifier verifyString:result.resultString withSign:result.signString])
            //            {
            //                //验证签名成功，交易结果无篡改
            //			}
            
        }
        else
        {
            //交易失败
            
        }
    }
    else
    {
        //失败
        
    }
    
}

- (AlixPayResult *)resultFromURL:(NSURL *)url {
	NSString * query = [[url query] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
#if ! __has_feature(objc_arc)
    return [[[AlixPayResult alloc] initWithString:query] autorelease];
#else
	return [[AlixPayResult alloc] initWithString:query];
#endif
}

- (AlixPayResult *)handleOpenURL:(NSURL *)url {
	AlixPayResult * result = nil;
	
	if (url != nil && [[url host] compare:@"safepay"] == 0) {
		result = [self resultFromURL:url];
	}
    
	return result;
}

//百度推送
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    [BPush registerDeviceToken:deviceToken]; // 必须
    
    [BPush bindChannel]; // 必须。可以在其它时机调用，只有在该方法返回（通过onMethod:response:回调）绑定成功时，app才能接收到Push消息。一个app绑定成功至少一次即可（如果access token变更请重新绑定）。
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"注册推送失败，原因：%@",error);
}

// 必须，如果正确调用了setDelegate，在bindChannel之后，结果在这个回调中返回。
// 若绑定失败，请进行重新绑定，确保至少绑定成功一次
- (void) onMethod:(NSString*)method response:(NSDictionary*)data
{
    if ([BPushRequestMethod_Bind isEqualToString:method])
    {
        NSDictionary* res = [[NSDictionary alloc] initWithDictionary:data];
        
//        NSString *appid = [res valueForKey:BPushRequestAppIdKey];
        NSString *userid = [res valueForKey:BPushRequestUserIdKey];
//        NSString *channelid = [res valueForKey:BPushRequestChannelIdKey];
//        int returnCode = [[res valueForKey:BPushRequestErrorCodeKey] intValue];
//        NSString *requestid = [res valueForKey:BPushRequestRequestIdKey];
        
        //上传百度userId到服务器
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSDictionary *parameters = @{@"key": APP_KEY, @"controller": @"system",@"action": @"bindMobile",@"system": @"ios",@"uid": [AccountManager getUid],@"userId": userid};
        [manager POST:BASIC_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            if (RIGHT)
            {
                NSLog(@"百度uid：%@上传成功",userid);
            } else {

            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [GlobalSharedClass showAlertView:@"请检查网络连接"];
        }];

    }
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
   // NSLog(@"Receive Notify: %@", [userInfo JSONString]);
    _userInfoDic = [userInfo copy];
    NSString *alert = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    if (application.applicationState == UIApplicationStateActive) {
        // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"您收到一条推送消息"
                                                            message:[NSString stringWithFormat:@"%@", alert]
                                                           delegate:self
                                                  cancelButtonTitle:@"确定"
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        [self handleNotification:_userInfoDic];
    }
//    [application setApplicationIconBadgeNumber:[[userInfo objectForKey:@"badge"]integerValue]];
    
//    [BPush handleNotification:userInfo]; // 可选
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSInteger codeInteger = [[_userInfoDic objectForKey:@"code"]integerValue];
    //urlString可能为连接、订单id或活动id huangrun
    NSString *urlString = [_userInfoDic objectForKey:@"uri"];
    if (urlString && ![urlString isKindOfClass:[NSNull class]]) {

        if (codeInteger == 1) {
            //网页
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        } else if (codeInteger == 2) {
            //订单详情
            AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
            allOrdersDetailVC.orderIdStr = urlString;
            
            [(UINavigationController *)[theTabBarVC.viewControllers firstObject] pushViewController:allOrdersDetailVC animated:YES];
            
        } else if (codeInteger == 3) {
            //活动列表
            PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
            promotionListVC.promotionIdStr = urlString;
            promotionListVC.hidesBottomBarWhenPushed = YES;

            [(UINavigationController *)[theTabBarVC.viewControllers firstObject] pushViewController:promotionListVC animated:YES];
        }
    } else {
        [GlobalSharedClass showAlertView:@"uri字段内容为空"];
    }
}

- (void)handleNotification:(NSDictionary *)userInfo {
    NSInteger codeInteger = [[_userInfoDic objectForKey:@"code"]integerValue];
    //urlString可能为连接、订单id或活动id huangrun
    NSString *urlString = [_userInfoDic objectForKey:@"uri"];
    if (urlString && ![urlString isKindOfClass:[NSNull class]]) {
        
        if (codeInteger == 1) {
            //网页
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        } else if (codeInteger == 2) {
            //订单详情
            AllOrdersDetailViewController *allOrdersDetailVC = [[AllOrdersDetailViewController alloc]initWithNibName:@"AllOrdersDetailViewController" bundle:nil];
            allOrdersDetailVC.orderIdStr = urlString;
            
            [(UINavigationController *)[theTabBarVC.viewControllers firstObject] pushViewController:allOrdersDetailVC animated:YES];
            
        } else if (codeInteger == 3) {
            //活动列表
            PromotionListViewController *promotionListVC = [[PromotionListViewController alloc]init];
            promotionListVC.promotionIdStr = urlString;
            promotionListVC.hidesBottomBarWhenPushed = YES;
            
            [(UINavigationController *)[theTabBarVC.viewControllers firstObject] pushViewController:promotionListVC animated:YES];
        } else if (codeInteger == 4) {
            //竞拍详情
            MyAuctionDetailViewController *myAuctionDetailVC = [[MyAuctionDetailViewController alloc]init];
            myAuctionDetailVC.auctionBagIdStr = urlString;
            [(UINavigationController *)[theTabBarVC.viewControllers firstObject] pushViewController:myAuctionDetailVC animated:YES];
        }
    } else {
        [GlobalSharedClass showAlertView:@"uri字段内容为空"];
    }
}

#pragma mark - 跳入经销商模块
- (void)configTheDealerTabBar
{
   _tbc = [[UITabBarController alloc]init];
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:NS_DEALER_LAST_PLACE]isEqualToString:@"dealer"]) {
    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]} forState:UIControlStateNormal];//设置tabBarItem文字颜色 huangrun
//    }

    //店铺管理
    StoreManageViewController *smvc = [[StoreManageViewController alloc]init];
    smvc.tabBarItem.image = [UIImage imageNamed:@"ico_store"];
    DealerNavViewController *dnavsmvc = [[DealerNavViewController alloc]initWithRootViewController:smvc];
    //添加产品
    AddGoodsViewController *agvc = [[AddGoodsViewController alloc]init];
    agvc.tabBarItem .image = [UIImage imageNamed:@"ico_product"];
    DealerNavViewController *dnavagvc = [[DealerNavViewController alloc]initWithRootViewController:agvc];
    
    //活动管理
    ActivityManageViewController *amvc = [[ActivityManageViewController alloc]init];
    amvc.tabBarItem.image = [UIImage imageNamed:@"ico_action"];
    DealerNavViewController *dnavamvc = [[DealerNavViewController alloc]initWithRootViewController:amvc];
    
    //订单管理
    OrderManageViewController *omvc = [[OrderManageViewController alloc]init];
    omvc.tabBarItem.image = [UIImage imageNamed:@"ico_order"];
    DealerNavViewController *dnavomvc = [[DealerNavViewController alloc]initWithRootViewController:omvc];
    
    //设置
    DealerSettingViewController *dsvc = [[DealerSettingViewController alloc]init];
    dsvc.tabBarItem.image = [UIImage imageNamed:@"ico_setting"];
    DealerNavViewController *dnavdsvc = [[DealerNavViewController alloc]initWithRootViewController:dsvc];
    
    _tbc.viewControllers = @[dnavsmvc,dnavagvc,dnavamvc,dnavomvc,dnavdsvc];
    self.window.rootViewController = _tbc;
}

- (void)configTheDealerRootVC:(NSNotification *)notification {
    //为了让导航栏的视图有变化,只好切换都重新配置tbc，牺牲性能了 huangrun
//    if(!tbc) {
        [self configTheDealerTabBar];
//    }
}

- (void)configTheRestRootVC:(NSNotification *)notification {
    theTabBarVC = [[TheTabBarViewController alloc]init];
    self.window.rootViewController = theTabBarVC;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
